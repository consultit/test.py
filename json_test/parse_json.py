'''
Created on Aug 28, 2019

@author: consultit
'''

import argparse, textwrap
from json.decoder import JSONDecoder
import xmltodict
from collections.abc import Sequence, Mapping
from numbers import Number

dInd = '°'
lInd = len(dInd)

if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    This program will parse an xml/json file.
    '''))    
    # set up arguments
    parser.add_argument('filename', metavar='FILE.<xml|json>', type=str, help='the xml/json file to parse')
    # parse arguments
    args = parser.parse_args()
    # do actions
    inputStr = open(args.filename, 'r').read()
    isJSON = True
    # check json
    try:
        reprDict = JSONDecoder().decode(inputStr)
    except Exception:
        print('No json file...')
        isJSON = False
    if not isJSON:
        # check xml
        try:
            reprDict = xmltodict.parse(inputStr)
        except Exception as e:
            print('No xml file...')
            print('Input file must be formatted as json or xml. Exiting...')
            exit(-1)

    # analyze reprDict
    def analyzeJsonElement(element, elemName=None, func=print, indent=''):
        # get json element
        res = None
        # array
        if isinstance(element, Sequence):
            for value in element:
                if func:
                    func(indent + elemName)
                if isinstance(value, str) or isinstance(value, Number):
                    if func:
                        func(indent + str(value))
                else:
                    analyzeJsonElement(value, func=func, indent=indent)
            res = ('seq', element)
        # object
        elif isinstance(element, Mapping):
            for key in element:
                value = element[key]
                indent += dInd
                if isinstance(value, str) or isinstance(value, Number):
                    if func:
                        func(indent + key + ':' + str(value))
                else:
                    if func and (not isinstance(value, Sequence)):
                        func(indent + key)
                    analyzeJsonElement(element.get(key), elemName=key, func=func, indent=indent)
                indent = indent[:-lInd]
            res = ('map', element.keys())
        return res

    res = analyzeJsonElement(reprDict)
