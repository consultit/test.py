'''
Created on Aug 28, 2019

@author: consultit
'''

import argparse, textwrap
from xml.etree.ElementTree import parse

if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    This program will parse an xml file.
    '''))    
    # set up arguments
    parser.add_argument('filename', metavar='FILE.xml', type=str, help='the xml file to parse')
    # parse arguments
    args = parser.parse_args()
    # do actions
    xmlTree = parse(args.filename)
    xmlRoot = xmlTree.getroot()
    #
    indent = ''
    for game in xmlRoot.iter('Game'):
        print(indent + game.tag + ': ' + str(game.attrib))
        indent += '--'
        #
        print(indent + '  ObjectTmplSet')
        for objTmplSet in game.iter('ObjectTmplSet'):
            print(indent + objTmplSet.tag + ': ' + str(objTmplSet.attrib))
            indent += '--'
            for objTmpl in objTmplSet.iter('ObjectTmpl'):
                print(indent + objTmpl.tag + ': ' + str(objTmpl.attrib))
                indent += '--'
                for compTmpl in objTmpl.iter('ComponentTmpl'):
                    print(indent + compTmpl.tag + ': ' + str(compTmpl.attrib))
                    indent += '--'
                    for param in compTmpl.iter('Param'):
                        print(indent + param.tag + ': ' + str(param.attrib))
                    indent = indent[:-2]
                indent = indent[:-2]
            indent = indent[:-2]
        #
        print(indent + '   ObjectSet')
        for objSet in game.iter('ObjectSet'):
            print(indent + objSet.tag + ': ' + str(objSet.attrib))
            indent += '--'
            for obj in objSet.iter('Object'):
                print(indent + obj.tag + ': ' + str(obj.attrib))
                indent += '--'                
                for comp in obj.iter('Component'):
                    print(indent + comp.tag + ': ' + str(comp.attrib))
                    indent += '--'
                    for param in comp.iter('Param'):
                        print(indent + param.tag + ': ' + str(param.attrib))
                    indent = indent[:-2]
                for param in obj.iter('Param'):
                    print(indent + param.tag + ': ' + str(param.attrib))
                indent = indent[:-2]
            indent = indent[:-2]
        indent = indent[:-2]
   
