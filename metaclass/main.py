'''
Created on 11 ago 2015

@author: consultit

'''

def print_infos(Class):
    print('making', Class.__name__)
    print('\tinstance of: ' , Class.__class__)
    print('\tbases : ' , Class.__bases__)
    print('\tmro : ' , Class.__class__.__mro__)
    print('\tdict.keys : ' , sorted(Class.__dict__.keys()))

class DD:
    def __set__(self, instance, value):
        print('DD.__set__', instance)
    def __get__(self, instance, owner):
        print('DD.__get__', instance)

class M(type):
    d = 'M'
#     d = DD()
    def m(self, *args):
        print('M.m', *args)
print_infos(M)

class SM(M):
    d = "SM"
#     d = DD()
    def m(self, *args):
        print('SM.m', *args)
print_infos(SM)

class C(metaclass=SM):
    d = "C"
#    d = DD()
    def m(self, *args):
        print('C.m', *args)
print_infos(C)

class SC(C):
#     d = "SC"
#     d = DD()
    def m(self, *args):
        print('SC.m', *args)
print_infos(SC)
 
if __name__ == '__main__':
    print('making instance')
    I = SC()
    for d in I.__class__.__mro__:
#         print(d, 'dict:', sorted(d.__dict__.keys()))
        if 'd' in d.__dict__:
            print('found "d" in', d)
            break

#     I.d = 'I'
    print(I.d)
    I.m()
