#!/bin/bash

export DISTUTILS_DEBUG=1

DESTDIR=${1}


if [ -z "${DESTDIR}" ]
then
	echo 'specify dest-dir'
	exit 1
fi

python setup.py sdist \
	&& cp -a dist/ ${DESTDIR} \
	&& rm -rf dist/ build/ test_py.egg-info/
