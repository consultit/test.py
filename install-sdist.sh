#!/bin/bash

export DISTUTILS_DEBUG=1

PREFIX=/REPOSITORY/trash/usr
INSTDIR=${PREFIX}/lib/python3.7/site-packages
SDISTPKG=test_py-1.0.1

rm -rf ${PREFIX}/*
mkdir -p ${INSTDIR}
export PYTHONPATH=${INSTDIR}

'rm' -rf ${SDISTPKG}/ \
	&& tar xvzf ${SDISTPKG}.tar.gz \
	&& cd ${SDISTPKG}/ \
	&& python setup.py install --prefix=${PREFIX} \
	&& cd ..
