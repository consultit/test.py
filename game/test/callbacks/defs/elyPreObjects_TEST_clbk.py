'''
Created on Sep 30, 2019

@author: consultit
'''

from COM.com.component import ComponentFamilyType
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectId


# elyPreObjects_TEST + GameConfig related
def genericCollisionNotify(event, gameConfig, body0, body1):
    '''generic event notify'''
    assert (gameConfig.owner_object.get_component(ComponentFamilyType('Common'))
            == gameConfig)
    assert (gameConfig.owner_object.get_component(ComponentFamilyType('Common'))
            == gameConfig)
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    objId0 = ObjectId(body0.get_tag('_owner_object'))
    objId1 = ObjectId(body1.get_tag('_owner_object'))
    object0 = objTmplMgr.get_created_object(objId0)
    object1 = objTmplMgr.get_created_object(objId1)
    print ('got ', event, ' between ', object0, ' (', body0, ') and ', object1,
           ' (', body1, ') handled by ', gameConfig, sep='')


callbacks = {
    'genericCollisionNotify': genericCollisionNotify
    }
