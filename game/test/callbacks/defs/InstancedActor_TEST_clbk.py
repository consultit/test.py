'''
Created on Sep 29, 2019

@author: consultit
'''

from COM.com.component import ComponentFamilyType

# InstancedActor_TEST + InstanceOf related
def forward_InstanceOf_InstancedActor_TEST(event, instanceOf):
    assert (instanceOf.owner_object.get_component(ComponentFamilyType('Scene'))
            == instanceOf)
    print('Got ', event, ' handled by forward_InstanceOf_InstancedActor_TEST() for ',
          instanceOf.owner_object, '.', instanceOf, sep='')


def stop_forward_InstanceOf_InstancedActor_TEST(event, instanceOf):
    assert (instanceOf.owner_object.get_component(ComponentFamilyType('Scene'))
            == instanceOf)
    print('Got ', event, ' handled by stop_forward_InstanceOf_InstancedActor_TEST for ',
          instanceOf.owner_object, '.', instanceOf, sep='')


callbacks = {
    'forward_InstanceOf_InstancedActor_TEST': forward_InstanceOf_InstancedActor_TEST,
    'stop_forward_InstanceOf_InstancedActor_TEST': stop_forward_InstanceOf_InstancedActor_TEST
    }
