'''
Created on Feb 27, 2020

@author: consultit
'''

from COM.components.behavior.activity import StateEventTypePair, \
    State
from COM.tools.utilities import Event, COMLogger

# f:f_up:f_q
# b:b_up:b_q
# sr:sr_up:sr_q
# sl:sl_up:sl_q
# rr:rr_up:rr_q
# rl:rl_up:rl_q
# j:j_up:j_q
# q:q_up
# ground:air


# # Character + MultiActivity related functions/variables
# Transition table: <eventType, currentState> -> nextState
def multiActivityTester0(event, multiActivity):
    # get data
    # get fsm and transition table through type conversions
    # get current_state,event_type@next_state
    eventType = multiActivity.get_event_type(Event(event))
    # retrieve the name of the activity
    activityName = eventType.value[:4]
    activity = multiActivity.activities[activityName]
    currentState = activity.fsm.getCurrentOrNextState()
    nextState = activity.transition_table.get(
        StateEventTypePair(currentState, eventType), State()).value
    # make transition
    if nextState:
        COMLogger.info(activityName + ': (' + currentState + ',' + 
                       str(eventType) + ') -> ' + nextState)
        activity.fsm.request(nextState, multiActivity)
    else:
        COMLogger.info(activityName + ': (' + currentState + ',' + 
                       str(eventType) + ') -> TRANSITION UNDEFINED')


# #
callbacks = {
    'multiActivityTester0': multiActivityTester0,
}
