'''
Created on Sep 29, 2019

@author: consultit
'''

from COM.com.component import ComponentFamilyType
from COM.components.behavior.activity import State, StateEventTypePair
from COM.tools.utilities import Event


# StaticActor_TEST + Activity related
def activity_StaticActor_TEST_Handler(event, activity):
    assert (activity.owner_object.get_component(ComponentFamilyType('Behavior'))
            == activity)
    print('Got ', event, ' handled by ev1_Activity_StaticActor_TEST for ',
          activity.owner_object, '.', activity, sep='')
    # on receipt try to let the fsm make a transition:
    # 1: get the fsm current state
    currState = State(activity.fsm.getCurrentOrNextState())
    # 2: get the type of event
    eventType = activity.get_event_type(Event(event))
    # 3: get next state from transition table, if any
    nextState = activity.transition_table.get(
        StateEventTypePair(currState, eventType), None)
    # 3: request a transition to nextState
    if nextState:
        activity.fsm.request(nextState.value, activity)


callbacks = {
    'ev0_Activity_StaticActor_TEST': activity_StaticActor_TEST_Handler,
    'ev1_Activity_StaticActor_TEST': activity_StaticActor_TEST_Handler,
    'ev2_Activity_StaticActor_TEST': activity_StaticActor_TEST_Handler,
    'ev3_Activity_StaticActor_TEST': activity_StaticActor_TEST_Handler,
    'ev4_Activity_StaticActor_TEST': activity_StaticActor_TEST_Handler,
    'ev5_Activity_StaticActor_TEST': activity_StaticActor_TEST_Handler,
    'ev6_Activity_StaticActor_TEST': activity_StaticActor_TEST_Handler,
    }
