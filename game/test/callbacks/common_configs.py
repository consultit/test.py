'''
Created on Sep 29, 2019

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# InstancedActor_TEST_clbk
spec = util.spec_from_file_location('InstancedActor_TEST_clbk',
             os.path.join(_this_library_dir, 'defs', 'InstancedActor_TEST_clbk.py'))
InstancedActor_TEST_clbk = util.module_from_spec(spec)
spec.loader.exec_module(InstancedActor_TEST_clbk)

# StaticActor_TEST_clbk
spec = util.spec_from_file_location('StaticActor_TEST_clbk',
            os.path.join(_this_library_dir, 'defs', 'StaticActor_TEST_clbk.py'))
StaticActor_TEST_clbk = util.module_from_spec(spec)
spec.loader.exec_module(StaticActor_TEST_clbk)

# elyPreObjects_TEST_clbk
spec = util.spec_from_file_location('elyPreObjects_TEST_clbk',
            os.path.join(_this_library_dir, 'defs', 'elyPreObjects_TEST_clbk.py'))
elyPreObjects_TEST_clbk = util.module_from_spec(spec)
spec.loader.exec_module(elyPreObjects_TEST_clbk)

# testActivity_clbk
spec = util.spec_from_file_location('testActivity_clbk',
            os.path.join(_this_library_dir, 'defs', 'testActivity_clbk.py'))
testActivity_clbk = util.module_from_spec(spec)
spec.loader.exec_module(testActivity_clbk)

# testMultiActivity_clbk
spec = util.spec_from_file_location('testMultiActivity_clbk',
            os.path.join(_this_library_dir, 'defs', 'testMultiActivity_clbk.py'))
testMultiActivity_clbk = util.module_from_spec(spec)
spec.loader.exec_module(testMultiActivity_clbk)

# callbacks definitions
# See: https://stackoverflow.com/questions/38987/how-to-merge-two-dictionaries-in-a-single-expression
callbacks = {
    'default_callback__':
                    lambda event, comp: print('Got ', event,
                        ' handled by default_callback__() for ', comp, sep=''),
    **InstancedActor_TEST_clbk.callbacks,
    **StaticActor_TEST_clbk.callbacks,
    **elyPreObjects_TEST_clbk.callbacks,
    **testActivity_clbk.callbacks,
    **testMultiActivity_clbk.callbacks,
}
