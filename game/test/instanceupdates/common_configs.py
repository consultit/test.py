'''
Created on Sep 29, 2019

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# StaticActor_TEST_updt
spec = util.spec_from_file_location('StaticActor_TEST_updt',
            os.path.join(_this_library_dir, 'defs', 'StaticActor_TEST_updt.py'))
StaticActor_TEST_updt = util.module_from_spec(spec)
spec.loader.exec_module(StaticActor_TEST_updt)
 
# instance_updates definitions
# See: https://stackoverflow.com/questions/38987/how-to-merge-two-dictionaries-in-a-single-expression
instance_updates = {
    **StaticActor_TEST_updt.instance_updates
}
