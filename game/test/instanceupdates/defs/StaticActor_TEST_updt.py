'''
Created on Sep 29, 2019

@author: consultit
'''

from COM.com.component import ComponentFamilyType

# StaticActor_TEST + Activity related
# Preserve state 1: through function object
# class PlayerUpdate(object):
#  
#     def __init__(self):
#         self._actionTime = 5
#         self._accumulatedTime = 0.0
#  
#     def __call__(self, dt, activity):
#         assert (activity.owner_object.get_component(
#             ComponentFamilyType('Behavior')) == activity)
#         if self._accumulatedTime >= self._actionTime:
#             print('playerUpdate called after ', self._accumulatedTime,
#                   ' secs for ', activity.owner_object, '.', activity, sep='')
#             self._accumulatedTime = 0.0
#         else:
#             self._accumulatedTime += dt
#  
#  
# playerUpdate = PlayerUpdate()

# Preserve state 2: through global variables
# _actionTime = 5.0
# _accumulatedTime = 0.0
# 
# 
# def playerUpdate(dt, activity):
#     global _actionTime, _accumulatedTime
#     assert (activity.owner_object.get_component(
#         ComponentFamilyType('Behavior')) == activity)
#     if _accumulatedTime >= _actionTime:
#         print('playerUpdate called after ', str(_accumulatedTime), ' secs for ',
#               activity.owner_object, '.', activity, sep='')
#         _accumulatedTime = 0.0
#     else:
#         _accumulatedTime += dt


# Preserve state 3: through function attributes
def playerUpdate(dt, activity):
    assert (activity.owner_object.get_component(
        ComponentFamilyType('Behavior')) == activity)
    if playerUpdate._accumulatedTime >= playerUpdate._actionTime:
        print('playerUpdate called after ', str(playerUpdate._accumulatedTime),
              ' secs for ', activity.owner_object, '.', activity, sep='')
        playerUpdate._accumulatedTime = 0.0
    else:
        playerUpdate._accumulatedTime += dt


playerUpdate._actionTime = 5.0
playerUpdate._accumulatedTime = 0.0

instance_updates = {
    'playerUpdate': playerUpdate,
    }
