'''
Created on Sep 29, 2019

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# StaticActor_TEST_trns
spec = util.spec_from_file_location('StaticActor_TEST_trns',
            os.path.join(_this_library_dir, 'defs', 'StaticActor_TEST_trns.py'))
StaticActor_TEST_trns = util.module_from_spec(spec)
spec.loader.exec_module(StaticActor_TEST_trns)

# testActivity_trns
spec = util.spec_from_file_location('testActivity_trns',
            os.path.join(_this_library_dir, 'defs', 'testActivity_trns.py'))
testActivity_trns = util.module_from_spec(spec)
spec.loader.exec_module(testActivity_trns)

# testMultiActivity_trns
spec = util.spec_from_file_location('testMultiActivity_trns',
            os.path.join(_this_library_dir, 'defs', 'testMultiActivity_trns.py'))
testMultiActivity_trns = util.module_from_spec(spec)
spec.loader.exec_module(testMultiActivity_trns)

# transitions definitions
# See: https://stackoverflow.com/questions/38987/how-to-merge-two-dictionaries-in-a-single-expression
transitions = {
    **StaticActor_TEST_trns.transitions,
    **testActivity_trns.transitions,
    **testMultiActivity_trns.transitions,
}
