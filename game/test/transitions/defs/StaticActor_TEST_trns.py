'''
Created on Sep 29, 2019

@author: consultit
'''

from COM.com.component import ComponentFamilyType

# StaticActor_TEST + Activity related
def enterState(self, activity):
    assert (activity.owner_object.get_component(ComponentFamilyType('Behavior'))
            == activity)
    print(activity, ' transition:', activity.fsm.oldState, sep='')
    print('\texit from ', activity.fsm.oldState, sep='')
    print('\tenter to ', activity.fsm.newState, sep='')


def exitState(self):
    print('\texit from ', self.oldState, sep='')
    print('\tenter to ', self.newState, sep='')


def filterState(self, request, args):
    activity = args[0]
    assert (activity.owner_object.get_component(ComponentFamilyType('Behavior'))
            == activity)
    print(activity, ' transition:', ' filter ', activity.fsm.state, sep='')
    # passthrough filter
    return (request,) + args


transitions = {
    'Enter_A_StaticActor_TEST': enterState,
    'Exit_A_StaticActor_TEST': exitState,
    'Filter_A_StaticActor_TEST': filterState,
    'Enter_B_StaticActor_TEST': enterState,
    'Exit_B_StaticActor_TEST': exitState,
    'Filter_B_StaticActor_TEST': filterState,
    'Enter_C_StaticActor_TEST': enterState,
    'Exit_C_StaticActor_TEST': exitState,
    'Filter_C_StaticActor_TEST': filterState,
    'Enter_D_StaticActor_TEST': enterState,
    'Exit_D_StaticActor_TEST': exitState,
    'Filter_D_StaticActor_TEST': filterState,
    'Enter_E_StaticActor_TEST': enterState,
    'Exit_E_StaticActor_TEST': exitState,
    'Filter_E_StaticActor_TEST': filterState,
    'Enter_F_StaticActor_TEST': enterState,
    'Exit_F_StaticActor_TEST': exitState,
    'Filter_F_StaticActor_TEST': filterState,
}
