'''
Created on Feb 24, 2020

@author: consultit
'''

from COM.tools.utilities import COMLogger

# 'Enter_<STATE>_<OBJECTTYPE>':Enter_<STATE>_<OBJECTTYPE>,
# 'Exit_<STATE>_<OBJECTTYPE>':Exit_<STATE>_<OBJECTTYPE>,
# FILTER Filter_<STATE>_<OBJECTTYPE>
# FROMTO <STATEA>_FromTo_<STATEB>_<OBJECTTYPE>


# # ActivityTester (Activity) related
# # States' functions
# I
def Enter_I_ActivityTester(self, activity):
    COMLogger.info('Enter_I_ActivityTester')


def Exit_I_ActivityTester(self):
    COMLogger.info('Exit_I_ActivityTester')


# F
def Enter_F_ActivityTester(self, activity):
    COMLogger.info('Enter_F_ActivityTester')


def Exit_F_ActivityTester(self):
    COMLogger.info('Exit_F_ActivityTester')


# B
def Enter_B_ActivityTester(self, activity):
    COMLogger.info('Enter_B_ActivityTester')


def Exit_B_ActivityTester(self):
    COMLogger.info('Exit_B_ActivityTester')


# Sr
def Enter_Sr_ActivityTester(self, activity):
    COMLogger.info('Enter_Sr_ActivityTester')


def Exit_Sr_ActivityTester(self):
    COMLogger.info('Exit_Sr_ActivityTester')


# Sl
def Enter_Sl_ActivityTester(self, activity):
    COMLogger.info('Enter_Sl_ActivityTester')


def Exit_Sl_ActivityTester(self):
    COMLogger.info('Exit_Sl_ActivityTester')


# Rr
def Enter_Rr_ActivityTester(self, activity):
    COMLogger.info('Enter_Rr_ActivityTester')


def Exit_Rr_ActivityTester(self):
    COMLogger.info('Exit_Rr_ActivityTester')


# Rl
def Enter_Rl_ActivityTester(self, activity):
    COMLogger.info('Enter_Rl_ActivityTester')


def Exit_Rl_ActivityTester(self):
    COMLogger.info('Exit_Rl_ActivityTester')


# J
def Enter_J_ActivityTester(self, activity):
    COMLogger.info('Enter_J_ActivityTester')


def Exit_J_ActivityTester(self):
    COMLogger.info('Exit_J_ActivityTester')


# F-Rr
def Enter_F_Rr_ActivityTester(self, activity):
    COMLogger.info('Enter_F_Rr_ActivityTester')


def Exit_F_Rr_ActivityTester(self):
    COMLogger.info('Exit_F_Rr_ActivityTester')


# F-Rl
def Enter_F_Rl_ActivityTester(self, activity):
    COMLogger.info('Enter_F_Rl_ActivityTester')


def Exit_F_Rl_ActivityTester(self):
    COMLogger.info('Exit_F_Rl_ActivityTester')


# F-Rr-Rl
def Enter_F_Rr_Rl_ActivityTester(self, activity):
    COMLogger.info('Enter_F_Rr_Rl_ActivityTester')

 
def Exit_F_Rr_Rl_ActivityTester(self):
    COMLogger.info('Exit_F_Rr_Rl_ActivityTester')

 
# Rr-Rl
def Enter_Rr_Rl_ActivityTester(self, activity):
    COMLogger.info('Enter_Rr_Rl_ActivityTester')

 
def Exit_Rr_Rl_ActivityTester(self):
    COMLogger.info('Exit_Rr_Rl_ActivityTester')

 
# F-J
def Enter_F_J_ActivityTester(self, activity):
    COMLogger.info('Enter_F_J_ActivityTester')


def Exit_F_J_ActivityTester(self):
    COMLogger.info('Exit_F_J_ActivityTester')


# B-Rr
def Enter_B_Rr_ActivityTester(self, activity):
    COMLogger.info('Enter_B_Rr_ActivityTester')


def Exit_B_Rr_ActivityTester(self):
    COMLogger.info('Exit_B_Rr_ActivityTester')


# B-Rl
def Enter_B_Rl_ActivityTester(self, activity):
    COMLogger.info('Enter_B_Rl_ActivityTester')


def Exit_B_Rl_ActivityTester(self):
    COMLogger.info('Exit_B_Rl_ActivityTester')


# Sr-Rr
def Enter_Sr_Rr_ActivityTester(self, activity):
    COMLogger.info('Enter_Sr_Rr_ActivityTester')


def Exit_Sr_Rr_ActivityTester(self):
    COMLogger.info('Exit_Sr_Rr_ActivityTester')


# Sr-Rl
def Enter_Sr_Rl_ActivityTester(self, activity):
    COMLogger.info('Enter_Sr_Rl_ActivityTester')


def Exit_Sr_Rl_ActivityTester(self):
    COMLogger.info('Exit_Sr_Rl_ActivityTester')


# Sl-Rr
def Enter_Sl_Rr_ActivityTester(self, activity):
    COMLogger.info('Enter_Sl_Rr_ActivityTester')


def Exit_Sl_Rr_ActivityTester(self):
    COMLogger.info('Exit_Sl_Rr_ActivityTester')


# Sl-Rl
def Enter_Sl_Rl_ActivityTester(self, activity):
    COMLogger.info('Enter_Sl_Rl_ActivityTester')


def Exit_Sl_Rl_ActivityTester(self):
    COMLogger.info('Exit_Sl_Rl_ActivityTester')


# F-Q
def Enter_F_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_F_Q_ActivityTester')


def Exit_F_Q_ActivityTester(self):
    COMLogger.info('Exit_F_Q_ActivityTester')


# Sr-Q
def Enter_Sr_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_Sr_Q_ActivityTester')


def Exit_Sr_Q_ActivityTester(self):
    COMLogger.info('Exit_Sr_Q_ActivityTester')


# Sl-Q
def Enter_Sl_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_Sl_Q_ActivityTester')


def Exit_Sl_Q_ActivityTester(self):
    COMLogger.info('Exit_Sl_Q_ActivityTester')


# Rr-Q
def Enter_Rr_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_Rr_Q_ActivityTester')


def Exit_Rr_Q_ActivityTester(self):
    COMLogger.info('Exit_Rr_Q_ActivityTester')


# Rl-Q
def Enter_Rl_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_Rl_Q_ActivityTester')


def Exit_Rl_Q_ActivityTester(self):
    COMLogger.info('Exit_Rl_Q_ActivityTester')


# F-Rr-Q
def Enter_F_Rr_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_F_Rr_Q_ActivityTester')


def Exit_F_Rr_Q_ActivityTester(self):
    COMLogger.info('Exit_F_Rr_Q_ActivityTester')


# F-Rl-Q
def Enter_F_Rl_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_F_Rl_Q_ActivityTester')


def Exit_F_Rl_Q_ActivityTester(self):
    COMLogger.info('Exit_F_Rl_Q_ActivityTester')


# F-Rr-Rl-Q
def Enter_F_Rr_Rl_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_F_Rr_Rl_Q_ActivityTester')


def Exit_F_Rr_Rl_Q_ActivityTester(self):
    COMLogger.info('Exit_F_Rr_Rl_Q_ActivityTester')

 
# Rr-Rl-Q
def Enter_Rr_Rl_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_Rr_Rl_Q_ActivityTester')

 
def Exit_Rr_Rl_Q_ActivityTester(self):
    COMLogger.info('Exit_Rr_Rl_Q_ActivityTester')

 
# F-J-Q
def Enter_F_J_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_F_J_Q_ActivityTester')


def Exit_F_J_Q_ActivityTester(self):
    COMLogger.info('Exit_F_J_Q_ActivityTester')


# Sr-Rr-Q
def Enter_Sr_Rr_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_Sr_Rr_Q_ActivityTester')


def Exit_Sr_Rr_Q_ActivityTester(self):
    COMLogger.info('Exit_Sr_Rr_Q_ActivityTester')


# Sr-Rl-Q
def Enter_Sr_Rl_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_Sr_Rl_Q_ActivityTester')


def Exit_Sr_Rl_Q_ActivityTester(self):
    COMLogger.info('Exit_Sr_Rl_Q_ActivityTester')


# Sl-Rr-Q
def Enter_Sl_Rr_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_Sl_Rr_Q_ActivityTester')


def Exit_Sl_Rr_Q_ActivityTester(self):
    COMLogger.info('Exit_Sl_Rr_Q_ActivityTester')


# Sl-Rl-Q
def Enter_Sl_Rl_Q_ActivityTester(self, activity):
    COMLogger.info('Enter_Sl_Rl_Q_ActivityTester')


def Exit_Sl_Rl_Q_ActivityTester(self):
    COMLogger.info('Exit_Sl_Rl_Q_ActivityTester')


# # 
transitions = {
    'Enter_I_ActivityTester':Enter_I_ActivityTester,
    'Exit_I_ActivityTester':Exit_I_ActivityTester,
    'Enter_F_ActivityTester':Enter_F_ActivityTester,
    'Exit_F_ActivityTester':Exit_F_ActivityTester,
    'Enter_B_ActivityTester':Enter_B_ActivityTester,
    'Exit_B_ActivityTester':Exit_B_ActivityTester,
    'Enter_Sr_ActivityTester':Enter_Sr_ActivityTester,
    'Exit_Sr_ActivityTester':Exit_Sr_ActivityTester,
    'Enter_Sl_ActivityTester':Enter_Sl_ActivityTester,
    'Exit_Sl_ActivityTester':Exit_Sl_ActivityTester,
    'Enter_Rr_ActivityTester':Enter_Rr_ActivityTester,
    'Exit_Rr_ActivityTester':Exit_Rr_ActivityTester,
    'Enter_Rl_ActivityTester':Enter_Rl_ActivityTester,
    'Exit_Rl_ActivityTester':Exit_Rl_ActivityTester,
    'Enter_J_ActivityTester':Enter_J_ActivityTester,
    'Exit_J_ActivityTester':Exit_J_ActivityTester,
    'Enter_F_Rr_ActivityTester':Enter_F_Rr_ActivityTester,
    'Exit_F_Rr_ActivityTester':Exit_F_Rr_ActivityTester,
    'Enter_F_Rl_ActivityTester':Enter_F_Rl_ActivityTester,
    'Exit_F_Rl_ActivityTester':Exit_F_Rl_ActivityTester,
    'Enter_F_Rr_Rl_ActivityTester':Enter_F_Rr_Rl_ActivityTester,
    'Exit_F_Rr_Rl_ActivityTester':Exit_F_Rr_Rl_ActivityTester,
    'Enter_Rr_Rl_ActivityTester':Enter_Rr_Rl_ActivityTester,
    'Exit_Rr_Rl_ActivityTester':Exit_Rr_Rl_ActivityTester,
    'Enter_F_J_ActivityTester':Enter_F_J_ActivityTester,
    'Exit_F_J_ActivityTester':Exit_F_J_ActivityTester,
    'Enter_B_Rr_ActivityTester':Enter_B_Rr_ActivityTester,
    'Exit_B_Rr_ActivityTester':Exit_B_Rr_ActivityTester,
    'Enter_B_Rl_ActivityTester':Enter_B_Rl_ActivityTester,
    'Exit_B_Rl_ActivityTester':Exit_B_Rl_ActivityTester,
    'Enter_Sr_Rr_ActivityTester':Enter_Sr_Rr_ActivityTester,
    'Exit_Sr_Rr_ActivityTester':Exit_Sr_Rr_ActivityTester,
    'Enter_Sr_Rl_ActivityTester':Enter_Sr_Rl_ActivityTester,
    'Exit_Sr_Rl_ActivityTester':Exit_Sr_Rl_ActivityTester,
    'Enter_Sl_Rr_ActivityTester':Enter_Sl_Rr_ActivityTester,
    'Exit_Sl_Rr_ActivityTester':Exit_Sl_Rr_ActivityTester,
    'Enter_Sl_Rl_ActivityTester':Enter_Sl_Rl_ActivityTester,
    'Exit_Sl_Rl_ActivityTester':Exit_Sl_Rl_ActivityTester,
    'Enter_F_Q_ActivityTester':Enter_F_Q_ActivityTester,
    'Exit_F_Q_ActivityTester':Exit_F_Q_ActivityTester,
    'Enter_Sr_Q_ActivityTester':Enter_Sr_Q_ActivityTester,
    'Exit_Sr_Q_ActivityTester':Exit_Sr_Q_ActivityTester,
    'Enter_Sl_Q_ActivityTester':Enter_Sl_Q_ActivityTester,
    'Exit_Sl_Q_ActivityTester':Exit_Sl_Q_ActivityTester,
    'Enter_Rr_Q_ActivityTester':Enter_Rr_Q_ActivityTester,
    'Exit_Rr_Q_ActivityTester':Exit_Rr_Q_ActivityTester,
    'Enter_Rl_Q_ActivityTester':Enter_Rl_Q_ActivityTester,
    'Exit_Rl_Q_ActivityTester':Exit_Rl_Q_ActivityTester,
    'Enter_F_Rr_Q_ActivityTester':Enter_F_Rr_Q_ActivityTester,
    'Exit_F_Rr_Q_ActivityTester':Exit_F_Rr_Q_ActivityTester,
    'Enter_F_Rl_Q_ActivityTester':Enter_F_Rl_Q_ActivityTester,
    'Exit_F_Rl_Q_ActivityTester':Exit_F_Rl_Q_ActivityTester,
    'Enter_F_Rr_Rl_Q_ActivityTester':Enter_F_Rr_Rl_Q_ActivityTester,
    'Exit_F_Rr_Rl_Q_ActivityTester':Exit_F_Rr_Rl_Q_ActivityTester,
    'Enter_Rr_Rl_Q_ActivityTester':Enter_Rr_Rl_Q_ActivityTester,
    'Exit_Rr_Rl_Q_ActivityTester':Exit_Rr_Rl_Q_ActivityTester,
    'Enter_F_J_Q_ActivityTester':Enter_F_J_Q_ActivityTester,
    'Exit_F_J_Q_ActivityTester':Exit_F_J_Q_ActivityTester,
    'Enter_Sr_Rr_Q_ActivityTester':Enter_Sr_Rr_Q_ActivityTester,
    'Exit_Sr_Rr_Q_ActivityTester':Exit_Sr_Rr_Q_ActivityTester,
    'Enter_Sr_Rl_Q_ActivityTester':Enter_Sr_Rl_Q_ActivityTester,
    'Exit_Sr_Rl_Q_ActivityTester':Exit_Sr_Rl_Q_ActivityTester,
    'Enter_Sl_Rr_Q_ActivityTester':Enter_Sl_Rr_Q_ActivityTester,
    'Exit_Sl_Rr_Q_ActivityTester':Exit_Sl_Rr_Q_ActivityTester,
    'Enter_Sl_Rl_Q_ActivityTester':Enter_Sl_Rl_Q_ActivityTester,
    'Exit_Sl_Rl_Q_ActivityTester':Exit_Sl_Rl_Q_ActivityTester,
}
