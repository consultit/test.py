'''
Created on Feb 27, 2020

@author: consultit
'''

from COM.tools.utilities import COMLogger

# 'Enter_<STATE>_<OBJECTTYPE>':Enter_<STATE>_<OBJECTTYPE>,
# 'Exit_<STATE>_<OBJECTTYPE>':Exit_<STATE>_<OBJECTTYPE>,
# FILTER Filter_<STATE>_<OBJECTTYPE>
# FROMTO <STATEA>_FromTo_<STATEB>_<OBJECTTYPE>

# # MultiActivityTester (MultiActivity) related


# # FSM1 States' functions
# FSM1_A
def Enter_FSM1_A_ActivityFSM1(self, multiActivity):
    COMLogger.info('Enter_FSM1_A_ActivityFSM1')


def Exit_FSM1_A_ActivityFSM1(self):
    COMLogger.info('Exit_FSM1_A_ActivityFSM1')


# FSM1_B
def Enter_FSM1_B_ActivityFSM1(self, multiActivity):
    COMLogger.info('Enter_FSM1_B_ActivityFSM1')


def Exit_FSM1_B_ActivityFSM1(self):
    COMLogger.info('Exit_FSM1_B_ActivityFSM1')


# FSM1_C
def Enter_FSM1_C_ActivityFSM1(self, multiActivity):
    COMLogger.info('Enter_FSM1_C_ActivityFSM1')


def Exit_FSM1_C_ActivityFSM1(self):
    COMLogger.info('Exit_FSM1_C_ActivityFSM1')


# FSM1_D
def Enter_FSM1_D_ActivityFSM1(self, multiActivity):
    COMLogger.info('Enter_FSM1_D_ActivityFSM1')


def Exit_FSM1_D_ActivityFSM1(self):
    COMLogger.info('Exit_FSM1_D_ActivityFSM1')


# # FSM2 States' functions
# FSM2_A
def Enter_FSM2_A_ActivityFSM2(self, multiActivity):
    COMLogger.info('Enter_FSM2_A_ActivityFSM2')


def Exit_FSM2_A_ActivityFSM2(self):
    COMLogger.info('Exit_FSM2_A_ActivityFSM2')


# FSM2_B
def Enter_FSM2_B_ActivityFSM2(self, multiActivity):
    COMLogger.info('Enter_FSM2_B_ActivityFSM2')


def Exit_FSM2_B_ActivityFSM2(self):
    COMLogger.info('Exit_FSM2_B_ActivityFSM2')


# FSM2_C
def Enter_FSM2_C_ActivityFSM2(self, multiActivity):
    COMLogger.info('Enter_FSM2_C_ActivityFSM2')


def Exit_FSM2_C_ActivityFSM2(self):
    COMLogger.info('Exit_FSM2_C_ActivityFSM2')


# # 
transitions = {
    'Enter_FSM1_A_ActivityFSM1' :Enter_FSM1_A_ActivityFSM1,
    'Exit_FSM1_A_ActivityFSM1' :Exit_FSM1_A_ActivityFSM1,
    'Enter_FSM1_B_ActivityFSM1' :Enter_FSM1_B_ActivityFSM1,
    'Exit_FSM1_B_ActivityFSM1' :Exit_FSM1_B_ActivityFSM1,
    'Enter_FSM1_C_ActivityFSM1' :Enter_FSM1_C_ActivityFSM1,
    'Exit_FSM1_C_ActivityFSM1' :Exit_FSM1_C_ActivityFSM1,
    'Enter_FSM1_D_ActivityFSM1' :Enter_FSM1_D_ActivityFSM1,
    'Exit_FSM1_D_ActivityFSM1' :Exit_FSM1_D_ActivityFSM1,
    'Enter_FSM2_A_ActivityFSM2' :Enter_FSM2_A_ActivityFSM2,
    'Exit_FSM2_A_ActivityFSM2' :Exit_FSM2_A_ActivityFSM2,
    'Enter_FSM2_B_ActivityFSM2' :Enter_FSM2_B_ActivityFSM2,
    'Exit_FSM2_B_ActivityFSM2' :Exit_FSM2_B_ActivityFSM2,
    'Enter_FSM2_C_ActivityFSM2' :Enter_FSM2_C_ActivityFSM2,
    'Exit_FSM2_C_ActivityFSM2' :Exit_FSM2_C_ActivityFSM2,
}
