'''
Created on Sep 29, 2019

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# Game_init
spec = util.spec_from_file_location('Game_init',
            os.path.join(_this_library_dir, 'defs', 'Game_init.py'))
Game_init = util.module_from_spec(spec)
spec.loader.exec_module(Game_init)

# StaticActor_TEST_init
spec = util.spec_from_file_location('StaticActor_TEST_init',
            os.path.join(_this_library_dir, 'defs', 'StaticActor_TEST_init.py'))
StaticActor_TEST_init = util.module_from_spec(spec)
spec.loader.exec_module(StaticActor_TEST_init)

# testActivity_init
spec = util.spec_from_file_location('testActivity_init',
            os.path.join(_this_library_dir, 'defs', 'testActivity_init.py'))
testActivity_init = util.module_from_spec(spec)
spec.loader.exec_module(testActivity_init)

# testMultiActivity_init
spec = util.spec_from_file_location('testMultiActivity_init',
            os.path.join(_this_library_dir, 'defs', 'testMultiActivity_init.py'))
testMultiActivity_init = util.module_from_spec(spec)
spec.loader.exec_module(testMultiActivity_init)

# initializations definitions
# See: https://stackoverflow.com/questions/38987/how-to-merge-two-dictionaries-in-a-single-expression
initializations = {
    **Game_init.initializations,
    **StaticActor_TEST_init.initializations,
    **testActivity_init.initializations,
    **testMultiActivity_init.initializations,
}
