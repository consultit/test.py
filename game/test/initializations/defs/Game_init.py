'''
Created on Oct 19, 2019

@author: consultit
'''

from panda3d.core import NodePath, TextNode
from COM.managers.gameGUIManager import GameGUIManager
from test.initializations.defs.common import ASSERT
import sys


# # specific data/functions declarations/definitions
def writeText(text, scale, color, location, app):
    '''common text writing'''
    global textNode
    textNode = NodePath(TextNode('CommonTextNode'))
    textNode.reparent_to(app.render2d)
    textNode.set_bin('fixed', 50)
    textNode.set_depth_write(False)
    textNode.set_depth_test(False)
    textNode.set_billboard_point_eye()
    textNode.node().set_text(text)
    textNode.set_scale(scale)
    textNode.set_color(color)
    textNode.set_pos(location)


def showMainMenu():
    elyMgr = GameGUIManager.get_global_ptr()._elyMgr
    if elyMgr:
        elyMgr.show_main_menu()

    
def showExitMenu():
    elyMgr = GameGUIManager.get_global_ptr()._elyMgr
    if elyMgr:
        elyMgr.show_exit_menu()


# event handler added to the main one
def rocketEventHandler(event, value):
    if value == 'main.body.load_logo':
        print('main.body.load_logo')


def on_exit_function():
    '''on exit function'''
    elyMgr = GameGUIManager.get_global_ptr()._elyMgr
    if elyMgr:
        elyMgr.gui_cleanup() 
    sys.exit(0)


def elyPreObjects_TEST_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    elyMgr = GameGUIManager.get_global_ptr()._elyMgr
    if elyMgr:
        # register the add element function to gui (Rocket) main menu
        # register the event handler to gui main menu for each event value
        elyMgr.register_gui_event_handler('main.body.load_logo', rocketEventHandler)
        # register the preset function to gui main menu
        # register the commit function to gui main menu
        elyMgr.set_gui_on_exit_function(on_exit_function)


def elyPostObjects_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    ASSERT('elyPostObjects', objectI, paramTable, pandaFramework, windowFramework)
    # add show main menu event handler
    pandaFramework.accept('m', showMainMenu)
    # handle 'close request' and 'esc' events
    pandaFramework.win.set_close_request_event('close_request_event')
    pandaFramework.accept('close_request_event', showExitMenu)
    pandaFramework.accept('escape', showExitMenu)


initializations = {
    'elyPreObjects_TEST_initialization': elyPreObjects_TEST_initialization,
    'elyPostObjects_initialization': elyPostObjects_initialization,
    }
