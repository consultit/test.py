'''
Created on Feb 27, 2020

@author: consultit
'''

from COM.com.component import ComponentFamilyType


def multiTester0_initialization(objectI, paramTable, pandaFramework, windowFramework):
    # Player1
    playerMultiActivity = objectI.get_component(ComponentFamilyType('Behavior'))
    # make both fsm initializations
    playerFSM1 = playerMultiActivity.activities['FSM1']
    playerFSM1.fsm.request('FSM1_A', playerFSM1)
    playerFSM2 = playerMultiActivity.activities['FSM2']
    playerFSM2.fsm.request('FSM2_A', playerFSM2)


# #
initializations = {
    'multiTester0_initialization': multiTester0_initialization,
}
