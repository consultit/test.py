'''
Created on Oct 19, 2019

@author: consultit
'''

from COM.com.object import ObjectId


def ASSERT(objectName, objectI, paramTable, pandaFramework, windowFramework):
    assert (objectI.object_id == ObjectId(objectName))
    print(objectName, 'init function called.', sep=' ')
    print('object id == ', objectI.object_id.value, sep='')
    print('parameters:', sep='')
    for param in paramTable:
        print('\t', param, ':', paramTable[param], sep='')
    print('pandaFramework: ', pandaFramework, sep='')
    print('windowFramework', windowFramework, sep='')
