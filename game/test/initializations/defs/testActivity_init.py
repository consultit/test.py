'''
Created on Feb 24, 2020

@author: consultit
'''

from COM.com.component import ComponentFamilyType


def tester0_initialization(objectI, paramTable, pandaFramework, windowFramework):
    # Player1
    playerActivity = objectI.get_component(ComponentFamilyType('Behavior'))
    playerActivity.fsm.request('I', playerActivity)


# #
initializations = {
    'tester0_initialization': tester0_initialization,
}
