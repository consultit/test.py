'''
Created on Sep 30, 2019

@author: consultit
'''

from test.initializations.defs.common import ASSERT


# Plane1_TEST related
def Plane1_TEST_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    ASSERT('Plane1_TEST', objectI, paramTable, pandaFramework,
                               windowFramework)


initializations = {
    'Plane1_TEST_initialization': Plane1_TEST_initialization,
    }
