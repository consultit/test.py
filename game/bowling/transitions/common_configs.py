'''
Created on Mar 06, 2020

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# player_trns
spec = util.spec_from_file_location('player_trns',
            os.path.join(_this_library_dir, 'defs', 'player_trns.py'))
player_trns = util.module_from_spec(spec)
spec.loader.exec_module(player_trns)

# transitions definitions
transitions = {
    **player_trns.transitions,
}
