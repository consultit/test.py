'''
Created on Mar 8, 2020

@author: consultit
'''

from COM.com.component import ComponentFamilyType
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.managers.gameManager import GameManager

# Enter_<STATE>_<OBJECTTYPE>,
# Exit_<STATE>_<OBJECTTYPE>,
# FILTER Filter_<STATE>_<OBJECTTYPE>
# FROMTO <STATEA>_FromTo_<STATEB>_<OBJECTTYPE>

pandaFramework = GameManager.get_global_ptr()


# helper for retrieving MultiActivity component in Exit_* functions
def _multiActivity(SELF):
    # 
    activityOwnerObj = ObjectTemplateManager.get_global_ptr().get_created_object(
        SELF._owner_object)
    return activityOwnerObj.get_component(
        ComponentFamilyType('Behavior'))._multi_activity

################################
### Movement Concurrent FSMs ###
################################


_linearSpeedFactor = 15.0
_angularSpeedFactor = 5.0


# # fsm_FB States' functions
# FBS_idle
def Enter_FBS_idle_FSM_ForwardBackward(self, multiActivity):
    #
    pandaFramework.messenger.send('idle')


def Exit_FBS_idle_FSM_ForwardBackward(self):
    pass


# FBS_forward
def Enter_FBS_forward_FSM_ForwardBackward(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().move_forward = True
    #
    if multiActivity.activities['fsm_SF'].fsm.getCurrentOrNextState() == 'SFS_slow':
        pandaFramework.messenger.send('walk')
    else:
        pandaFramework.messenger.send('run')


def Exit_FBS_forward_FSM_ForwardBackward(self):
    #
    npc1 = _multiActivity(self).owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().move_forward = False
    #
    pandaFramework.messenger.send('idle')


# FBS_backward
def Enter_FBS_backward_FSM_ForwardBackward(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().move_backward = True
    #
    if multiActivity.activities['fsm_SF'].fsm.getCurrentOrNextState() == 'SFS_slow':
        pandaFramework.messenger.send('walk')
    else:
        pandaFramework.messenger.send('run')


def Exit_FBS_backward_FSM_ForwardBackward(self):
    #
    npc1 = _multiActivity(self).owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().move_backward = False
    #
    pandaFramework.messenger.send('idle')


# # fsm_LR States' functions (Movement)
# LRS_idle
def Enter_LRS_idle_FSM_LeftRight(self, multiActivity):
    pass


def Exit_LRS_idle_FSM_LeftRight(self):
    pass


# LRS_left
def Enter_LRS_left_FSM_LeftRight(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_LRS_left_FSM_LeftRight(self):
    #
    npc1 = _multiActivity(self).owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().rotate_head_left = False


# LRS_right
def Enter_LRS_right_FSM_LeftRight(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_LRS_right_FSM_LeftRight(self):
    #
    npc1 = _multiActivity(self).owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().rotate_head_right = False


# # fsm_SF States' functions (Movement)
# from SFS_slow to SFS_fast
def SFS_slow_FromTo_SFS_fast_FSM_SlowFast(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().max_linear_speed *= _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed *= _angularSpeedFactor
    #
    state = multiActivity.activities['fsm_FB'].fsm.getCurrentOrNextState()
    if (state == 'FBS_forward') or (state == 'FBS_backward'):
        pandaFramework.messenger.send('run')


# from SFS_fast to SFS_slow
def SFS_fast_FromTo_SFS_slow_FSM_SlowFast(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().max_linear_speed /= _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed /= _angularSpeedFactor
    #
    state = multiActivity.activities['fsm_FB'].fsm.getCurrentOrNextState()
    if (state == 'FBS_forward') or (state == 'FBS_backward'):
        pandaFramework.messenger.send('walk')

#################################
### Animation Concurrent FSMs ###
#################################


# # fsm_Anim States' functions (Animation)
# ANS_idle
def Enter_ANS_idle_FSM_Animation(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1Model.animations.stop_all()


def Exit_ANS_idle_FSM_Animation(self):
    pass


# from ANS_idle to ANS_walk
def ANS_idle_FromTo_ANS_walk_FSM_Animation(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1Model.animations.loop('walk', False)    


# from ANS_walk to ANS_idle
def ANS_walk_FromTo_ANS_idle_FSM_Animation(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1Model.animations.stop('walk')

    
# from ANS_idle to ANS_run
def ANS_idle_FromTo_ANS_run_FSM_Animation(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1Model.animations.loop('run', False)


# from ANS_run to ANS_idle
def ANS_run_FromTo_ANS_idle_FSM_Animation(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1Model.animations.stop('run')


# from ANS_walk to ANS_run
def ANS_walk_FromTo_ANS_run_FSM_Animation(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1Model.animations.stop('walk')
    npc1Model.animations.loop('run', False)


# from ANS_run to ANS_walk
def ANS_run_FromTo_ANS_walk_FSM_Animation(self, multiActivity):
    # 
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1Model.animations.stop('run')
    npc1Model.animations.loop('walk', False)


# # 
transitions = {
    # fsm_FB
    'Enter_FBS_idle_FSM_ForwardBackward' :Enter_FBS_idle_FSM_ForwardBackward,
    'Exit_FBS_idle_FSM_ForwardBackward' :Exit_FBS_idle_FSM_ForwardBackward,
    'Enter_FBS_forward_FSM_ForwardBackward' :Enter_FBS_forward_FSM_ForwardBackward,
    'Exit_FBS_forward_FSM_ForwardBackward' :Exit_FBS_forward_FSM_ForwardBackward,
    'Enter_FBS_backward_FSM_ForwardBackward' :Enter_FBS_backward_FSM_ForwardBackward,
    'Exit_FBS_backward_FSM_ForwardBackward' :Exit_FBS_backward_FSM_ForwardBackward,
    # fsm_LR
    'Enter_LRS_idle_FSM_LeftRight' :Enter_LRS_idle_FSM_LeftRight,
    'Exit_LRS_idle_FSM_LeftRight' :Exit_LRS_idle_FSM_LeftRight,
    'Enter_LRS_left_FSM_LeftRight' :Enter_LRS_left_FSM_LeftRight,
    'Exit_LRS_left_FSM_LeftRight' :Exit_LRS_left_FSM_LeftRight,
    'Enter_LRS_right_FSM_LeftRight' :Enter_LRS_right_FSM_LeftRight,
    'Exit_LRS_right_FSM_LeftRight' :Exit_LRS_right_FSM_LeftRight,
    # fsm_SF
    'SFS_slow_FromTo_SFS_fast_FSM_SlowFast' :SFS_slow_FromTo_SFS_fast_FSM_SlowFast,
    'SFS_fast_FromTo_SFS_slow_FSM_SlowFast' :SFS_fast_FromTo_SFS_slow_FSM_SlowFast,
    # fsm_Anim
    'Enter_ANS_idle_FSM_Animation' :Enter_ANS_idle_FSM_Animation,
    'Exit_ANS_idle_FSM_Animation' :Exit_ANS_idle_FSM_Animation,
    'ANS_idle_FromTo_ANS_walk_FSM_Animation' :ANS_idle_FromTo_ANS_walk_FSM_Animation,
    'ANS_walk_FromTo_ANS_idle_FSM_Animation' :ANS_walk_FromTo_ANS_idle_FSM_Animation,
    'ANS_idle_FromTo_ANS_run_FSM_Animation' :ANS_idle_FromTo_ANS_run_FSM_Animation,
    'ANS_run_FromTo_ANS_idle_FSM_Animation' :ANS_run_FromTo_ANS_idle_FSM_Animation,
    'ANS_walk_FromTo_ANS_run_FSM_Animation' :ANS_walk_FromTo_ANS_run_FSM_Animation,
    'ANS_run_FromTo_ANS_walk_FSM_Animation' :ANS_run_FromTo_ANS_walk_FSM_Animation,
}
