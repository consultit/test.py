'''
Created on Mar 9, 2020

@author: consultit
'''

# ## TODO

from COM.com.component import ComponentFamilyType

# # player


def animUpdate(dt, activity):
    ownerObj = activity._multi_activity.owner_object
    ownerModel = ownerObj.get_component(ComponentFamilyType('Scene'))
    ownerCharCtrl = ownerObj.get_component(
        ComponentFamilyType('PhysicsControl'))
    linearSpeed = abs(ownerCharCtrl.p3CharacterController.node().linear_speed.y)
    ownerModel.animations.find_anim('walk').set_play_rate(0.0065 * linearSpeed)


# #
instance_updates = {
    'animUpdate': animUpdate,
}
