'''
Created on Mar 06, 2020

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# player_updt
spec = util.spec_from_file_location('player_updt',
            os.path.join(_this_library_dir, 'defs', 'player_updt.py'))
player_updt = util.module_from_spec(spec)
spec.loader.exec_module(player_updt)

# instance_updates definitions
instance_updates = {
    **player_updt.instance_updates,
}
