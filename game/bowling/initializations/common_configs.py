'''
Created on Mar 06, 2020

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# camera_init
spec = util.spec_from_file_location('camera_init',
            os.path.join(_this_library_dir, 'defs', 'camera_init.py'))
camera_init = util.module_from_spec(spec)
spec.loader.exec_module(camera_init)

# floor_init
spec = util.spec_from_file_location('floor_init',
            os.path.join(_this_library_dir, 'defs', 'floor_init.py'))
floor_init = util.module_from_spec(spec)
spec.loader.exec_module(floor_init)

# player_init
spec = util.spec_from_file_location('player_init',
            os.path.join(_this_library_dir, 'defs', 'player_init.py'))
player_init = util.module_from_spec(spec)
spec.loader.exec_module(player_init)

# initializations definitions
initializations = {
    **camera_init.initializations,
    **floor_init.initializations,
    **player_init.initializations,
}
