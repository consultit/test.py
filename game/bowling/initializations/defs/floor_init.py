'''
Created on Mar 7, 2020

@author: consultit
'''

from COM.tools.utilities import StringSequence
from COM.com.component import ComponentFamilyType
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectId
from COM.managers.gamePhysicsManager import GamePhysicsManager
from panda3d.core import LVector3f, LVecBase3f, NodePath
from random import random

_PARENTOBJECT = 'render'

_PINTOBECLONED = 'pinToBeCloned'
_pins = {}

_BALLTOBECLONED = 'ballToBeCloned'
_balls = {}


def _create_object(name, pos, objectToBeCloned):
    '''Create pin'''
    
    global _PARENTOBJECT
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    # get object to be cloned
    toBeClonedObject = objTmplMgr.get_created_object(
        ObjectId(objectToBeCloned))
    if not toBeClonedObject:
        return
    # get the object-to-be-cloned parameter table
    objParams = toBeClonedObject.stored_obj_tmpl_params
    # get the object-to-be-cloned components' parameter tables
    compParams = toBeClonedObject.stored_comp_tmpl_params
    # # create the clone
    # tweak clone object's common parameters
    # set clone object store_params
    objParams.pop('store_params', None)
    objParams['store_params'] = StringSequence(['false'])
    # set clone object store_params
    objParams.pop('parent', None)
    objParams['parent'] = StringSequence([_PARENTOBJECT])
    # set clone object pos
    objParams.pop('pos', None)
    objParams['pos'] = StringSequence(
        [str(pos.x) + ',' + str(pos.y) + ',' + str(pos.z)])
    # tweak clone components' parameter tables
    # set RigidBody use_shape_of
    compParams['RigidBody'].pop('use_shape_of', None)
    compParams['RigidBody']['use_shape_of'] = StringSequence([objectToBeCloned])
    # create actually the clone and...
    clone = objTmplMgr.create_object(
        toBeClonedObject.object_type, ObjectId(name), objParams, compParams,
        False)
    # ...initialize it...
    clone.world_setup()
    # ... and return
    return clone


def _setup_pins_arrangement(refNode):
    '''Create and arrange pins'''
    
    global _PINTOBECLONED
    # read positions in floor model based on empties 'Pin.LL.NN'
    # only lane '00' has all pins, the other lanes only the first one
    pinPos = {}
    for pin in refNode.find_all_matches('**/Pin*'):
        lane, num = pin.get_name().split('.')[1:]
        if not lane in pinPos.keys():
            pinPos[lane] = {}
        pinPos[lane][num] = pin.get_pos()
    # create the pins
    for lane in pinPos.keys():
        # complete pinPos for this lane (if not '00')
        if lane != '00':
            laneDist = pinPos[lane]['00'] - pinPos['00']['00']
            # complete positions or this lane
            for num in pinPos['00']:
                # exclude first pin (is defined for each lane)
                if num != '00':
                    pinPos[lane][num] = pinPos['00'][num] + laneDist
        # now we have a complete set of positions for this lane
        for num in pinPos[lane]:
            pinName = 'Pin.' + lane + '.' + num
            # set position
            pos = pinPos[lane][num]
            # clone pin
            clonedPin = _create_object(pinName, pos, _PINTOBECLONED)
            if clonedPin:
                _pins[pinName] = clonedPin


def _setup_balls_arrangement(refNode, ballsPerLane=3):
    '''Create and arrange balls'''
    
    global _BALLTOBECLONED
    # read positions in floor model based on empties 'Ball.LL'
    # only one ball per lane is defined
    ballPos = {}
    for ball in refNode.find_all_matches('**/Ball*'):
        lane = ball.get_name().split('.')[1]
        ballPos[lane] = ball.get_pos()
    # create the balls
    # get ball's radius
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    elyPhysicsMgr = GamePhysicsManager.get_global_ptr()._elyMgr
    ballClonedObject = objTmplMgr.get_created_object(
        ObjectId(_BALLTOBECLONED))
    if not ballClonedObject:
        return
    ballElyRigidBody = ballClonedObject.get_component(
        ComponentFamilyType('Physics')).p3RigidBody
    modelDims = LVecBase3f()
    modelDeltaCenter = LVector3f()
    ballRadius = elyPhysicsMgr.get_bounding_dimensions(ballElyRigidBody,
                                                       modelDims,
                                                       modelDeltaCenter)
    for lane in ballPos.keys():
        # get the number of balls to create
        ballsNum = max(1, ballsPerLane)
        for num in range(ballsNum):
            ballName = 'Ball.' + lane + '.' + '{0:02d}'.format(num)
            # set position
            if num == 0:
                pos = ballPos[lane]
            else:
                gap = ballRadius * (1.0 + 0.2 * (2.0 * random() - 1.0))
                deltaPos = LVector3f(0.0, -gap * 2 * num, gap * num)
                pos = ballPos[lane] + deltaPos
            # clone ball
            clonedBall = _create_object(ballName, pos, _BALLTOBECLONED)
            if clonedBall:
                _balls[ballName] = clonedBall
                
            
def floor_initialization(objectI, paramTable, pandaFramework, windowFramework):
    # get floor model
    floorModel = objectI.get_component(ComponentFamilyType('Scene')).node_path
    # setup arrangement of pins
    _setup_pins_arrangement(floorModel)
    # setup arrangement of balls
    _setup_balls_arrangement(floorModel)
    
    ### FIXME ###
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    objTmplMgr.destroy_object(ObjectId(_PINTOBECLONED))
    objTmplMgr.destroy_object(ObjectId(_BALLTOBECLONED))
    elyPhysicsMgr = GamePhysicsManager.get_global_ptr()._elyMgr
    elyPhysicsMgr.enable_picking(
        pandaFramework.render, NodePath.any_path(pandaFramework.camNode),
        pandaFramework.mouseWatcher.node(), 'p', 'p-up')    


# #
initializations = {
    'floor_initialization': floor_initialization,
}
