'''
Created on Mar 8, 2020

@author: consultit
'''

from COM.com.component import ComponentFamilyType


def player_initialization(objectI, paramTable, pandaFramework, windowFramework):
    # set the initial states
    multiActivity = objectI.get_component(ComponentFamilyType('Behavior'))
    # preform FSMs' initialization
    multiActivity.activities['fsm_FB'].fsm.request('FBS_idle', multiActivity)
    multiActivity.activities['fsm_LR'].fsm.request('LRS_idle', multiActivity)
    multiActivity.activities['fsm_SF'].fsm.request('SFS_slow', multiActivity)
    multiActivity.activities['fsm_Anim'].fsm.request('ANS_idle', multiActivity)


# #
initializations = {
    'player_initialization': player_initialization,
}
