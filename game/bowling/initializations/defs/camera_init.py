'''
Created on Mar 6, 2020

@author: consultit
'''

from COM.tools.utilities import parse_compound_string, ParameterTable, \
    StringSequence, COMLogger
from COM.com.component import ComponentFamilyType, ComponentType
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectId
from COM.managers.gameGUIManager import GameGUIManager
from COM.managers.gamePhysicsManager import GamePhysicsManager
from COM.managers.gameManager import GameManager
from ely.gui import P3GUIDictionary, P3GUIFactory, P3GUIElementPtr, \
    P3GUIElementFormControlInputPtr, P3GUIElementFormControlSelectPtr
from panda3d.core import NodePath, LVecBase4f, LVector3f, TextNode
from enum import Enum

# common text writing
_textNode = NodePath('_textNode')


def createText(text, scale, color, location, app):
    '''common text writing'''
    
    textNode = NodePath(TextNode('CommonTextNode'))
    textNode.reparent_to(app.render2d)
    textNode.set_bin('fixed', 50)
    textNode.set_depth_write(False)
    textNode.set_depth_test(False)
    textNode.set_billboard_point_eye()
    textNode.node().set_text(text)
    textNode.set_scale(scale)
    textNode.set_color(color)
    textNode.set_pos(location)
    return textNode


class _CameraType(Enum):
    free_view_camera = 0
    chaser_camera = 1
    object_picker = 2
    none = 3


_cameraType = _CameraType.none
# the camera Object
_camera = None
# ParameterTables helper data
_cameraDriverParams = None
_cameraChaserParams = None
# the chased Object
_chasedObject = ObjectId()
# constraint type flag
_pickerCsIspherical = True

 
def _rmluiAddElements(mainMenu):
    '''add elements (tags) function for main menu'''

    # <button onclick='camera.options'>Camera options</button><br/>
    content = mainMenu.get_element_by_id('content')
    exitT = mainMenu.get_element_by_id('main::button::exit')
    if content:
        # create inputT element
        params = P3GUIDictionary()
        params.set_string('onclick', 'camera::options')
        inputT = P3GUIFactory.instance_element(P3GUIElementPtr(), 'button', 'button', params)
        P3GUIFactory.instance_element_text(inputT, 'Camera options')
        # create br element
        params.clear()
        params.set_string('id', 'br')
        br = P3GUIFactory.instance_element(P3GUIElementPtr(), 'br', 'br', params)
        # insert elements
        content.insert_before(inputT, exitT)
        content.insert_before(br, exitT)


# helpers
def _setElementValue(param, paramsTable, document):
    inputElem = P3GUIElementFormControlInputPtr(document.get_element_by_id(param))
    inputElem.set_value(paramsTable[param][0])
 
 
def _setElementChecked(param, checked, unchecked, defaultValue, paramsTable, document):
    inputElem = P3GUIElementFormControlInputPtr(
            document.get_element_by_id(param))
    actualChecked = paramsTable[param][0]
    if actualChecked == checked:
        inputElem.set_attribute_string('checked', 'true')
    elif actualChecked == unchecked:
        inputElem.remove_attribute('checked')
    elif defaultValue == checked:
        inputElem.set_attribute_string('checked', 'true')
    else:
        inputElem.remove_attribute('checked')

 
def _setOptionValue(event, param, paramsTable):
    paramsTable[param] = StringSequence([event.get_parameter_string(param, '0.0')])
 
 
def _setOptionChecked(event, param, checked, unchecked, paramsTable):
    paramValue = event.get_parameter_string(param, '')
    if paramValue == 'true':
        paramsTable[param] = StringSequence([checked])
    else:
        paramsTable[param] = StringSequence([unchecked])


def _rmluiEventHandler(event, value):
    '''event handler added to the main one'''

    global _cameraType, _chasedObject, _pickerCsIspherical
    elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
    if value == 'camera::options':
        # hide main menu
        elyGUIMgr.get_main_menu().hide()
        #  Load and show the camera options document.
        cameraOptionsMenu = elyGUIMgr.get_rml_context().load_document('ely-camera-options.rml')
        if not cameraOptionsMenu.empty:
            cameraOptionsMenu.get_element_by_id('title').set_inner_rml(
                cameraOptionsMenu.get_title())
            # #update radio buttons
            # camera type
            if _cameraType == _CameraType.free_view_camera:
                cameraOptionsMenu.get_element_by_id('free_view_camera').set_attribute_bool(
                        'checked', True)
            elif _cameraType == _CameraType.chaser_camera:
                cameraOptionsMenu.get_element_by_id('chaser_camera').set_attribute_bool(
                        'checked', True)
            elif _cameraType == _CameraType.object_picker:
                cameraOptionsMenu.get_element_by_id('object_picker').set_attribute_bool(
                        'checked', True)
            elif _cameraType == _CameraType.none:
                cameraOptionsMenu.get_element_by_id('none').set_attribute_bool(
                        'checked', True)           
            # 
            cameraOptionsMenu.show()          
    elif value == 'camera::body::load_logo':
        COMLogger.info('camera::body::load_logo')
    elif value == 'camera::free_view_camera::options':
        #  This event is sent from the 'onchange' of the 'free_view_camera'
        # radio button. It shows or hides the related options.
        cameraOptionsMenu = event.target_element.owner_document
        if cameraOptionsMenu.empty:
            return
        free_view_camera_options = cameraOptionsMenu.get_element_by_id('free_view_camera_options')
        if not free_view_camera_options.empty:
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                free_view_camera_options.set_property('display', 'none')
            else:
                free_view_camera_options.set_property('display', 'block')
                # set elements' values from options' values
                # pitch limit and pitch limit value: enabled@limit
                paramValuesStr = parse_compound_string(
                        _cameraDriverParams['pitch_limit'][0], '@')
                # pitch limit value
                inputValueElem = P3GUIElementFormControlInputPtr(
                        cameraOptionsMenu.get_element_by_id('pitch_limit_value'))
                inputValueElem.set_value(paramValuesStr[1])
                # pitch limit
                inputPitchElem = P3GUIElementFormControlInputPtr(
                        cameraOptionsMenu.get_element_by_id('pitch_limit'))
                # checks
                if not inputPitchElem.get_attribute('checked').empty:
                    if paramValuesStr[0] == 'true':
                        # enable value input element
                        inputValueElem.set_disabled(False)
                    else:
                        # checked . unchecked: change event thrown
                        inputPitchElem.remove_attribute('checked')
                else:
                    if paramValuesStr[0] != 'true':
                        # disable value input element
                        inputValueElem.set_disabled(True)
                    else:
                        # unchecked . checked: change event thrown
                        inputPitchElem.set_attribute_string('checked', 'true')
                # max linear speed
                _setElementValue('max_linear_speed', _cameraDriverParams,
                        cameraOptionsMenu)
                # max angular speed
                _setElementValue('max_angular_speed', _cameraDriverParams,
                        cameraOptionsMenu)
                # linear accel
                _setElementValue('linear_accel', _cameraDriverParams,
                        cameraOptionsMenu)
                # angular accel
                _setElementValue('angular_accel', _cameraDriverParams,
                        cameraOptionsMenu)
                # linear friction
                _setElementValue('linear_friction', _cameraDriverParams,
                        cameraOptionsMenu)
                # angular friction
                _setElementValue('angular_friction', _cameraDriverParams,
                        cameraOptionsMenu)
                # fast factor
                _setElementValue('fast_factor', _cameraDriverParams,
                        cameraOptionsMenu)
                # sens_x
                _setElementValue('sens_x', _cameraDriverParams,
                        cameraOptionsMenu)
                # sens_y
                _setElementValue('sens_y', _cameraDriverParams,
                        cameraOptionsMenu)
    elif value == 'camera::chaser_camera::options':
        #  This event is sent from the 'onchange' of the 'chaser_camera'
        # radio button. It shows or hides the related options.
        cameraOptionsMenu = event.target_element.owner_document
        if cameraOptionsMenu.empty:
            return
        chaserCameraOptions = cameraOptionsMenu.get_element_by_id('chaser_camera_options')
        if not chaserCameraOptions.empty:
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                chaserCameraOptions.set_property('display', 'none')
            else:
                chaserCameraOptions.set_property('display', 'block')
                # set elements' values from options' values
                # chased object
                objectsSelect = P3GUIElementFormControlSelectPtr(
                    cameraOptionsMenu.get_element_by_id('chased_object'))
                if not objectsSelect.empty:
                    # remove all options
                    objectsSelect.remove_all()
                    # set object list
                    createdObjects = ObjectTemplateManager.get_global_ptr().created_objects
                    selectedIdx = objectsSelect.add('', '')
                    for objectT in createdObjects:
                        # don't add steady or with no parent objects
                        if objectT.is_steady or objectT.node_path.get_parent().is_empty():  # FIXME: remove mandatory objects
                            continue
                        # add options
                        objectId = objectT.object_id
                        i = objectsSelect.add(str(objectId), str(objectId))
                        if objectId == _chasedObject:
                            selectedIdx = i
                    # set first option as selected
                    objectsSelect.set_selection(selectedIdx)
                # fixed relative position
                _setElementChecked('fixed_relative_position', 'true', 'false',
                        'true', _cameraChaserParams, cameraOptionsMenu)
                # backward
                _setElementChecked('backward', 'true', 'false', 'true',
                        _cameraChaserParams, cameraOptionsMenu)
                # abs max distance
                _setElementValue('max_distance', _cameraChaserParams,
                        cameraOptionsMenu)
                # abs min distance
                _setElementValue('min_distance', _cameraChaserParams,
                        cameraOptionsMenu)
                # abs max height
                _setElementValue('max_height', _cameraChaserParams,
                        cameraOptionsMenu)
                # abs min height
                _setElementValue('min_height', _cameraChaserParams,
                        cameraOptionsMenu)
                # abs lookat distance
                _setElementValue('look_at_distance', _cameraChaserParams,
                        cameraOptionsMenu)
                # abs lookat height
                _setElementValue('look_at_height', _cameraChaserParams,
                        cameraOptionsMenu)
                # friction
                _setElementValue('friction', _cameraChaserParams,
                        cameraOptionsMenu)
    elif value == 'camera::object_picker::options':
        #  This event is sent from the 'onchange' of the 'object_picker'
        # radio button. It shows or hides the related options.
        cameraOptionsMenu = event.target_element.owner_document
        if cameraOptionsMenu.empty:
            return
        objectPickerOptions = cameraOptionsMenu.get_element_by_id('object_picker_options')
        if not objectPickerOptions.empty:
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                objectPickerOptions.set_property('display', 'none')
            else:
                objectPickerOptions.set_property('display', 'block')
                # set elements' values from options' values
                # constraint type
                if _pickerCsIspherical:
                    cameraOptionsMenu.get_element_by_id(
                                'spherical_constraint').set_attribute_bool(
                                'checked', True)
                else:
                    cameraOptionsMenu.get_element_by_id(
                                'generic_constraint').set_attribute_bool(
                                'checked', True)
    elif value == 'pitch_limit::change':
        cameraOptionsMenu = event.target_element.owner_document
        # check
        if not P3GUIElementFormControlInputPtr(
                cameraOptionsMenu.get_element_by_id('pitch_limit')).get_attribute(
                'checked').empty:
            P3GUIElementFormControlInputPtr(
                    cameraOptionsMenu.get_element_by_id('pitch_limit_value')).set_disabled(
                    False)
        else:
            P3GUIElementFormControlInputPtr(
                    cameraOptionsMenu.get_element_by_id('pitch_limit_value')).set_disabled(
                    True)
    # #Submit
    elif value == 'camera::form::submit_options':
        # check if ok or cancel
        paramValue = event.get_parameter_string('submit', 'cancel')
        if paramValue == 'ok':
            # set new camera type
            paramValue = event.get_parameter_string('camera', 'none')
            if paramValue == 'free_view_camera':
                _cameraType = _CameraType.free_view_camera
                # set options' values from elements' values
                # pitch limit and value: enabled@limit
                enabled = ('true' if 
                        event.get_parameter_string('pitch_limit', '')
                                == 'true' else 'false')
                if enabled == 'true':
                    limit = event.get_parameter_string('pitch_limit_value', '0.0')
                else:
                    limit = parse_compound_string(
                        _cameraDriverParams['pitch_limit'][0], '@')[1]
                _cameraDriverParams['pitch_limit'] = StringSequence([enabled + '@' + limit])
                # max linear speed
                _setOptionValue(event, 'max_linear_speed', _cameraDriverParams)
                # max angular speed
                _setOptionValue(event, 'max_angular_speed', _cameraDriverParams)
                # linear accel
                _setOptionValue(event, 'linear_accel', _cameraDriverParams)
                # angular accel
                _setOptionValue(event, 'angular_accel', _cameraDriverParams)
                # linear friction
                _setOptionValue(event, 'linear_friction', _cameraDriverParams)
                # angular friction
                _setOptionValue(event, 'angular_friction', _cameraDriverParams)
                # fast factor
                _setOptionValue(event, 'fast_factor', _cameraDriverParams)
                # sens_x
                _setOptionValue(event, 'sens_x', _cameraDriverParams)
                # sens_y
                _setOptionValue(event, 'sens_y', _cameraDriverParams)
            elif paramValue == 'chaser_camera':
                _cameraType = _CameraType.chaser_camera
                # set options' values from elements' values
                # chased object
                _chasedObject = ObjectId(
                    event.get_parameter_string('chased_object', ''))
                if str(_chasedObject):
                    _cameraChaserParams['chased_object'] = StringSequence([str(_chasedObject)])
                # fixed relative position
                _setOptionChecked(event, 'fixed_relative_position', 'true',
                        'false', _cameraChaserParams)
                # backward
                _setOptionChecked(event, 'backward', 'true', 'false',
                        _cameraChaserParams)
                # abs max distance
                _setOptionValue(event, 'max_distance', _cameraChaserParams)
                # abs min distance
                _setOptionValue(event, 'min_distance', _cameraChaserParams)
                # abs max height
                _setOptionValue(event, 'max_height', _cameraChaserParams)
                # abs min height
                _setOptionValue(event, 'min_height', _cameraChaserParams)
                # abs lookat distance
                _setOptionValue(event, 'look_at_distance', _cameraChaserParams)
                # abs lookat height
                _setOptionValue(event, 'look_at_height', _cameraChaserParams)
                # friction
                _setOptionValue(event, 'friction', _cameraChaserParams)     
            elif paramValue == 'object_picker':
                _cameraType = _CameraType.object_picker
                # set options' values from elements' values
                # constraint type
                paramValue = event.get_parameter_string('constraint_type', '')
                if paramValue == 'spherical':
                    _pickerCsIspherical = True
                elif paramValue == 'generic':
                    _pickerCsIspherical = False
            else:
                # default
                _cameraType = _CameraType.none
        # close (i.e. unload) the camera options menu and set as closed..
        cameraOptionsMenu = event.target_element.owner_document
        cameraOptionsMenu.close()
        # return to main menu.
        elyGUIMgr.get_main_menu().show()

# /<DEFAULT CAMERA CONTROL>
# /ENABLING
#     WindowFramework* gameWindow =
#     GameManager.get_global_ptr().windowFramework()
#     # reset trackball transform
#     LMatrix4 cameraMat = gameWindow.get_camera_group().get_transform().get_mat()
#     cameraMat.invert_in_place()
#     SMARTPTR(Trackball)trackBall =
#     DCAST(Trackball, gameWindow.get_mouse().find('**/+Trackball').node())
#     trackBall.set_mat(cameraMat)
#     # (re)enable trackball
#     GameManager.get_global_ptr().enable_mouse()
# /DISABLING
#     # disable the trackball
#     GameManager.get_global_ptr().disable_mouse()
# /</DEFAULT CAMERA CONTROL>

 
# helpers
def _setCameraType(newType, actualType):
    global _camera, _textNode
    pandaFramework = GameManager.get_global_ptr()
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    elyPhysicsMgr = GamePhysicsManager.get_global_ptr()._elyMgr
    # return if no camera type change is needed
    if newType == actualType:
        return
    # unset actual camera type
    if actualType == _CameraType.free_view_camera:
        cameraControl = _camera.get_component(ComponentFamilyType('Control'))
        if cameraControl.p3Driver.node().is_enabled:
            # enabled: then disable it
            # disable
            if not cameraControl.p3Driver.node().disable():
                return
            # remove text
            pandaFramework.render2d.find('CommonTextNode').remove_node()
    elif actualType == _CameraType.chaser_camera:
        cameraControl = _camera.get_component(ComponentFamilyType('Control'))
        if cameraControl.p3Chaser.node().is_enabled:
            # enabled: then disable it
            # disable
            if not cameraControl.p3Chaser.node().disable():
                return
            # remove text
            pandaFramework.render2d.find('CommonTextNode').remove_node()
    elif actualType == _CameraType.object_picker:
        if elyPhysicsMgr.is_picking_enabled:
            cameraControl = _camera.get_component(ComponentType('Driver'))
            if cameraControl and cameraControl.p3Driver.node().is_enabled:
                # enabled: then disable it
                # disable
                if not cameraControl.p3Driver.node().disable():
                    return
            # picker on: remove
            elyPhysicsMgr.disable_picking()
            # remove text
            pandaFramework.render2d.find('CommonTextNode').remove_node()
    # set new type
    if newType == _CameraType.free_view_camera:
        # add a new Driver component to camera and ...
        if not objTmplMgr.add_component_to_object(
            _camera.object_id, ComponentType('Driver'), _cameraDriverParams):
            return
        # ... enable it
        if not _camera.get_component(ComponentFamilyType('Control')).p3Driver.node().enable():
            return
        # write text
        _textNode = createText('Free View Camera', 0.05,
                              LVecBase4f(1.0, 1.0, 0.0, 1.0),
                              LVector3f(-1.0, 0, -0.9), pandaFramework)   
    elif newType == _CameraType.chaser_camera:
        # add a new Chaser component to camera and ...
        if not objTmplMgr.add_component_to_object(
            _camera.object_id, ComponentType('Chaser'), _cameraChaserParams):
            return
        # ... enable it
        if not _camera.get_component(ComponentFamilyType('Control')).p3Chaser.node().enable():
            return
        # write text
        chasedObjectId = _camera.get_component(ComponentFamilyType('Control'))._chasedId
        _textNode = createText('Camera Chasing \'' + str(chasedObjectId) + '\'',
                0.05, LVecBase4f(1.0, 1.0, 0.0, 1.0), LVector3f(-1.0, 0, -0.9),
                pandaFramework)
    elif newType == _CameraType.object_picker:
        if not elyPhysicsMgr.is_picking_enabled:
            # add a new Driver component to camera without mouse movement control and ...
            cameraDriverParamsNoMouse = ParameterTable(_cameraDriverParams)
            cameraDriverParamsNoMouse['mouse_head'] = StringSequence(['disabled'])
            cameraDriverParamsNoMouse['mouse_pitch'] = StringSequence(['disabled'])
            cameraDriverParamsNoMouse['mouse_move'] = StringSequence(['disabled'])
            if not objTmplMgr.add_component_to_object(
                _camera.object_id, ComponentType('Driver'), cameraDriverParamsNoMouse):
                return
            # ... enable it
            if not _camera.get_component(ComponentFamilyType('Control')).p3Driver.node().enable():
                return
            # picker off: add
            elyPhysicsMgr.enable_picking(pandaFramework.render,
                                         NodePath.any_path(pandaFramework.camNode),
                                         pandaFramework.mouseWatcher.node(),
                                         'p', 'p-up')
            # write text
            _textNode = createText('Object Picker Active', 0.05,
                    LVecBase4f(1.0, 1.0, 0.0, 1.0), LVector3f(-1.0, 0, -0.9),
                    pandaFramework)
   

def _rmluiPreset():
    '''preset function called from main menu'''

    global _cameraType
    _setCameraType(_CameraType.none, _cameraType)
 
 
def _rmluiCommit():
    '''commit function called main menu'''

    global _cameraType
    _setCameraType(_cameraType, _CameraType.none)


def _showCameraTrans(textNode, cameraNP, render, task):
    x, y, z = cameraNP.get_pos(render)
    h, p, r = cameraNP.get_hpr(render)
    textNode.set_text('pos: ({:+07.1f},{:+07.1f},{:+07.1f})\nrot: ({:+07.1f},{:+07.1f},{:+07.1f})'.format(x, y, z, h, p, r))
    return task.cont


def camera_initialization(objectI, paramTable, pandaFramework, windowFramework):
    global _camera, _cameraDriverParams, _cameraChaserParams
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
    # set camera
    _camera = objectI
    # get Driver and Chaser component template parameters
    _cameraDriverParams = objTmplMgr.get_created_object(
        ObjectId('cameraDriverTmp')).stored_comp_tmpl_params['Driver']
    _cameraChaserParams = objTmplMgr.get_created_object(
        ObjectId('cameraChaserTmp')).stored_comp_tmpl_params['Chaser']
    # destroy no more needed cameraDriver and cameraChaser objects
    objTmplMgr.destroy_object(ObjectId('cameraDriverTmp'))
    objTmplMgr.destroy_object(ObjectId('cameraChaserTmp'))
    
    # register the add element function to (RmlUi) main menu
    elyGUIMgr.register_gui_add_elements_function(_rmluiAddElements)
    # register the event handler to main menu for each event value
    elyGUIMgr.register_gui_event_handler('camera::options', _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('camera::body::load_logo', _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('camera::form::submit_options', _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('camera::free_view_camera::options', _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('camera::chaser_camera::options', _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('camera::object_picker::options', _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('pitch_limit::change', _rmluiEventHandler)
    # register the preset function to main menu
    elyGUIMgr.register_gui_preset_function(_rmluiPreset)
    # register the commit function to main menu
    elyGUIMgr.register_gui_commit_function(_rmluiCommit)
    # add a task to update kinematic body from the driver
    cameraTransText = createText('', 0.04, LVecBase4f(0.2, 0.0, 0.7, 1.0),
                                LVector3f(0.40, 0, -0.9), pandaFramework)   
    pandaFramework.task_mgr.add(_showCameraTrans, '_showCameraTrans',
                                appendTask=True,
                                extraArgs=[cameraTransText.node(),
                                           _camera.node_path,
                                           pandaFramework.render])


# #
initializations = {
    'camera_initialization': camera_initialization,
}
