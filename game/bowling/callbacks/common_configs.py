'''
Created on Oct 22, 2019

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# game_preconfiguration_clbk
spec = util.spec_from_file_location('game_preconfiguration_clbk',
            os.path.join(_this_library_dir, 'defs', 'game_preconfiguration_clbk.py'))
game_preconfiguration_clbk = util.module_from_spec(spec)
spec.loader.exec_module(game_preconfiguration_clbk)

# camera_clbk
spec = util.spec_from_file_location('camera_clbk',
            os.path.join(_this_library_dir, 'defs', 'camera_clbk.py'))
camera_clbk = util.module_from_spec(spec)
spec.loader.exec_module(camera_clbk)

# player_clbk
spec = util.spec_from_file_location('player_clbk',
            os.path.join(_this_library_dir, 'defs', 'player_clbk.py'))
player_clbk = util.module_from_spec(spec)
spec.loader.exec_module(player_clbk)

# callbacks definitions
callbacks = {
    'default_callback__':
                    lambda event, *args: print('Got ', event,
                        ' handled by default_callback__() for ', *args, sep=''),
    **game_preconfiguration_clbk.callbacks,
    **camera_clbk.callbacks,
    **player_clbk.callbacks,
}
