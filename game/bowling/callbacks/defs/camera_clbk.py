'''
Created on Mar 6, 2020

@author: consultit
'''

# #Camera + Driver related CALLBACKs
# fast:stop-fast
_isFast = False


def _update_fast_camera(cameraDrv, factor):
    global _isFast
    cameraDrv.max_linear_speed = cameraDrv.max_linear_speed * factor
    cameraDrv.max_angular_speed = cameraDrv.max_angular_speed * factor
    cameraDrv.linear_accel = cameraDrv.linear_accel * factor
    cameraDrv.angular_accel = cameraDrv.angular_accel * factor
    sens = cameraDrv.sens
    cameraDrv.sens = [sens[0] * factor, sens[1] * factor]
    _isFast = not _isFast
    

def fast_Driver_Camera(event, driver):
    if not _isFast:
        speedFactor = driver.p3Driver.node().fast_factor
        _update_fast_camera(driver.p3Driver.node(), speedFactor)
    

def stop_fast_Driver_Camera(event, driver):
    if _isFast:
        speedFactorInv = 1.0 / driver.p3Driver.node().fast_factor
        _update_fast_camera(driver.p3Driver.node(), speedFactorInv)
   

# forward:stop-forward:fast-forward
def forward_Driver_Camera(event, driver):
    driver.p3Driver.node().move_forward = True


def stop_forward_Driver_Camera(event, driver):
    driver.p3Driver.node().move_forward = False


def fast_forward_Driver_Camera(event, driver):
    fast_Driver_Camera(event, driver)
    forward_Driver_Camera(event, driver)


# backward:stop-backward:fast-backward
def backward_Driver_Camera(event, driver):
    driver.p3Driver.node().move_backward = True


def stop_backward_Driver_Camera(event, driver):
    driver.p3Driver.node().move_backward = False


def fast_backward_Driver_Camera(event, driver):
    fast_Driver_Camera(event, driver)
    backward_Driver_Camera(event, driver)


# strafe_left:stop-strafe_left:fast-strafe_left
def strafe_left_Driver_Camera(event, driver):
    driver.p3Driver.node().move_strafe_left = True


def stop_strafe_left_Driver_Camera(event, driver):
    driver.p3Driver.node().move_strafe_left = False


def fast_strafe_left_Driver_Camera(event, driver):
    fast_Driver_Camera(event, driver)
    strafe_left_Driver_Camera(event, driver)


# strafe_right:stop-strafe_right:fast-strafe_right
def strafe_right_Driver_Camera(event, driver):
    driver.p3Driver.node().move_strafe_right = True


def stop_strafe_right_Driver_Camera(event, driver):
    driver.p3Driver.node().move_strafe_right = False


def fast_strafe_right_Driver_Camera(event, driver):
    fast_Driver_Camera(event, driver)
    strafe_right_Driver_Camera(event, driver)


# head_left:stop-head_left:fast-head_left
def head_left_Driver_Camera(event, driver):
    driver.p3Driver.node().rotate_head_left = True


def stop_head_left_Driver_Camera(event, driver):
    driver.p3Driver.node().rotate_head_left = False


def fast_head_left_Driver_Camera(event, driver):
    fast_Driver_Camera(event, driver)
    head_left_Driver_Camera(event, driver)


# head_right:stop-head_right:fast-head_right
def head_right_Driver_Camera(event, driver):
    driver.p3Driver.node().rotate_head_right = True


def stop_head_right_Driver_Camera(event, driver):
    driver.p3Driver.node().rotate_head_right = False


def fast_head_right_Driver_Camera(event, driver):
    fast_Driver_Camera(event, driver)
    head_right_Driver_Camera(event, driver)


# pitch_up:stop-pitch_up:fast-pitch_up
def pitch_up_Driver_Camera(event, driver):
    driver.p3Driver.node().rotate_pitch_up = True


def stop_pitch_up_Driver_Camera(event, driver):
    driver.p3Driver.node().rotate_pitch_up = False


def fast_pitch_up_Driver_Camera(event, driver):
    fast_Driver_Camera(event, driver)
    pitch_up_Driver_Camera(event, driver)


# pitch_down:stop-pitch_down:fast-pitch_down
def pitch_down_Driver_Camera(event, driver):
    driver.p3Driver.node().rotate_pitch_down = True


def stop_pitch_down_Driver_Camera(event, driver):
    driver.p3Driver.node().rotate_pitch_down = False


def fast_pitch_down_Driver_Camera(event, driver):
    fast_Driver_Camera(event, driver)
    pitch_down_Driver_Camera(event, driver)


# up:stop-up:fast-up
def up_Driver_Camera(event, driver):
    driver.p3Driver.node().move_up = True


def stop_up_Driver_Camera(event, driver):
    driver.p3Driver.node().move_up = False


def fast_up_Driver_Camera(event, driver):
    fast_Driver_Camera(event, driver)
    up_Driver_Camera(event, driver)


# down:stop-down:fast-down
def down_Driver_Camera(event, driver):
    driver.p3Driver.node().move_down = True


def stop_down_Driver_Camera(event, driver):
    driver.p3Driver.node().move_down = False


def fast_down_Driver_Camera(event, driver):
    fast_Driver_Camera(event, driver)
    down_Driver_Camera(event, driver)


# #Camera + Chaser related
def head_left_Chaser_Camera(event, chaser):
    chaser.p3Chaser.node().rotate_head_left = True


def stop_head_left_Chaser_Camera(event, chaser):
    chaser.p3Chaser.node().rotate_head_left = False


def head_right_Chaser_Camera(event, chaser):
    chaser.p3Chaser.node().rotate_head_right = True


def stop_head_right_Chaser_Camera(event, chaser):
    chaser.p3Chaser.node().rotate_head_right = False


def pitch_up_Chaser_Camera(event, chaser):
    chaser.p3Chaser.node().rotate_pitch_up = True


def stop_pitch_up_Chaser_Camera(event, chaser):
    chaser.p3Chaser.node().rotate_pitch_up = False


def pitch_down_Chaser_Camera(event, chaser):
    chaser.p3Chaser.node().rotate_pitch_down = True


def stop_pitch_down_Chaser_Camera(event, chaser):
    chaser.p3Chaser.node().rotate_pitch_down = False


def hold_lookat_Chaser_Camera(event, chaser):
    chaser.p3Chaser.node().hold_look_at = True


def stop_hold_lookat_Chaser_Camera(event, chaser):
    chaser.p3Chaser.node().hold_look_at = False


# #
callbacks = {
    'fast_Driver_Camera':fast_Driver_Camera,
    'stop_fast_Driver_Camera':stop_fast_Driver_Camera,
    'forward_Driver_Camera':forward_Driver_Camera,
    'stop_forward_Driver_Camera':stop_forward_Driver_Camera,
    'fast_forward_Driver_Camera':fast_forward_Driver_Camera,
    'backward_Driver_Camera':backward_Driver_Camera,
    'stop_backward_Driver_Camera':stop_backward_Driver_Camera,
    'fast_backward_Driver_Camera':fast_backward_Driver_Camera,
    'strafe_left_Driver_Camera':strafe_left_Driver_Camera,
    'stop_strafe_left_Driver_Camera':stop_strafe_left_Driver_Camera,
    'fast_strafe_left_Driver_Camera':fast_strafe_left_Driver_Camera,
    'strafe_right_Driver_Camera':strafe_right_Driver_Camera,
    'stop_strafe_right_Driver_Camera':stop_strafe_right_Driver_Camera,
    'fast_strafe_right_Driver_Camera':fast_strafe_right_Driver_Camera,
    'head_left_Driver_Camera':head_left_Driver_Camera,
    'stop_head_left_Driver_Camera':stop_head_left_Driver_Camera,
    'fast_head_left_Driver_Camera':fast_head_left_Driver_Camera,
    'head_right_Driver_Camera':head_right_Driver_Camera,
    'stop_head_right_Driver_Camera':stop_head_right_Driver_Camera,
    'fast_head_right_Driver_Camera':fast_head_right_Driver_Camera,
    'pitch_up_Driver_Camera':pitch_up_Driver_Camera,
    'stop_pitch_up_Driver_Camera':stop_pitch_up_Driver_Camera,
    'fast_pitch_up_Driver_Camera':fast_pitch_up_Driver_Camera,
    'pitch_down_Driver_Camera':pitch_down_Driver_Camera,
    'stop_pitch_down_Driver_Camera':stop_pitch_down_Driver_Camera,
    'fast_pitch_down_Driver_Camera':fast_pitch_down_Driver_Camera,
    'up_Driver_Camera':up_Driver_Camera,
    'stop_up_Driver_Camera':stop_up_Driver_Camera,
    'fast_up_Driver_Camera':fast_up_Driver_Camera,
    'down_Driver_Camera':down_Driver_Camera,
    'stop_down_Driver_Camera':stop_down_Driver_Camera,
    'fast_down_Driver_Camera':fast_down_Driver_Camera,
    'head_left_Chaser_Camera':head_left_Chaser_Camera,
    'stop_head_left_Chaser_Camera':stop_head_left_Chaser_Camera,
    'head_right_Chaser_Camera':head_right_Chaser_Camera,
    'stop_head_right_Chaser_Camera':stop_head_right_Chaser_Camera,
    'pitch_up_Chaser_Camera':pitch_up_Chaser_Camera,
    'stop_pitch_up_Chaser_Camera':stop_pitch_up_Chaser_Camera,
    'pitch_down_Chaser_Camera':pitch_down_Chaser_Camera,
    'stop_pitch_down_Chaser_Camera':stop_pitch_down_Chaser_Camera,
    'hold_lookat_Chaser_Camera':hold_lookat_Chaser_Camera,
    'stop_hold_lookat_Chaser_Camera':stop_hold_lookat_Chaser_Camera,
}
