'''
Created on Mar 7, 2020

@author: consultit
'''

from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectId
from COM.managers.gameManager import GameManager
from COM.managers.gamePhysicsManager import GamePhysicsManager
from panda3d.core import NodePath
from COM.tools.utilities import COMLogger

_physicsDebug = False


def togglePhysicsDebug(event, gameConfig):
    '''Physics: debug settings'''
    global _physicsDebug
    pandaFramework = GameManager.get_global_ptr()
    elyPhysicsMgr = GamePhysicsManager.get_global_ptr()._elyMgr
    if _physicsDebug:
        elyPhysicsMgr.get_global_ptr().debug(False)
        elyPhysicsMgr.reference_node_path_debug.reparent_to(NodePath())
    else:
        elyPhysicsMgr.reference_node_path_debug.reparent_to(pandaFramework.render)
        elyPhysicsMgr.get_global_ptr().debug(True)
    _physicsDebug = not _physicsDebug


# # Notify Collisions related
def notifyCollisions(event, gameConfig, p3PhysicComp0, p3PhysicComp1):
    '''notifies a collision between two objects (with physical components)'''
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    objId0 = ObjectId(p3PhysicComp0.get_tag('_owner_object'))
    objId1 = ObjectId(p3PhysicComp1.get_tag('_owner_object'))
    object0 = objTmplMgr.get_created_object(objId0)
    object1 = objTmplMgr.get_created_object(objId1)
    COMLogger.info('got ' + event + ' between ' + str(object0) + ' (' + 
              str(p3PhysicComp0) + ') and ' + str(object1) + ' (' + 
              str(p3PhysicComp1) + ') handled by ' + str(gameConfig))


# #
callbacks = {
    'notifyCollisions': notifyCollisions,
    'togglePhysicsDebug': togglePhysicsDebug,
}   
