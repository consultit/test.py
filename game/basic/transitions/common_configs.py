'''
Created on Oct 22, 2019

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# Actor_trns
spec = util.spec_from_file_location('Actor_trns',
            os.path.join(_this_library_dir, 'defs', 'Actor_trns.py'))
Actor_trns = util.module_from_spec(spec)
spec.loader.exec_module(Actor_trns)

# Character_trns
spec = util.spec_from_file_location('Character_trns',
            os.path.join(_this_library_dir, 'defs', 'Character_trns.py'))
Character_trns = util.module_from_spec(spec)
spec.loader.exec_module(Character_trns)

# testCharacter_trns
spec = util.spec_from_file_location('testCharacter_trns',
            os.path.join(_this_library_dir, 'defs', 'testCharacter_trns.py'))
testCharacter_trns = util.module_from_spec(spec)
spec.loader.exec_module(testCharacter_trns)

# transitions definitions
transitions = {
    **Actor_trns.transitions,
    **Character_trns.transitions,
    **testCharacter_trns.transitions,
}
