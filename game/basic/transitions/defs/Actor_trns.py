'''
Created on Oct 26, 2019

@author: consultit
'''

# raise if Ely not available
import panda3d.core
import ely.libtools

from COM.tools.utilities import COMLogger

# ENTER Enter_<STATE>_<OBJECTTYPE>
# EXIT Exit_<STATE>_<OBJECTTYPE>
# FILTER Filter_<STATE>_<OBJECTTYPE>
# FROMTO <STATEA>_FromTo_<STATEB>_<OBJECTTYPE>


# # Actor (Activity) related
# # States' functions
# Forward
def Enter_Forward_Actor(self, activity):
    COMLogger.info('Enter_Forward_Actor' + str(activity))


def Exit_Forward_Actor(self):
    COMLogger.info('Exit_Forward_Actor')


def Filter_Forward_Actor(self, request, args):
    activity = args[0]
    COMLogger.info('Filter_Forward_Actor' + request + str(activity) +
                   activity.fsm.state)
    # passthrough filter
    return (request,) + args


# Backward
def Enter_Backward_Actor(self, activity):
    COMLogger.info('Enter_Backward_Actor' + str(activity))


def Exit_Backward_Actor(self):
    COMLogger.info('Exit_Backward_Actor')


def Filter_Backward_Actor(self, request, args):
    activity = args[0]
    COMLogger.info('Filter_Backward_Actor' + request + str(activity) +
                   activity.fsm.state)
    # passthrough filter
    return (request,) + args


# Head_left
def Enter_Head_left_Actor(self, activity):
    COMLogger.info('Enter_Head_left_Actor' + str(activity))


def Exit_Head_left_Actor(self):
    COMLogger.info('Exit_Head_left_Actor')


def Filter_Head_left_Actor(self, request, args):
    activity = args[0]
    COMLogger.info('Filter_Head_left_Actor' + request + str(activity) +
                   activity.fsm.state)
    # passthrough filter
    return (request,) + args


# Head_right
def Enter_Head_right_Actor(self, activity):
    COMLogger.info('Enter_Head_right_Actor' + str(activity))


def Exit_Head_right_Actor(self):
    COMLogger.info('Exit_Head_right_Actor')


def Filter_Head_right_Actor(self, request, args):
    activity = args[0]
    COMLogger.info('Filter_Head_right_Actor' + request + str(activity) +
                   activity.fsm.state)
    # passthrough filter
    return (request,) + args


# Up
def Enter_Up_Actor(self, activity):
    COMLogger.info('Enter_Up_Actor' + str(activity))


def Exit_Up_Actor(self):
    COMLogger.info('Exit_Up_Actor')


def Filter_Up_Actor(self, request, args):
    activity = args[0]
    COMLogger.info('Filter_Up_Actor' + request + str(activity) +
                   activity.fsm.state)
    # passthrough filter
    return (request,) + args


# Down
def Enter_Down_Actor(self, activity):
    COMLogger.info('Enter_Down_Actor' + str(activity))


def Exit_Down_Actor(self):
    COMLogger.info('Exit_Down_Actor')


def Filter_Down_Actor(self, request, args):
    activity = args[0]
    COMLogger.info('Filter_Down_Actor' + request + str(activity) +
                   activity.fsm.state)
    # passthrough filter
    return (request,) + args


# Forward_FromTo_Head_left
def Forward_FromTo_Head_left_Actor(self, activity):
    COMLogger.info('Forward_FromTo_Head_left_Actor' + str(activity))


# Forward_FromTo_Head_right
def Forward_FromTo_Head_right_Actor(self, activity):
    COMLogger.info('Forward_FromTo_Head_right_Actor' + str(activity))


# #
transitions = {
    'Enter_Forward_Actor': Enter_Forward_Actor,
    'Exit_Forward_Actor': Exit_Forward_Actor,
    'Filter_Forward_Actor': Filter_Forward_Actor,
    'Enter_Backward_Actor': Enter_Backward_Actor,
    'Exit_Backward_Actor': Exit_Backward_Actor,
    'Filter_Backward_Actor': Filter_Backward_Actor,
    'Enter_Head_left_Actor': Enter_Head_left_Actor,
    'Exit_Head_left_Actor': Exit_Head_left_Actor,
    'Filter_Head_left_Actor': Filter_Head_left_Actor,
    'Enter_Head_right_Actor': Enter_Head_right_Actor,
    'Exit_Head_right_Actor': Exit_Head_right_Actor,
    'Filter_Head_right_Actor': Filter_Head_right_Actor,
    'Enter_Up_Actor': Enter_Up_Actor,
    'Exit_Up_Actor': Exit_Up_Actor,
    'Filter_Up_Actor': Filter_Up_Actor,
    'Enter_Down_Actor': Enter_Down_Actor,
    'Exit_Down_Actor': Exit_Down_Actor,
    'Filter_Down_Actor': Filter_Down_Actor,
    'Forward_FromTo_Head_left_Actor': Forward_FromTo_Head_left_Actor,
    'Forward_FromTo_Head_right_Actor': Forward_FromTo_Head_right_Actor,
}
