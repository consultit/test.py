'''
Created on Oct 28, 2019

@author: consultit
'''

from COM.com.component import ComponentFamilyType
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.tools.utilities import COMLogger

# 'Enter_<STATE>_<OBJECTTYPE>':Enter_<STATE>_<OBJECTTYPE>,
# 'Exit_<STATE>_<OBJECTTYPE>':Exit_<STATE>_<OBJECTTYPE>,
# FILTER Filter_<STATE>_<OBJECTTYPE>
# FROMTO <STATEA>_FromTo_<STATEB>_<OBJECTTYPE>


# # Character (Activity) related
# # States' functions
# I
def Enter_I_Character(self, activity):
    COMLogger.info('Enter_I_Character')


def Exit_I_Character(self):
    COMLogger.info('Exit_I_Character')


# F
def Enter_F_Character(self, activity):
    COMLogger.info('Enter_F_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.loop('walk', False)
    npc1P3CharCtrl.node().move_forward = True


def Exit_F_Character(self):
    COMLogger.info('Exit_F_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('walk')
    npc1P3CharCtrl.node().move_forward = False


# B
def Enter_B_Character(self, activity):
    COMLogger.info('Enter_B_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.loop('walk', False)
    npc1P3CharCtrl.node().move_backward = True


def Exit_B_Character(self):
    COMLogger.info('Exit_B_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('walk')
    npc1P3CharCtrl.node().move_backward = False


# Sr
def Enter_Sr_Character(self, activity):
    COMLogger.info('Enter_Sr_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_right = True


def Exit_Sr_Character(self):
    COMLogger.info('Exit_Sr_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_right = False


# Sl
def Enter_Sl_Character(self, activity):
    COMLogger.info('Enter_Sl_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_left = True


def Exit_Sl_Character(self):
    COMLogger.info('Exit_Sl_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_left = False


# Rr
def Enter_Rr_Character(self, activity):
    COMLogger.info('Enter_Rr_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_Rr_Character(self):
    COMLogger.info('Exit_Rr_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().rotate_head_right = False


# Rl
def Enter_Rl_Character(self, activity):
    COMLogger.info('Enter_Rl_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_Rl_Character(self):
    COMLogger.info('Exit_Rl_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().rotate_head_left = False


# J
def Enter_J_Character(self, activity):
    COMLogger.info('Enter_J_Character')
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().jump = True


def Exit_J_Character(self):
    COMLogger.info('Exit_J_Character')
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().jump = False


# F-Rr
def Enter_F_Rr_Character(self, activity):
    COMLogger.info('Enter_F_Rr_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.loop('walk', False)
    npc1P3CharCtrl.node().move_forward = True
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_F_Rr_Character(self):
    COMLogger.info('Exit_F_Rr_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('walk')
    npc1P3CharCtrl.node().move_forward = False
    npc1P3CharCtrl.node().rotate_head_right = False


# F-Rl
def Enter_F_Rl_Character(self, activity):
    COMLogger.info('Enter_F_Rl_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.loop('walk', False)
    npc1P3CharCtrl.node().move_forward = True
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_F_Rl_Character(self):
    COMLogger.info('Exit_F_Rl_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('walk')
    npc1P3CharCtrl.node().move_forward = False
    npc1P3CharCtrl.node().rotate_head_left = False


# F-Rr-Rl
def Enter_F_Rr_Rl_Character(self, activity):
    COMLogger.info('Enter_F_Rr_Rl_Character')


    # 
def Exit_F_Rr_Rl_Character(self):
    COMLogger.info('Exit_F_Rr_Rl_Character')


    # 
# Rr-Rl
def Enter_Rr_Rl_Character(self, activity):
    COMLogger.info('Enter_Rr_Rl_Character')


    # 
def Exit_Rr_Rl_Character(self):
    COMLogger.info('Exit_Rr_Rl_Character')


    # 
# F-J
def Enter_F_J_Character(self, activity):
    COMLogger.info('Enter_F_J_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    # enable animation blending
    npc1Model.part_bundle.set_anim_blend_flag(True)
    npc1Model.part_bundle.set_control_effect(
            npc1Model.animations.find_anim('walk'), 0.5)
    npc1Model.part_bundle.set_control_effect(
            npc1Model.animations.find_anim('jump'), 0.5)
    npc1Model.animations.loop('walk', False)
    npc1Model.animations.loop('jump', False)
    npc1P3CharCtrl.node().move_forward = True
    npc1P3CharCtrl.node().jump = True


def Exit_F_J_Character(self):
    COMLogger.info('Exit_F_J_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('jump')
    npc1Model.animations.stop('walk')
    # disable animation blending
    npc1Model.part_bundle.set_anim_blend_flag(False)
    npc1P3CharCtrl.node().move_forward = False
    npc1P3CharCtrl.node().jump = False


# B-Rr
def Enter_B_Rr_Character(self, activity):
    COMLogger.info('Enter_B_Rr_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.loop('walk', False)
    npc1P3CharCtrl.node().move_backward = True
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_B_Rr_Character(self):
    COMLogger.info('Exit_B_Rr_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('walk')
    npc1P3CharCtrl.node().move_backward = False
    npc1P3CharCtrl.node().rotate_head_right = False


# B-Rl
def Enter_B_Rl_Character(self, activity):
    COMLogger.info('Enter_B_Rl_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.loop('walk', False)
    npc1P3CharCtrl.node().move_backward = True
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_B_Rl_Character(self):
    COMLogger.info('Exit_B_Rl_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('walk')
    npc1P3CharCtrl.node().move_backward = False
    npc1P3CharCtrl.node().rotate_head_left = False


# Sr-Rr
def Enter_Sr_Rr_Character(self, activity):
    COMLogger.info('Enter_Sr_Rr_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_right = True
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_Sr_Rr_Character(self):
    COMLogger.info('Exit_Sr_Rr_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_right = False
    npc1P3CharCtrl.node().rotate_head_right = False


# Sr-Rl
def Enter_Sr_Rl_Character(self, activity):
    COMLogger.info('Enter_Sr_Rl_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_right = True
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_Sr_Rl_Character(self):
    COMLogger.info('Exit_Sr_Rl_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_right = False
    npc1P3CharCtrl.node().rotate_head_left = False


# Sl-Rr
def Enter_Sl_Rr_Character(self, activity):
    COMLogger.info('Enter_Sl_Rr_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_left = True
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_Sl_Rr_Character(self):
    COMLogger.info('Exit_Sl_Rr_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_left = False
    npc1P3CharCtrl.node().rotate_head_right = False


# Sl-Rl
def Enter_Sl_Rl_Character(self, activity):
    COMLogger.info('Enter_Sl_Rl_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_left = True
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_Sl_Rl_Character(self):
    COMLogger.info('Exit_Sl_Rl_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_left = False
    npc1P3CharCtrl.node().rotate_head_left = False

    
_linearSpeedFactor = 3.0
_angularSpeedFactor = 3.0


# F-Q
def Enter_F_Q_Character(self, activity):
    COMLogger.info('Enter_F_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.loop('run', False)
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed * _linearSpeedFactor
    npc1P3CharCtrl.node().move_forward = True


def Exit_F_Q_Character(self):
    COMLogger.info('Exit_F_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('run')
    npc1P3CharCtrl.node().move_forward = False
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed / _linearSpeedFactor


# Sr-Q
def Enter_Sr_Q_Character(self, activity):
    COMLogger.info('Enter_Sr_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed * _linearSpeedFactor
    npc1P3CharCtrl.node().move_strafe_right = True


def Exit_Sr_Q_Character(self):
    COMLogger.info('Exit_Sr_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_right = False
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed / _linearSpeedFactor


# Sl-Q
def Enter_Sl_Q_Character(self, activity):
    COMLogger.info('Enter_Sl_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed * _linearSpeedFactor
    npc1P3CharCtrl.node().move_strafe_left = True


def Exit_Sl_Q_Character(self):
    COMLogger.info('Exit_Sl_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().move_strafe_left = False
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed / _linearSpeedFactor


# Rr-Q
def Enter_Rr_Q_Character(self, activity):
    COMLogger.info('Enter_Rr_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed * _angularSpeedFactor
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_Rr_Q_Character(self):
    COMLogger.info('Exit_Rr_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().rotate_head_right = False
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed / _angularSpeedFactor


# Rl-Q
def Enter_Rl_Q_Character(self, activity):
    COMLogger.info('Enter_Rl_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed * _angularSpeedFactor
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_Rl_Q_Character(self):
    COMLogger.info('Exit_Rl_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().rotate_head_left = False
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed / _angularSpeedFactor


# F-Rr-Q
def Enter_F_Rr_Q_Character(self, activity):
    COMLogger.info('Enter_F_Rr_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.loop('run', False)
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed * _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed * _angularSpeedFactor
    npc1P3CharCtrl.node().move_forward = True
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_F_Rr_Q_Character(self):
    COMLogger.info('Exit_F_Rr_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('run')
    npc1P3CharCtrl.node().move_forward = False
    npc1P3CharCtrl.node().rotate_head_right = False
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed / _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed / _angularSpeedFactor


# F-Rl-Q
def Enter_F_Rl_Q_Character(self, activity):
    COMLogger.info('Enter_F_Rl_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.loop('run', False)
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed * _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed * _angularSpeedFactor
    npc1P3CharCtrl.node().move_forward = True
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_F_Rl_Q_Character(self):
    COMLogger.info('Exit_F_Rl_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('run')
    npc1P3CharCtrl.node().move_forward = False
    npc1P3CharCtrl.node().rotate_head_left = False
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed / _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed / _angularSpeedFactor


# F-Rr-Rl-Q
def Enter_F_Rr_Rl_Q_Character(self, activity):
    COMLogger.info('Enter_F_Rr_Rl_Q_Character')


    # 
def Exit_F_Rr_Rl_Q_Character(self):
    COMLogger.info('Exit_F_Rr_Rl_Q_Character')


    # 
# Rr-Rl-Q
def Enter_Rr_Rl_Q_Character(self, activity):
    COMLogger.info('Enter_Rr_Rl_Q_Character')


    # 
def Exit_Rr_Rl_Q_Character(self):
    COMLogger.info('Exit_Rr_Rl_Q_Character')


    # 
# F-J-Q
def Enter_F_J_Q_Character(self, activity):
    COMLogger.info('Enter_F_J_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    # enable animation blending
    npc1Model.part_bundle.set_anim_blend_flag(True)
    npc1Model.part_bundle.set_control_effect(
            npc1Model.animations.find_anim('run'), 0.5)
    npc1Model.part_bundle.set_control_effect(
            npc1Model.animations.find_anim('jump'), 0.5)
    npc1Model.animations.loop('run', False)
    npc1Model.animations.loop('jump', False)
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed * _linearSpeedFactor
    npc1P3CharCtrl.node().move_forward = True
    npc1P3CharCtrl.node().jump = True


def Exit_F_J_Q_Character(self):
    COMLogger.info('Exit_F_J_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1Model.animations.stop('jump')
    npc1Model.animations.stop('run')
    # disable animation blending
    npc1Model.part_bundle.set_anim_blend_flag(False)
    npc1P3CharCtrl.node().move_forward = False
    npc1P3CharCtrl.node().jump = False
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed / _linearSpeedFactor


# Sr-Rr-Q
def Enter_Sr_Rr_Q_Character(self, activity):
    COMLogger.info('Enter_Sr_Rr_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed * _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed * _angularSpeedFactor
    npc1P3CharCtrl.node().move_strafe_right = True
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_Sr_Rr_Q_Character(self):
    COMLogger.info('Exit_Sr_Rr_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed / _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed / _angularSpeedFactor
    npc1P3CharCtrl.node().move_strafe_right = False
    npc1P3CharCtrl.node().rotate_head_right = False


# Sr-Rl-Q
def Enter_Sr_Rl_Q_Character(self, activity):
    COMLogger.info('Enter_Sr_Rl_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed * _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed * _angularSpeedFactor
    npc1P3CharCtrl.node().move_strafe_right = True
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_Sr_Rl_Q_Character(self):
    COMLogger.info('Exit_Sr_Rl_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed / _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed / _angularSpeedFactor
    npc1P3CharCtrl.node().move_strafe_right = False
    npc1P3CharCtrl.node().rotate_head_left = False


# Sl-Rr-Q
def Enter_Sl_Rr_Q_Character(self, activity):
    COMLogger.info('Enter_Sl_Rr_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed * _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed * _angularSpeedFactor
    npc1P3CharCtrl.node().move_strafe_left = True
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_Sl_Rr_Q_Character(self):
    COMLogger.info('Exit_Sl_Rr_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed / _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed / _angularSpeedFactor
    npc1P3CharCtrl.node().move_strafe_left = False
    npc1P3CharCtrl.node().rotate_head_right = False


# Sl-Rl-Q
def Enter_Sl_Rl_Q_Character(self, activity):
    COMLogger.info('Enter_Sl_Rl_Q_Character')
    # 
    npc1 = activity.owner_object
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed * _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed * _angularSpeedFactor
    npc1P3CharCtrl.node().move_strafe_left = True
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_Sl_Rl_Q_Character(self):
    COMLogger.info('Exit_Sl_Rl_Q_Character')
    # 
    npc1 = ObjectTemplateManager.get_global_ptr().get_created_object(self._owner_object)
    npc1P3CharCtrl = npc1.get_component(ComponentFamilyType('PhysicsControl')).p3CharacterController
    # 
    npc1P3CharCtrl.node().max_linear_speed = npc1P3CharCtrl.node().max_linear_speed / _linearSpeedFactor
    npc1P3CharCtrl.node().max_angular_speed = npc1P3CharCtrl.node().max_angular_speed / _angularSpeedFactor
    npc1P3CharCtrl.node().move_strafe_left = False
    npc1P3CharCtrl.node().rotate_head_left = False


# # 
transitions = {
    'Enter_I_Character':Enter_I_Character,
    'Exit_I_Character':Exit_I_Character,
    'Enter_F_Character':Enter_F_Character,
    'Exit_F_Character':Exit_F_Character,
    'Enter_B_Character':Enter_B_Character,
    'Exit_B_Character':Exit_B_Character,
    'Enter_Sr_Character':Enter_Sr_Character,
    'Exit_Sr_Character':Exit_Sr_Character,
    'Enter_Sl_Character':Enter_Sl_Character,
    'Exit_Sl_Character':Exit_Sl_Character,
    'Enter_Rr_Character':Enter_Rr_Character,
    'Exit_Rr_Character':Exit_Rr_Character,
    'Enter_Rl_Character':Enter_Rl_Character,
    'Exit_Rl_Character':Exit_Rl_Character,
    'Enter_J_Character':Enter_J_Character,
    'Exit_J_Character':Exit_J_Character,
    'Enter_F_Rr_Character':Enter_F_Rr_Character,
    'Exit_F_Rr_Character':Exit_F_Rr_Character,
    'Enter_F_Rl_Character':Enter_F_Rl_Character,
    'Exit_F_Rl_Character':Exit_F_Rl_Character,
    'Enter_F_Rr_Rl_Character':Enter_F_Rr_Rl_Character,
    'Exit_F_Rr_Rl_Character':Exit_F_Rr_Rl_Character,
    'Enter_Rr_Rl_Character':Enter_Rr_Rl_Character,
    'Exit_Rr_Rl_Character':Exit_Rr_Rl_Character,
    'Enter_F_J_Character':Enter_F_J_Character,
    'Exit_F_J_Character':Exit_F_J_Character,
    'Enter_B_Rr_Character':Enter_B_Rr_Character,
    'Exit_B_Rr_Character':Exit_B_Rr_Character,
    'Enter_B_Rl_Character':Enter_B_Rl_Character,
    'Exit_B_Rl_Character':Exit_B_Rl_Character,
    'Enter_Sr_Rr_Character':Enter_Sr_Rr_Character,
    'Exit_Sr_Rr_Character':Exit_Sr_Rr_Character,
    'Enter_Sr_Rl_Character':Enter_Sr_Rl_Character,
    'Exit_Sr_Rl_Character':Exit_Sr_Rl_Character,
    'Enter_Sl_Rr_Character':Enter_Sl_Rr_Character,
    'Exit_Sl_Rr_Character':Exit_Sl_Rr_Character,
    'Enter_Sl_Rl_Character':Enter_Sl_Rl_Character,
    'Exit_Sl_Rl_Character':Exit_Sl_Rl_Character,
    'Enter_F_Q_Character':Enter_F_Q_Character,
    'Exit_F_Q_Character':Exit_F_Q_Character,
    'Enter_Sr_Q_Character':Enter_Sr_Q_Character,
    'Exit_Sr_Q_Character':Exit_Sr_Q_Character,
    'Enter_Sl_Q_Character':Enter_Sl_Q_Character,
    'Exit_Sl_Q_Character':Exit_Sl_Q_Character,
    'Enter_Rr_Q_Character':Enter_Rr_Q_Character,
    'Exit_Rr_Q_Character':Exit_Rr_Q_Character,
    'Enter_Rl_Q_Character':Enter_Rl_Q_Character,
    'Exit_Rl_Q_Character':Exit_Rl_Q_Character,
    'Enter_F_Rr_Q_Character':Enter_F_Rr_Q_Character,
    'Exit_F_Rr_Q_Character':Exit_F_Rr_Q_Character,
    'Enter_F_Rl_Q_Character':Enter_F_Rl_Q_Character,
    'Exit_F_Rl_Q_Character':Exit_F_Rl_Q_Character,
    'Enter_F_Rr_Rl_Q_Character':Enter_F_Rr_Rl_Q_Character,
    'Exit_F_Rr_Rl_Q_Character':Exit_F_Rr_Rl_Q_Character,
    'Enter_Rr_Rl_Q_Character':Enter_Rr_Rl_Q_Character,
    'Exit_Rr_Rl_Q_Character':Exit_Rr_Rl_Q_Character,
    'Enter_F_J_Q_Character':Enter_F_J_Q_Character,
    'Exit_F_J_Q_Character':Exit_F_J_Q_Character,
    'Enter_Sr_Rr_Q_Character':Enter_Sr_Rr_Q_Character,
    'Exit_Sr_Rr_Q_Character':Exit_Sr_Rr_Q_Character,
    'Enter_Sr_Rl_Q_Character':Enter_Sr_Rl_Q_Character,
    'Exit_Sr_Rl_Q_Character':Exit_Sr_Rl_Q_Character,
    'Enter_Sl_Rr_Q_Character':Enter_Sl_Rr_Q_Character,
    'Exit_Sl_Rr_Q_Character':Exit_Sl_Rr_Q_Character,
    'Enter_Sl_Rl_Q_Character':Enter_Sl_Rl_Q_Character,
    'Exit_Sl_Rl_Q_Character':Exit_Sl_Rl_Q_Character,
}
