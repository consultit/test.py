'''
Created on Oct 28, 2019

@author: consultit
'''

from COM.com.component import ComponentFamilyType, ComponentType
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.tools.utilities import COMLogger
from COM.managers.gameManager import GameManager

objTmplMgr = ObjectTemplateManager.get_global_ptr()
pandaFramework = GameManager.get_global_ptr()

# Enter_<STATE>_<OBJECTTYPE>,
# Exit_<STATE>_<OBJECTTYPE>,
# FILTER Filter_<STATE>_<OBJECTTYPE>
# FROMTO <STATEA>_FromTo_<STATEB>_<OBJECTTYPE>

# The global behavior of the character can be expressed, at any time, by a
# collection of different types of behavior:
# - the movement
# - animation
# - the sound
# - etc ...
# Each of these behaviors can be described by a collection of states. This
# collection can, in turn, be further partitioned into several subsets of states
# which are mutually exclusive to each other. Each of these subsets corresponds
# to an FSM that describes the transitions between component states.
# To summarize, each behavior can be modeled by one or more concurrent FSMs.
# Note that dependencies may exist between the various finite state machines,
# for ex the fsmX can be in the x state iff the fsmY is in the y state.
# (*) "iff" stands for "if and only if".


# helper for retrieving MultiActivity component in Exit_* functions
def _multiActivity(SELF):
    #
    activityOwnerObj = objTmplMgr.get_created_object(
        SELF._owner_object)
    return activityOwnerObj.get_component(
        ComponentFamilyType('Behavior'))._multi_activity


# hack
from panda3d.core import ConfigVariableList, LVector3f
import sys
for dataDir in ConfigVariableList('model-path'):
    sys.path.append(dataDir)
from actor_data import animToFrameWR, animToFrameRW, playRateRatiosWR, \
    playRateRatiosRW, run_cycle_length, walk_cycle_length, walk_run_delta, \
    run_walk_delta, walkSpeed, runSpeed, angularSpeedFactor

################################
### Movement Concurrent FSMs ###
################################


# # fsm_FB States' functions
# FBS_idle
def Enter_FBS_idle_FSM_ForwardBackward(self, multiActivity):
    COMLogger.info('Enter_FBS_idle_FSM_ForwardBackward')
    #
    pandaFramework.messenger.send('idle')


def Exit_FBS_idle_FSM_ForwardBackward(self):
    COMLogger.info('Exit_FBS_idle_FSM_ForwardBackward')


# FBS_forward
def Enter_FBS_forward_FSM_ForwardBackward(self, multiActivity):
    COMLogger.info('Enter_FBS_forward_FSM_ForwardBackward')
    #
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().move_forward = True
    npc1P3CharCtrl.node().max_linear_speed = LVector3f(walkSpeed)
    #
    if (multiActivity.activities['fsm_SF'].fsm.getCurrentOrNextState() ==
            'SFS_slow'):
        pandaFramework.messenger.send('walk')
    else:
        pandaFramework.messenger.send('run')


def Exit_FBS_forward_FSM_ForwardBackward(self):
    COMLogger.info('Exit_FBS_forward_FSM_ForwardBackward')
    #
    npc1 = _multiActivity(self).owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().move_forward = False
    #
    pandaFramework.messenger.send('idle')


# FBS_backward
def Enter_FBS_backward_FSM_ForwardBackward(self, multiActivity):
    COMLogger.info('Enter_FBS_backward_FSM_ForwardBackward')
    #
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().move_backward = True
    #
    if (multiActivity.activities['fsm_SF'].fsm.getCurrentOrNextState() ==
            'SFS_slow'):
        pandaFramework.messenger.send('walk')
    else:
        pandaFramework.messenger.send('run')


def Exit_FBS_backward_FSM_ForwardBackward(self):
    COMLogger.info('Exit_FBS_backward_FSM_ForwardBackward')
    #
    npc1 = _multiActivity(self).owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().move_backward = False
    #
    pandaFramework.messenger.send('idle')


# # fsm_LR States' functions (Movement)
# LRS_idle
def Enter_LRS_idle_FSM_LeftRight(self, multiActivity):
    COMLogger.info('Enter_LRS_idle_FSM_LeftRight')


def Exit_LRS_idle_FSM_LeftRight(self):
    COMLogger.info('Exit_LRS_idle_FSM_LeftRight')


# LRS_left
def Enter_LRS_left_FSM_LeftRight(self, multiActivity):
    COMLogger.info('Enter_LRS_left_FSM_LeftRight')
    #
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().rotate_head_left = True


def Exit_LRS_left_FSM_LeftRight(self):
    COMLogger.info('Exit_LRS_left_FSM_LeftRight')
    #
    npc1 = _multiActivity(self).owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().rotate_head_left = False


# LRS_right
def Enter_LRS_right_FSM_LeftRight(self, multiActivity):
    COMLogger.info('Enter_LRS_right_FSM_LeftRight')
    #
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().rotate_head_right = True


def Exit_LRS_right_FSM_LeftRight(self):
    COMLogger.info('Exit_LRS_right_FSM_LeftRight')
    #
    npc1 = _multiActivity(self).owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().rotate_head_right = False


# # fsm_SF States' functions (Movement)
# from SFS_slow to SFS_fast
def SFS_slow_FromTo_SFS_fast_FSM_SlowFast(self, multiActivity):
    COMLogger.info('SFS_slow_FromTo_SFS_fast_FSM_SlowFast')
    #
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().max_linear_speed = LVector3f(runSpeed)
    npc1P3CharCtrl.node().max_angular_speed *= angularSpeedFactor
    #
    state = multiActivity.activities['fsm_FB'].fsm.getCurrentOrNextState()
    if (state == 'FBS_forward') or (state == 'FBS_backward'):
        pandaFramework.messenger.send('run')


# from SFS_fast to SFS_slow
def SFS_fast_FromTo_SFS_slow_FSM_SlowFast(self, multiActivity):
    COMLogger.info('SFS_fast_FromTo_SFS_slow_FSM_SlowFast')
    #
    npc1 = multiActivity.owner_object
    npc1P3CharCtrl = npc1.get_component(
        ComponentFamilyType('PhysicsControl')).p3CharacterController
    npc1P3CharCtrl.node().max_linear_speed = LVector3f(walkSpeed)
    npc1P3CharCtrl.node().max_angular_speed /= angularSpeedFactor
    #
    state = multiActivity.activities['fsm_FB'].fsm.getCurrentOrNextState()
    if (state == 'FBS_forward') or (state == 'FBS_backward'):
        pandaFramework.messenger.send('walk')

#################################
### Animation Concurrent FSMs ###
#################################


_walkRunDeltaSpeedRatioInv = 1.0 / (abs(runSpeed - walkSpeed))
_animToFrameMapWR = None
_animToFrameMapRW = None


def _lerpValueWR(p3CharCtrl):
    # lerpValue = (S - Swalk) / (Srun - Swalk)
    currSpeed = p3CharCtrl.node().linear_speed.length() + 0.01  # correction
    lerpValue = ((currSpeed - walkSpeed) * _walkRunDeltaSpeedRatioInv)
    return lerpValue


def _lerpValueRW(p3CharCtrl):
    # lerpValue = 1.0 - (S - Swalk) / (Srun - Swalk)
    currSpeed = p3CharCtrl.node().linear_speed.length() - 0.02  # correction
    lerpValue = 1.0 - ((currSpeed - walkSpeed) * _walkRunDeltaSpeedRatioInv)
    return lerpValue


# # fsm_Anim States' functions (Animation)
# ANS_idle


def Enter_ANS_idle_FSM_Animation(self, multiActivity):
    COMLogger.info('Enter_ANS_idle_FSM_Animation')
    #
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    if npc1Model.component_type == ComponentType('ActorModel'):
        npc1Model.actor.stop()
    else:
        npc1Model.animations.stop_all()


def Exit_ANS_idle_FSM_Animation(self):
    COMLogger.info('Exit_ANS_idle_FSM_Animation')


# from ANS_idle to ANS_walk
def ANS_idle_FromTo_ANS_walk_FSM_Animation(self, multiActivity):
    COMLogger.info('ANS_idle_FromTo_ANS_walk_FSM_Animation')
    #
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    if npc1Model.component_type == ComponentType('ActorModel'):
        npc1Model.actor.loop('walk', 0)
    else:
        npc1Model.animations.loop('walk', False)


# from ANS_walk to ANS_idle
def ANS_walk_FromTo_ANS_idle_FSM_Animation(self, multiActivity):
    COMLogger.info('ANS_walk_FromTo_ANS_idle_FSM_Animation')
    #
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    if npc1Model.component_type == ComponentType('ActorModel'):
        npc1Model.actor.stop('walk')
    else:
        npc1Model.animations.stop('walk')


# from ANS_idle to ANS_run
def ANS_idle_FromTo_ANS_run_FSM_Animation(self, multiActivity):
    COMLogger.info('ANS_idle_FromTo_ANS_run_FSM_Animation')
    #
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    if npc1Model.component_type == ComponentType('ActorModel'):
        npc1Model.actor.loop('run', 0)
    else:
        npc1Model.animations.loop('run', False)


# from ANS_run to ANS_idle
def ANS_run_FromTo_ANS_idle_FSM_Animation(self, multiActivity):
    COMLogger.info('ANS_run_FromTo_ANS_idle_FSM_Animation')
    #
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    if npc1Model.component_type == ComponentType('ActorModel'):
        npc1Model.actor.stop('run')
    else:
        npc1Model.animations.stop('run')


# from ANS_walk to ANS_run
def ANS_walk_FromTo_ANS_run_FSM_Animation(self, multiActivity):
    global _animToFrameMapWR
    COMLogger.info('ANS_walk_FromTo_ANS_run_FSM_Animation')
    #
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    if npc1Model.component_type == ComponentType('ActorModel'):
        # 1st way
        # npc1Model.actor.stop('walk')
        # npc1Model.actor.loop('run', 0)
        
        # 2nd way
        # npc1Model.anim_transitions['run-walk'].stopTransition()
        # npc1Model.anim_transitions['walk-run'].startTransition()
        
        # 3rd way
        animTransParam = npc1Model.anim_transitions['by-param']
        animTransParam.stopTransition()
        animTransParam.animCtrlFrom = npc1Model.actor.get_anim_control('walk')
        animTransParam.animCtrlTo = npc1Model.actor.get_anim_control('run')
        npc1P3CharCtrl = npc1.get_component(
            ComponentFamilyType('PhysicsControl')).p3CharacterController
        # optimization
        if _animToFrameMapWR:
            animTransParam.animToFrameMap = _animToFrameMapWR
        else:
            animTransParam.animToFrame = animToFrameWR
            _animToFrameMapWR = animTransParam.animToFrameMap
        animTransParam.playRateRatios = playRateRatiosWR
        animTransParam.lerpValue = (_lerpValueWR, [npc1P3CharCtrl])
        animTransParam.startTransition()
    else:
        npc1Model.animations.stop('walk')
        npc1Model.animations.loop('run', False)


# from ANS_run to ANS_walk
def ANS_run_FromTo_ANS_walk_FSM_Animation(self, multiActivity):
    global _animToFrameMapRW
    COMLogger.info('ANS_run_FromTo_ANS_walk_FSM_Animation')
    #
    npc1 = multiActivity.owner_object
    npc1Model = npc1.get_component(ComponentFamilyType('Scene'))
    if npc1Model.component_type == ComponentType('ActorModel'):
        # 1st way
        # npc1Model.actor.stop('run')
        # npc1Model.actor.loop('walk', 0)
        
        # 2nd way
        # npc1Model.anim_transitions['walk-run'].stopTransition()
        # npc1Model.anim_transitions['run-walk'].startTransition()
        
        # 3rd way
        animTransParam = npc1Model.anim_transitions['by-param']
        animTransParam.stopTransition()
        animTransParam.animCtrlFrom = npc1Model.actor.get_anim_control('run')
        animTransParam.animCtrlTo = npc1Model.actor.get_anim_control('walk')
        npc1P3CharCtrl = npc1.get_component(
            ComponentFamilyType('PhysicsControl')).p3CharacterController
        # optimization
        if _animToFrameMapRW:
            animTransParam.animToFrameMap = _animToFrameMapRW
        else:
            animTransParam.animToFrame = animToFrameRW
            _animToFrameMapRW = animTransParam.animToFrameMap
        animTransParam.playRateRatios = playRateRatiosRW
        animTransParam.lerpValue = (_lerpValueRW, [npc1P3CharCtrl])
        animTransParam.startTransition()
    else:
        npc1Model.animations.stop('run')
        npc1Model.animations.loop('walk', False)


# #
transitions = {
    # fsm_FB
    'Enter_FBS_idle_FSM_ForwardBackward': Enter_FBS_idle_FSM_ForwardBackward,
    'Exit_FBS_idle_FSM_ForwardBackward': Exit_FBS_idle_FSM_ForwardBackward,
    'Enter_FBS_forward_FSM_ForwardBackward': Enter_FBS_forward_FSM_ForwardBackward,
    'Exit_FBS_forward_FSM_ForwardBackward': Exit_FBS_forward_FSM_ForwardBackward,
    'Enter_FBS_backward_FSM_ForwardBackward': Enter_FBS_backward_FSM_ForwardBackward,
    'Exit_FBS_backward_FSM_ForwardBackward': Exit_FBS_backward_FSM_ForwardBackward,
    # fsm_LR
    'Enter_LRS_idle_FSM_LeftRight': Enter_LRS_idle_FSM_LeftRight,
    'Exit_LRS_idle_FSM_LeftRight': Exit_LRS_idle_FSM_LeftRight,
    'Enter_LRS_left_FSM_LeftRight': Enter_LRS_left_FSM_LeftRight,
    'Exit_LRS_left_FSM_LeftRight': Exit_LRS_left_FSM_LeftRight,
    'Enter_LRS_right_FSM_LeftRight': Enter_LRS_right_FSM_LeftRight,
    'Exit_LRS_right_FSM_LeftRight': Exit_LRS_right_FSM_LeftRight,
    # fsm_SF
    'SFS_slow_FromTo_SFS_fast_FSM_SlowFast': SFS_slow_FromTo_SFS_fast_FSM_SlowFast,
    'SFS_fast_FromTo_SFS_slow_FSM_SlowFast': SFS_fast_FromTo_SFS_slow_FSM_SlowFast,
    # fsm_Anim
    'Enter_ANS_idle_FSM_Animation': Enter_ANS_idle_FSM_Animation,
    'Exit_ANS_idle_FSM_Animation': Exit_ANS_idle_FSM_Animation,
    'ANS_idle_FromTo_ANS_walk_FSM_Animation': ANS_idle_FromTo_ANS_walk_FSM_Animation,
    'ANS_walk_FromTo_ANS_idle_FSM_Animation': ANS_walk_FromTo_ANS_idle_FSM_Animation,
    'ANS_idle_FromTo_ANS_run_FSM_Animation': ANS_idle_FromTo_ANS_run_FSM_Animation,
    'ANS_run_FromTo_ANS_idle_FSM_Animation': ANS_run_FromTo_ANS_idle_FSM_Animation,
    'ANS_walk_FromTo_ANS_run_FSM_Animation': ANS_walk_FromTo_ANS_run_FSM_Animation,
    'ANS_run_FromTo_ANS_walk_FSM_Animation': ANS_run_FromTo_ANS_walk_FSM_Animation,
}
