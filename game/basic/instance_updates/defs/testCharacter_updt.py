'''
Created on Mar 9, 2020

@author: consultit
'''

from COM.com.component import ComponentFamilyType, ComponentType

# # player
playRateFactors = {
    'walk': 0.02,
    'run': 0.0035,
    }


def animUpdate(dt, activity):
    ownerObj = activity._multi_activity.owner_object
    ownerModel = ownerObj.get_component(ComponentFamilyType('Scene'))
    ownerCharCtrl = ownerObj.get_component(
        ComponentFamilyType('PhysicsControl'))
    linearSpeed = abs(ownerCharCtrl.p3CharacterController.node().linear_speed.y)
    if ownerModel.component_type == ComponentType('ActorModel'):
        currAnim = ownerModel.actor.get_current_anim()
        if currAnim:
            playRateFactor = playRateFactors[currAnim]
            animCtrl = ownerModel.actor.get_anim_control(currAnim)
            animCtrl.set_play_rate(playRateFactor * linearSpeed)
    else:
        for n in range(ownerModel.animations.get_num_anims()):
            animCtrl = ownerModel.animations.get_anim(n)
            if animCtrl.is_playing():
                playRateFactor = playRateFactors[
                    ownerModel.animations.get_anim_name(n)]
                animCtrl.set_play_rate(playRateFactor * linearSpeed)


# #
instance_updates = {
    'animUpdate': animUpdate,
}
