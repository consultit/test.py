'''
Created on Oct 28, 2019

@author: consultit
'''

from COM.tools.utilities import COMLogger

# # player0 related
_timeElapsed = 0.0
_TIMETHRESHOLD = 3.0  # seconds


def playerUpdate(dt, activity):
    global _timeElapsed, _TIMETHRESHOLD, _jumping, _currentState
    if _timeElapsed > _TIMETHRESHOLD:
        # every _TIMETHRESHOLD seconds jump
        COMLogger.info(str(activity) + '\'s playerUpdate called after ' + 
                       str(_timeElapsed) + ' sec')
        # reset timer
        _timeElapsed = 0.0
    else:
        _timeElapsed += dt


# #
instance_updates = {
    'playerUpdate': playerUpdate,
}
