'''
Created on Oct 22, 2019

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# Character_updt
spec = util.spec_from_file_location('Character_updt',
            os.path.join(_this_library_dir, 'defs', 'Character_updt.py'))
Character_updt = util.module_from_spec(spec)
spec.loader.exec_module(Character_updt)

# testCharacter_updt
spec = util.spec_from_file_location('testCharacter_updt',
            os.path.join(_this_library_dir, 'defs', 'testCharacter_updt.py'))
testCharacter_updt = util.module_from_spec(spec)
spec.loader.exec_module(testCharacter_updt)

# instance_updates definitions
instance_updates = {
    **Character_updt.instance_updates,
    **testCharacter_updt.instance_updates,
}
