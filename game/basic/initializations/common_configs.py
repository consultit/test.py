'''
Created on Oct 22, 2019

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# Game_init
spec = util.spec_from_file_location('Game_init',
            os.path.join(_this_library_dir, 'defs', 'Game_init.py'))
Game_init = util.module_from_spec(spec)
spec.loader.exec_module(Game_init)

# Camera_init
spec = util.spec_from_file_location('Camera_init',
            os.path.join(_this_library_dir, 'defs', 'Camera_init.py'))
Camera_init = util.module_from_spec(spec)
spec.loader.exec_module(Camera_init)

# Actor_init
spec = util.spec_from_file_location('Actor_init',
            os.path.join(_this_library_dir, 'defs', 'Actor_init.py'))
Actor_init = util.module_from_spec(spec)
spec.loader.exec_module(Actor_init)

# Character_init
spec = util.spec_from_file_location('Character_init',
            os.path.join(_this_library_dir, 'defs', 'Character_init.py'))
Character_init = util.module_from_spec(spec)
spec.loader.exec_module(Character_init)

# RecastNavMesh_init
spec = util.spec_from_file_location('RecastNavMesh_init',
            os.path.join(_this_library_dir, 'defs', 'RecastNavMesh_init.py'))
RecastNavMesh_init = util.module_from_spec(spec)
spec.loader.exec_module(RecastNavMesh_init)

# Physics_init
spec = util.spec_from_file_location('Physics_init',
            os.path.join(_this_library_dir, 'defs', 'Physics_init.py'))
Physics_init = util.module_from_spec(spec)
spec.loader.exec_module(Physics_init)

# OpenSteerPlugIn_init
spec = util.spec_from_file_location('OpenSteerPlugIn_init',
            os.path.join(_this_library_dir, 'defs', 'OpenSteerPlugIn_init.py'))
OpenSteerPlugIn_init = util.module_from_spec(spec)
spec.loader.exec_module(OpenSteerPlugIn_init)

# testCharacter_init
spec = util.spec_from_file_location('testCharacter_init',
            os.path.join(_this_library_dir, 'defs', 'testCharacter_init.py'))
testCharacter_init = util.module_from_spec(spec)
spec.loader.exec_module(testCharacter_init)

# initializations definitions
initializations = {
    **Game_init.initializations,
    **Camera_init.initializations,
    **Actor_init.initializations,
    **Character_init.initializations,
    **RecastNavMesh_init.initializations,
    **Physics_init.initializations,
    **OpenSteerPlugIn_init.initializations,
    **testCharacter_init.initializations,
}
