'''
Created on Oct 23, 2019

@author: consultit
'''

# raise if Ely not available
import panda3d.core
import ely.libtools

from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectId
from COM.com.component import ComponentFamilyType
from panda3d.core import NodePath, LPoint3f, LVecBase3f, LVecBase4f, LVector3f, \
    TransformState
from panda3d.direct import CMetaInterval, CLerpInterval, CLerpNodePathInterval
from ely.physics import BT3SoftBodyConfig, BT3SoftBodyMaterial


def staticPlaneBox_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    # set camera transform
    cameraObj = objTmplMgr.get_created_object(ObjectId('camera'))
    if cameraObj:
        cameraObj.node_path.set_pos(0.0, -350.0, 40.0)
        cameraObj.node_path.set_hpr(0.0, -10.0, 0.0)


def redCarSphere_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    # change some attrs
    sphereRigidBody = objectI.get_component(ComponentFamilyType('Physics'))
    sphereRigidBody.p3RigidBody.node().collision_object_attrs.friction = 0.9


def hinge_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    # change some attrs
    hinge = objectI.get_component(ComponentFamilyType('Physics'))
    hinge.p3Constraint.node().constraints_attrs.angular_only = True


_targetPosInterval1 = []
_targetPosInterval2 = []
_targetHprInterval1 = []
_targetHprInterval2 = []
_targetPace = []


def _letMoveBackForth(target, duration, startPos, endPos, startHpr, endHpr):
    '''let target move back and forth'''
    
    global _targetPosInterval1, _targetPosInterval2, _targetHprInterval1, \
        _targetHprInterval2, _targetPace
    # Create the lerp intervals needed to walk back and forth
    walkTime = duration * 0.9 / 2.0
    turnTime = duration * 0.1 / 2.0
    # walk
    _targetPosInterval1.append(CLerpNodePathInterval('_targetPosInterval1' + 
            str(len(_targetPosInterval1)),
            walkTime, CLerpInterval.BT_no_blend,
            True, False, target, NodePath()))
    _targetPosInterval1[-1].set_start_pos(startPos)
    _targetPosInterval1[-1].set_end_pos(endPos)

    _targetPosInterval2.append(CLerpNodePathInterval('_targetPosInterval2' + 
            str(len(_targetPosInterval2)),
            walkTime, CLerpInterval.BT_no_blend,
            True, False, target, NodePath()))
    _targetPosInterval2[-1].set_start_pos(endPos)
    _targetPosInterval2[-1].set_end_pos(startPos)

    # turn
    _targetHprInterval1.append(CLerpNodePathInterval('_targetHprInterval1' + 
            str(len(_targetHprInterval1)), turnTime,
            CLerpInterval.BT_no_blend,
            True, False, target, NodePath()))
    _targetHprInterval1[-1].set_start_hpr(startHpr)
    _targetHprInterval1[-1].set_end_hpr(endHpr)

    _targetHprInterval2.append(CLerpNodePathInterval('_targetHprInterval2' + 
            str(len(_targetHprInterval2)), turnTime,
            CLerpInterval.BT_no_blend,
            True, False, target, NodePath()))
    _targetHprInterval2[-1].set_start_hpr(endHpr)
    _targetHprInterval2[-1].set_end_hpr(startHpr)

    # Create and play the sequence that coordinates the intervals
    _targetPace.append(CMetaInterval('_targetPace' + str(len(_targetPace))))
    _targetPace[-1].add_c_interval(_targetPosInterval1[-1], 0,
            CMetaInterval.RS_previous_end)
    _targetPace[-1].add_c_interval(_targetHprInterval1[-1], 0,
            CMetaInterval.RS_previous_end)
    _targetPace[-1].add_c_interval(_targetPosInterval2[-1], 0,
            CMetaInterval.RS_previous_end)
    _targetPace[-1].add_c_interval(_targetHprInterval2[-1], 0,
            CMetaInterval.RS_previous_end)
    _targetPace[-1].loop()


def canGhost_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    # let ghost moving endlessly back and forth
    canGhost = objectI.get_component(ComponentFamilyType('Physics'))
    pos = objectI.node_path.get_pos(pandaFramework.render)
    _letMoveBackForth(NodePath.any_path(canGhost.p3Ghost.node()), 20.0,
            pos + LVector3f(-20.0, 0.0, 0.0), pos + LVector3f(20.0, 0.0, 0.0),
            LVecBase3f(0.0, 0.0, 90.0), LVecBase3f(180.0, 0.0, 90.0))


def softCube_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    modelComp = objectI.get_component(ComponentFamilyType('Scene'))
    softBodyComp = objectI.get_component(ComponentFamilyType('Physics'))
    psb = softBodyComp.p3SoftBody.node()
    # perform some initialization
    modelComp.node_path.set_color(LVecBase4f(0.2, 1.0, 0.8, 1.0))
    psb.scale(LVecBase3f(4.0, 4.0, 4.0))
    psb.translate(LPoint3f(40.0, -100.0, 20.0))
    psb.set_volume_mass(300.0)
    psb.config.positions_solver_iterations = 1
    psb.generate_clusters(16)
    psb.get_collision_object_attrs().set_shape_margin(0.01)
    psb.config.collisions_flags = BT3SoftBodyConfig.Cluster_Soft_Soft | \
            BT3SoftBodyConfig.Cluster_Rigid_Soft
    psb.get_material(0).linear_stiffness_coefficient = 0.8


def cover_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    softBodyComp = objectI.get_component(ComponentFamilyType('Physics'))
    clothBody = softBodyComp.p3SoftBody.node()
    # perform some initialization
    clothBody.collision_object_attrs.shape_margin = 0.5
    material = clothBody.append_material()
    material.linear_stiffness_coefficient = 0.4
    material.flags = BT3SoftBodyMaterial.DebugDraw
    clothBody.generate_bending_constraints(2, material)
    clothBody.set_total_mass(150)
    # 6: visual state
    softBodyComp.p3SoftBody.set_two_sided(True)


def softBall_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    modelComp = objectI.get_component(ComponentFamilyType('Scene'))
    softBodyComp = objectI.get_component(ComponentFamilyType('Physics'))
    pressureBody = softBodyComp.p3SoftBody.node()
    # perform some initialization
    modelComp.node_path.set_color(LVecBase4f(0.8, 0.2, 1.0, 1.0))
    pressureBody.get_materials()[0].linear_stiffness_coefficient = 0.1
    pressureBody.config.dynamic_friction_coefficient = 1
    pressureBody.config.damping_coefficient = 0.001
    pressureBody.config.pressure_coefficient = 2500
    pressureBody.generate_clusters(16)
    pressureBody.get_collision_object_attrs().set_shape_margin(0.01)
    pressureBody.config.collisions_flags = BT3SoftBodyConfig.Cluster_Soft_Soft | \
            BT3SoftBodyConfig.Cluster_Rigid_Soft
    pressureBody.get_material(0).linear_stiffness_coefficient = 0.8


def softModel_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    softBodyComp = objectI.get_component(ComponentFamilyType('Physics'))
    triMeshBody = softBodyComp.p3SoftBody.node()
    # perform some initialization
    pm = triMeshBody.append_material()
    pm.linear_stiffness_coefficient = 0.5
    pm.flags = pm.flags - BT3SoftBodyMaterial.DebugDraw
    triMeshBody.generate_bending_constraints(2, pm)
    triMeshBody.config.positions_solver_iterations = 2
    triMeshBody.config.dynamic_friction_coefficient = 0.5   
    triMeshBody.generate_clusters(16)
    triMeshBody.get_collision_object_attrs().set_shape_margin(0.01)
    flags = triMeshBody.config.collisions_flags
    triMeshBody.config.collisions_flags = flags | \
            BT3SoftBodyConfig.Vertex_Face_Soft_Soft
    triMeshBody.randomize_constraints()
    x, a, s = LPoint3f(0.0, -200.0, 40.0), LVecBase3f(-30.0, -45.0, 60.0), 0.05
    m = TransformState.make_pos_hpr(x, a)
    triMeshBody.transform(m)
    #triMeshBody.scale(s)


# #
initializations = {
    'staticPlaneBox_initialization': staticPlaneBox_initialization,
    'redCarSphere_initialization': redCarSphere_initialization,
    'hinge_initialization': hinge_initialization,
    'canGhost_initialization': canGhost_initialization,
    'softCube_initialization': softCube_initialization,
    'cover_initialization': cover_initialization,
    'softBall_initialization': softBall_initialization,
    'softModel_initialization': softModel_initialization,
}
