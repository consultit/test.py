'''
Created on Oct 28, 2019

@author: consultit
'''

from COM.com.component import ComponentFamilyType
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectId


def player0_initialization(objectI, paramTable, pandaFramework, windowFramework):
    # Player1
    playerActivity = objectI.get_component(ComponentFamilyType('Behavior'))
    playerActivity.fsm.request('I', playerActivity)
    # play sound
    npc1Sound3d = objectI.get_component(ComponentFamilyType('Audio'))
    npc1Sound3d.p3Sound3d.node().get_sound_by_name('walk-sound').set_loop(True)
    npc1Sound3d.p3Sound3d.node().get_sound_by_name('walk-sound').play()
    # set camera transform
    cameraObj = ObjectTemplateManager.get_global_ptr().get_created_object(
        ObjectId('camera'))
    cameraObj.node_path.set_pos(0.0, -130.0, 25.0)
    cameraObj.node_path.set_hpr(0.0, -7.5, 0.0)


# #
initializations = {
    'player0_initialization': player0_initialization,
}
