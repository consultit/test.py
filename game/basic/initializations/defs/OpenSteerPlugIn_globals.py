'''
Created on Nov 18, 2019

@author: consultit
'''

import sys

# # locals xxx
# _drawStaticGeometryInitDone = False
# # Render-To-Texture (rtt) stuff
# _rttMeshDrawer2d = None
# _rttBuffer = None
# _rttRender2d = None
# # draw other static geometry stuff
# _staticMeshDrawer3d = None
# # common text writing
# _textNode = None

# flag for libRmlUi initialization
_rmlUiInitialized = False
_FLT_MAX = sys.float_info.max
_activeSteerPlugInType = None
_steerPlugInNames = [
    'one_turning', 'pedestrian', 'boid', 'multiple_pursuit', 'soccer',
    'capture_the_flag', 'low_speed_turn', 'map_drive', 'none']
# common globals
_ENVIRONMENTOBJECT = 'terrainLowPoly'
_TOBECLONEDOBJECT = 'steerVehicleToBeCloned'
_steerPlugIns = {}  # map<_SteerPlugInType, SteerPlugIn*>
_addKey = 'y'
_removeKey = 'shift-y'
# boid globals
_WORLDCENTEROBJECT = 'beachhouse2_1'
# multiple_pursuit globals
_mpWandererObjectId = None
_mpNewWanderedObjectId = None
_mpWandererExternalUpdate = False
# capture_the_flag globals
_ctfSeekerObjectId = None
_ctfNewSeekerObjectId = None
_ctfHomeBaseRadius = None
# _ctfMinStartRadius = None xxx
# _ctfMaxStartRadius = None xxx
_ctfBrakingRate = None
_ctfAvoidancePredictTimeMin = None
_ctfAvoidancePredictTimeMax = None
_ctfSeekerExternalUpdate = False
# low_speed_turn globals
_lstSteeringSpeed = None
#
_soccerActorSelected = None
_demoSelect = None
_usePathFences = True
_curvedSteering = True
# debug drawing
_debugDrawing = {}
