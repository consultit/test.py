'''
Created on Nov 2, 2019

@author: consultit
'''

from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectId
from COM.com.component import ComponentFamilyType
from COM.tools.utilities import COMLogger
from COM.managers.gameAIManager import GameAIManager
from COM.managers.gamePhysicsManager import GamePhysicsManager
from ely.physics import BT3RigidBody
from ely.ai import RNNavMeshSettings
from basic.initializations.defs.common import getCollisionEntryFromCamera
from panda3d.core import LVecBase3f, LVector3f, NodePath, LPoint3f, BitMask32

_TOBECLONEDOBJECT = 'crowdAgentToBeCloned'


def _updateKinematic(crowdAgent, rigidBody, task):
    rigidBody.set_pos(crowdAgent.get_pos())
    rigidBody.set_hpr(crowdAgent.get_hpr())
    return task.cont


# # crowdAgent(s)
def crowdAgent_initialization(objectI, paramTable, pandaFramework,
                                        windowFramework):
    # set rigid body as kinematic
    rigidBodyComp = objectI.get_component(ComponentFamilyType('Physics'))
    rigidBodyComp.p3RigidBody.node().mass = 0.0
    rigidBodyComp.p3RigidBody.node().body_type = BT3RigidBody.KINEMATIC
    ### BEWARE ###
    # Any time to correct correct object transform, you need to:
    # 1) remove crowd agent from nav mesh
    crowdAgentComp = objectI.get_component(ComponentFamilyType('AI'))
    navMesh = crowdAgentComp.p3CrowdAgent.node().nav_mesh
    navMesh.remove_crowd_agent(crowdAgentComp.p3CrowdAgent)
    # 2) perform transform modifications
    crowdAgentComp.p3CrowdAgent.set_transform(
        rigidBodyComp.p3RigidBody.node().get_transform())
    # 3) re-add crowd agent to nav mesh.
    navMesh.add_crowd_agent(crowdAgentComp.p3CrowdAgent)
    # add a task to update kinematic body from the driver
    pandaFramework.task_mgr.add(
        _updateKinematic, '_updateKinematic' + str(objectI.object_id),
        appendTask=True, extraArgs=[crowdAgentComp.p3CrowdAgent,
                                    rigidBodyComp.p3RigidBody])


def _setMoveTarget(app, agent):
    '''handle set move target'''
    # get the collision entry, if any
    entry0 = getCollisionEntryFromCamera(
        app, GameAIManager.get_global_ptr()._elyMgr)
    if entry0:
        target = entry0.get_surface_point(NodePath())
        agent.move_target = target


# # course2
def course2_initialization(objectI, paramTable, pandaFramework, windowFramework):
    global _TOBECLONEDOBJECT
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    elyPhysicsMgr = GamePhysicsManager.get_global_ptr()._elyMgr
    # get NavMesh component
    p3NavMeshComp = objectI.get_component(ComponentFamilyType('AI')).p3NavMesh.node()
    # collide mask of the model should be the same of GameAIManager
    modelComp = objectI.get_component(ComponentFamilyType('Scene'))
    modelComp.node_path.set_collide_mask(BitMask32(0x10))
    # find the crowd agent _TOBECLONEDOBJECT
    crowdAgentObject = objTmplMgr.get_created_object(ObjectId(_TOBECLONEDOBJECT))
    p3CrowdAgentComp = crowdAgentObject.get_component(ComponentFamilyType('AI')).p3CrowdAgent.node()
    if crowdAgentObject:
        # get the _TOBECLONEDOBJECT dimensions
        modelDims = LVecBase3f()
        modelDeltaCenter = LVector3f()
        modelRadius = elyPhysicsMgr.get_global_ptr().utilities.get_bounding_dimensions(
            crowdAgentObject.node_path, modelDims, modelDeltaCenter)
        # set crowd agent dimensions
        navMeshSettings = RNNavMeshSettings(p3NavMeshComp.nav_mesh_settings)
        navMeshSettings.agent_radius = modelRadius
        navMeshSettings.agent_height = modelDims.get_z()
        p3NavMeshComp.nav_mesh_settings = navMeshSettings
        # now setup the underlying NavMeshType
        if p3NavMeshComp.setup() != 0:
            COMLogger.info('Error setup of ' + str(p3NavMeshComp))
    # add agent to nav mesh
    p3NavMeshComp.add_crowd_agent(NodePath.any_path(p3CrowdAgentComp))
    # handle move targets on scene surface
    pandaFramework.accept('t', _setMoveTarget, [pandaFramework, p3CrowdAgentComp])
    # set camera transform
    cameraObj = objTmplMgr.get_created_object(ObjectId('camera'))
    cameraObj.node_path.set_pos(-17.0, -133.0, 92.0)
    cameraObj.node_path.set_hpr(7.0, -11.0, 0.0)


# #
initializations = {
    'crowdAgent_initialization': crowdAgent_initialization,
    'course2_initialization': course2_initialization,
}
