'''
Created on Oct 26, 2019

@author: consultit
'''

# raise if Ely not available
import panda3d.core
import ely.libtools

from COM.com.component import ComponentFamilyType
from COM.com.object import ObjectId
from COM.com.objectTemplateManager import ObjectTemplateManager
from ely.physics import BT3RigidBody
import math

objTmplMgr = ObjectTemplateManager.get_global_ptr()


# #Actor1 related
def _toggleActor1Control(actor1):
    driver = actor1.get_component(ComponentFamilyType('Control'))
    if driver.p3Driver.node().is_enabled:
        # if enabled then disable it
        # disable
        driver.p3Driver.node().disable()
    else:
        # enable
        driver.p3Driver.node().enable()


def _updateKinematic(driver, rigidBodyK, rigidBodyD, task):
    rigidBodyK.set_pos(driver.get_pos())
    rigidBodyK.set_hpr(driver.get_hpr())
    rigidBodyD.node().collision_object_attrs.set_activate()
    print(round(rigidBodyD.node().linear_velocity.length(), 2),
          round(rigidBodyK.node().linear_velocity.length(), 2))
    return task.cont


def constraintAP_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    bt3Constraint = objectI.get_component(
        ComponentFamilyType('Physics')).p3Constraint.node()
    bt3Constraint.constraints_attrs.debug_draw_size = 2.0
    bt3Constraint.constraints_attrs.set_limit(-math.pi / 32.0, math.pi / 32.0,
                                              _softness=0.9,
                                              _biasFactor=0.3,
                                              _relaxationFactor=1.0)
    # 1 method to retrieve BT3RigidBodies (more generic)
    objectAId = ObjectId(
        bt3Constraint.owner_objects[0].get_tag('_owner_object'))
    actor1Obj = objTmplMgr.get_created_object(objectAId)
    actor1RBK = actor1Obj.get_component(
        ComponentFamilyType('Physics')).p3RigidBody  # kinematic
    objectBId = ObjectId(
        bt3Constraint.owner_objects[1].get_tag('_owner_object'))
    probeObj = objTmplMgr.get_created_object(objectBId)
    probeRBD = probeObj.get_component(
        ComponentFamilyType('Physics')).p3RigidBody  # dynamic
    # 2 method to retrieve BT3RigidBodies (more specific)
    # actor1RBK = bt3Constraint.owner_objects[0]  # kinematic
    # probeRBD = bt3Constraint.owner_objects[1]  # dynamic
    # actor1Obj = objTmplMgr.get_created_object(ObjectId('Actor1'))
    
    # add a task to update kinematic body from the driver
    actor1Driver = actor1Obj.get_component(ComponentFamilyType('Control'))
    pandaFramework.task_mgr.add(
        _updateKinematic, '_updateKinematic', appendTask=True,
        extraArgs=[actor1Driver.p3Driver, actor1RBK, probeRBD])


def Actor1_initialization(objectI, paramTable, pandaFramework, windowFramework):
    # Actor1
    # play animation
    actor1Model = objectI.get_component(ComponentFamilyType('Scene'))
    actor1Model.animations.loop('walk', False)
    # play sound
    actor1Sound3d = objectI.get_component(ComponentFamilyType('Audio'))
    actor1Sound3d.p3Sound3d.node().get_sound_by_name('walk-sound').set_loop(True)
    actor1Sound3d.p3Sound3d.node().get_sound_by_name('walk-sound').play()
    # enable/disable Actor1 control by event
    pandaFramework.accept('f1', _toggleActor1Control, [objectI])
    # set rigid body as kinematic
    actor1RigidBody = objectI.get_component(ComponentFamilyType('Physics'))
    actor1RigidBody.p3RigidBody.node().mass = 0.0
    actor1RigidBody.p3RigidBody.node().body_type = BT3RigidBody.KINEMATIC
    # correct driver transform: pos and hrp only 
    actor1Driver = objectI.get_component(ComponentFamilyType('Control'))
    actor1Driver.p3Driver.set_pos(actor1RigidBody.p3RigidBody.get_pos())
    actor1Driver.p3Driver.set_hpr(actor1RigidBody.p3RigidBody.get_hpr())


# #
initializations = {
    'Actor1_initialization': Actor1_initialization,
    'constraintAP_initialization': constraintAP_initialization,
}
