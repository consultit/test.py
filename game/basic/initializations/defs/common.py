'''
Created on Oct 25, 2019

@author: consultit
'''

from panda3d.core import NodePath, TextNode, LPoint3f


def createText(text, scale, color, location, app):
    '''common text writing'''
    
    textNode = NodePath(TextNode('CommonTextNode'))
    textNode.reparent_to(app.render2d)
    textNode.set_bin('fixed', 50)
    textNode.set_depth_write(False)
    textNode.set_depth_test(False)
    textNode.set_billboard_point_eye()
    textNode.node().set_text(text)
    textNode.set_scale(scale)
    textNode.set_color(color)
    textNode.set_pos(location)
    return textNode


def getCollisionEntryFromCamera(app, elyMgr):
    '''throws a ray and returns the first collision entry or nullptr'''    
    
    # get the mouse watcher
    mwatcher = app.mouseWatcherNode
    if mwatcher.has_mouse():
        # Get to and from pos in camera coordinates
        pMouse = mwatcher.get_mouse()
        #
        pFrom, pTo = (LPoint3f(), LPoint3f())
        if app.camLens.extrude(pMouse, pFrom, pTo):
            # Transform to global coordinates
            pFrom = app.render.get_relative_point(app.cam, pFrom)
            pTo = app.render.get_relative_point(app.cam, pTo)
            direction = (pTo - pFrom).normalized()
            elyMgr.get_utilities().get_collision_ray().set_origin(pFrom)
            elyMgr.get_utilities().get_collision_ray().set_direction(direction)
            elyMgr.get_utilities().get_collision_traverser().traverse(app.render)
            # check collisions
            if elyMgr.get_utilities().get_collision_handler().get_num_entries() > 0:
                # Get the closest entry
                elyMgr.get_utilities().get_collision_handler().sort_entries()
                return elyMgr.get_utilities().get_collision_handler().get_entry(0)
    return None
