'''
Created on Nov 13, 2019

@author: consultit
'''

# raise if Ely not available
import panda3d.core
import ely.libtools

from COM.tools.utilities import COMLogger, StringSequence
from COM.com.component import ComponentFamilyType, ComponentType
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectId
from COM.managers.gameManager import GameManager
from COM.managers.gameAIManager import GameAIManager
from COM.managers.gameGUIManager import GameGUIManager
from ely.ai import OSSteerPlugIn, OSSteerVehicle
from ely.gui import P3GUIDictionary, P3GUIFactory, P3GUIElementPtr, \
    P3GUIElementFormControlSelectPtr
from basic.initializations.defs.common import getCollisionEntryFromCamera
from enum import IntEnum
from panda3d.core import LPoint3f, NodePath, LVecBase3f, LVector3f, BitMask32, \
    TextureStage, TexGenAttrib
import basic.initializations.defs.OpenSteerPlugIn_globals as G


def _runInitOnce():
    '''called by all steer plugins, executed only once'''
    
    if G._rmlUiInitialized:
        return
    elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    elyAIMgr = GameAIManager.get_global_ptr()._elyMgr
    gameMgr = GameManager.get_global_ptr()
    # collide mask of the model should be the same of GameAIManager
    environmentObj = objTmplMgr.get_created_object(
        ObjectId(G._ENVIRONMENTOBJECT))
    modelComp = environmentObj.get_component(ComponentFamilyType('Scene'))
    modelComp.node_path.set_collide_mask(BitMask32(0x10))    
    # initilize globals
    G._activeSteerPlugInType = _SteerPlugInType.none
    G._soccerActorSelected = _SoccerActor.ball
    G._demoSelect = _DemoSelect.demo_select_2
    G._mpWandererObjectId = ObjectId('')
    G._mpNewWanderedObjectId = ObjectId('')
    G._soccerActorSelected = _SoccerActor.ball
    G.demoSelect = _DemoSelect.demo_select_2
        # set camera transform
    cameraObj = objTmplMgr.get_created_object(ObjectId('camera'))
    cameraObj.node_path.set_pos(0.0, -350.0, 40.0)
    cameraObj.node_path.set_hpr(0.0, -10.0, 0.0)
    # libRmlUi initialization
    # register the add element function to (RmlUi) main menu
    elyGUIMgr.register_gui_add_elements_function(_rmluiAddElements)
    # register the event handler to main menu for each event value
    elyGUIMgr.register_gui_event_handler('steerPlugIn::options', _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('steerPlugIn::body::load_logo', _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('add_key::change', _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('steerPlugIn::form::submit_options', _rmluiEventHandler)
    # register the preset function to main menu
    elyGUIMgr.register_gui_preset_function(_rmluiPreset)
    # register the commit function to main menu
    elyGUIMgr.register_gui_commit_function(_rmluiCommit)
    # prepare for debug drawing 
    # reference_node_path_debug/reference_node_path_debug_2d are shared among plugins
    elyAIMgr.reference_node_path_debug.reparent_to(gameMgr.render)
    elyAIMgr.reference_node_path_debug_2d.reparent_to(gameMgr.aspect2d)
    # flag rmlUi initialized
    G._rmlUiInitialized = True


class _SteerPlugInType(IntEnum):
    one_turning = 0
    pedestrian = 1
    boid = 2
    multiple_pursuit = 3
    soccer = 4
    capture_the_flag = 5
    low_speed_turn = 6
    map_drive = 7
    none = 8


# soccer globals
class _SoccerActor(IntEnum):
    player_teamA = 0
    player_teamB = 1
    ball = 2


# map_drive globals
class _DemoSelect(IntEnum):
    demo_select_0 = 0
    demo_select_1 = 1
    demo_select_2 = 2


def _rmluiAddElements(mainMenu):
    '''add elements (tags) function for main menu'''
    
    # <button onclick='steerPlugIn::options'>SteerPlugIn options</button><br/>
    content = mainMenu.get_element_by_id('content')
    exitT = mainMenu.get_element_by_id('main::button::exit')
    if content:
        # create inputT element
        params = P3GUIDictionary()
        params.set_string('onclick', 'steerPlugIn::options')
        inputT = P3GUIFactory.instance_element(P3GUIElementPtr(), 'button', 'button', params)
        P3GUIFactory.instance_element_text(inputT, 'SteerPlugIn options')
        # create br element
        params.clear()
        params.set_string('id', 'br')
        br = P3GUIFactory.instance_element(P3GUIElementPtr(), 'br', 'br', params)
        # inster elements
        content.insert_before(inputT, exitT)
        content.insert_before(br, exitT)


def _createObjectSelectionList(objectsSelect, actualSelectedObject):
    '''to create a selection list of objects'''
    
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    # remove all options
    objectsSelect.remove_all()
    # set object list
    createdObjects = objTmplMgr.created_objects
    # add an empty obobjectI
    selectedIdx = objectsSelect.add('', '')
    for objectT in createdObjects:
        # don't add steady or with no parent objects
        if objectT.is_steady or objectT.node_path.get_parent().is_empty():  # FIXME: remove mandatory objects
            continue
        # add options
        objectId = objectT.object_id
        i = objectsSelect.add(str(objectId), str(objectId))
        if objectId == actualSelectedObject:
            selectedIdx = i
    # set first option as selected
    objectsSelect.set_selection(selectedIdx)


def _setElementValue(param, value, document):
    inputElem = P3GUIElementPtr(document.get_element_by_id(param))
    inputElem.set_attribute_float('value', value)


def _rmluiEventHandler(event, value):
    '''event handler added to the main one'''
    
    elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
    if value == 'steerPlugIn::options':
        # hide main menu
        elyGUIMgr.get_main_menu().hide()
        #  Load and show the camera options document.
        steerPlugInOptionsMenu = elyGUIMgr.get_rml_context().load_document(
            'ely-steerPlugIn-options.rml')
        if not steerPlugInOptionsMenu.empty:
            steerPlugInOptionsMenu.get_element_by_id('title').set_inner_rml(
                steerPlugInOptionsMenu.get_title())
            # # update controls
            # add key/remove key
            steerPlugInOptionsMenu.get_element_by_id('add_key').set_attribute_string(
                'value', G._addKey)
            steerPlugInOptionsMenu.get_element_by_id('remove_key').set_inner_rml(
                G._removeKey)
            # hide element for not existing plug in and check the active one
            for p in _SteerPlugInType:
                elem = steerPlugInOptionsMenu.get_element_by_id(G._steerPlugInNames[p])
                if (p == _SteerPlugInType.none) or (p in G._steerPlugIns):
                    # there is the plugin
                    if G._activeSteerPlugInType == p:
                        elem.set_attribute_float('checked', True)
                else:
                    elem.parent_node.set_property('display', 'none')
            # 
            steerPlugInOptionsMenu.show()
    elif (value == 'steerPlugIn::body::load_logo'):
        COMLogger.info('steerPlugIn::body::load_logo')
    elif value == 'add_key::change':
        #  This event is sent from the 'onchange' of the 'add_key'
        # inputT button. It shows or hides the related options.
        steerPlugInOptionsMenu = event.target_element.owner_document
        if steerPlugInOptionsMenu.empty:
            return
        # The 'value' parameter of an 'onchange' event is set to
        # the value the control would send if it was submitted
        # so, the empty string if it is clear or to the 'value'
        # attribute of the control if it is set.
        paramValue = event.get_parameter_string('value', '')
        if paramValue:
            steerPlugInOptionsMenu.get_element_by_id(
                'remove_key').set_inner_rml('shift-' + paramValue)
    elif value == 'steerPlugIn::multiple_pursuit::options':
        #  This event is sent from the 'onchange' of the 'multiple_pursuit'
        # radio button. It shows or hides the related options.
        steerPlugInOptionsMenu = event.target_element.owner_document
        if steerPlugInOptionsMenu.empty:
            return
        multiple_pursuit_options = steerPlugInOptionsMenu.get_element_by_id('multiple_pursuit_options')
        if not multiple_pursuit_options.empty:
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                multiple_pursuit_options.set_property('display', 'none')
            else:
                multiple_pursuit_options.set_property('display', 'block')
                # set elements' values from options' values
                # wanderer object
                objectsSelect = P3GUIElementFormControlSelectPtr(
                    steerPlugInOptionsMenu.get_element_by_id('wanderer_object'))
                if not objectsSelect.empty:
                    # use the helper to create the selection list
                    _createObjectSelectionList(objectsSelect, G._mpNewWanderedObjectId)
                # external update
                if G._mpWandererExternalUpdate:
                    steerPlugInOptionsMenu.get_element_by_id(
                                'external_update_wanderer_yes').set_attribute_bool(
                                'checked', True)
                else:
                    steerPlugInOptionsMenu.get_element_by_id(
                                'external_update_wanderer_no').set_attribute_bool(
                                'checked', True)
    elif value == 'steerPlugIn::soccer::options':
        #  This event is sent from the 'onchange' of the 'soccer'
        # radio button. It shows or hides the related options.
        steerPlugInOptionsMenu = event.target_element.owner_document
        if steerPlugInOptionsMenu.empty:
            return
        soccer_options = steerPlugInOptionsMenu.get_element_by_id('soccer_options')
        if not soccer_options.empty:
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                soccer_options.set_property('display', 'none')
            else:
                soccer_options.set_property('display', 'block')
                # set elements' values from options' values
                # players/ball
                if G._soccerActorSelected == _SoccerActor.player_teamA:
                    steerPlugInOptionsMenu.get_element_by_id('player_teamA').set_attribute_bool(
                                'checked', True)
                elif G._soccerActorSelected == _SoccerActor.player_teamB:
                        steerPlugInOptionsMenu.get_element_by_id('player_teamB').set_attribute_bool(
                                'checked', True)
                elif G._soccerActorSelected == _SoccerActor.ball:
                        steerPlugInOptionsMenu.get_element_by_id('ball').set_attribute_bool(
                                'checked', True)
                else:
                    pass
    elif value == 'steerPlugIn::capture_the_flag::options':
        #  This event is sent from the 'onchange' of the 'capture_the_flag'
        # radio button. It shows or hides the related options.
        steerPlugInOptionsMenu = event.target_element.owner_document
        if steerPlugInOptionsMenu.empty:
            return
        capture_the_flag_options = steerPlugInOptionsMenu.get_element_by_id('capture_the_flag_options')
        if not capture_the_flag_options.empty:
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                capture_the_flag_options.set_property('display', 'none')
            else:
                capture_the_flag_options.set_property('display', 'block')
                # set elements' values from options' values
                # seeker object
                objectsSelect = P3GUIElementFormControlSelectPtr(
                    steerPlugInOptionsMenu.get_element_by_id('seeker_object'))
                if not objectsSelect.empty:
                    # use the helper to create the selection list
                    _createObjectSelectionList(objectsSelect, G._ctfSeekerObjectId)
                # external update
                if G._ctfSeekerExternalUpdate:
                    steerPlugInOptionsMenu.get_element_by_id(
                        'external_update_seeker_yes').set_attribute_bool(
                            'checked', True)
                else:
                    steerPlugInOptionsMenu.get_element_by_id(
                        'external_update_seeker_no').set_attribute_bool(
                            'checked', True)
                # other options
                # home base radius
                _setElementValue('home_base_radius', G._ctfHomeBaseRadius,
                                 steerPlugInOptionsMenu)
                # braking rate
                _setElementValue('braking_rate', G._ctfBrakingRate,
                                 steerPlugInOptionsMenu)
                # avoidance predict time_min
                _setElementValue('avoidance_predict_time_min', G._ctfAvoidancePredictTimeMin,
                                 steerPlugInOptionsMenu)
                # avoidance predict time_max
                _setElementValue('avoidance_predict_time_max', G._ctfAvoidancePredictTimeMax,
                                 steerPlugInOptionsMenu)
    elif value == 'avoidance_predict_time_min::change':
        steerPlugInOptionsMenu = event.target_element.owner_document
        maxV = steerPlugInOptionsMenu.get_element_by_id(
            'avoidance_predict_time_min').get_attribute_float('max', 0.0)
        step = steerPlugInOptionsMenu.get_element_by_id(
            'avoidance_predict_time_min').get_attribute_float('step', 0.0)
        G._ctfAvoidancePredictTimeMin = maxV * event.get_parameter_float('value', 0.0)
        # check
        if G._ctfAvoidancePredictTimeMin > G._ctfAvoidancePredictTimeMax:
            G._ctfAvoidancePredictTimeMax = G._ctfAvoidancePredictTimeMin + step
            _setElementValue('avoidance_predict_time_max', G._ctfAvoidancePredictTimeMax,
                             steerPlugInOptionsMenu)
    elif value == 'avoidance_predict_time_max::change':
        steerPlugInOptionsMenu = event.target_element.owner_document
        maxV = steerPlugInOptionsMenu.get_element_by_id(
            'avoidance_predict_time_max').get_attribute_float('max', 0.0)
        step = steerPlugInOptionsMenu.get_element_by_id(
            'avoidance_predict_time_max').get_attribute_float('step', 0.0)
        G._ctfAvoidancePredictTimeMax = maxV * event.get_parameter_float('value', 0.0)
        # check
        if G._ctfAvoidancePredictTimeMax < G._ctfAvoidancePredictTimeMin:
            G._ctfAvoidancePredictTimeMin = G._ctfAvoidancePredictTimeMax - step
            _setElementValue('avoidance_predict_time_min', G._ctfAvoidancePredictTimeMin,
                             steerPlugInOptionsMenu)
    elif value == 'steerPlugIn::low_speed_turn::options':
        #  This event is sent from the 'onchange' of the 'low_speed_turn'
        # radio button. It shows or hides the related options.
        steerPlugInOptionsMenu = event.target_element.owner_document
        if steerPlugInOptionsMenu.empty:
            return
        low_speed_turn_options = steerPlugInOptionsMenu.get_element_by_id('low_speed_turn_options')
        if low_speed_turn_options:
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                low_speed_turn_options.set_property('display', 'none')
            else:
                low_speed_turn_options.set_property('display', 'block')
                # set elements' values from options' values
                # steering speed
                _setElementValue('steering_speed', G._lstSteeringSpeed,
                                 steerPlugInOptionsMenu)
    elif value == 'steerPlugIn::map_drive::options':
        #  This event is sent from the 'onchange' of the 'map_drive'
        # radio button. It shows or hides the related options.
        steerPlugInOptionsMenu = event.target_element.owner_document
        if steerPlugInOptionsMenu.empty:
            return
        map_drive_options = steerPlugInOptionsMenu.get_element_by_id('map_drive_options')
        if not map_drive_options.empty:
            # The 'value' parameter of an 'onchange' event is set to
            # the value the control would send if it was submitted
            # so, the empty string if it is clear or to the 'value'
            # attribute of the control if it is set.
            if not event.get_parameter_string('value', ''):
                map_drive_options.set_property('display', 'none')
            else:
                map_drive_options.set_property('display', 'block')
                # set elements' values from options' values
                # demo select
                if G._demoSelect == _DemoSelect.demo_select_0:
                    steerPlugInOptionsMenu.get_element_by_id('demo_select_0').set_attribute_bool(
                            'checked', True)
                elif G._demoSelect == _DemoSelect.demo_select_1:
                    steerPlugInOptionsMenu.get_element_by_id('demo_select_1').set_attribute_bool(
                            'checked', True)
                elif G._demoSelect == _DemoSelect.demo_select_2:
                    steerPlugInOptionsMenu.get_element_by_id('demo_select_2').set_attribute_bool(
                            'checked', True)
                else:
                    pass
                # use path fences
                if G._usePathFences:
                    steerPlugInOptionsMenu.get_element_by_id(
                            'use_path_fences_yes').set_attribute_bool('checked',
                            True)
                else:
                    steerPlugInOptionsMenu.get_element_by_id(
                            'use_path_fences_no').set_attribute_bool('checked',
                            True)
                # curved steering
                if G._curvedSteering:
                    steerPlugInOptionsMenu.get_element_by_id(
                            'curved_steering_yes').set_attribute_bool('checked',
                            True)
                else:
                    steerPlugInOptionsMenu.get_element_by_id(
                            'curved_steering_no').set_attribute_bool('checked',
                            True)
    # # Submit
    elif value == 'steerPlugIn::form::submit_options':
        # check if ok or cancel
        paramValue = event.get_parameter_string('submit', 'cancel')
        if paramValue == 'ok':
            # set new add key
            paramValue = event.get_parameter_string('add_key', '')
            if paramValue:
                G._addKey = paramValue
                G._removeKey = 'shift-' + G._addKey
            # set new steer plugin type
            paramValue = event.get_parameter_string('steerPlugIn', '')
            if paramValue == G._steerPlugInNames[_SteerPlugInType.one_turning]:
                G._activeSteerPlugInType = _SteerPlugInType.one_turning
            elif paramValue == G._steerPlugInNames[_SteerPlugInType.pedestrian]:
                G._activeSteerPlugInType = _SteerPlugInType.pedestrian
            elif paramValue == G._steerPlugInNames[_SteerPlugInType.boid]:
                G._activeSteerPlugInType = _SteerPlugInType.boid
            elif paramValue == G._steerPlugInNames[_SteerPlugInType.multiple_pursuit]:
                G._activeSteerPlugInType = _SteerPlugInType.multiple_pursuit
                # set options' values from elements' values
                # new wanderer object
                G._mpNewWanderedObjectId = ObjectId(event.get_parameter_string(
                    'wanderer_object', ''))
                # external update
                paramValue = event.get_parameter_string(
                    'external_update_wanderer', '')
                if paramValue == 'yes':
                    G._mpWandererExternalUpdate = True
                else:
                    # paramValue == 'no'
                    G._mpWandererExternalUpdate = False
            elif paramValue == G._steerPlugInNames[_SteerPlugInType.soccer]:
                G._activeSteerPlugInType = _SteerPlugInType.soccer
                # set options' values from elements' values
                # player/ball
                paramValue = event.get_parameter_string('player_ball', '')
                if paramValue == 'player_teamA':
                    G._soccerActorSelected = _SoccerActor.player_teamA
                elif paramValue == 'player_teamB':
                    G._soccerActorSelected = _SoccerActor.player_teamB
                else:
                    # default: ball
                    G._soccerActorSelected = _SoccerActor.ball
            elif paramValue == G._steerPlugInNames[_SteerPlugInType.capture_the_flag]:
                G._activeSteerPlugInType = _SteerPlugInType.capture_the_flag
                # set options' values from elements' values
                # new seeker object
                G._ctfNewSeekerObjectId = event.get_parameter_string(
                    'seeker_object', '')
                # external update
                paramValue = event.get_parameter_string('external_update_seeker', '')
                if paramValue == 'yes':
                    G._ctfSeekerExternalUpdate = True
                else:
                    # paramValue == 'no'
                    G._ctfSeekerExternalUpdate = False
                # other options
                # home base radius
                G._ctfHomeBaseRadius = event.get_parameter_float(
                    'home_base_radius', 0.0)
                # braking rate
                G._ctfBrakingRate = event.get_parameter_float(
                    'braking_rate', 0.0)
                # avoidance predict time_min
                G._ctfAvoidancePredictTimeMin = event.get_parameter_float(
                    'avoidance_predict_time_min', 0.0)
                # avoidance predict time_max
                G._ctfAvoidancePredictTimeMax = event.get_parameter_float(
                    'avoidance_predict_time_max', 0.0)
            elif paramValue == G._steerPlugInNames[_SteerPlugInType.low_speed_turn]:
                G._activeSteerPlugInType = _SteerPlugInType.low_speed_turn
                # other options
                # home base radius
                G._lstSteeringSpeed = event.get_parameter_float(
                    'steering_speed', 0.0)
            elif paramValue == G._steerPlugInNames[_SteerPlugInType.map_drive]:
                G._activeSteerPlugInType = _SteerPlugInType.map_drive
                # set options' values from elements' values
                # demo select
                paramValue = event.get_parameter_string('demo_select', '')
                if paramValue == 'demo_select_0':
                    G._demoSelect = _DemoSelect.demo_select_0
                elif paramValue == 'demo_select_1':
                    G._demoSelect = _DemoSelect.demo_select_1
                else:
                    # default: demo_select_2
                    G._demoSelect = _DemoSelect.demo_select_2
                # use path fences
                paramValue = event.get_parameter_string('use_path_fences', '')
                if paramValue == 'yes':
                    G._usePathFences = True
                # paramValue == 'no'
                else:
                    G._usePathFences = False
                # curved steering
                paramValue = event.get_parameter_string('curved_steering', '')
                if paramValue == 'yes':
                    G._curvedSteering = True
                else:
                # paramValue == 'no'
                    G._curvedSteering = False
            else:
                # default
                G._activeSteerPlugInType = _SteerPlugInType.none
        # close (i.e. unload) the camera options menu and set as closed..
        steerPlugInOptionsMenu = event.target_element.owner_document
        steerPlugInOptionsMenu.close()
        # return to main menu.
        elyGUIMgr.get_main_menu().show()

 
def _add_vehicle():
    elyAIMgr = GameAIManager.get_global_ptr()._elyMgr
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    gameMgr = GameManager.get_global_ptr()
    if G._activeSteerPlugInType == _SteerPlugInType.none:
        return
    # get data
    steerPlugIn = G._steerPlugIns[G._activeSteerPlugInType]
    # get object to be cloned
    toBeClonedObject = objTmplMgr.get_created_object(
        ObjectId(G._TOBECLONEDOBJECT))
    if not toBeClonedObject:
        return
    # get SteerPlugIn ObjectId
    steerPlugInObjectId = steerPlugIn.owner_object.object_id
    # get underlying OpenSteer PlugIn name
    openSteerPlugInName = steerPlugIn.p3SteerPlugIn.node().plug_in_type
    # get the object-to-be-cloned parameter table
    objParams = toBeClonedObject.stored_obj_tmpl_params
    # get the object-to-be-cloned components' parameter tables
    compParams = toBeClonedObject.stored_comp_tmpl_params
    # get position under mouse pointer
    hitPos = LPoint3f(0.0, 0.0, 0.0)
    entry0 = getCollisionEntryFromCamera(gameMgr, elyAIMgr)
    if entry0:
        hitPos = entry0.get_surface_point(NodePath())
    # # create the clone
    # tweak clone object's common parameters
    # set clone object store_params
    objParams.pop('store_params', None)
    objParams['store_params'] = StringSequence([ 'False'])
    # set clone object pos
    objParams.pop('pos', None)
    COMLogger.info('Hit position: ' + str(hitPos))
    objParams['pos'] = StringSequence(
        [str(hitPos.x) + ',' + str(hitPos.y) + ',' + str(hitPos.z)])
    # tweak clone components' parameter tables
    compParams['SteerVehicle'].pop('vehicle_type', None)
    compParams['SteerVehicle'].pop('mov_type', None)
    compParams['SteerVehicle'].pop('thrown_events', None)
    if openSteerPlugInName == OSSteerPlugIn.ONE_TURNING:
        # set SteerVehicle type, mov_type
        compParams['SteerVehicle']['vehicle_type'] = StringSequence(['one_turning'])
        compParams['SteerVehicle']['mov_type'] = StringSequence(['kinematic'])
    elif openSteerPlugInName == OSSteerPlugIn.PEDESTRIAN:
        # set SteerVehicle type, mov_type, thrown_events
        compParams['SteerVehicle']['vehicle_type'] = StringSequence(['pedestrian'])
        compParams['SteerVehicle']['mov_type'] = StringSequence(['kinematic'])
        compParams['SteerVehicle']['thrown_events'] = StringSequence(
            ['avoid_obstacle@OBSTACLE_PED@1.0:avoid_close_neighbor@CLOSE_NEIGHBOR_PED@30.0'])
    elif openSteerPlugInName == OSSteerPlugIn.BOID:
        # set SteerVehicle type, mov_type, max_force, max_speed, speed
        compParams['SteerVehicle']['vehicle_type'] = StringSequence(['boid'])
        compParams['SteerVehicle']['mov_type'] = StringSequence(['opensteer'])
        compParams['SteerVehicle']['thrown_events'] = StringSequence(
            ['avoid_obstacle@OBSTACLE_BOID@1.0:avoid_close_neighbor@CLOSE_NEIGHBOR_BOID@30.0'])
        # 
        compParams['SteerVehicle'].pop('max_force', None)
        compParams['SteerVehicle']['max_force'] = StringSequence(['27'])
        compParams['SteerVehicle'].pop('max_speed', None)
        compParams['SteerVehicle']['max_speed'] = StringSequence(['20'])
        compParams['SteerVehicle'].pop('speed', None)
        compParams['SteerVehicle']['speed'] = StringSequence(['3'])
        # set InstanceOf instance_of, scale
        compParams['InstanceOf'].pop('instance_of', None)
        compParams['InstanceOf']['instance_of'] = StringSequence(['Smiley'])
        compParams['InstanceOf'].pop('scale', None)
        compParams['InstanceOf']['scale'] = StringSequence(['0.5,0.5,0.5'])
    elif openSteerPlugInName == OSSteerPlugIn.MULTIPLE_PURSUIT:
        compParams['SteerVehicle'].pop('up_axis_fixed', None)
        # set SteerVehicle type, mov_type, up_axis_fixed
        compParams['SteerVehicle']['vehicle_type'] = StringSequence(['mp_pursuer'])
        compParams['SteerVehicle']['mov_type'] = StringSequence(['kinematic'])
        compParams['SteerVehicle']['up_axis_fixed'] = StringSequence(['True'])
    elif openSteerPlugInName == OSSteerPlugIn.SOCCER:
        compParams['SteerVehicle'].pop('max_force', None)
        compParams['SteerVehicle'].pop('max_speed', None)
        compParams['SteerVehicle'].pop('speed', None)
        compParams['SteerVehicle'].pop('up_axis_fixed', None)
        compParams['InstanceOf'].pop('instance_of', None)
        compParams['InstanceOf'].pop('scale', None)
        # set SteerVehicle type, mov_type, max_force, max_speed, up_axis_fixed
        param = 'ball' if G._soccerActorSelected == _SoccerActor.ball else 'player'
        compParams['SteerVehicle']['vehicle_type'] = StringSequence([param])
        compParams['SteerVehicle']['mov_type'] = StringSequence(['kinematic'])
        param = '9.0' if G._soccerActorSelected == _SoccerActor.ball else '3000.7'
        compParams['SteerVehicle']['max_force'] = StringSequence([param])
        param = '9.0' if G._soccerActorSelected == _SoccerActor.ball else '10.0'
        compParams['SteerVehicle']['max_speed'] = StringSequence([param])
        param = '1.0' if G._soccerActorSelected == _SoccerActor.ball else '0.0'
        compParams['SteerVehicle']['speed'] = StringSequence([param])
        compParams['SteerVehicle']['thrown_events'] = StringSequence(
            ['avoid_neighbor@NEIGHBOR_SOCCER@1.0'])
        compParams['SteerVehicle']['up_axis_fixed'] = StringSequence(['true'])
        # set InstanceOf instance_of, scale
        if G._soccerActorSelected == _SoccerActor.ball:
            param = 'ball'
        elif G._soccerActorSelected == _SoccerActor.player_teamA:
            param = 'Panda'
        else:
            param = 'Gorilla'
        compParams['InstanceOf']['instance_of'] = StringSequence([param])
        if G._soccerActorSelected == _SoccerActor.ball:
            param = '1.0'
        elif G._soccerActorSelected == _SoccerActor.player_teamA:
            param = '0.5'
        else:
            param = '0.5'        
        compParams['InstanceOf']['scale'] = StringSequence([param])
    elif openSteerPlugInName == OSSteerPlugIn.CAPTURE_THE_FLAG:
        compParams['SteerVehicle'].pop('up_axis_fixed', None)
        # set SteerVehicle type, mov_type, thrown_events, speed, up_axis_fixed
        compParams['SteerVehicle']['vehicle_type'] = StringSequence(['ctf_enemy'])
        compParams['SteerVehicle']['mov_type'] = StringSequence(['kinematic'])
        compParams['SteerVehicle']['thrown_events'] = StringSequence(
            ['avoid_obstacle@@1.0:avoid_close_neighbor@@'])
        compParams['SteerVehicle'].pop('speed', None)
        compParams['SteerVehicle']['speed'] = StringSequence(['1.0'])
        compParams['SteerVehicle']['up_axis_fixed'] = StringSequence(['true'])
    elif openSteerPlugInName == OSSteerPlugIn.LOW_SPEED_TURN:
        # set SteerVehicle type, mov_type
        compParams['SteerVehicle']['vehicle_type'] = StringSequence(['low_speed_turn'])
        compParams['SteerVehicle']['mov_type'] = StringSequence(['kinematic'])
    elif openSteerPlugInName == OSSteerPlugIn.MAP_DRIVE:
        compParams['SteerVehicle'].pop('max_speed', None)
        compParams['SteerVehicle'].pop('max_force', None)
        compParams['SteerVehicle'].pop('up_axis_fixed', None)
        compParams['InstanceOf'].pop('instance_of', None)
        compParams['InstanceOf'].pop('scale', None)
        # set SteerVehicle type, mov_type, max_speed, max_force, up_axis_fixed, thrown_events
        compParams['SteerVehicle']['vehicle_type'] = StringSequence(['map_driver'])
        compParams['SteerVehicle']['mov_type'] = StringSequence(['kinematic'])
        compParams['SteerVehicle']['max_speed'] = StringSequence(['20'])
        compParams['SteerVehicle']['max_force'] = StringSequence(['8'])
        compParams['SteerVehicle']['up_axis_fixed'] = StringSequence(['true'])
        compParams['SteerVehicle']['thrown_events'] = StringSequence(
            ['avoid_obstacle@OBSTACLE_MAPDRIVE@1.0:path_following@PATH_FOLLOW_MAPDRIVE@1.0'])
        # set InstanceOf instance_of, scale
        compParams['InstanceOf']['instance_of'] = StringSequence(['RedCar'])
        compParams['InstanceOf']['scale'] = StringSequence(['0.2'])
    # set SteerVehicle add_to_plugin
    compParams['SteerVehicle'].pop('add_to_plugin', None)
    compParams['SteerVehicle']['add_to_plugin'] = StringSequence(
        [str(steerPlugInObjectId)])
    # create actually the clone and...
    clone = objTmplMgr.create_object(toBeClonedObject.object_type, ObjectId(),
                                     objParams, compParams, False)
    # ...initialize it
    clone.world_setup()

 
def _remove_vehicle():
    elyAIMgr = GameAIManager.get_global_ptr()._elyMgr
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    gameMgr = GameManager.get_global_ptr()
    if G._activeSteerPlugInType == _SteerPlugInType.none:
        return
    # get data
    steerPlugIn = G._steerPlugIns[G._activeSteerPlugInType]
    # get underlying OpenSteer PlugIn name
    openSteerPlugInName = steerPlugIn.p3SteerPlugIn.plug_in_type
    # get object under mouse pointer
    hitObject = None
    entry0 = getCollisionEntryFromCamera(gameMgr, elyAIMgr)
    if entry0:
        if entry0.into_node_path.has_tag('_owner_object'):
            hitObject = objTmplMgr.get_created_object(
                ObjectId(entry0.into_node_path.get_tag('_owner_object')))
    if hitObject:  
        # check if it is has a SteerVehicle component
        aiComp = hitObject.get_component(ComponentType('SteerVehicle'))
        if aiComp:
            # check if it is the type requested
            vehicleType = aiComp.p3SteerVehicle.vehicle_type
            if ((openSteerPlugInName == OSSteerPlugIn.ONE_TURNING) and 
                (vehicleType != OSSteerVehicle.ONE_TURNING)):
                return
            elif ((openSteerPlugInName == OSSteerPlugIn.PEDESTRIAN) and
                  (vehicleType != OSSteerVehicle.PEDESTRIAN)):
                return
            elif ((openSteerPlugInName == OSSteerPlugIn.BOID) and
                  (vehicleType != OSSteerVehicle.BOID)):
                return
            elif ((openSteerPlugInName == OSSteerPlugIn.MULTIPLE_PURSUIT) and
                  ((vehicleType != OSSteerVehicle.MP_WANDERER) and 
                   (vehicleType != OSSteerVehicle.MP_PURSUER))):
                return
            elif ((openSteerPlugInName == OSSteerPlugIn.SOCCER) and
                  ((vehicleType != OSSteerVehicle.PLAYER) and 
                   (vehicleType != OSSteerVehicle.BALL))):
                return
            elif ((openSteerPlugInName == OSSteerPlugIn.CAPTURE_THE_FLAG) and
                  ((vehicleType != OSSteerVehicle.CTF_SEEKER) and 
                   (vehicleType != OSSteerVehicle.CTF_ENEMY))):
                return
            elif ((openSteerPlugInName == OSSteerPlugIn.LOW_SPEED_TURN) and
                  (vehicleType != OSSteerVehicle.LOW_SPEED_TURN)):
                return
            elif ((openSteerPlugInName == OSSteerPlugIn.MAP_DRIVE) and
                  (vehicleType != OSSteerVehicle.MAP_DRIVER)):
                return
            # try to remove this SteerVehicle from this SteerPlugIn
            if steerPlugIn.p3SteerPlugIn.remove_steer_vehicle(aiComp.p3SteerVehicle) >= 0:
                # the SteerVehicle belonged to this SteerPlugIn:
                # remove actually the clone
                objTmplMgr.destroy_object(hitObject.object_id)
 

def _rmluiPreset():
    '''remove add/remove vehicle event handlers'''
    
    gameMgr = GameManager.get_global_ptr()
    if G._activeSteerPlugInType == _SteerPlugInType.none:
        return
    gameMgr.ignore(G._addKey)
    gameMgr.ignore(G._removeKey)


def _rmluiCommit():
    '''commit function called from main menu'''
    
    gameMgr = GameManager.get_global_ptr()
    if G._activeSteerPlugInType == _SteerPlugInType.none:
        return
    # add add/remove vehicle event handlers
    gameMgr.accept(G._addKey, _add_vehicle)
    gameMgr.accept(G._removeKey, _remove_vehicle)


def _rmluiHelperCommit(plugInType, objectId, newObjectId, vehicleType, externalUpdate):
    '''helper commit for mp and ctf'''
    
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    if G._activeSteerPlugInType != plugInType:
        return
    # get data
    steerPlugIn = G._steerPlugIns[G._activeSteerPlugInType]
    # get SteerPlugIn ObjectId
    steerPlugInObjectId = steerPlugIn.owner_object.object_id
    if objectId == newObjectId:
        return
    # remove SteerVehicle component from wandererObject
    objTmplMgr.remove_component_from_object(objectId, ComponentType('SteerVehicle'))
    # get object to be cloned
    toBeClonedObject = objTmplMgr.get_created_object(ObjectId(G._TOBECLONEDOBJECT))
    if not toBeClonedObject:
        return
    # get the object-to-be-cloned components' parameter tables
    compParams = toBeClonedObject.stored_comp_tmpl_params
    # tweak clone components' parameter tables
    compParams['SteerVehicle'].pop('vehicle_type', None)
    compParams['SteerVehicle']['vehicle_type'] = StringSequence([vehicleType])
    if externalUpdate:
        # replace the AI component with a externally updated wanderer
        # set SteerVehicle external_update
        compParams['SteerVehicle'].pop('external_update', None)
        compParams['SteerVehicle']['external_update'] = StringSequence(['True'])
    else:
        # replace the AI component with a normal wanderer
        # set SteerVehicle mov_type
        compParams['SteerVehicle'].pop('mov_type', None)
        compParams['SteerVehicle']['mov_type'] = StringSequence(['kinematic'])
    # set SteerVehicle add_to_plugin
    compParams['SteerVehicle'].pop('add_to_plugin', None)
    compParams['SteerVehicle']['add_to_plugin'] = StringSequence([str(steerPlugInObjectId)])
    # add the SteerVehicle component
    objTmplMgr.add_component_to_object(newObjectId, ComponentType('SteerVehicle'),
                                       compParams['SteerVehicle'])
    # change object
    objectId = newObjectId


def _rmluiMultiplePursuitCommit():
    '''commit function for Multiple Pursuit'''
    
    # call helper
    _rmluiHelperCommit(_SteerPlugInType.multiple_pursuit, G._mpWandererObjectId,
                       G._mpNewWanderedObjectId, 'mp_wanderer',
                       G._mpWandererExternalUpdate)


def _rmluiCaptureTheFlagCommit():
    '''commit function for Capture The Flag'''
    
    # call helper
    _rmluiHelperCommit(_SteerPlugInType.capture_the_flag, G._ctfSeekerObjectId,
                       G._ctfNewSeekerObjectId, 'ctf_seeker', G._ctfSeekerExternalUpdate)
    # update plugin options (with gAvoidancePredictTime included)
    if G._activeSteerPlugInType != _SteerPlugInType.capture_the_flag:
        return
    steerPlugIn = G._steerPlugIns[G._activeSteerPlugInType]
    # 
    ctfPlugIn = steerPlugIn.p3SteerPlugIn
    # set options
    ctfPlugIn.home_base_radius = G._ctfHomeBaseRadius
    ctfPlugIn.braking_rate = G._ctfBrakingRate
    ctfPlugIn.avoidance_predict_time_min = G._ctfAvoidancePredictTimeMin
    ctfPlugIn.avoidance_predict_time_max = G._ctfAvoidancePredictTimeMax
 

def _rmlUiLowSpeedTurnCommit():
    '''commit function for Low Speed Turn'''

    # update plugin options
    if G._activeSteerPlugInType != _SteerPlugInType.low_speed_turn:
        return
    steerPlugIn = G._steerPlugIns[G._activeSteerPlugInType]
    # 
    lstPlugIn = steerPlugIn.p3SteerPlugIn
    # set options
    lstPlugIn.steering_speed = G._lstSteeringSpeed
 

def _rmlUiMapDriveCommit():
    '''commit function for Map Drive'''

    # update plugin options
    if G._activeSteerPlugInType != _SteerPlugInType.map_drive:
        return
    steerPlugIn = G._steerPlugIns[G._activeSteerPlugInType]
    # 
    mapDrivePlugIn = steerPlugIn.p3SteerPlugIn
    # set options
#     mapDrivePlugIn.setOptions(G._demoSelect, G._usePathFences, G._curvedSteering) xxx
    mapDrivePlugIn.node().map_path_fences = G._usePathFences
    if G._curvedSteering:
        mapDrivePlugIn.node().map_prediction_type = OSSteerPlugIn.CURVED_PREDICTION
    else:
        mapDrivePlugIn.node().map_prediction_type = OSSteerPlugIn.LINEAR_PREDICTION


def _debugDrawToTexture(steerPlugIn, sceneNP, app):
    'debug draw to texture'

    steerPlugIn.debug_drawing_to_texture(sceneNP, app.win)


def _onTextureReady(sceneNP, textureStage, texture):
    'debug drawing texture is ready'
    
    # set up texture where to render
    sceneNP.clear_texture(textureStage)
    textureStage.set_mode(TextureStage.M_modulate)
    # take into account sceneNP dimensions
    sceneNP.set_tex_offset(textureStage, 0.5, 0.5)
    sceneNP.set_tex_scale(textureStage, 1.0 / 512.0, 1.0 / 512.0)
    sceneNP.set_tex_gen(textureStage, TexGenAttrib.M_world_position)
    sceneNP.set_texture(textureStage, texture, 10)


# # initializations
def steerPlugInOneTurning_initialization(objectI, paramTable, pandaFramework,
                                         windowFramework):
    '''steerPlugInOneTurning'''

    aiComp = objectI.get_component(ComponentFamilyType('AI'))
    # # init libRmlUi
    G._steerPlugIns[_SteerPlugInType.one_turning] = aiComp
    _runInitOnce()


def steerPlugInPedestrian_initialization(objectI, paramTable, pandaFramework,
                                         windowFramework):
    '''steerPlugInPedestrian'''
    
    aiComp = objectI.get_component(ComponentFamilyType('AI'))
    # # init libRmlUi
    G._steerPlugIns[_SteerPlugInType.pedestrian] = aiComp
    _runInitOnce()


def steerPlugInBoid_initialization(objectI, paramTable, pandaFramework,
                                         windowFramework):
    '''steerPlugInBoid'''
    
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    elyAIMgr = GameAIManager.get_global_ptr()._elyMgr
    # tweak some parameter
    aiComp = objectI.get_component(ComponentFamilyType('AI'))
    # set world center/radius around G._WORLDCENTEROBJECT
    worldCenterObjectNP = objTmplMgr.get_created_object(
        ObjectId(G._WORLDCENTEROBJECT)).node_path
    modelDims = LVecBase3f()
    modelDeltaCenter = LVector3f() 
    elyAIMgr.utilities.get_bounding_dimensions(worldCenterObjectNP,
                                               modelDims, modelDeltaCenter)
    boidsPlugIn = aiComp.p3SteerPlugIn.node()
    boidsPlugIn.world_center = (worldCenterObjectNP.get_pos(pandaFramework.render) + 
                                LVector3f(0.0, 0.0, 1.5 * modelDims.get_z()))
    boidsPlugIn.world_radius = 1.5 * modelDims.get_z()
    # # init libRmlUi
    G._steerPlugIns[_SteerPlugInType.boid] = aiComp
    _runInitOnce()
 

def steerPlugInMultiplePursuit_initialization(objectI, paramTable, pandaFramework,
                                         windowFramework):
    '''steerPlugInMultiplePursuit'''
    
    elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
    aiComp = objectI.get_component(ComponentFamilyType('AI'))
    # # init libRmlUi
    G._steerPlugIns[_SteerPlugInType.multiple_pursuit] = aiComp
    _runInitOnce()
    # custom setup
    elyGUIMgr.register_gui_event_handler('steerPlugIn::multiple_pursuit::options',
                                         _rmluiEventHandler)
    elyGUIMgr.register_gui_commit_function(_rmluiMultiplePursuitCommit)
 

def steerPlugInSoccer_initialization(objectI, paramTable, pandaFramework,
                                         windowFramework):
    '''steerPlugInSoccer'''
    
    elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
    aiComp = objectI.get_component(ComponentFamilyType('AI'))
    # set soccer field
    micTestplugIn = aiComp.p3SteerPlugIn.node()
    pathwayPoints = micTestplugIn.pathway_points
    if len(pathwayPoints) >= 2:
        micTestplugIn.set_playing_field(pathwayPoints[0], pathwayPoints[1])
    # # init libRmlUi
    G._steerPlugIns[_SteerPlugInType.soccer] = aiComp
    _runInitOnce()
    # custom setup
    elyGUIMgr.register_gui_event_handler('steerPlugIn::soccer::options', _rmluiEventHandler)


def steerPlugInCtf_initialization(objectI, paramTable, pandaFramework,
                                         windowFramework):
    '''steerPlugInCtf'''
    
    elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
    aiComp = objectI.get_component(ComponentFamilyType('AI'))
    # set home base center
    ctfPlugIn = aiComp.p3SteerPlugIn.node()
    pathwayPoints = ctfPlugIn.pathway_points
    if len(pathwayPoints) >= 1:
        ctfPlugIn.home_base_center = pathwayPoints[0]
    # get default options
    G._ctfHomeBaseRadius = ctfPlugIn.home_base_radius  # 1.5
#     _ctfMinStartRadius = ctfPlugIn.m_CtfPlugInData.gMinStartRadius# 30.0 xxx
#     _ctfMaxStartRadius = ctfPlugIn.m_CtfPlugInData.gMaxStartRadius# 40.0 xxx
    G._ctfBrakingRate = ctfPlugIn.braking_rate  # 0.75
    G._ctfAvoidancePredictTimeMin = ctfPlugIn.avoidance_predict_time_min  # 0.9
    G._ctfAvoidancePredictTimeMax = ctfPlugIn.avoidance_predict_time_max  # 2.0
    # # init libRmlUi
    G._steerPlugIns[_SteerPlugInType.capture_the_flag] = aiComp
    _runInitOnce()
    # custom setup
    elyGUIMgr.register_gui_event_handler('steerPlugIn::capture_the_flag::options',
                                         _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('min_start_radius::change',
                                         _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('max_start_radius::change',
                                         _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('avoidance_predict_time_min::change',
                                         _rmluiEventHandler)
    elyGUIMgr.register_gui_event_handler('avoidance_predict_time_max::change',
                                         _rmluiEventHandler)
    elyGUIMgr.register_gui_commit_function(_rmluiCaptureTheFlagCommit)


def steerPlugInLST_initialization(objectI, paramTable, pandaFramework,
                                         windowFramework):
    '''steerPlugInLST'''
    
    elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
    aiComp = objectI.get_component(ComponentFamilyType('AI'))
    # set home base center
    lstPlugIn = aiComp.p3SteerPlugIn.node()
    # get default options
    G._lstSteeringSpeed = lstPlugIn.steering_speed  # 1.0
    # # init libRmlUi
    G._steerPlugIns[_SteerPlugInType.low_speed_turn] = aiComp
    _runInitOnce()
    # custom setup
    elyGUIMgr.register_gui_event_handler('steerPlugIn::low_speed_turn::options', _rmluiEventHandler)
    elyGUIMgr.register_gui_commit_function(_rmlUiLowSpeedTurnCommit)


def steerPlugInMapDrive_initialization(objectI, paramTable, pandaFramework,
                                         windowFramework):
    '''steerPlugInMapDrive'''
    
    elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    aiComp = objectI.get_component(ComponentFamilyType('AI'))
    # 
    mapDrivePlugIn = aiComp.p3SteerPlugIn.node()
    # get map dimensions and center
    minMaxX = [G._FLT_MAX, -G._FLT_MAX]  # min,max
    minMaxY = [G._FLT_MAX, -G._FLT_MAX]  # min,max
    mapCenter = LPoint3f(0, 0, 0)
    pathwayPoints = mapDrivePlugIn.pathway_points
    for point in pathwayPoints:
        if point.get_x() < minMaxX[0]:
            minMaxX[0] = point.get_x()
        if point.get_x() > minMaxX[1]:
            minMaxX[1] = point.get_x()
        # 
        if point.get_y() < minMaxY[0]:
            minMaxY[0] = point.get_y()
        if point.get_y() > minMaxY[1]:
            minMaxY[1] = point.get_y()
        # 
        mapCenter += point
    # take the middle point
    if len(pathwayPoints) > 0:
        mapCenter /= len(pathwayPoints)
    # set worldSize to max spread between dX and dY and resolution = 200
    mapDrivePlugIn.make_map()
    # # init libRmlUi
    G._steerPlugIns[_SteerPlugInType.map_drive] = aiComp
    _runInitOnce()
    # custom setup
    elyGUIMgr.register_gui_event_handler('steerPlugIn::map_drive::options',
                                         _rmluiEventHandler)
    elyGUIMgr.register_gui_commit_function(_rmlUiMapDriveCommit)
    # # setup debug drwing on texture
    # set the texture stage used for debug draw texture
    rttTexStage = TextureStage('rttTexStage')
    # collide mask of the model should be the same of GameAIManager
    environmentObj = objTmplMgr.get_created_object(
        ObjectId(G._ENVIRONMENTOBJECT))
    terrainNP = environmentObj.get_component(ComponentFamilyType('Scene')).node_path
    terrainNP.set_name('terrainNP')
    terrainNP.set_scale(256, 256, 1)
    tex = pandaFramework.loader.load_texture('dry-grass.png')
    terrainNP.set_texture(tex)
    # print debug draw to texture
    pandaFramework.accept('t', _debugDrawToTexture,
                          [mapDrivePlugIn, terrainNP, pandaFramework])
    pandaFramework.accept('debug_drawing_texture_ready', _onTextureReady,
                          [terrainNP, rttTexStage])


def steerVehicleToBeCloned_init(objectI, paramTable, pandaFramework,
                                         windowFramework):
    '''steerVehicleToBeCloned'''
    
    aiComp = objectI.get_component(ComponentFamilyType('AI'))
    # check if it is a soccer player
    player = aiComp.p3SteerVehicle.node()
    if player.vehicle_type == OSSteerVehicle.PLAYER:
        micTestplugIn = player.steer_plug_in
        if G._soccerActorSelected == _SoccerActor.player_teamA:
            micTestplugIn.add_player_to_team(player, OSSteerPlugIn.TEAM_A)
        else:
            micTestplugIn.add_player_to_team(player, OSSteerPlugIn.TEAM_B)


# #
initializations = {
    'steerPlugInOneTurning_initialization': steerPlugInOneTurning_initialization,
    'steerPlugInPedestrian_initialization': steerPlugInPedestrian_initialization,
    'steerPlugInBoid_initialization': steerPlugInBoid_initialization,
    'steerPlugInMultiplePursuit_initialization': steerPlugInMultiplePursuit_initialization,
    'steerPlugInSoccer_initialization': steerPlugInSoccer_initialization,
    'steerPlugInCtf_initialization': steerPlugInCtf_initialization,
    'steerPlugInLST_initialization': steerPlugInLST_initialization,
    'steerPlugInMapDrive_initialization': steerPlugInMapDrive_initialization,
    # common SteerVehicle initialization
    'steerVehicleToBeCloned_init': steerVehicleToBeCloned_init,
}
