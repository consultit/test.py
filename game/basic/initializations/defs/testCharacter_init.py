'''
Created on Oct 28, 2020

@author: consultit
'''

from COM.com.component import ComponentFamilyType, ComponentType
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectId
from ely.physics import BT3RigidBody
from ely.direct.animTransition import AnimTransitionParam

objTmplMgr = ObjectTemplateManager.get_global_ptr()


def _updateKinematic(testPlayer0Ghost, rigidBodyK, deltaPos, task):
    rigidBodyK.set_pos(testPlayer0Ghost.get_pos() + deltaPos)
    rigidBodyK.set_hpr(testPlayer0Ghost.get_hpr())
    print(round(rigidBodyK.node().linear_velocity.length(), 2))
    return task.cont


def probe_initialization(objectI, paramTable, pandaFramework, windowFramework):
    # set rigid body as kinematic
    probeRBK = objectI.get_component(
        ComponentFamilyType('Physics')).p3RigidBody
    probeRBK.node().body_type = BT3RigidBody.KINEMATIC
    testPlayer0Obj = objTmplMgr.get_created_object(ObjectId('testPlayer0'))
    testPlayer0Ghost = testPlayer0Obj.get_component(
        ComponentFamilyType('Physics')).p3Ghost
    deltaPos = probeRBK.get_pos() - testPlayer0Ghost.get_pos()
    pandaFramework.task_mgr.add(
        _updateKinematic, '_updateKinematic', appendTask=True,
        extraArgs=[testPlayer0Ghost, probeRBK, deltaPos])


def testPlayer0_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    # set the initial states
    multiActivity = objectI.get_component(ComponentFamilyType('Behavior'))
    # preform FSMs' initialization
    multiActivity.activities['fsm_FB'].fsm.request('FBS_idle', multiActivity)
    multiActivity.activities['fsm_LR'].fsm.request('LRS_idle', multiActivity)
    multiActivity.activities['fsm_SF'].fsm.request('SFS_slow', multiActivity)
    multiActivity.activities['fsm_Anim'].fsm.request('ANS_idle', multiActivity)
    # play sound
    npc1Sound3d = objectI.get_component(ComponentFamilyType('Audio'))
    npc1Sound3d.p3Sound3d.node().get_sound_by_name('walk-sound').set_loop(True)
    npc1Sound3d.p3Sound3d.node().get_sound_by_name('walk-sound').play()
    # set camera transform
    cameraObj = objTmplMgr.get_created_object(ObjectId('camera'))
    if cameraObj:
        cameraObj.node_path.set_pos(0.0, -130.0, 25.0)
        cameraObj.node_path.set_hpr(0.0, -7.5, 0.0)
    # add some AnimTransitionParams manually
    actorComp = objectI.get_component(ComponentFamilyType('Scene'))
    if actorComp.component_type == ComponentType('ActorModel'):
        # we use one only AnimTransition: we change the several parameters
        animTransParam = AnimTransitionParam(pandaFramework)
        #animTransParam.animFrom = default is good
        #animTransParam.animTo = default is good
        #animTransParam.animToFrame = default is good
        animTransParam.blendType = None
        animTransParam.frameBlend = None
        animTransParam.partName = None
        animTransParam.playRate = 1.0
        #animTransParam.playRateRatios = default is good
        animTransParam.usePose = True
        animTransParam.loop = True
        animTransParam.duration = 10
        #animTransParam.doneEvent = default is good
        # store the transition into the actor comp
        actorComp.anim_transitions['by-param'] = animTransParam


# #
initializations = {
    'testPlayer0_initialization': testPlayer0_initialization,
    'probe_initialization': probe_initialization,
}
