'''
Created on Oct 23, 2019

@author: consultit
'''

# raise if Ely not available
import panda3d.core
import ely.libtools

import sys
from COM.managers.gameGUIManager import GameGUIManager
from COM.tools.utilities import COMLogger


def _showMainMenu():
    GameGUIManager.get_global_ptr()._elyMgr.show_main_menu()

    
def _showExitMenu():
    GameGUIManager.get_global_ptr()._elyMgr.show_exit_menu()

      
def _rmluiEventHandler(event, value):
    '''event handler added to the main one'''
    if value == 'main.body.load_logo':
        COMLogger.info('main.body.load_logo')


def _on_exit_function():
    '''on exit function'''
    GameGUIManager.get_global_ptr()._elyMgr.gui_cleanup() 
    sys.exit(0)


def elyPreObjects_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    # start with mouse disabled
    pandaFramework.disable_mouse()
    #
    elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
    # register the add element function to gui (Rocket) main menu
    # register the event handler to gui main menu for each event value
    elyGUIMgr.register_gui_event_handler('main.body.load_logo', _rmluiEventHandler)
    # register the preset function to gui main menu
    # register the commit function to gui main menu
    elyGUIMgr.set_gui_on_exit_function(_on_exit_function)


def elyPostObjects_initialization(objectI, paramTable, pandaFramework,
                               windowFramework):
    # add show main menu event handler
    pandaFramework.accept('m', _showMainMenu)
    # handle 'close request' and 'esc' events
    pandaFramework.win.set_close_request_event('close_request_event')
    pandaFramework.accept('close_request_event', _showExitMenu)
    pandaFramework.accept('escape', _showExitMenu)


# #
initializations = {
    'elyPreObjects_initialization': elyPreObjects_initialization,
    'elyPostObjects_initialization': elyPostObjects_initialization,
}
