'''
Created on Nov 13, 2019

@author: consultit
'''

from COM.managers.gameManager import GameManager
from COM.tools.utilities import COMLogger
import basic.initializations.defs.OpenSteerPlugIn_globals as G
from panda3d.core import NodePath


def steerPluginsToggleDebug(event, plugIn):
    '''DEBUG DRAWING'''

    plugInId = id(plugIn)
    if plugInId not in G._debugDrawing:
        G._debugDrawing[plugInId] = False
    
    gameMgr = GameManager.get_global_ptr()
    # invert debug drawing
    G._debugDrawing[plugInId] = not G._debugDrawing[plugInId]
    if G._debugDrawing[plugInId]:
        # enable
        plugIn.p3SteerPlugIn.node().enable_debug_drawing(gameMgr.camera)
        plugIn.p3SteerPlugIn.node().toggle_debug_drawing(G._debugDrawing[plugInId])
    else:
        # disable
        plugIn.p3SteerPlugIn.node().toggle_debug_drawing(G._debugDrawing[plugInId])
        plugIn.p3SteerPlugIn.node().disable_debug_drawing()


def handleVehicleEvent(*args):
    '''handle vehicle's events'''
    
    COMLogger.info(' | '.join([str(param) for param in args]))
    

# #
callbacks = {
    'steerPluginsToggleDebug': steerPluginsToggleDebug,
    'handleVehicleEvent': handleVehicleEvent,
}
