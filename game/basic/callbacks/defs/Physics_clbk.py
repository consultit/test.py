'''
Created on Nov 8, 2019

@author: consultit
'''

from COM.com.object import ObjectId
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.tools.utilities import COMLogger


# # ghost overlap
def canGhostOverlap(event, ghost, p3PhysicComp, p3GhostComp):
    # get character object
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    objId0 = ObjectId(p3PhysicComp.get_tag('_owner_object'))
    objId1 = ObjectId(p3GhostComp.get_tag('_owner_object'))
    object0 = objTmplMgr.get_created_object(objId0)
    object1 = objTmplMgr.get_created_object(objId1)
    COMLogger.info('got ' + event + ' between ' + str(object0) + ' (' + 
              str(p3PhysicComp) + ') and ' + str(object1) + ' (' + 
              str(p3GhostComp) + ') handled by ' + str(ghost))


# # SimpleCar + Vehicle related
def forward_Vehicle_SimpleCar(event, vehicle):
    vehicle.p3Vehicle.node().move_forward = True


def stop_forward_Vehicle_SimpleCar(event, vehicle):
    vehicle.p3Vehicle.node().move_forward = False


def backward_Vehicle_SimpleCar(event, vehicle):
    vehicle.p3Vehicle.node().move_backward = True


def stop_backward_Vehicle_SimpleCar(event, vehicle):
    vehicle.p3Vehicle.node().move_backward = False


def turn_left_Vehicle_SimpleCar(event, vehicle):
    vehicle.p3Vehicle.node().rotate_head_left = True


def stop_turn_left_Vehicle_SimpleCar(event, vehicle):
    vehicle.p3Vehicle.node().rotate_head_left = False


def turn_right_Vehicle_SimpleCar(event, vehicle):
    vehicle.p3Vehicle.node().rotate_head_right = True


def stop_turn_right_Vehicle_SimpleCar(event, vehicle):
    vehicle.p3Vehicle.node().rotate_head_right = False


def brake_Vehicle_SimpleCar(event, vehicle):
    vehicle.p3Vehicle.node().brake = True


def stop_brake_Vehicle_SimpleCar(event, vehicle):
    vehicle.p3Vehicle.node().brake = False


def vehicleEventNotify(event, vehicle, p3VerhicleComp):
    COMLogger.info('got ' + event + ' from ' + str(p3VerhicleComp) + 
                   ' handled by ' + str(vehicle))


# # SimplePlayer + CharacterController related
def forward_CharacterController_SimplePlayer(event, characterController):
    characterController.p3CharacterController.node().move_forward = True


def stop_forward_CharacterController_SimplePlayer(event, characterController):
    characterController.p3CharacterController.node().move_forward = False


def backward_CharacterController_SimplePlayer(event, characterController):
    characterController.p3CharacterController.node().move_backward = True


def stop_backward_CharacterController_SimplePlayer(event, characterController):
    characterController.p3CharacterController.node().move_backward = False


def turn_left_CharacterController_SimplePlayer(event, characterController):
    characterController.p3CharacterController.node().rotate_head_left = True


def stop_turn_left_CharacterController_SimplePlayer(event, characterController):
    characterController.p3CharacterController.node().rotate_head_left = False


def turn_right_CharacterController_SimplePlayer(event, characterController):
    characterController.p3CharacterController.node().rotate_head_right = True


def stop_turn_right_CharacterController_SimplePlayer(event, characterController):
    characterController.p3CharacterController.node().rotate_head_right = False


def jump_CharacterController_SimplePlayer(event, characterController):
    characterController.p3CharacterController.node().jump = True


def stop_jump_CharacterController_SimplePlayer(event, characterController):
    characterController.p3CharacterController.node().jump = False


def characterControllerEventNotify(event, characterController,
                                   p3CharacterControllerComp):
    COMLogger.info('got ' + event + ' from ' + str(p3CharacterControllerComp) + 
                   ' handled by ' + str(characterController))


# #
callbacks = {
    'canGhostOverlap': canGhostOverlap,
    'forward_Vehicle_SimpleCar': forward_Vehicle_SimpleCar,
    'stop_forward_Vehicle_SimpleCar':stop_forward_Vehicle_SimpleCar,
    'backward_Vehicle_SimpleCar':backward_Vehicle_SimpleCar,
    'stop_backward_Vehicle_SimpleCar':stop_backward_Vehicle_SimpleCar,
    'turn_left_Vehicle_SimpleCar':turn_left_Vehicle_SimpleCar,
    'stop_turn_left_Vehicle_SimpleCar':stop_turn_left_Vehicle_SimpleCar,
    'turn_right_Vehicle_SimpleCar':turn_right_Vehicle_SimpleCar,
    'stop_turn_right_Vehicle_SimpleCar':stop_turn_right_Vehicle_SimpleCar,
    'brake_Vehicle_SimpleCar':brake_Vehicle_SimpleCar,
    'stop_brake_Vehicle_SimpleCar':stop_brake_Vehicle_SimpleCar,
    'vehicleEventNotify':vehicleEventNotify,
    'forward_CharacterController_SimplePlayer': forward_CharacterController_SimplePlayer,
    'stop_forward_CharacterController_SimplePlayer':stop_forward_CharacterController_SimplePlayer,
    'backward_CharacterController_SimplePlayer':backward_CharacterController_SimplePlayer,
    'stop_backward_CharacterController_SimplePlayer':stop_backward_CharacterController_SimplePlayer,
    'turn_left_CharacterController_SimplePlayer':turn_left_CharacterController_SimplePlayer,
    'stop_turn_left_CharacterController_SimplePlayer':stop_turn_left_CharacterController_SimplePlayer,
    'turn_right_CharacterController_SimplePlayer':turn_right_CharacterController_SimplePlayer,
    'stop_turn_right_CharacterController_SimplePlayer':stop_turn_right_CharacterController_SimplePlayer,
    'jump_CharacterController_SimplePlayer':jump_CharacterController_SimplePlayer,
    'stop_jump_CharacterController_SimplePlayer':stop_jump_CharacterController_SimplePlayer,
    'characterControllerEventNotify':characterControllerEventNotify,
}
