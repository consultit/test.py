'''
Created on Feb 28, 2020

@author: consultit
'''

from COM.components.behavior.activity import StateEventTypePair, \
    State
from COM.tools.utilities import Event, COMLogger

# # Character + MultiActivity related functions/variables
# Transition table: <eventType, currentState> -> nextState

activityNames = {
    'FB':'fsm_FB',
    'LR':'fsm_LR',
    'SF':'fsm_SF',
    'AN':'fsm_Anim',
    }


def multiActivityTestPlayer0(event, multiActivity):
    # get data
    # get fsm and transition table through type conversions
    # get current_state,event_type@next_state
    eventType = multiActivity.get_event_type(Event(event))
    # retrieve the name of the activity
    activityName = activityNames[eventType.value[:2]]
    activity = multiActivity.activities[activityName]
    currentState = activity.fsm.getCurrentOrNextState()
    nextState = activity.transition_table.get(
        StateEventTypePair(currentState, eventType), State()).value
    # make transition
    if nextState:
        COMLogger.info(activityName + ': (' + currentState + ',' + 
                       str(eventType) + ') -> ' + nextState)
        activity.fsm.request(nextState, multiActivity)
    else:
        COMLogger.info(activityName + ': (' + currentState + ',' + 
                       str(eventType) + ') -> TRANSITION UNDEFINED')


# #
callbacks = {
    'multiActivityTestPlayer0': multiActivityTestPlayer0,
}
