'''
Created on Nov 1, 2019

@author: consultit
'''


# # HUD3d + Sound3d related
def fire_Sound3d_HUD3d(event, sound3d):
    '''on fire play sound'''
    # play sound
    sound3d.p3Sound3d.node().get_sound_by_name('fire').play()


# #
callbacks = {
    'fire_Sound3d_HUD3d': fire_Sound3d_HUD3d,
}
        
