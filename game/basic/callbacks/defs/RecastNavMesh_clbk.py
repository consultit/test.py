'''
Created on Nov 2, 2019

@author: consultit
'''

from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectId
from COM.com.component import ComponentType
from COM.managers.gamePhysicsManager import GamePhysicsManager
from COM.managers.gameAIManager import GameAIManager
from COM.managers.gameManager import GameManager
from COM.tools.utilities import COMLogger, StringSequence
from panda3d.core import NodePath

_TOBECLONEDOBJECT = 'crowdAgentToBeCloned'
_debugDrawing = False


# # RecastNavMesh + NavMesh related CALLBACKs
def add_crowd_agent_NavMesh_RecastNavMesh(event, navMesh):
    global _TOBECLONEDOBJECT
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    elyPhysicsMgr = GamePhysicsManager.get_global_ptr()._elyMgr
    # get object to be cloned
    toBeClonedObject = objTmplMgr.get_created_object(
        ObjectId(_TOBECLONEDOBJECT))
    if not toBeClonedObject:
        return
    # get NavMesh ObjectId
    navMeshObjectId = navMesh.object_id
    # get the object-to-be-cloned parameter table
    objParams = toBeClonedObject.stored_obj_tmpl_params
    # get the object-to-be-cloned components' parameter tables
    compParams = toBeClonedObject.stored_comp_tmpl_params
    # get position under mouse pointer (using physics)
    if not elyPhysicsMgr.is_picking_enabled:
        COMLogger.info('Enable picking to add CrowdAgent(s)!')
        return
    if elyPhysicsMgr.picking_ray_cast():
        hitPos = elyPhysicsMgr.hit_ray_cast_pos
    else:
        return
    # # create the clone
    # tweak clone object's parameters
    # set clone object store_params
    objParams.pop('store_params', None)
    objParams['store_params'] = StringSequence([ 'False'])
    # set clone object pos
    objParams.pop('pos', None)
    COMLogger.info('Hit position: ' + str(hitPos))
    objParams['pos'] = StringSequence([str(hitPos.x) + ',' + str(hitPos.y) + ','
                                       +str(hitPos.z)])
    # tweak clone components' parameter tables
    # set CrowdAgent add_to_plugin
    compParams['CrowdAgent'].pop('add_to_navmesh', None)
    compParams['CrowdAgent']['add_to_navmesh'] = StringSequence([navMeshObjectId])
    # create actually the clone
    clone = objTmplMgr.create_object(toBeClonedObject.object_type, ObjectId(),
                             objParams, compParams, False)
    # ...initialize it
    clone.world_setup()


def remove_crowd_agent_NavMesh_RecastNavMesh(event, navMesh):
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    elyPhysicsMgr = GamePhysicsManager.get_global_ptr()._elyMgr
    # get object under mouse pointer
    if not elyPhysicsMgr.is_picking_enabled:
        COMLogger.info('Enable picking to add CrowdAgent(s)!')
        return
    physicsComp = NodePath.any_path(elyPhysicsMgr.picking_ray_cast())
    if physicsComp:
        hitObjectId = ObjectId(physicsComp.get_tag('_owner_object'))
        hitObject = objTmplMgr.get_created_object(hitObjectId)
        aiComp = hitObject.get_component(ComponentType('CrowdAgent'))
        # check if it is has a CrowdAgent component
        # and try to remove this CrowdAgent from this NavMesh
        if aiComp and navMesh.p3NavMesh.node().remove_crowd_agent(aiComp.p3CrowdAgent):
            # the CrowdAgent belonged to this NavMesh:
            # remove actually the clone
            objTmplMgr.destroy_object(hitObjectId)


def navMeshesToggleDebug(event, navMesh):
    '''DEBUG DRAWING'''
    
    global _debugDrawing
    elyAIMgr = GameAIManager.get_global_ptr()._elyMgr
    pandaFramework = GameManager.get_global_ptr()
    # invert debug drawing
    _debugDrawing = not _debugDrawing
    if _debugDrawing:
        # enable
        elyAIMgr.reference_node_path_debug.reparent_to(pandaFramework.render)
        navMesh.p3NavMesh.node().enable_debug_drawing(pandaFramework.camera)
        navMesh.p3NavMesh.node().toggle_debug_drawing(_debugDrawing)
    else:
        # disable
        navMesh.p3NavMesh.node().toggle_debug_drawing(_debugDrawing)
        navMesh.p3NavMesh.node().disable_debug_drawing()
        elyAIMgr.reference_node_path_debug.reparent_to(NodePath())

    
# #
callbacks = {
    'add_crowd_agent_NavMesh_RecastNavMesh': add_crowd_agent_NavMesh_RecastNavMesh,
    'remove_crowd_agent_NavMesh_RecastNavMesh': remove_crowd_agent_NavMesh_RecastNavMesh,
    'navMeshesToggleDebug': navMeshesToggleDebug,
}

