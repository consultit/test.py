'''
Created on Oct 28, 2019

@author: consultit
'''

from COM.components.behavior.activity import StateEventTypePair, \
    State
from COM.com.object import ObjectId
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.component import ComponentFamilyType
from COM.tools.utilities import Event, COMLogger

# f:f_up:f_q
# b:b_up:b_q
# sr:sr_up:sr_q
# sl:sl_up:sl_q
# rr:rr_up:rr_q
# rl:rl_up:rl_q
# j:j_up:j_q
# q:q_up
# ground:air


# # Character + Activity related functions/variables
# Transition table: <eventType, currentState> -> nextState
def activityPlayer0(event, activity):
    # get data
    # get fsm and transition table through type conversions
    # get current_state,event_type@next_state
    eventType = activity.get_event_type(Event(event))
    currentState = activity.fsm.getCurrentOrNextState()
    nextState = activity.transition_table.get(
        StateEventTypePair(currentState, eventType), State()).value
    # make transition
    if nextState:
        COMLogger.info('(' + currentState + ',' + str(eventType) + ') -> ' + 
                       nextState)
        activity.fsm.request(nextState, activity)
    else:
        COMLogger.info(
            'activityPlayer0: Transition not defined for event-type \'' + 
                       str(eventType) + '\' and state \'' + currentState + '\'')

    
def groundAirPlayer0(event, activity, b3CharacterController):
    # get character object
    characterObjId = ObjectId(b3CharacterController.get_tag('_owner_object'))
    characterObj = ObjectTemplateManager.get_global_ptr().get_created_object(
        characterObjId)
    characterModel = characterObj.get_component(ComponentFamilyType('Scene'))
    if event == 'OnGround':
        # stop animation
        characterModel.animations.stop('run')
    elif event == 'InAir':
        # play animation
        characterModel.animations.loop('run', True)
    else:
        COMLogger.info('groundAirPlayer0: unrecognized event')


# #
callbacks = {
    'activityPlayer0': activityPlayer0,
    'groundAirPlayer0': groundAirPlayer0,
}
