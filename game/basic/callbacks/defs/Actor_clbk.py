'''
Created on Oct 26, 2019

@author: consultit
'''

# raise if Ely not available
import panda3d.core
import ely.libtools

from COM.com.component import ComponentFamilyType

# #Actor + Activity related CALLBACKNAMEs & CALLBACKs
# fast:stop-fast
_isFast = False


def _update_fast_actor(actorDrv, factor):
    global _isFast
    actorDrv.max_linear_speed = actorDrv.max_linear_speed * factor
    actorDrv.max_angular_speed = actorDrv.max_angular_speed * factor
    actorDrv.linear_accel = actorDrv.linear_accel * factor
    actorDrv.angular_accel = actorDrv.angular_accel * factor
    sens = actorDrv.sens
    actorDrv.sens = [sens[0] * factor, sens[1] * factor]
    _isFast = not _isFast


def fast_Activity_Actor(event, activity):
    global _isFast
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    if not _isFast:
        speedFactor = driver.p3Driver.node().fast_factor
        _update_fast_actor(driver.p3Driver.node(), speedFactor)
    

def stop_fast_Activity_Actor(event, activity):
    global _isFast
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    if _isFast:
        speedFactorInv = 1.0 / driver.p3Driver.node().fast_factor
        _update_fast_actor(driver.p3Driver.node(), speedFactorInv)
    

# forward:stop-forward:fast-forward
def forward_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().move_forward = True
    activity.fsm.request('Forward', activity)


def stop_forward_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().move_forward = False


def fast_forward_Activity_Actor(event, activity):
    fast_Activity_Actor(event, activity)
    forward_Activity_Actor(event, activity)


# backward:stop-backward:fast-backward
def backward_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().move_backward = True
    activity.fsm.request('Backward', activity)


def stop_backward_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().move_backward = False


def fast_backward_Activity_Actor(event, activity):
    fast_Activity_Actor(event, activity)
    backward_Activity_Actor(event, activity)


# head_left:stop-head_left:fast-head_left
def head_left_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().rotate_head_left = True
    activity.fsm.request('Head_left', activity)


def stop_head_left_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().rotate_head_left = False


def fast_head_left_Activity_Actor(event, activity):
    fast_Activity_Actor(event, activity)
    head_left_Activity_Actor(event, activity)


# head_right:stop-head_right:fast-head_right
def head_right_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().rotate_head_right = True
    activity.fsm.request('Head_right', activity)


def stop_head_right_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().rotate_head_right = False


def fast_head_right_Activity_Actor(event, activity):
    fast_Activity_Actor(event, activity)
    head_right_Activity_Actor(event, activity)


# up:stop-up:fast-up
def up_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().move_up = True
    activity.fsm.request('Up', activity)


def stop_up_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().move_up = False


def fast_up_Activity_Actor(event, activity):
    fast_Activity_Actor(event, activity)
    up_Activity_Actor(event, activity)


# down:stop-down:fast-down
def down_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().move_down = True
    activity.fsm.request('Down', activity)


def stop_down_Activity_Actor(event, activity):
    # get driver
    driver = activity.owner_object.get_component(ComponentFamilyType('Control'))
    driver.p3Driver.node().move_down = False


def fast_down_Activity_Actor(event, activity):
    fast_Activity_Actor(event, activity)
    down_Activity_Actor(event, activity)


# #
callbacks = {
    'fast_Activity_Actor': fast_Activity_Actor,
    'stop_fast_Activity_Actor': stop_fast_Activity_Actor,
    'forward_Activity_Actor': forward_Activity_Actor,
    'stop_forward_Activity_Actor': stop_forward_Activity_Actor,
    'fast_forward_Activity_Actor': fast_forward_Activity_Actor,
    'backward_Activity_Actor': backward_Activity_Actor,
    'stop_backward_Activity_Actor': stop_backward_Activity_Actor,
    'fast_backward_Activity_Actor': fast_backward_Activity_Actor,
    'head_left_Activity_Actor': head_left_Activity_Actor,
    'stop_head_left_Activity_Actor': stop_head_left_Activity_Actor,
    'fast_head_left_Activity_Actor': fast_head_left_Activity_Actor,
    'head_right_Activity_Actor': head_right_Activity_Actor,
    'stop_head_right_Activity_Actor': stop_head_right_Activity_Actor,
    'fast_head_right_Activity_Actor': fast_head_right_Activity_Actor,
    'up_Activity_Actor': up_Activity_Actor,
    'stop_up_Activity_Actor': stop_up_Activity_Actor,
    'fast_up_Activity_Actor': fast_up_Activity_Actor,
    'down_Activity_Actor': down_Activity_Actor,
    'stop_down_Activity_Actor': stop_down_Activity_Actor,
    'fast_down_Activity_Actor': fast_down_Activity_Actor,
}
