'''
Created on Oct 22, 2019

@author: consultit
'''

from importlib import util
import os
global _this_library_dir

# Game_clbk
spec = util.spec_from_file_location('Game_clbk',
            os.path.join(_this_library_dir, 'defs', 'Game_clbk.py'))
Game_clbk = util.module_from_spec(spec)
spec.loader.exec_module(Game_clbk)

# Camera_clbk
spec = util.spec_from_file_location('Camera_clbk',
            os.path.join(_this_library_dir, 'defs', 'Camera_clbk.py'))
Camera_clbk = util.module_from_spec(spec)
spec.loader.exec_module(Camera_clbk)

# Actor_clbk
spec = util.spec_from_file_location('Actor_clbk',
            os.path.join(_this_library_dir, 'defs', 'Actor_clbk.py'))
Actor_clbk = util.module_from_spec(spec)
spec.loader.exec_module(Actor_clbk)

# Character_clbk
spec = util.spec_from_file_location('Character_clbk',
            os.path.join(_this_library_dir, 'defs', 'Character_clbk.py'))
Character_clbk = util.module_from_spec(spec)
spec.loader.exec_module(Character_clbk)

# HUD_clbk
spec = util.spec_from_file_location('HUD_clbk',
            os.path.join(_this_library_dir, 'defs', 'HUD_clbk.py'))
HUD_clbk = util.module_from_spec(spec)
spec.loader.exec_module(HUD_clbk)

# RecastNavMesh_clbk
spec = util.spec_from_file_location('RecastNavMesh_clbk',
            os.path.join(_this_library_dir, 'defs', 'RecastNavMesh_clbk.py'))
RecastNavMesh_clbk = util.module_from_spec(spec)
spec.loader.exec_module(RecastNavMesh_clbk)

# Physics_clbk
spec = util.spec_from_file_location('Physics_clbk',
            os.path.join(_this_library_dir, 'defs', 'Physics_clbk.py'))
Physics_clbk = util.module_from_spec(spec)
spec.loader.exec_module(Physics_clbk)

# OpenSteerPlugIn_clbk
spec = util.spec_from_file_location('OpenSteerPlugIn_clbk',
            os.path.join(_this_library_dir, 'defs', 'OpenSteerPlugIn_clbk.py'))
OpenSteerPlugIn_clbk = util.module_from_spec(spec)
spec.loader.exec_module(OpenSteerPlugIn_clbk)

# testCharacter_clbk
spec = util.spec_from_file_location('testCharacter_clbk',
            os.path.join(_this_library_dir, 'defs', 'testCharacter_clbk.py'))
testCharacter_clbk = util.module_from_spec(spec)
spec.loader.exec_module(testCharacter_clbk)

# callbacks definitions
callbacks = {
    'default_callback__':
                    lambda event, *args: print('Got ', event,
                        ' handled by default_callback__() for ', *args, sep=''),
    **Game_clbk.callbacks,
    **Camera_clbk.callbacks,
    **Actor_clbk.callbacks,
    **Character_clbk.callbacks,
    **HUD_clbk.callbacks,
    **RecastNavMesh_clbk.callbacks,
    **Physics_clbk.callbacks,
    **OpenSteerPlugIn_clbk.callbacks,
    **testCharacter_clbk.callbacks,
}
