'''
Created on Aug 30, 2019

@author: consultit
'''

# HACK TO BE REMOVED IN PRODUCTION
import sys
import os
scriptPath = os.path.dirname(os.path.abspath(__file__))
test_pyPath = os.path.join(scriptPath, '..')
sys.path.append(test_pyPath)
#

from COM.com.componentTemplateManager import ComponentTemplateManager
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.managers.gameManager import GameManager
from COM.managers.gameDataInfo import GameDataInfo
from COM.managers.gameGUIManager import GameGUIManager
from COM.managers.gameAIManager import GameAIManager
from COM.managers.gameAudioManager import GameAudioManager
from COM.managers.gameBehaviorManager import GameBehaviorManager
from COM.managers.gameControlManager import GameControlManager
from COM.managers.gamePhysicsManager import GamePhysicsManager
from COM.managers.gameSceneManager import GameSceneManager
from COM.tools.utilities import COMLogger
from panda3d.core import GeomNode, load_prc_file_data, load_prc_file, \
    Filename, BitMask32
import json
import logging
import argparse
import textwrap

if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This is the Ely game.
    
    Note: pass '-O' option to python interpreter to ignore assertion tests.
    '''))
    # set up arguments
    parser.add_argument('-g', '--game-world', type=str, action='append',
                        help='the (ordered) xml/json game world file list')
    parser.add_argument('-c', '--config-prc', type=str, default='config.prc',
                        help='the config.prc file')
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    parser.add_argument('--callbacks', type=str, default='callbacks.py',
                        help='the file containing the callbacks dictionary')
    parser.add_argument('--initializations', type=str, default='initializations.py',
                        help='the file containing the initializations dictionary')
    parser.add_argument('--instance-updates', type=str, default='instance_updates.py',
                        help='the file containing the instance updates dictionary')
    parser.add_argument('--transitions', type=str, default='transitions.py',
                        help='the file containing the transitions dictionary')
    parser.add_argument('--to-json', action='store_true',
                        help='convert to json the xml file and exits')
    parser.add_argument('--log-level', choices=['debug', 'info', 'warning', 'error', 'critical'],
                        default='info', help='the log level of messages printed to output')
    rpGroup = parser.add_argument_group('RenderPipeline',
                                        'RenderPipeline related arguments')
    rpGroup.add_argument('-r', '--render-pipeline', action='store_true',
                         help='use RenderPipeline')
    rpGroup.add_argument('-t', '--day-time', type=str, default='20:15',
                         help='the day time to use')
    # parse arguments
    args = parser.parse_args()
    # do actions
    ELY_CONFIGFILE = args.game_world
    ELY_DATADIRS = args.data_dir
    ELY_CONFIGPRC = args.config_prc
    ELY_CALLBACKS_LA = args.callbacks
    ELY_INITIALIZATIONS_LA = args.initializations
    ELY_INSTANCEUPDATES_LA = args.instance_updates
    ELY_TRANSITIONS_LA = args.transitions
    #
    if args.to_json:
        gameDoc, isJson = GameManager._read_configuration_file(ELY_CONFIGFILE)
        if not isJson:
            jsonFile = open(ELY_CONFIGFILE.rstrip('.xml') + '.json', 'w')
            json.dump(gameDoc, jsonFile)
            jsonFile.close()
        exit(0)
    # set data dir(s)
    if ELY_DATADIRS:
        for dataDir in ELY_DATADIRS:
            load_prc_file_data('', 'model-path ' + dataDir)
    # set log level
    if args.log_level == 'debug':
        COMLogger.setLevel(logging.DEBUG)
    elif args.log_level == 'warning':
        COMLogger.setLevel(logging.WARNING)
    elif args.log_level == 'error':
        COMLogger.setLevel(logging.ERROR)
    elif args.log_level == 'critical':
        COMLogger.setLevel(logging.CRITICAL)
    else:
        COMLogger.setLevel(logging.INFO)
    # Load your configuration
    load_prc_file(Filename(ELY_CONFIGPRC))

    # Setup the game framework
    # ComponentTemplate manager
    componentTmplMgr = ComponentTemplateManager()
    # ObjectTemplate manager
    objectTmplMgr = ObjectTemplateManager()
    # First create a Game manager: mandatory
    gameMgr = GameManager()
    # Then create a Game GUI manager
    gameGUIMgr = GameGUIManager(gameMgr.win, gameMgr.mouseWatcher)
    # Add game data info
    gameMgr.data_info[GameDataInfo.CONFIG_FILE] = ','.join(ELY_CONFIGFILE)
    gameMgr.data_info[GameDataInfo.DATA_DIRS] = ','.join(ELY_DATADIRS)
    gameMgr.data_info[GameDataInfo.CALLBACKS] = ELY_CALLBACKS_LA
    gameMgr.data_info[GameDataInfo.INITIALIZATIONS] = ELY_INITIALIZATIONS_LA
    gameMgr.data_info[GameDataInfo.INSTANCE_UPDATES] = ELY_INSTANCEUPDATES_LA
    gameMgr.data_info[GameDataInfo.TRANSITIONS] = ELY_TRANSITIONS_LA

    # Other managers (depending on GameManager)
    # AI
    gameAIMgr = GameAIManager(app=gameMgr, sort=10, priority=0,
                              asyncTaskChain=None,
                              mask=BitMask32(0x10))
    # Audio
    gameAudioMgr = GameAudioManager(app=gameMgr, sort=10, priority=0,
                                    asyncTaskChain=None,
                                    mask=GeomNode.get_default_collide_mask())
    # Behavior
    gameBehaviorMgr = GameBehaviorManager(app=gameMgr, sort=10, priority=0,
                                          asyncTaskChain=None)
    # Control
    gameControlMgr = GameControlManager(app=gameMgr, sort=10, priority=0,
                                        asyncTaskChain=None,
                                        mask=GeomNode.get_default_collide_mask())
    # Scene
    gameSceneMgr = GameSceneManager(app=gameMgr, sort=10, priority=0,
                                    asyncTaskChain=None)
    # Physics
    gamePhysicsMgr = GamePhysicsManager(app=gameMgr, sort=10, priority=0,
                                        asyncTaskChain=None,
                                        groupMask=GeomNode.get_default_collide_mask(),
                                        collideMask=GeomNode.get_default_collide_mask())

    # Set the game up
    gameMgr.game_setup()

    # Do the main loop
    gameMgr.run()
