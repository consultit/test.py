import inspect
import random

this_frame = inspect.currentframe

class lazy_property(object):
    def __init__(self, function):
        print(this_frame().f_code.co_name)
        self.fget = function

    def __get__(self, obj, cls):
        print(this_frame().f_code.co_name)
        value = self.fget(obj)
        setattr(obj, self.fget.__name__, value)
        return value

class WithSortedRandoms:
    @lazy_property
    def lazily_initialized(self):
        print(this_frame().f_code.co_name)
        return sorted([[random.random() for _ in range(5)]])

if __name__ == '__main__':
    wsr = WithSortedRandoms()
    wsr.lazily_initialized
    wsr.lazily_initialized
