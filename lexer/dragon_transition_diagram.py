'''
Created on Jun 11, 2019

@author: consultit
'''

from enum import Enum, IntEnum


class LexicalUnit(Enum):
    IF = 0
    THEN = 1
    ELSE = 2
    ID = 3
    NUMBER = 4
    RELOP = 5


class TOKEN(object):
    
    def __init__(self):
        self.name = None
        self.attribute = None
        
    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self, name):
        if isinstance(name, LexicalUnit) or (name == None):
            self._name = name
        else:
            print(str(name) + ' is not an instance of ' + str(LexicalUnit) + 
                  ' or of a subclass thereof.')

state = 0

def nextChar():
    pass


def retract():
    pass


def fail():
    pass


# Relp stuff
RelopAttr = Enum('RelopAttr', 'LT LE EQ NE GT GE')


def getRelop():
    global state
    retToken = TOKEN(LexicalUnit.RELOP)
    while True:
        if state == 0:
            c = nextChar()
            if c == '<':
                state = 1
            elif c == '=':
                state = 5
            elif c == '>':
                state = 6
            else:
                fail()
        elif state == 1:
            pass
        elif state == 2:
            pass
        elif state == 3:
            pass
        elif state == 4:
            pass
        elif state == 5:
            pass
        elif state == 6:
            pass
        elif state == 7:
            pass
        elif state == 8:
            retract()
            retToken.attribute = RelopAttr.GT
            return retToken

if __name__ == '__main__':
    pass
