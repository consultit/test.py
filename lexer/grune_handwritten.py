'''
Created on Jun 14, 2018

@author: consultit
'''

import sys, enum, argparse, textwrap

class TokenClass(enum.Enum):
    '''
    Define class constants; 0 − 255 reserved for ASCII characters.
    '''

    EoF = 256
    IDENTIFIER = 257
    INTEGER = 258
    ERRONEOUS = 259

class PositionInFile(object):
    '''
    Position in File.
    '''
    
    def __init__(self):
        '''
        Constructor.
        '''
        
        self.file_name = ''
        self.line_number = 0
        self.char_number = 0


class TokenType(object):
    '''
    Token type.
    '''

    def __init__(self):
        '''
        Constructor.
        '''
        
        self.tkClass = None
        self.repr = ''
        self.pos = PositionInFile()


class Lexer(object):
    '''
    Lexical analyzer written by hand.
    
    See "D. Grune, K. van Reeuwijk, H.E. Bal, C.J.H. Jacobs, K. Langendoen, 
        2.5 Creating a lexical analyzer by hand, in: Modern Compiler Design, 2nd ed., 
        Springer, New York, 2012: pp. 65–72."
        
    Lexer's token descriptions:
        identifier -> [a-zA-Z][a-zA-Z0-9]*(_[a-zA-Z0-9]+)*
        integer -> [0-9][0-9]*
        operator-or-separator -> '+'|'-'|'*'|'/'|';'|','|'('|')'|'{'|'}'
        ERRONEOUS -> <ERRONEOUS>
        EoF -> <EoF>
    Comments are defined by this token description:
         comment -> '#'[^#\n]*('#'|'\n')       
    '''

    def __init__(self, filename):
        '''
        Lexer reads input from a file or stdin.
        '''
        
        if filename:
            self.input_file = open(filename, 'rb')
            self.file_name = filename
        else:
            self.input_file = sys.stdin.buffer
            self.file_name = 'stdin'
        self.Token = TokenType()
        
    def start_lex(self):
        '''
        Starts the machine.
        '''
        
        self.input = self.get_input()
        self.dot = 0
        self.input_char = self.input[self.dot]
        self.line_number = 1
        self.char_number = 1
        
    def get_next_token(self):
        '''
        Scans the input to obtain the next token.
        ''' 
        
        self.skip_layout_and_comment()
        # now we are at the start of a token or at end−of−file, so:
        self.note_token_position()
        # split on first character of the token
        start_dot = self.dot
        if self.is_end_of_input(self.input_char):
            self.Token.tkClass= TokenClass.EoF
            self.Token.repr = '<EoF>'
            return
        if self.is_letter(self.input_char):
            self.recognize_identifier()
        elif self.is_digit(self.input_char):
            self.recognize_integer()
        elif self.is_operator(self.input_char) or self.is_separator(self.input_char):
            self.Token.tkClass = self.input_char
            self.next_char()
        else:
            self.Token.tkClass = TokenClass.ERRONEOUS
            self.next_char()
            
        self.Token.repr = self.input_to_zstring(start_dot, self.dot - start_dot)
        
    def skip_layout_and_comment(self):
        '''
        Skips layout characters and comments.
        '''
        
        while self.is_layout(self.input_char):
            self.next_char()
        while self.is_comment_starter(self.input_char):
            self.next_char()
            while not self.is_comment_stopper(self.input_char):
                if self.is_end_of_input(self.input_char):
                    return
                self.next_char()
            self.next_char()
            while self.is_layout(self.input_char):
                self.next_char()
            
    def recognize_identifier(self):
        '''
        Recognizes identifier token class.
        '''
        
        self.Token.tkClass = TokenClass.IDENTIFIER
        self.next_char()
        while self.is_letter_or_digit(self.input_char):
            self.next_char()
        while self.is_underscore(self.input_char) and ((self.dot + 1) < len(self.input)) and self.is_letter_or_digit(self.input[self.dot + 1]):
            self.next_char()
            while self.is_letter_or_digit(self.input_char):
                self.next_char()
 
    def recognize_integer(self):
        '''
        Recognizes integer token class.
        '''
        
        self.Token.tkClass = TokenClass.INTEGER
        self.next_char()
        while self.is_digit(self.input_char):
            self.next_char()
        
    def get_input(self):
        '''
        Returns the input (character list).
        '''
        
        return [chr(b) for b in self.input_file.read()]
    
    def next_char(self):
        '''
        Sets next char.
        '''
        
        self.dot += 1
        if self.dot < len(self.input):
            self.input_char = self.input[self.dot]
            if self.input_char == '\n':
                self.line_number += 1
                self.char_number = 0
            else:
                self.char_number += 1
        else:
            self.dot = -1
    
    def note_token_position(self):
        '''
        Records the position in the file of the token.
        '''
        
        self.Token.pos.file_name = self.file_name
        self.Token.pos.line_number = self.line_number
        self.Token.pos.char_number = self.char_number
        
    def input_to_zstring(self, start, lenght):
        '''
        Converts the chunk of the input which forms the token into a string.
        '''
        
        return str().join(self.input[start:start + lenght])
    
    def is_end_of_input(self, ch): return self.dot < 0
    def is_layout(self, ch): return (not self.is_end_of_input(ch)) and (ord(ch) <= ord(' '))
    def is_comment_starter(self, ch): return ch == '#'
    def is_comment_stopper(self, ch): return (ch == '#') or (ch == '\n')
    
    def is_uc_letter(self, ch): return (ord('A') <= ord(ch)) and (ord(ch) <= ord('Z'))
    def is_lc_letter(self, ch): return (ord('a') <= ord(ch)) and (ord(ch) <= ord('z'))
    def is_letter(self, ch): return self.is_uc_letter(ch) or self.is_lc_letter(ch)
    def is_digit(self, ch): return (ord('0') <= ord(ch)) and (ord(ch) <= ord('9'))
    def is_letter_or_digit(self, ch): return self.is_letter(ch) or self.is_digit(ch)
    def is_underscore(self, ch): return ch == '_'
    
    def is_operator(self, ch): return ch in '+ − */'
    def is_separator(self, ch): return ch in ';,(){}'
        
if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    This script will run the handwritten lexical analyzer.
    '''))
    # set up arguments
    parser.add_argument('-f', '--filename', type=str, help='file to be lexically analyzed')
    # parse arguments
    args = parser.parse_args()
    # do actions
    lex = Lexer(args.filename)
    lex.start_lex()
    while True:
        lex.get_next_token()
        if lex.Token.tkClass == TokenClass.IDENTIFIER: 
            print('Identifier', end='')
        elif lex.Token.tkClass == TokenClass.INTEGER:
            print ('Integer', end='')
        elif lex.Token.tkClass == TokenClass.ERRONEOUS: 
            print('Erroneous token', end='')
        elif lex.Token.tkClass == TokenClass.EoF:
            print('End-of-file pseudo-token', end='')
        else:
            print('Operator or separator', end='')
        pos = lex.Token.pos
        print(': ' + repr(lex.Token.repr) + 
              '    (file: ' + pos.file_name + 
              ', line nr: ' + str(pos.line_number) + ', char nr: ' + str(pos.char_number) + ')')
        if lex.Token.tkClass == TokenClass.EoF:
            break
