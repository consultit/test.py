'''
Created on Dec 26, 2018

@author: consultit
'''

import sys, argparse, textwrap
import enum

class RE(object):
    '''
    R(egular) E(xpression).
    '''

    class Kind(enum.IntEnum):
        
        EMPTY = -1
        CHAR = 0  # x
        ANYCHAR = 1  # .
        ANYOFCHAR = 2  # [xyz...]
        BASIC = 3
        OPTIONAL = 100  # R?
        ZEROMORE = 101  # R*
        ONEMORE = 102  # R+
        CONCATENATE = 103  # R1R2
        ALTERNATE = 104  # R1|R2
        GROUPING = 105  # (R)
        NOT = 106  # ^R
        COMPOSED = 1000

    def __init__(self, pattern, reKind=None):
        self.__pattern = pattern
        self.__kind = reKind

    def __add__(self, other):
        try:
            return RE(self.__pattern + other.__pattern)
        except:
            return RE(self.__pattern)
    
    def __iadd__(self, other):
        try:
            self.__pattern += other.__pattern
        except:
            pass
        return self
        
    @property
    def pattern(self):
        return self.__pattern
    
    @property
    def kind(self):
        return self.__kind
    
    @property
    def isBasic(self):
        return self.__kind < RE.Kind.BASIC

class TokenClass(object):
    '''
    Token Class.
    '''

    def __init__(self, value, descr=None):
        self.__value = value
        self.__descr = descr
        
    @property
    def value(self):
        return self.__value

    @property
    def descr(self):
        return self.__descr

class TokenDescription(object):
    '''
    Token Description.
    '''

    def __init__(self, tokenClass, re):
        self.__tkClass = tokenClass
        self.__re = re

    @property
    def tkClass(self):
        return self.__tkClass

    @property
    def re(self):
        return self.__re

class Item(object):
    '''
    (Dotted) Item.
    '''
    
    class Kind(enum.IntEnum):
        
        SHIFT = 0
        REDUCE = 1
        NONBASIC = 2

    def __init__(self, tokenDescr, dotPos, itemKind):
        self.__tkDescr = tokenDescr
        self.__dotPos = dotPos
        self.__kind = itemKind

    @property
    def tkDescr(self):
        return self.__tkDescr

    @property
    def leftStr(self):
        return self.__tkDescr.re.pattern[:self.__dotPos]

    @property
    def rightStr(self):
        return self.__tkDescr.re.pattern[self.__dotPos:]
    
    @property
    def kind(self):
        return self.__kind

class Token(object):
    '''
    Token.
    '''

    def __init__(self):
        self.Class = None
#         self.repr = ''
#         self.pos = PositionInFile()

class PositionInFile(object):
    '''
    Position in File.
    '''
    
    def __init__(self):
        self.file_name = ''
        self.line_number = 0
        self.char_number = 0

class Lexer(object):
    '''
    Lexical analyzer.
    
    See "D. Grune, K. van Reeuwijk, H.E. Bal, C.J.H. Jacobs, K. Langendoen: 
        Modern Compiler Design, 2nd ed., Springer, New York, 2012."
        
    Lexer's token descriptions:
        identifier -> [a-zA-Z][a-zA-Z0-9]*(_[a-zA-Z0-9]+)*
        integer -> [0-9][0-9]*
        operator-or-separator -> "+"|"-"|"*"|"/"|";"|","|"("|")"|"{"|"}"
        EoF -> <EoF>
        comment -> "#"[^#\n]*("#"|"\n")
        whitespace -> " "|"\t"
        ERRONEOUS -> <ERRONEOUS>
    '''

    class TkClassValues(enum.IntEnum):
        
        NOCLASS = -1
        identifier = 0
        integer = 1
        operator_or_separator = 2
        EoF = 3
        comment = 4
        whitespace = 5
        ERRONEOUS = 6    

    def __init__(self, filename):
        '''
        Lexer reads inputChar from a file or stdin.
        '''
        
        if filename:
            self.input_file = open(filename, 'rb')
            self.file_name = filename
        else:
            self.input_file = sys.stdin.buffer
            self.file_name = 'stdin'
        self.token = Token()
#         # token descriptions indexed by token's class
#         self.tokenDescriptions = {}
#         # items indexed by token's class
#         self.Items = {}
        # initialize all data structures
        self.initialize()
    
    def initialize(self):
        '''
        Initialize all data structures.

            identifier -> °[a-zA-Z]°(°[a-zA-Z0-9]°)*°(°_°(°[a-zA-Z0-9]°)+°)*°
            integer -> °[0-9]°(°[0-9]°)*°
            operator-or-separator -> °(°"+"°|°"-"°|°"*"°|°"/"°|°";"°|°","°|°"("°|°")"°|°"{"°|°"}"°)°
            EoF -> °<EoF>°
            comment -> °"#"°(°[^#\n]°)*°(°"#"°|°"\n"°)°
            whitespace -> °(°" "°|°"\t"°)°
            ERRONEOUS -> °<ERRONEOUS>°
        '''
        # create all basic REs used in all descriptions
        # identifier -> [a-zA-Z][a-zA-Z0-9]*(_[a-zA-Z0-9]+)*
        # identifier -> °[a-zA-Z]°(°[a-zA-Z0-9]°)*°(°_°(°[a-zA-Z0-9]°)+°)*°
        identifierClass = TokenClass(Lexer.TkClassValues.identifier, 'identifier')
        identifierRE = RE('[a-zA-Z]([a-zA-Z0-9])*(_([a-zA-Z0-9])+)*')
        identifierDescr = TokenDescription(identifierClass, identifierRE)
        self.identifierItems = [
            Item(identifierDescr, 0, Item.Kind.SHIFT),
            Item(identifierDescr, 8, Item.Kind.NONBASIC),
            Item(identifierDescr, 9, Item.Kind.SHIFT),
            Item(identifierDescr, 20, Item.Kind.NONBASIC),
            Item(identifierDescr, 22, Item.Kind.NONBASIC),
            Item(identifierDescr, 23, Item.Kind.SHIFT),
            Item(identifierDescr, 24, Item.Kind.NONBASIC),
            Item(identifierDescr, 25, Item.Kind.SHIFT),
            Item(identifierDescr, 36, Item.Kind.NONBASIC),
            Item(identifierDescr, 38, Item.Kind.NONBASIC),
            Item(identifierDescr, 40, Item.Kind.REDUCE),
            ]
        # integer -> [0-9][0-9]*
        # integer -> °[0-9]°(°[0-9]°)*°
        integerClass = TokenClass(Lexer.TkClassValues.integer, 'integer')
        integerRE = RE('[0-9]([0-9])*')
        integerDescr = TokenDescription(integerClass, integerRE)
        self.integerItems = [
            Item(integerDescr, 0, Item.Kind.SHIFT),
            Item(integerDescr, 5, Item.Kind.NONBASIC),
            Item(integerDescr, 6, Item.Kind.SHIFT),
            Item(integerDescr, 11, Item.Kind.NONBASIC),
            Item(integerDescr, 13, Item.Kind.REDUCE),
            ]
        # operator-or-separator -> "+"|"-"|"*"|"/"|";"|","|"("|")"|"{"|"}"
        # operator-or-separator -> °(°"+"°|°"-"°|°"*"°|°"/"°|°";"°|°","°|°"("°|°")"°|°"{"°|°"}"°)°
        opOrSepClass = TokenClass(Lexer.TkClassValues.opOrSep, 'operator-or-separator')
        opOrSepRE = RE('("+"|"-"|"*"|"/"|";"|","|"("|")"|"{"|"}")')
        opOrSepDescr = TokenDescription(opOrSepClass, opOrSepRE)
        self.opOrSepItems = [
            Item(opOrSepDescr, 0, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 1, Item.Kind.SHIFT),
            Item(opOrSepDescr, 4, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 5, Item.Kind.SHIFT),
            Item(opOrSepDescr, 8, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 9, Item.Kind.SHIFT),
            Item(opOrSepDescr, 12, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 13, Item.Kind.SHIFT),
            Item(opOrSepDescr, 16, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 17, Item.Kind.SHIFT),
            Item(opOrSepDescr, 20, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 21, Item.Kind.SHIFT),
            Item(opOrSepDescr, 24, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 25, Item.Kind.SHIFT),
            Item(opOrSepDescr, 28, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 29, Item.Kind.SHIFT),
            Item(opOrSepDescr, 32, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 33, Item.Kind.SHIFT),
            Item(opOrSepDescr, 35, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 36, Item.Kind.SHIFT),
            Item(opOrSepDescr, 39, Item.Kind.NONBASIC),
            Item(opOrSepDescr, 40, Item.Kind.REDUCE),
            ]
        # EoF -> <EoF>
        # EoF -> °<EoF>°
        eofClass = TokenClass(Lexer.TkClassValues.eof, 'EoF')
        eofRE = RE('<EoF>')
        eofDescr = TokenDescription(eofClass, eofRE)
        self.eofItems = [
            Item(eofDescr, 0, Item.Kind.SHIFT),
            Item(eofDescr, 6, Item.Kind.REDUCE),
            ]
        # comment -> "#"[^#\n]*("#"|"\n")
        # comment -> °"#"°(°[^#\n]°)*°(°"#"°|°"\n"°)°
        commentClass = TokenClass(Lexer.TkClassValues.comment, 'comment')
        commentRE = RE('"#"([^#\n])*("#"|"\n")')
        commentDescr = TokenDescription(commentClass, commentRE)
        self.commentItems = [
            Item(commentDescr, 0, Item.Kind.SHIFT),
            Item(commentDescr, 4, Item.Kind.NONBASIC),
            Item(commentDescr, 5, Item.Kind.SHIFT),
            Item(commentDescr, 11, Item.Kind.NONBASIC),
            Item(commentDescr, 13, Item.Kind.NONBASIC),
            Item(commentDescr, 14, Item.Kind.SHIFT),
            Item(commentDescr, 17, Item.Kind.NONBASIC),
            Item(commentDescr, 18, Item.Kind.SHIFT),
            Item(commentDescr, 21, Item.Kind.NONBASIC),
            Item(commentDescr, 22, Item.Kind.REDUCE),
           ]
        # whitespace -> " "|"\t"
        # whitespace -> °(°" "°|°"\t"°)°
        whitespaceClass = TokenClass(Lexer.TkClassValues.whitespace, 'whitespace')
        whitespaceRE = RE('(" "|"\t")')
        whitespaceDescr = TokenDescription(whitespaceClass, whitespaceRE)
        self.whitespaceItems = [
            Item(whitespaceDescr, 0, Item.Kind.NONBASIC),
            Item(whitespaceDescr, 1, Item.Kind.SHIFT),
            Item(whitespaceDescr, 4, Item.Kind.NONBASIC),
            Item(whitespaceDescr, 5, Item.Kind.SHIFT),
            Item(whitespaceDescr, 8, Item.Kind.NONBASIC),
            Item(whitespaceDescr, 9, Item.Kind.REDUCE),
           ]
        # ERRONEOUS -> <ERRONEOUS>
        # ERRONEOUS -> °<ERRONEOUS>°
        erroneousClass = TokenClass(Lexer.TkClassValues.erroneous, 'ERRONEOUS')
        erroneousRE = RE('<ERRONEOUS>')
        erroneousDescr = TokenDescription(erroneousClass, erroneousRE)
        self.erroneousItems = [
            Item(erroneousDescr, 0, Item.Kind.SHIFT),
            Item(erroneousDescr, 12, Item.Kind.REDUCE),
            ]
        
    def startLex(self):
        '''
        Starts the machine.
        '''
        
        self.inputChar = self.__getInput()
        self.readIndex = 0
#         self.dot = 0
#         self.input_char = self.inputChar[self.dot]
        self.line_number = 1
        self.char_number = 1
        
    def getNextToken(self):
        '''
        Scans the inputChar to obtain the next token.
        ''' 

        sStartOfToken = self.readIndex
        endOfLastToken = None 
        classOfLastToken = None
        
        itemSet = self.initialItemSet()
        while itemSet:
            ch = self.inputChar[self.readIndex]
            itemSet = self.nextItemSet(itemSet, ch)
            tkClass = self.classOfTokenRecognizedIn(itemSet)
            if tkClass != Lexer.TkClassValues.NOCLASS:
                classOfLastToken = tkClass
                endOfLastToken = self.readIndex
            self.readIndex += 1
        
        self.Token.Class = classOfLastToken
        self.Token.repr = self.inputChar[sStartOfToken:endOfLastToken]
        self.readIndex += 1
            
#         self.skip_layout_and_comment()
#         # now we are at the start of a token or at end−of−file, so:
#         self.noteTokenPosition()
#         # split on first character of the token
#         start_dot = self.dot
#         if self.is_end_of_input(self.input_char):
#             self.token.Class = Token.Class.EoF
#             self.token.repr = '<EoF>'
#             return
#         if self.is_letter(self.input_char):
#             self.recognize_identifier()
#         elif self.is_digit(self.input_char):
#             self.recognize_integer()
#         elif self.is_operator(self.input_char) or self.is_separator(self.input_char):
#             self.token.Class = self.input_char
#             self.next_char()
#         else:
#             self.token.Class = Token.Class.ERRONEOUS
#             self.next_char()
#             
#         self.token.repr = self.inputToZstring(start_dot, self.dot - start_dot)
        
    def skip_layout_and_comment(self):
        '''
        Skips layout characters and comments.
        '''
        
        while self.is_layout(self.input_char):
            self.next_char()
        while self.is_comment_starter(self.input_char):
            self.next_char()
            while not self.is_comment_stopper(self.input_char):
                if self.is_end_of_input(self.input_char):
                    return
                self.next_char()
            self.next_char()
            while self.is_layout(self.input_char):
                self.next_char()
            
    def recognize_identifier(self):
        '''
        Recognizes identifier token class.
        '''
        
        self.token.Class = Token.Class.IDENTIFIER
        self.next_char()
        while self.is_letter_or_digit(self.input_char):
            self.next_char()
        while self.is_underscore(self.input_char) and ((self.dot + 1) < len(self.inputChar)) and self.is_letter_or_digit(self.inputChar[self.dot + 1]):
            self.next_char()
            while self.is_letter_or_digit(self.input_char):
                self.next_char()
 
    def recognize_integer(self):
        '''
        Recognizes integer token class.
        '''
        
        self.token.Class = Token.Class.INTEGER
        self.next_char()
        while self.is_digit(self.input_char):
            self.next_char()
        
    def __getInput(self):
        '''
        Returns the inputChar (character list).
        '''
        
        return [chr(b) for b in self.input_file.read()]
    
    def next_char(self):
        '''
        Sets next char.
        '''
        
        self.dot += 1
        if self.dot < len(self.inputChar):
            self.input_char = self.inputChar[self.dot]
            if self.input_char == '\n':
                self.line_number += 1
                self.char_number = 0
            else:
                self.char_number += 1
        else:
            self.dot = -1
    
    def noteTokenPosition(self):
        '''
        Records the position in the file of the token.
        '''
        
        self.token.pos.file_name = self.file_name
        self.token.pos.line_number = self.line_number
        self.token.pos.char_number = self.char_number
        
    def inputToZstring(self, start, lenght):
        '''
        Converts the chunk of the inputChar which forms the token into a string.
        '''
        
        return str().join(self.inputChar[start:start + lenght])
    
    def is_end_of_input(self, ch): return self.dot < 0

    def is_layout(self, ch): return (not self.is_end_of_input(ch)) and (ord(ch) <= ord(' '))

    def is_comment_starter(self, ch): return ch == '#'

    def is_comment_stopper(self, ch): return (ch == '#') or (ch == '\n')
    
    def is_uc_letter(self, ch): return (ord('A') <= ord(ch)) and (ord(ch) <= ord('Z'))

    def is_lc_letter(self, ch): return (ord('a') <= ord(ch)) and (ord(ch) <= ord('z'))

    def is_letter(self, ch): return self.is_uc_letter(ch) or self.is_lc_letter(ch)

    def is_digit(self, ch): return (ord('0') <= ord(ch)) and (ord(ch) <= ord('9'))

    def is_letter_or_digit(self, ch): return self.is_letter(ch) or self.is_digit(ch)

    def is_underscore(self, ch): return ch == '_'
    
    def is_operator(self, ch): return ch in '+ − */'

    def is_separator(self, ch): return ch in ';,(){}'
        
if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    This script will run the handwritten lexical analyzer.
    '''))
    # set up arguments
    parser.add_argument('-f', '--filename', type=str, help='file to be lexically analyzed')
    # parse arguments
    args = parser.parse_args()
    # do actions
    lex = Lexer(args.filename)
    lex.startLex()
#     while True:
#         lex.getNextToken()
#         if lex.token.Class == Token.Class.IDENTIFIER: 
#             print('Identifier', end='')
#         elif lex.token.Class == Token.Class.INTEGER:
#             print ('Integer', end='')
#         elif lex.token.Class == Token.Class.ERRONEOUS: 
#             print('Erroneous token', end='')
#         elif lex.token.Class == Token.Class.EoF:
#             print('End-of-file pseudo-token', end='')
#         else:
#             print('Operator or separator', end='')
#         pos = lex.token.pos
#         print(': ' + repr(lex.token.repr) + 
#               '    (file: ' + pos.file_name + 
#               ', line nr: ' + str(pos.line_number) + ', char nr: ' + str(pos.char_number) + ')')
#         if lex.token.Class == Token.Class.EoF:
#             break
