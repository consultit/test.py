'''
Created on Nov 29, 2018

@author: consultit
'''

import sys, enum, argparse, textwrap

class TokenClass(enum.IntEnum):
    '''
    Define class constants; 0 − 255 reserved for ASCII characters.
    '''

    NOCLASS = -1
    identifier = 0
    integer    = 1
    operator_or_separator = 2
    EoF        = 3
    comment    = 4
    whitespace = 5
    ERRONEOUS  = 6

class PositionInFile(object):
    '''
    Position in File.
    '''
    
    def __init__(self):
        '''
        Constructor.
        '''
        
        self.file_name = ''
        self.line_number = 0
        self.char_number = 0


class TokenType(object):
    '''
    Token type.
    '''

    def __init__(self):
        '''
        Constructor.
        '''
        
        self.tkClass = TokenClass.NOCLASS
        self.repr = str()
        self.pos = PositionInFile()
        self.length = 0

class Lexer(object):
    '''
    Lexical analyzer naively automatically generated.
    
    See "D. Grune, K. van Reeuwijk, H.E. Bal, C.J.H. Jacobs, K. Langendoen, 
        2.5 Creating a lexical analyzer by hand, in: Modern Compiler Design, 2nd ed., 
        Springer, New York, 2012: pp. 73–74."
        
    Lexer's token descriptions:
        identifier -> [a-zA-Z][a-zA-Z0-9]*(_[a-zA-Z0-9]+)*
        integer    -> [0-9][0-9]*
        operator-or-separator -> '+'|'-'|'*'|'/'|';'|','|'('|')'|'{'|'}'
        EoF        -> <EoF>
        comment    -> '#'[^#\n]*('#'|'\n')
        whitespace -> [all ASCII < Space]+
        ERRONEOUS  -> <ERRONEOUS>
    '''

    def __init__(self, filename):
        '''
        Lexer reads input from a file or stdin.
        '''
        
        if filename:
            self.input_file = open(filename, 'rb')
            self.file_name = filename
        else:
            self.input_file = sys.stdin.buffer
            self.file_name = 'stdin'
        self.Token = TokenType()
        self.Length = 0
        
    def start_lex(self):
        '''
        Starts the machine.
        '''
        
        self.input = self.get_input()
        self.dot = 0
        self.input_char = self.input[self.dot]
        self.line_number = 1
        self.char_number = 1
        
    def get_next_token(self):
        '''
        Scans the input to obtain the next token.
        ''' 
        
        self.Token.length = 0
        self.start_dot = self.dot
        self.start_line_number = self.line_number
        self.start_char_number = self.char_number
        ## Try to match token description 'identifier -> [a-zA-Z][a-zA-Z0-9]*(_[a-zA-Z0-9]+)*' :
        Length = self.try_to_match_token_description_identifier()
        if Length > self.Token.length:
            (self.Token.tkClass, self.Token.length, self.Token.repr) = (
                TokenClass.identifier , Length, self.input_to_zstring(self.start_dot, self.dot - self.start_dot))
        ## Try to match token description 'integer    -> [0-9][0-9]*' :
        Length = self.try_to_match_token_description_integer()
        if Length > self.Token.length:
            (self.Token.tkClass, self.Token.length, self.Token.repr) = (
                TokenClass.integer , Length, self.input_to_zstring(self.start_dot, self.dot - self.start_dot))
        ## Try to match token description 'operator-or-separator -> '+'|'-'|'*'|'/'|';'|','|'('|')'|'{'|'}'' :
        Length = self.try_to_match_token_description_operator_or_separator()
        if Length > self.Token.length:
            (self.Token.tkClass, self.Token.length, self.Token.repr) = (
                TokenClass.operator_or_separator , Length, self.input_to_zstring(self.start_dot, self.dot - self.start_dot))
        ## Try to match token description 'EoF        -> <EoF>' :
        Length = self.try_to_match_token_description_EoF()
        if Length > self.Token.length:
            (self.Token.tkClass, self.Token.length, self.Token.repr) = (
                TokenClass.EoF , Length, self.input_to_zstring(self.start_dot, self.dot - self.start_dot))
        ## Try to match token description 'comment    -> '#'[^#\n]*('#'|'\n')' :
        Length = self.try_to_match_token_description_comment()
        if Length > self.Token.length:
            (self.Token.tkClass, self.Token.length, self.Token.repr) = (
                TokenClass.comment , Length, self.input_to_zstring(self.start_dot, self.dot - self.start_dot))
        ## Try to match token description 'whitespace -> [ \t\n]+' :
        Length = self.try_to_match_token_description_whitespace()
        if Length > self.Token.length:
            (self.Token.tkClass, self.Token.length, self.Token.repr) = (
                TokenClass.whitespace , Length, self.input_to_zstring(self.start_dot, self.dot - self.start_dot))
        ## Try to match token description 'ERRONEOUS  -> <ERRONEOUS>' :
        Length = self.try_to_match_token_description_ERRONEOUS()
        if Length > self.Token.length:
            (self.Token.tkClass, self.Token.length, self.Token.repr) = (
                TokenClass.ERRONEOUS , Length, self.input_to_zstring(self.start_dot, self.dot - self.start_dot))
        # update input position
        self.reset_input()
        for i in range(self.Token.length):
            self.next_char()
        
    def try_to_match_token_description_identifier(self):
        length = 0
        self.reset_input()
        self.note_token_position()
        while self.is_letter_or_digit(self.input_char):
            length += 1
            self.next_char()
            while self.is_underscore(self.input_char) and ((self.dot + 1) < len(self.input)) and self.is_letter_or_digit(self.input[self.dot + 1]):
                length += 1
                self.next_char()
                while self.is_letter_or_digit(self.input_char):
                    length += 1
                    self.next_char()
        return length
            
    def try_to_match_token_description_integer(self):
        length = 0
        self.reset_input()
        self.note_token_position()
        while self.is_digit(self.input_char):
            length += 1
            self.next_char()
        return length
        
    def try_to_match_token_description_operator_or_separator(self):
        length = 0
        self.reset_input()
        self.note_token_position()
        if self.is_operator(self.input_char) or self.is_separator(self.input_char):
            length += 1
            self.next_char()
        return length

    def try_to_match_token_description_EoF(self):
        length = 0
        self.reset_input()
        self.note_token_position()
        if self.is_end_of_input(self.input_char):
            length += 1
            self.next_char()
        return length

    def try_to_match_token_description_comment(self):
        length = 0
        self.reset_input()
        self.note_token_position()
        if self.is_comment_starter(self.input_char):
            length += 1
            self.next_char()
            while not self.is_comment_stopper(self.input_char):
                if self.is_end_of_input(self.input_char):
                    return
                length += 1
                self.next_char()
            length += 1
            self.next_char()
        return length

    def try_to_match_token_description_whitespace(self):
        length = 0
        self.reset_input()
        self.note_token_position()
        while self.is_layout(self.input_char):
            length += 1
            self.next_char()
        return length

    def try_to_match_token_description_ERRONEOUS(self):
        length = 0
        self.reset_input()
        self.note_token_position()
        length += 1
        self.next_char()
        return length
    
    def reset_input(self):
        self.dot = self.start_dot
        self.line_number = self.start_line_number
        self.char_number = self.start_char_number
        self.input_char = self.input[self.dot]
        
    def get_input(self):
        '''
        Returns the input (character list).
        '''
        
        return [chr(b) for b in self.input_file.read()]
    
    def next_char(self):
        '''
        Sets next char.
        '''
        
        self.dot += 1
        if self.dot < len(self.input):
            self.input_char = self.input[self.dot]
            if self.input_char == '\n':
                self.line_number += 1
                self.char_number = 0
            else:
                self.char_number += 1
        else:
            self.dot = -1
    
    def note_token_position(self):
        '''
        Records the position in the file of the token.
        '''
        
        self.Token.pos.file_name = self.file_name
        self.Token.pos.line_number = self.line_number
        self.Token.pos.char_number = self.char_number
        
    def input_to_zstring(self, start, lenght):
        '''
        Converts the chunk of the input which forms the token into a string.
        '''
        
        return str().join(self.input[start:start + lenght])
    
    def is_end_of_input(self, ch): return self.dot < 0
    def is_layout(self, ch): return (not self.is_end_of_input(ch)) and (ord(ch) <= ord(' '))
    def is_comment_starter(self, ch): return ch == '#'
    def is_comment_stopper(self, ch): return (ch == '#') or (ch == '\n')
    
    def is_uc_letter(self, ch): return (ord('A') <= ord(ch)) and (ord(ch) <= ord('Z'))
    def is_lc_letter(self, ch): return (ord('a') <= ord(ch)) and (ord(ch) <= ord('z'))
    def is_letter(self, ch): return self.is_uc_letter(ch) or self.is_lc_letter(ch)
    def is_digit(self, ch): return (ord('0') <= ord(ch)) and (ord(ch) <= ord('9'))
    def is_letter_or_digit(self, ch): return self.is_letter(ch) or self.is_digit(ch)
    def is_underscore(self, ch): return ch == '_'
    
    def is_operator(self, ch): return ch in '+−*/'
    def is_separator(self, ch): return ch in ';,(){}'
        
if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    This script will run the automatically generated lexical analyzer in naive way.
    '''))
    # set up arguments
    parser.add_argument('-f', '--filename', type=str, help='file to be lexically analyzed')
    # parse arguments
    args = parser.parse_args()
    # do actions
    lex = Lexer(args.filename)
    lex.start_lex()
    while True:
        lex.get_next_token()
        if lex.Token.tkClass == TokenClass.identifier: 
            print('identifier', end='')
        elif lex.Token.tkClass == TokenClass.integer:
            print ('integer', end='')
        elif lex.Token.tkClass == TokenClass.operator_or_separator:
            print ('operator-or-separator', end='')
        elif lex.Token.tkClass == TokenClass.EoF:
            print ('EoF', end='')
        elif lex.Token.tkClass == TokenClass.comment:
            print ('comment', end='')
        elif lex.Token.tkClass == TokenClass.whitespace:
            print ('whitespace', end='')
        elif lex.Token.tkClass == TokenClass.ERRONEOUS:
            print ('ERRONEOUS', end='')
        else:
            print('UNEXPECTED', end='')
        pos = lex.Token.pos
        print(': ' + repr(lex.Token.repr) + 
              '    (file: ' + pos.file_name + 
              ', line nr: ' + str(pos.line_number) + ', char nr: ' + str(pos.char_number) + ')')
        if lex.Token.tkClass == TokenClass.EoF:
            break
