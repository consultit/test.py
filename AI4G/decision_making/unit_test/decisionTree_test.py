'''
Created on Jun 18, 2019

@author: consultit
'''

import unittest
from AI4G.decision_making.decisionTree import InsideClosedInterval, GTE, \
    OutsideClosedInterval, InsideOpenInterval, OutsideOpenInterval, LTE, LT, GT, \
    InstanceMethod, Not, Action, Decision
from common.random import RandFloat, RandInt32, MAXFLOAT


def setUpModule():
    pass

    
def tearDownModule():
    pass


class DecisionTreeTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testNumericIntervals(self):
        m, M = -10.0, 10.0
        # m <= v <= M
        self.assertTrue(InsideClosedInterval(0.0, m, M))  # -10.0 <= 0.0 <= 10.0 (T)
        self.assertTrue(InsideClosedInterval(m, m, M))  # -10.0 <= -10.0 <= 10.0 (T)
        self.assertTrue(InsideClosedInterval(M, m, M))  # -10.0 <= 10.0 <= 10.0 (T)
        self.assertFalse(InsideClosedInterval(-20.0, m, M))  # -10.0 <= -20.0 <= 10.0 (F)
        self.assertFalse(InsideClosedInterval(20.0, m, M))  # -10.0 <= 20.0 <= 10.0 (F)
        # (v < m) or (v > M)
        self.assertTrue(OutsideClosedInterval(-20.0, m, M))  # (-20.0 < -10.0) or (-20.0 > 10.0) (T)
        self.assertTrue(OutsideClosedInterval(20.0, m, M))  # (20.0 < -10.0) or (20.0 > 10.0) (T)
        self.assertFalse(OutsideClosedInterval(0.0, m, M))  # (0.0 < -10.0) or (0.0 > 10.0) (F)
        self.assertFalse(OutsideClosedInterval(M, m, M))  # (10.0 < -10.0) or (10.0 > 10.0) (F)
        self.assertFalse(OutsideClosedInterval(m, m, M))  # (-10.0 < -10.0) or (-10.0 > 10.0) (F)
        # m < v < M
        self.assertTrue(InsideOpenInterval(0.0, m, M))  # -10.0 < 0.0 < 10.0 (T)
        self.assertFalse(InsideOpenInterval(M, m, M))  # -10.0 < 10.0 < 10.0 (F)
        self.assertFalse(InsideOpenInterval(m, m, M))  # -10.0 < -10.0 < 10.0 (F)
        self.assertFalse(InsideOpenInterval(-20.0, m, M))  # -10.0 < -20.0 < 10.0 (F)
        self.assertFalse(InsideOpenInterval(20.0, m, M))  # -10.0 < 20.0 < 10.0 (F)
        # (v <= m) or (v >= M)
        self.assertTrue(OutsideOpenInterval(-20.0, m, M))  # (-20.0 <= -10.0) or (-20.0 >= 10.0) (T)
        self.assertTrue(OutsideOpenInterval(20.0, m, M))  # (20.0 <= -10.0) or (20.0 >= 10.0) (T)
        self.assertTrue(OutsideOpenInterval(M, m, M))  # (10.0 <= -10.0) or (10.0 >= 10.0) (T)
        self.assertTrue(OutsideOpenInterval(m, m, M))  # (-10.0 <= -10.0) or (-10.0 >= 10.0) (T)
        self.assertFalse(OutsideOpenInterval(0.0, m, M))  # (0.0 <= -10.0) or (0.0 >= 10.0) (F)
        # v >= m
        self.assertTrue(GTE(0.0, m))  # 0.0 >= -10.0 (T)
        self.assertTrue(GTE(m, m))  # -10.0 >= -10.0 (T)
        self.assertFalse(GTE(-20.0, m))  # -20.0 >= -10.0 (F)
        # v > m
        self.assertTrue(GT(0.0, m))  # 0.0 > -10.0 (T)
        self.assertFalse(GT(m, m))  # -10.0 > -10.0 (F)
        self.assertFalse(GT(-20.0, m))  # -20.0 > -10.0 (F)
        # v <= M
        self.assertTrue(LTE(0.0, M))  # 0.0 <= 10.0 (T)
        self.assertTrue(LTE(M, M))  # 10.0 <= 10.0 (T)
        self.assertFalse(LTE(20.0, M))  # 20.0 <= 10.0 (F)
        # v < M
        self.assertTrue(LT(0.0, M))  # 0.0 < 10.0 (T)
        self.assertFalse(LT(M, M))  # 10.0 < 10.0 (F)
        self.assertFalse(LT(20.0, M))  # 20.0 < 10.0 (F)

    def testInstanceMethods(self):
        
        class C(object):
            
            def false(self, arg, kwarg=0):
                return False

            def true(self, arg, kwarg=0):
                return True
            
        # test methods
        c = C()
        self.assertTrue(InstanceMethod(c, 'true', 1, kwarg=1))
        self.assertFalse(Not(InstanceMethod(c, 'true', 1, kwarg=1)))
        self.assertFalse(InstanceMethod(c, 'false', 1, kwarg=1))
        self.assertTrue(Not(InstanceMethod(c, 'false', 1, kwarg=1)))

    def testDecisionTree(self):
        '''
        test: see Figure 5.3, p. 301 of 'AI for Games' book
        '''

        class Player(object):
            
            def __init__(self, pos):
                self._pos = pos
                      
            def getPos(self):
                return self._pos
            
            def setPos(self, pos):
                self._pos = pos
        
        class Enemy(object):
            
            def __init__(self, pos, onFlank, maxDistance):
                self._pos = pos
                self._onFlank = onFlank
                self._maxDistance = maxDistance
            
            def getPos(self):
                return self._pos
            
            def onFlank(self):
                return self._onFlank
            
            def isAudible(self, distance):
                return abs(distance) < self._maxDistance
            
        # game play data
        enemy = Enemy(20.0, onFlank=True, maxDistance=10.0)
        player = Player(0.0)
        # create the tree
        creep = Action('Creep')
        attack = Action('Attack')
        move = Action('Move')
        doNothing = Action('doNothing')
        enemyOnFlank = Decision(
            test=InstanceMethod(enemy, 'onFlank'),
            trueNode=move,
            falseNode=attack,
            name='enemyOnFlank')
        enemyAway = Decision(
            test=LT(abs(enemy.getPos() - player.getPos()), 10),
            trueNode=attack,
            falseNode=enemyOnFlank,
            name='enemyAway')
        enemyAudible = Decision(
            test=InstanceMethod(enemy, 'isAudible', player.getPos() - enemy.getPos()),
            trueNode=creep,
            falseNode=doNothing,
            name='enemyAudible')
        enemyVisible = Decision(
            test=GTE(abs(enemy.getPos() - player.getPos()), enemy._maxDistance),
            trueNode=enemyAway,
            falseNode=enemyAudible,
            name='enemyVisible')
        # perform algorithm
        self.assertEqual(str(enemyVisible.makeDecision()), 'Move')


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((DecisionTreeTEST('testNumericIntervals'),
                    DecisionTreeTEST('testInstanceMethods'),
                    DecisionTreeTEST('testDecisionTree')))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
