'''
Created on Mar 13, 2020

@author: consultit
'''

from abc import ABCMeta, abstractmethod


class Test(metaclass=ABCMeta):

    @abstractmethod
    def __bool__(self):
        '''
        Perform the test.
        '''


class DecisionTreeNode(metaclass=ABCMeta):
    '''
    DecisionTreeNode abstract class.

    See: 'Millington, Ian. AI for Games. 3rd ed., Taylor Et Francis, a CRC Title,
        Part of the Taylor Et Francis Imprint, a Member of the Taylor Et
        Francis Group, the Academic Division of TetF Informa, Plc, 2019.'
    '''

    def __init__(self, name=''):
        self._name = name

    @abstractmethod
    def makeDecision(self):
        '''
        Recursively  walk  through  the  tree.
        '''

    def __str__(self):
        return self._name

    def __repr__(self):
        return self._name


class Action(DecisionTreeNode):
    '''
    Action class.
    '''

    def makeDecision(self):
        return self


class Decision(DecisionTreeNode):

    def __init__(self, test, trueNode, falseNode, *args, **kwargs):
        super(Decision, self).__init__(*args, **kwargs)
        self._test = test
        self._trueNode = trueNode
        self._falseNode = falseNode

    def testValue(self):
        return self._test.value

    def getBranch(self):
        '''
        Perform the test.
        '''
        if self._test:
            return self._trueNode
        else:
            return self._falseNode

    def makeDecision(self):
        '''
        Make  the  decision  and  recurse  based  on  the  result.
        '''
        branch = self.getBranch()
        return branch.makeDecision()


# # Some common generic tests
from math import inf


class Not(Test):
    '''
    Test inversion.
    '''

    def __init__(self, test):
        self._test = test

    def __bool__(self):
        return not self._test


class InsideClosedInterval(Test):
    '''
    Check MIN =< value =< MAX.

    Use cases with numeric types:
        m <= v <= M: InsideClosedInterval(v, MIN=m, MAX=M)
        m <= v: InsideClosedInterval(v, MIN=m)
        v <= M: InsideClosedInterval(v, MAX=M)
        (v < m) or (v > M): Not(InsideClosedInterval(v, MIN=m, MAX=M))
        v < M: Not(InsideClosedInterval(v, MIN=M))
        m < v: Not(InsideClosedInterval(v, MAX=m))
    '''

    def __init__(self, value, MIN=-inf, MAX=inf):
        self._value = value
        self._min = MIN
        self._max = MAX

    @property
    def value(self):
        return self._value

    def __bool__(self):
        return (self._min <= self._value) and (self._value <= self._max)


class InsideOpenInterval(Test):
    '''
    Check MIN < value < MAX.

    Use cases with numeric types:
        m < v < M: InsideOpenInterval(v, MIN=m, MAX=M)
        m < v: InsideOpenInterval(v, MIN=m)
        v < M: InsideOpenInterval(v, MAX=M)
        (v <= m) or (v >= M): Not(InsideOpenInterval(v, MIN=m, MAX=M))
        v <= M: Not(InsideOpenInterval(v, MIN=M))
        m <= v: Not(InsideOpenInterval(v, MAX=m))
    '''

    def __init__(self, value, MIN=-inf, MAX=inf):
        self._value = value
        self._min = MIN
        self._max = MAX

    @property
    def value(self):
        return self._value

    def __bool__(self):
        return (self._min < self._value) and (self._value < self._max)


def OutsideClosedInterval(value, MINREF, MAXREF):
    return Not(InsideClosedInterval(value, MIN=MINREF, MAX=MAXREF))


def OutsideOpenInterval(value, MINREF, MAXREF):
    return Not(InsideOpenInterval(value, MIN=MINREF, MAX=MAXREF))


def LTE(value, REF):
    '''
    Less than or equal.
    '''
    return InsideClosedInterval(value, MAX=REF)


def LT(value, REF):
    '''
    Less than.
    '''
    return InsideOpenInterval(value, MAX=REF)


def GTE(value, REF):
    '''
    Greater than or equal.
    '''
    return InsideClosedInterval(value, MIN=REF)


def GT(value, REF):
    '''
    Greater than.
    '''
    return InsideOpenInterval(value, MIN=REF)


class InstanceMethod(Test):
    '''
    Check instance method.

    Use cases:

        class C(object):

            def meth(self, arg1, arg2, kwarg1=value1, kwarg2=value2):
                ...
                return boolValue

        InstanceMethod(C(), meth, arg1, arg2, kwarg1=value1, kwarg2=value2) 
    '''

    def __init__(self, instance, method, *args, **kwargs):
        self._method = getattr(instance, method)
        self._args = args
        self._kwargs = kwargs

    @property
    def value(self):
        return self._method

    def __bool__(self):
        return self._method(*self._args, **self._kwargs)
