'''
Created on Jun 5, 2019

@author: consultit
'''

from ely.direct.data_structures_and_algorithms.ch14 import graph
from test_py.AI4G.path_finding.utilities import graph_from_edgelist
from direct.showbase.ShowBase import ShowBase
from panda3d.core import load_prc_file_data
from heapq import heappush, heappop
from dataclasses import dataclass, field
from typing import Any
import itertools
import importlib
import argparse
import textwrap
m14graph = importlib.import_module('test_py.DSAP.14_graph_app')

HALF_WIDTH = 40.0
HALF_HEIGHT = 30.0


class Graph(graph.Graph):

    class Vertex(graph.Graph.Vertex):
        '''This structure is used to keep track of the information we need for each node.'''

        def __init__(self, *args, **kwargs):
            super(Graph.Vertex, self).__init__(*args, **kwargs)
            self.connection = None
            self.costSoFar = 0.0

    class Edge(graph.Graph.Edge):

        def getFromNode(self):
            '''The node that this connection came from.'''
            return self.endpoints()[0]

        def getToNode(self):
            '''The node that this connection leads to.'''
            return self.endpoints()[1]

        def getCost(self):
            '''The non-negative cost of this connection.'''
            return self.element()

    def getConnections(self, fromNode):
        '''An array of connections outgoing from the given node.'''
        return self.incident_edges(fromNode, True)


Node = Graph.Vertex
NodeRecord = Graph.Vertex


class PathfindingList:
    '''Implemented using heapq.

    See: https://docs.python.org/3.7/library/heapq.html .
    '''

    @dataclass(order=True)
    class Entry:
        costSoFar: float
        counter: int
        node: Any = field(compare=False)

        def __str__(self):
            return '[' + str(self.costSoFar) + ',' + str(self.node) + ']'

    def __init__(self):
        self.pq = []  # list of entries arranged in a heap
        self.entry_finder = {}  # mapping of tasks to entries
        self.counter = itertools.count()
        self.REMOVED = 'REMOVED'  # placeholder for a removed node

    def clear(self):
        self.pq.clear()
        self.entry_finder.clear()
        self.counter = itertools.count()

    def smallestElement(self):
        '''Returns the smallest element.'''
        if not self.pq:
            raise Exception(str(self) + ' is empty.')
        # pop all smallest REMOVED elements until the first not REMOVED one
        while self.pq:
            if self.pq[0].node == self.REMOVED:
                heappop(self.pq)
            else:
                return self.pq[0].node

    def contains(self, node):
        if not isinstance(node, Node):
            raise Exception(str(node) + ' is not an instance of an ' +
                            str(Node) + ' or of a subclass thereof.')
        return node in self.entry_finder

    def find(self, node):
        if not isinstance(node, Node):
            raise Exception(str(node) + ' is not an instance of an ' +
                            str(Node) + ' or of a subclass thereof.')
        return node if node in self.entry_finder else None

    def __iadd__(self, other):
        '''Add a new node or update the costSoFar of an existing node'''
        if not isinstance(other, NodeRecord):
            raise Exception(str(other) + ' is not an instance of an ' +
                            str(NodeRecord) + ' or of a subclass thereof.')
        if other in self.entry_finder:
            self.__isub__(other)
        entry = PathfindingList.Entry(
            other.costSoFar, next(self.counter), other)
        self.entry_finder[other] = entry
        heappush(self.pq, entry)
        return self

    def __isub__(self, other):
        '''Mark an existing node as REMOVED.  Raise KeyError if not found.'''
        if not isinstance(other, NodeRecord):
            raise Exception(str(other) + ' is not an instance of an ' +
                            str(NodeRecord) + ' or of a subclass thereof.')
        entry = self.entry_finder.pop(other)
        entry.node = self.REMOVED
        return self

    def __iter__(self):
        return self.pq.__iter__()

    def __len__(self):
        return len(self.entry_finder)

    def __repr__(self):
        return ('[' + ''.join(['(' + str(v.costSoFar) + ',' + str(k) + '),'
                               for k, v in self.entry_finder.items()]).rstrip(',') + ']')


def pathfindDijkstra(graph, start, goal):

    # Initialize the record for the start node.
    startRecord = start

    # Initialize the open and closed lists.
    openL = PathfindingList()
    openL += startRecord
    closedL = PathfindingList()

    # Iterate through processing each node.
    while len(openL) > 0:
        # Find the smallest element in the openL list.
        current = openL.smallestElement()

        # If it is the goal node, then terminate.
        if current == goal:
            break

        # Otherwise get its outgoing connections.
        connections = graph.getConnections(current)

        # Loop through each connection in turn.
        for connection in connections:
            # Get the cost estimate for the end node.
            endNode = connection.getToNode()
            endNodeCost = current.costSoFar + connection.getCost()

            # Skip if the node is closed.
            if closedL.contains(endNode):
                continue

            # ... or if it is openL and we’ve found a worse route.
            elif openL.contains(endNode):
                # Here we find the record in the openL list
                # corresponding to the endNode.
                endNodeRecord = openL.find(endNode)
                if endNodeRecord.costSoFar <= endNodeCost:
                    continue

            # Otherwise we know we’ve got an unvisited node, so make a
            # record for it.
            else:
                endNodeRecord = endNode

            # We’re here if we need to update the node. Update the
            # cost and connection.
            endNodeRecord.costSoFar = endNodeCost
            endNodeRecord.connection = connection

            # And add it to the openL list anyway, since an existing node needs
            # to update its costSoFar.
            openL += endNodeRecord

        # We’ve finished looking at the connections for the current
        # node, so add it to the closed list and remove it from the
        # openL list.
        openL -= current
        closedL += current

    # We’re here if we’ve either found the goal, or if we’ve no more
    # nodes to search, find which.
    if current != goal:
        # We’ve run out of nodes without finding the goal, so there’s
        # no solution.
        return None

    else:
        # Compile the list of connections in the path.
        path = []
        # Work back along the path, accumulating connections.
        while current != start:
            path += [current.connection]
            current = current.connection.getFromNode()

        # Reverse the path, and return it.
        return reversed(path)

# E = (
# ('SFO', 'LAX', 337), ('SFO', 'BOS', 2704), ('SFO', 'ORD', 1846),
# ('SFO', 'DFW', 1464), ('LAX', 'DFW', 1235), ('LAX', 'MIA', 2342),
# ('DFW', 'ORD', 802), ('DFW', 'JFK', 1391), ('DFW', 'MIA', 1121),
# ('ORD', 'BOS', 867), ('ORD', 'PVD', 849), ('ORD', 'JFK', 740),
# ('ORD', 'BWI', 621), ('MIA', 'BWI', 946), ('MIA', 'JFK', 1090),
# ('MIA', 'BOS', 1258), ('BWI', 'JFK', 184), ('JFK', 'PVD', 144),
# ('JFK', 'BOS', 187),
# )
#
# E = (
# ('SFO', 'LAX', 337), ('LAX', 'MIA', 2342), ('MIA', 'BWI', 946),
# ('BWI', 'JFK', 184),
# )


E = (
    ('A', 'B', 10), ('B', 'C', 1), ('C', 'D', 1),
    ('A', 'E', 10), ('E', 'F', 10), ('F', 'G', 10), ('G', 'D', 10),
    ('A', 'H', 1), ('H', 'I', 1), ('I', 'L', 1), ('L', 'M', 1), ('M', 'D', 10),
    ('A', 'D', 10), ('B', 'F', 10), ('G', 'C',
                                     10), ('H', 'G', 10), ('I', 'E', 10), ('M', 'B', 1),
)


def findGraphNode(element, graph):
    for node in graph.vertices():
        if node.element() == element:
            return node


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This is the Dijkstra test.
    '''))
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()
    # do actions
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)

    app = ShowBase()

    # input data
    graph = graph_from_edgelist(E, Graph=Graph, directed=True)
    start = findGraphNode('A', graph)
    goal = findGraphNode('D', graph)

    # compute dijkstra
    path = [edge for edge in pathfindDijkstra(graph, start, goal)]

    print(str(start) + ' -> ' +
          ''.join([str(conn.getToNode()) + ' -> ' for conn in path]).rstrip(' -> '))

    m14graph.drawGraph(app, graph, HALF_WIDTH, HALF_HEIGHT,
                       path=path, color=(1.0, 1.0, 0.0, 1.0))

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 100.0, 0.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
