'''
Created on Jun 18, 2019

@author: consultit
'''

import unittest
from AI4G.path_finding.dijkstra import NodeRecord, Node, PathfindingList
from common.random import RandFloat, RandInt32, MAXFLOAT


def setUpModule():
    pass

    
def tearDownModule():
    pass


class PathfindingListTEST(unittest.TestCase):

    def setUp(self):
        # at least 5
        self.NUM = 50
        self.nodeRecords = [NodeRecord(RandInt32(50)) for _ in range(self.NUM)]
        self.pathFinfList = PathfindingList()
        self.smallestCost = MAXFLOAT
        for nr in self.nodeRecords:
            #n = Node(RandInt32())
            #nr.node = n
            nr.costSoFar = RandFloat()
            if self.smallestCost > nr.costSoFar:
                self.smallestCost = nr.costSoFar
            self.pathFinfList += nr

    def tearDown(self):
        del self.NUM
        del self.nodeRecords
        del self.pathFinfList
        del self.smallestCost

    def testContainsFind(self):
        for _ in range(max(5, self.NUM * 5)):
            # get contained value
            idx = abs(RandInt32(self.NUM - 1))
            nr = self.nodeRecords[idx]
            self.assertTrue(self.pathFinfList.contains(nr))
            self.assertEqual(self.pathFinfList.find(nr), nr)
            # get not contained value
            nr = NodeRecord(RandInt32())
            nr.node = Node(RandInt32())
            self.assertFalse(self.pathFinfList.contains(nr))
            self.assertEqual(self.pathFinfList.find(nr), None)
    
    def testIAdd(self):
        self.pathFinfList.clear()
        for nr in self.nodeRecords:
            self.pathFinfList += nr
        self.testContainsFind()

    def testISub(self):
        nodeRecords = [NodeRecord(nr) for nr in self.pathFinfList]
        for nr in self.pathFinfList:
            self.pathFinfList -= nr.node
            self.assertRaises(Exception, self.pathFinfList.contains, nr.node)
        self.assertEqual(len(self.pathFinfList), 0)
        
    def testSmallestElement(self):
        for _ in range(max(5, self.NUM * 5)):
            self.assertAlmostEqual(self.smallestCost,
                                   self.pathFinfList.smallestElement().costSoFar)
            # clear and reset all data
            self.tearDown()
            self.setUp()


class SecondTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testA(self):
        pass
    
    def testB(self):
        pass

        
def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((PathfindingListTEST('testContainsFind'), PathfindingListTEST('testIAdd'),
                    PathfindingListTEST('testISub'), PathfindingListTEST('testSmallestElement')))
    suite.addTest(SecondTEST('testA'))
    suite.addTest(SecondTEST('testB'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
