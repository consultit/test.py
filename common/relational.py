'''
Created on Jun 30, 2019

@author: consultit
'''

from abc import ABCMeta, abstractmethod


class Relational(metaclass=ABCMeta):

    # base (mandatory)
    @abstractmethod
    def __eq__(self, other):
        '''
        '''

    # base (mandatory)
    @abstractmethod
    def __lt__(self, other):
        '''
        '''

    # derived
    def __le__(self, other):
        return (self < other) or (self == other)

    # derived
    # __ne__() defaults to object.__ne__() which returns 'not object.__eq__()'

    # derived
    def __ge__(self, other):
        return not (self < other)

    # derived
    def __gt__(self, other):
        return not ((self < other) or (self == other))
