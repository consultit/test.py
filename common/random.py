'''
Created on Jun 20, 2019

@author: consultit
'''

import numpy 
import random
random.seed()

DIM = 16
MAX32 = numpy.iinfo(numpy.int8).max
MAX64 = numpy.iinfo(numpy.int8).max
MAXFLOAT = numpy.iinfo(numpy.int8).max
MAXDOUBLE = numpy.iinfo(numpy.int8).max

def RandInt32(MAX=MAX32):
    return numpy.int32(random.randrange(-MAX, MAX))

def RandInt64(MAX=MAX64):
    return numpy.int64(random.randrange(-MAX, MAX))

def RandFloat(MAX=MAXFLOAT):
    return numpy.float((random.random() * 2.0 - 1.0) * MAX)

def RandDouble(MAX=MAXDOUBLE):
    return numpy.double((random.random() * 2.0 - 1.0) * MAX)

        