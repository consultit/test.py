'''
Created on Dec 23, 2018

@author: consultit
'''

from random import randint
from math import log2
import os 
from builtins import Exception

# factorial
def factorial(n):
    res = None
    if n == 0:
        res = 1
    else:
        res = n * factorial(n - 1)
    print(' ' * (n) + 'factorial(' + str(n) + ') = ' + str(res))
    return res

# English ruler
def draw_line(tick_length, tick_label=''):
    '''Draw one line with given tick length (followed by optional label).'''
    line = '-' * tick_length
    if tick_label:
        line += ' ' + tick_label
    print(line)
    
def draw_interval(center_length):
    '''Draw tick interval based upon a central tick length.'''
    if center_length > 0:  # stop when length drops to 0
        draw_interval(center_length - 1)  # recursively draw top ticks
        draw_line(center_length)  # draw center tick
        draw_interval(center_length - 1)  # recursively draw bottom ticks

def draw_ruler(num_inches, major_length):
    '''Draw English ruler with given number of inches, major tick length.'''
    draw_line(major_length, str(0))  # draw inch 0 line
    for j in range(1, 1 + num_inches):
        draw_interval(major_length - 1)  # draw interior ticks for inch
        draw_line(major_length, str(j))  # draw inch j line and label

# Binary Search    
def binary_search(data, target, low, high, numRec):
    '''
    Return True if target is found in indicated portion of a Python list.
    
    The search only considers the portion from data[low] to data[high] inclusive.
    '''
    numRec += 1
    if low > high:
        return (False, None, numRec)  # interval is empty; no match
    else:
        mid = (low + high) // 2
        if target == data[mid]:  # found a match
            return (True, mid, numRec)
        elif target < data[mid]:
            # recur on the portion left of the middle
            return binary_search(data, target, low, mid - 1, numRec)
        else:
            # recur on the portion right of the middle
            return binary_search(data, target, mid + 1, high, numRec)

# File Systems
def disk_usage(path, numRec, printDescr=True):
    '''Return the number of bytes used by a ﬁle/folder and any descendents.'''
    total = -1
    numRec += 1
    try:
        total = os.path.getsize(path)  # account for direct usage
        if os.path.isdir(path):  # if this is a directory,
            for filename in os.listdir(path):  # then for each child:
                childpath = os.path.join(path, filename)  # compose full path to child
                res = disk_usage(childpath, numRec, printDescr) # add child’s usage to total
                total += res[0]
                numRec = res[1]
    except Exception as e:
        print(e) 
    if printDescr: print('{0:<7}'.format(total), path)  # descriptive output (optional)
    return (total, numRec)  # return the grand total    
    
if __name__ == '__main__':
    print('factorial')
    factorial(5)
    print('English ruler')
    for r in range(1, 6):
        draw_ruler(1, r)
    print('binary search')
    num = 1024
    iterations = 1000
    ln2NumPlus1 = 0.0
    data = sorted([randint(1, 100) for i in range(num)])
    #target = -1
    #target = data[num - 1]
    for s in range(iterations):
        target = data[randint(0, num - 1)]
        found, index, numRec = binary_search(data, target, 0, num - 1, 0)
        ln2NumPlus1 = (ln2NumPlus1 * s + numRec) / (s + 1.0)
    print('log2(' + str(num) + ')', log2(num), ln2NumPlus1 - 1, sep=' - ')
    for s in range(iterations):
        found, index, numRec = binary_search(data, data[randint(0, num - 1)], 0, num - 1, 0)
        if not found:
            print(found, index, numRec, sep=' - ')
    print(binary_search([n + 1 for n in range(17)], 17, 0, 16, 0))
    print('file system disk usage')
    print(disk_usage(os.path.join(os.environ['HOME'], 'trash/cs'), 0, False))
    