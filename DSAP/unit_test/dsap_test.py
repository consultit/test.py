'''
Created on Jun 18, 2019

@author: consultit
'''

import unittest
from ely.direct.data_structures_and_algorithms.ch11.avl_tree import AVLTreeMap
from test_py.common.random import RandInt32


def setUpModule():
    pass

    
def tearDownModule():
    pass


class AVLTreeMapTEST(unittest.TestCase):

    def setUp(self):
        # at least 5
        self.NUM = 10
        self.avlMap = AVLTreeMap()
        self.values = [RandInt32() for i in range(self.NUM)]
        for v in self.values:
            self.avlMap[v] = str(v)
        self.sortedV = sorted(set(self.values))

    def tearDown(self):
        del self.avlMap
        del self.values
        del self.sortedV

    def testOrder(self):
        self.assertEqual(self.sortedV[0], self.avlMap.first().key())
        self.assertEqual(self.sortedV[-1], self.avlMap.last().key())
        self.assertEqual(self.sortedV, [k for k in self.avlMap])
        
    def testContainment(self):
        self.assertGreaterEqual(self.NUM, len(self.avlMap))
        for kIn in self.values:
            self.assertTrue(kIn in self.avlMap)
        for kIn in self.sortedV:
            self.assertTrue(kIn in self.avlMap)
        self.avlMap.clear()
        self.assertEqual(0, len(self.avlMap))

    def testAdd(self):
        self.avlMap[1] = '1'
        self.avlMap.__setitem__(2, '2')
        self.assertTrue(1 in self.avlMap)
        self.assertTrue(2 in self.avlMap)
    
    def testDel(self):
        self.avlMap[1] = '1'
        self.avlMap[2] = '2'
        self.avlMap[3] = '3'
        self.avlMap.delete(self.avlMap.find_position(1))
        self.avlMap.__delitem__(2)
        del self.avlMap[3]
        self.assertFalse(1 in self.avlMap)
        self.assertFalse(2 in self.avlMap)
        self.assertFalse(3 in self.avlMap)
        
    def _getLGKeys(self, seq, val):
        if val < seq[0]:
            return (None, seq[0])
        if val > seq[-1]:
            return (seq[-1], None)
        for i, s in enumerate(seq):
            if val < s:
                return (seq[i - 1], seq[i])

    def testFindLTGT(self):
        # find less than and greater than a key value
        # existent key
        keys = [k for k in self.avlMap]
        keyIdx = int(len(keys) / 2)
        key = keys[keyIdx]
        lessKey = keys[keyIdx - 1]
        greaterKey = keys[keyIdx + 1]
        self.assertEqual(self.avlMap.find_lt(key)[0], lessKey)
        self.assertEqual(self.avlMap.find_gt(key)[0], greaterKey)
        # not existent key
        noKey = int((keys[0] + keys[-1]) / 2)
        while noKey in keys:
            noKey += 1
        lessKey, greaterKey = self._getLGKeys(keys, noKey)
        print(keys, noKey, sep=' | ')
        if lessKey:
            self.assertEqual(self.avlMap.find_lt(noKey)[0], lessKey)
            print(self.avlMap.find_lt(noKey)[0])
        if greaterKey:
            self.assertEqual(self.avlMap.find_gt(noKey)[0], greaterKey)
            print(self.avlMap.find_gt(noKey)[0])
        

class SecondTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testA(self):
        pass
    
    def testB(self):
        pass

        
def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((AVLTreeMapTEST('testOrder'), AVLTreeMapTEST('testContainment'),
                    AVLTreeMapTEST('testAdd'), AVLTreeMapTEST('testDel'),
                    AVLTreeMapTEST('testFindLTGT')))
#     suite.addTest(SecondTEST('testA'))
#     suite.addTest(SecondTEST('testB'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
