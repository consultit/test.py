'''
Created on Jan 09, 2019

@author: consultit
'''

from ely.direct.data_structures_and_algorithms.ch11.binary_search_tree import TreeMap
from ely.direct.data_structures_and_algorithms.ch11.avl_tree import AVLTreeMap
from direct.showbase.ShowBase import ShowBase
from panda_test_py.Actors.testTree import setup3dText, dynFont, computeLevels, drawTree
from random import randint 
from panda3d.core import LPoint3f


def createPopulateDrawTreeMap(treeClass, vList, orig):
    
    global app, screenWidth, screenHeight
    class Text3d(object):

        def __call__(self, text):
            return setup3dText(app, None, text, font=dynFont, scale=(1,), rotate=False)

    tMap = treeClass()
    for k in vList:
        tMap[k] = str(k)
    # draw key tree
    for p in tMap.inorder():
        nodeI = app.render.attach_new_node(p.value() + 'I')
        print(nodeI.get_name())
        # changes the text value of the node depending on whether it is the parent's left or right child (if any)
        parent = tMap.parent(p)
        if parent:
            if p == tMap.left(parent):
                p.element()._value = Text3d()(p.value() + ' l')
            else:
                p.element()._value = Text3d()(p.value() + ' r')
        else:
            p.element()._value = Text3d()(p.value())
        p.value().instance_to(nodeI)

    # alternate definitions
#     def getTreePosNP(p):
#         '''Must return a NodePath'''
#         return p.value()
    getTreePosNP = lambda p: p.value()
    
    levelWidths, levelHeight = computeLevels(tMap, screenWidth, screenHeight, getTreePosNP)
    drawTree(app, tMap, levelWidths, levelHeight, getTreePosNP, orig)
    return tMap


class MyInt(object):
    '''
    Custom object that can be used as a key to the AVLTreeMap.
    
    Because only '<' and '==' operators are used on the keys of AVLTreeMap, we need
    to override (at least) '__lt__' and '__eq__'; the other operators could be 
    overridden in terms of these ones.  
    '''
    
    def __init__(self, value):
        self.value = value
    
    # base (mandatory)
    def __lt__(self, other):
        return self.value < other.value

    # derived
    def __le__(self, other):
        return (self < other) or (self == other)

    # base (mandatory)
    def __eq__(self, other):
        return self.value == other.value

    # derived
    def __ge__(self, other):
        return not (self < other)
    
    # derived
    def __gt__(self, other):
        return not ((self < other) or (self == other))
    
    # optional (for print)
    def __repr__(self):
        return 'My_' + str(self.value)
    
    # optional (for set)
    def __hash__(self):
        return hash(self.value)


if __name__ == '__main__':
    app = ShowBase()
    # input data
    screenWidth = (-30.0, 30.0)
    screenHeight = (-25.0, 25.0)
    
    # create an unordered list of values
    intRandomList = [randint(1, 100) for i in range(40)]
    valueRandomList = [MyInt(i) for i in intRandomList]
    valueAscendingOrderList = sorted(valueRandomList)
    valueDescendingOrderList = sorted(valueRandomList, reverse=True)
    
    # the final result
    print(sorted(list(set(valueRandomList))))
    
    # create and populate a tree map with the random list
    tMapRandom = createPopulateDrawTreeMap(TreeMap, valueRandomList, LPoint3f(0.0, 0.0, 0.0))
    # create and populate a tree map with the list in ascending order
    tMapSortedAscending = createPopulateDrawTreeMap(TreeMap, valueAscendingOrderList, LPoint3f(100.0, 0.0, 0.0))
    # create and populate a tree map with the list in descending order
    tMapSortedDescending = createPopulateDrawTreeMap(TreeMap, valueDescendingOrderList, LPoint3f(200.0, 0.0, 0.0))
    
    # create and populate a AVL tree map with the random list
    tMapRandom = createPopulateDrawTreeMap(AVLTreeMap, valueRandomList, LPoint3f(0.0, 50.0, 0.0))
    # create and populate a AVL tree map with the list in ascending order
    tMapSortedAscending = createPopulateDrawTreeMap(AVLTreeMap, valueAscendingOrderList, LPoint3f(100.0, 50.0, 0.0))
    # create and populate a AVL tree map with the list in descending order
    tMapSortedDescending = createPopulateDrawTreeMap(AVLTreeMap, valueDescendingOrderList, LPoint3f(200.0, 50.0, 0.0))

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 100.0, 0.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
