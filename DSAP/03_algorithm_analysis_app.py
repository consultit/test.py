'''
Created on Dec 13, 2018

@author: consultit
'''

from random import random, randint
import time

def create_random_sequence(min_value, max_value, min_num, max_num):
    S = []
    count = randint(min_num, max_num)
    for i in range(count):
        S.append(randint(min_value, max_value) * random())
    return S, count
 
def prefix_average1(S):
    '''Return list such that, for all j, A[j] equals average of S[0], ..., S[j].'''
    n = len(S)
    A = [0] * n  # create new list of n zeros
    for j in range(n):
        total = 0  # begin computing S[0] + ... + S[j]
        for i in range(j + 1):
            total += S[i]
        A[j] = total / (j + 1)  # record the average
    return A

def prefix_average2(S):
    '''Return list such that, for all j, A[j] equals average of S[0], ..., S[j].'''
    n = len(S)
    A = [0] * n  # create new list of n zeros
    for j in range(n):
        A[j] = sum(S[0:j + 1]) / (j + 1)  # record the average
    return A

def prefix_average3(S):
    '''Return list such that, for all j, A[j] equals average of S[0], ..., S[j].'''
    n = len(S)
    A = [0] * n  # create new list of n zeros
    total = 0  # compute preﬁx sum as S[0] + S[1] + ...
    for j in range(n):
        total += S[j]  # update preﬁx sum to include S[j]
        A[j] = total / (j + 1)  # compute average based on current sum
    return A

def disjoint1(A, B, C):
    '''Return True if there is no element common to all three lists.'''
    for a in A:
        for b in B:
            for c in C:
                if a == b == c:
                    return False  # we found a common value
    return True  # if we reach this, sets are disjoint

def disjoint2(A, B, C):
    '''Return True if there is no element common to all three lists.'''
    for a in A:
        for b in B:
            if a == b:  # only check C if we found match from A and B
                for c in C:
                    if a == c:  # (and thus a == b == c)
                        return False  # we found a common value
    return True  # if we reach this, sets are disjoint

if __name__ == '__main__':
    # prefix_average*
    S, count = create_random_sequence(100, 200, 10000, 20000)
    # 1
    tStart = time.perf_counter()
    A1 = prefix_average1(S)
    tEnd = time.perf_counter()
    print('prefix_average1', count, tEnd - tStart, sep=' - ')
    # 2
    tStart = time.perf_counter()
    A2 = prefix_average2(S)
    tEnd = time.perf_counter()
    print('prefix_average2', count, tEnd - tStart, sep=' - ')
    # 3
    tStart = time.perf_counter()
    A3 = prefix_average3(S)
    tEnd = time.perf_counter()
    print('prefix_average3', count, tEnd - tStart, sep=' - ')
    
    # disjoint*
    A, countA = create_random_sequence(100, 200, 100, 100)
    B, countB = create_random_sequence(100, 200, 100, 100)
    C, countC = create_random_sequence(100, 200, 100, 100)
    # 1: A != B != C
    tStart = time.perf_counter()
    res = disjoint1(A, B, C)
    tEnd = time.perf_counter()
    print('disjoint1: A != B != C', res, countA, countB, countC, tEnd - tStart, sep=' - ')
    # 2: A != B != C
    tStart = time.perf_counter()
    res = disjoint2(A, B, C)
    tEnd = time.perf_counter()
    print('disjoint2: A != B != C', res, countA, countB, countC, tEnd - tStart, sep=' - ')
    # 1: A == B == C
    tStart = time.perf_counter()
    res = disjoint1(A, A, A)
    tEnd = time.perf_counter()
    # 1: A == B == C
    print('disjoint1: A == B == C', res, countA, countA, countA, tEnd - tStart, sep=' - ')
    tStart = time.perf_counter()
    res = disjoint2(A, A, A)
    tEnd = time.perf_counter()
    print('disjoint2: A == B == C', res, countA, countA, countA, tEnd - tStart, sep=' - ')
