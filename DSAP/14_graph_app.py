'''
Created on Apr 29, 2019

@author: consultit
'''

from ely.direct.data_structures_and_algorithms.ch14.graph import Graph
from ely.direct.data_structures_and_algorithms.ch14.graph_examples import \
    figure_14_3, figure_14_14
from direct.showbase.ShowBase import ShowBase
from panda_test_py.Actors.testTree import setup3dText, dynFont
from panda3d.core import LPoint3f, load_prc_file_data
from math import sqrt, ceil
from random import random
import os
import argparse
import textwrap


def drawEdge(app, nodeA, nodeB, model='edge', label=None, color=None):
    lenAB = (nodeB.get_pos() - nodeA.get_pos()).length()
    edge = app.loader.load_model(model)
    if color:
        edge.set_color(color)
    edge.reparent_to(app.render)
    pMin = LPoint3f()
    pMax = LPoint3f()
    edge.calc_tight_bounds(pMin, pMax)
    edgeLen = abs(pMax.y - pMin.y)
    scale = lenAB / edgeLen
    edge.set_scale(1.5, scale, 1.5)
    edge.set_pos(nodeA.get_pos())
    edge.look_at(nodeB)
    if label:
        pos = nodeA.get_pos() + (nodeB.get_pos() - nodeA.get_pos()) * 0.3
#         setup3dText(app, app.render, str(label), font=dynFont, scale=(1,), rotate=False, pos=pos)
        setup3dText(app, app.render, str(label), labelColor=(0.75, 0, 0.5, 1), font=dynFont,
                    pos=pos, scale=(1, 1, 1), slant=0.3, wordwrap=5.0, smallCaps=False,
                    smallCapsScale=0.5, shadow=(0.05, 0.05), shadowColor=(0, 0, 0, 1),
                    frameColor=(1, 1, 0, 0), frameAsMargin=(0.0, 0.0, 0.0, 0.0),
                    cardColor=(0, 0, 0, 0), cardAsMargin=(0, 0, 0, 0), cardDecal=True,
                    rotate=False, rotateTime=5)


def drawEdgeArrow(app, nodeA, nodeB, model='edge-arrow', label=None, color=None):
    drawEdge(app, nodeA, nodeB, model, label, color)


def testDraw(app):
    # test
    txtA = setup3dText(app, None, 'A', font=dynFont, scale=(1,), rotate=False)
    txtA.reparent_to(app.render)
    txtB = setup3dText(app, app.render, 'B', font=dynFont, scale=(1,), rotate=False,
                       pos=LPoint3f(-10.0, 0.0, 10.0))
    txtC = setup3dText(app, app.render, 'C', font=dynFont, scale=(1,), rotate=False,
                       pos=LPoint3f(10.0, 0.0, 10.0))
    # A - B
    drawEdge(app, txtA, txtB)
    # A -> C
    drawEdgeArrow(app, txtA, txtC)


def drawGraph(app, graph, HALF_WIDTH, HALF_HEIGHT, path=None, color=None):
    # draw graph
    rowColMax = ceil(sqrt(float(graph.vertex_count())))
    dw = 2.0 * HALF_WIDTH / rowColMax
    dh = 2.0 * HALF_HEIGHT / rowColMax
    dR = 0.3
    r = 0
    c = 0
    for v in graph.vertices():
        dwR = ((2.0 * random() - 1.0) * dR + 1.0) * dw
        dhR = ((2.0 * random() - 1.0) * dR + 1.0) * dh
        txtV = setup3dText(app, app.render, str(v), font=dynFont, scale=(1,), rotate=False,
                           pos=LPoint3f(-HALF_WIDTH + dwR / 2.0 + c * dwR, 0.0,
                                        HALF_HEIGHT - dhR / 2.0 - r * dhR))
        # set v's element as text node
        v._element = txtV
        c += 1
        if (c - r) > 1:
            r += 1
            c = 0
        # print (r, c, sep=',')

    if graph.is_directed():
        DRAWEDGEFUNC = drawEdgeArrow
    else:
        DRAWEDGEFUNC = drawEdge
    for e in graph.edges():
        if path and (e in path):
            DRAWEDGEFUNC(app, e.endpoints()[0].element(), e.endpoints()[
                         1].element(), label=e.element(), color=color)
        else:
            DRAWEDGEFUNC(app, e.endpoints()[0].element(), e.endpoints()[
                         1].element(), label=e.element())


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This is the graph test.
    '''))
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()
    # do actions
    if args.data_dir:
        for dataDir in args.data_dir:
            load_prc_file_data('', 'model-path ' + dataDir)

    app = ShowBase()

#     testDraw()

    # test graph
    HALF_WIDTH = 40.0
    HALF_HEIGHT = 30.0
#     graph = figure_14_3()
    graph = figure_14_14()

    # draw graph
    drawGraph(app, graph, HALF_WIDTH, HALF_HEIGHT)

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 100.0, 0.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
