'''
Created on Apr 22, 2019

@author: consultit
'''

from ely.direct.data_structures_and_algorithms.ch07.positional_list import PositionalList
from direct.showbase.ShowBase import ShowBase
from random import randint 


class MyInt(object):
    '''
    Custom object that can be used as a key to the AVLTreeMap.
    
    Because only '<' and '==' operators are used on the keys of AVLTreeMap, we need
    to override (at least) '__lt__' and '__eq__'; the other operators could be 
    overridden in terms of these ones.  
    '''
    
    def __init__(self, value):
        self.value = value
    
    # base (mandatory)
    def __lt__(self, other):
        return self.value < other.value

    # derived
    def __le__(self, other):
        return (self < other) or (self == other)

    # base (mandatory)
    def __eq__(self, other):
        return self.value == other.value

    # derived
    def __ge__(self, other):
        return not (self < other)
    
    # derived
    def __gt__(self, other):
        return not ((self < other) or (self == other))
    
    # optional (for print)
    def __repr__(self):
        return 'My_' + str(self.value)
    
    # optional (for set)
    def __hash__(self):
        return hash(self.value)


if __name__ == '__main__':
    app = ShowBase()
    # input data
    screenWidth = (-30.0, 30.0)
    screenHeight = (-25.0, 25.0)
    
    # create an unordered list of values
    intRandomList = [randint(1, 100) for i in range(40)]
    valueRandomList = [MyInt(i) for i in intRandomList]
    
    # the final result
    print(sorted(list(set(valueRandomList))))

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 100.0, 0.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
