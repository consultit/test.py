'''
Created on Nov 30, 2018

@author: consultit
'''

import argparse, textwrap
import png
import struct
import io
import os

class PNGConverter(object):
    '''This class converts files of 'THE MNIST DATABASE of handwritten digits' into PNG files.'''
    
    DATATYPE = {
        0x08: (1, '>B', 'unsigned byte'),
        0x09: (1, '>b', 'signed byte'),
        0x0B: (2, '>h', 'short (2 bytes)'),
        0x0C: (4, '>i', 'int (4 bytes)'),
        0x0D: (4, '>f', 'float (4 bytes)'),
        0x0E: (8, '>d', 'double (8 bytes)'),
    }
    
    def __init__(self, outPath, imageFileName, labelFileName):
        if not os.path.exists(outPath):
            os.makedirs(outPath)
        self.outPath = os.path.realpath(outPath)
        # handle image file
        self.imgFile = open(imageFileName, 'rb')
        header = self.readIDXHeader(self.imgFile)
        print(imageFileName, 'DATA TYPE =', header[4], sep=' ')
        self.imgFileTypeLen = header[0]
        self.imgFileFmt = header[1]
        self.imgFileDims = header[2]
        self.imgFileDataOffset = header[3]
        for i in range(len(self.imgFileDims)):
            print('\tDIMENSION-' + str(i) + ' = ', self.imgFileDims[i])
        # handle label file
        self.lblFile = open(labelFileName, 'rb')
        header = self.readIDXHeader(self.lblFile)
        print(labelFileName, 'DATA TYPE =', header[4], sep=' ')
        self.lblFileTypeLen = header[0]
        self.lblFileFmt = header[1]
        self.lblFileDims = header[2]
        self.lblFileDataOffset = header[3]
        for i in range(len(self.lblFileDims)):
            print('\tDIMENSION-' + str(i) + ' = ', self.lblFileDims[i])

    def __del__(self):
        self.imgFile.close()
        self.lblFile.close()

    def convertImageData(self, data):
        return 255 - data

    def convertLabelData(self, data):
        return data
        
    def convert(self, start, numImg):
        '''
        Convert 'numImg' images staring at 'start' (0-inizialized) image index. 
        
        Returns the label value list.
        '''
        
        if ((self.imgFileDims[0] != self.lblFileDims[0]) or (start < 0) 
            or ((start + numImg) > self.imgFileDims[0])):
            return 0
        # format string for numbering image files
        fmtStr = '{:0>' + str(len(str(start + numImg))) + 'd}'
        # handle images
        imgRows = self.imgFileDims[1]
        imgColumns = self.imgFileDims[2]
        imgSizeLen = imgRows * imgColumns * self.imgFileTypeLen
        imgStartOffset = self.imgFileDataOffset + imgSizeLen * start
        self.imgFile.seek(imgStartOffset, io.SEEK_SET)
        # handle labels
        lblSizeLen = self.lblFileTypeLen
        lblStartOffset = self.lblFileDataOffset + lblSizeLen * start
        self.lblFile.seek(lblStartOffset, io.SEEK_SET)
        # cycle over all requested images and labels
        labels = []
        for i in range(numImg):
            image = []
            for r in range(imgRows):
                image.append([])
                for c in range(imgColumns):
                    d = struct.unpack(self.imgFileFmt, self.imgFile.read(self.imgFileTypeLen))
                    p = self.convertImageData(d[0])
                    image[r].append(p)
            assert(self.imgFile.tell() == imgStartOffset + (i + 1) * imgSizeLen)
            # read label
            d = struct.unpack(self.lblFileFmt, self.lblFile.read(self.lblFileTypeLen))
            l = self.convertLabelData(d[0])
            labels.append(l)
            # image is ready to be saved
            
            imgIdx = fmtStr.format(start + i)
            imgFileName = os.path.join(self.outPath, 'image' + imgIdx + '_' + str(l) + '_.png')
            imgOutFile = open(imgFileName, 'wb')
            writer = png.Writer(imgColumns, imgRows, greyscale=True, bitdepth=8)
            writer.write(imgOutFile, image)
            imgOutFile.close()
        return labels
                
    def readIDXHeader(self, idxFile):
        '''This is a (generic) IDX's header reader'''
        
        idxFile.seek(0, io.SEEK_SET)
        magicNum = struct.unpack('BBBB', idxFile.read(4))
        typeLen = PNGConverter.DATATYPE[magicNum[2]][0]
        fmt = PNGConverter.DATATYPE[magicNum[2]][1]
        dataDescr = PNGConverter.DATATYPE[magicNum[2]][2]
        dims = []
        for i in range(magicNum[3]):
            dim = struct.unpack('>i', idxFile.read(4))        
            dims.append(dim[0])
        dataOffset = idxFile.tell()
        return (typeLen, fmt, dims, dataOffset, dataDescr)

        
if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    Converts files of 'THE MNIST DATABASE of handwritten digits' into PNG files.
      
    '''))
    # set up arguments
    parser.add_argument('outpath', metavar='PATH', type=str, help='the path where to save PNG files')
    parser.add_argument('imagefile', metavar='IMAGES.png', type=str, help='the IMAGE input file')
    parser.add_argument('labelfile', metavar='LABELS.png', type=str, help='the LABEL input file')
    parser.add_argument('-s', '--start-index', type=int, default=0, help='the index of first image to convert')
    parser.add_argument('-n', '--num-images', type=int, default=10, help='the number of images to convert')
    # parse arguments
    args = parser.parse_args()
    #
    print(PNGConverter(args.outpath, args.imagefile, args.labelfile).convert(args.start_index, args.num_images))
