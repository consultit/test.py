'''
Created on Dec 14, 2018

@author: consultit
'''

import matplotlib.pylab as plt
import numpy as np


def plotSigmoid1(x):
    f = 1.0 / (1.0 + np.exp(-x))
    plt.plot(x, f)
    plt.xlabel('x')
    plt.ylabel('f(x)')

def plotSigmoid2(x):
    W = (0.5, 1.0, 2.0)
    L = ('w = 0.5', 'w = 1.0', 'w = 2.0')
    for w, l in zip(W, L):
        f = 1.0 / (1.0 + np.exp(-x * w))
        plt.plot(x, f, label=l)
    plt.xlabel('x')
    plt.ylabel('h_w(x)')
    plt.legend(loc=2)

def plotSigmoid3(x):
    w = 5.0
    B = (-8.0, 0.0, 8.0)
    L = ('b = -8.0', 'b = 0.0', 'b = 8.0')
    for b, l in zip(B, L):
        f = 1.0 / (1.0 + np.exp(-(x * w + b)))
        plt.plot(x, f, label=l)
    plt.xlabel('x')
    plt.ylabel('h_wb(x)')
    plt.legend(loc=2)

if __name__ == '__main__':
    x = np.arange(-8, 8, 0.1)
#     plotSigmoid1(x)
#     plotSigmoid2(x)
    plotSigmoid3(x)
    plt.show()
