'''
Created on Dec 14, 2018

@author: consultit
'''

import numpy as np
import time

def f(x):
    return 1.0 / (1.0 + np.exp(-x))

def simple_looped_nn_calc(n_layers, x, w, b):
    h = None
    # Setup the input array which the weights will be multiplied by for each layer
    # If it's the first layer, the input array will be the x input vector
    # If it's not the first layer, the input to the next layer will be the 
    # output of the previous layer
    for l in range(n_layers - 1):
        if l == 0:
            node_in = x
        else:
            node_in = h
        # Setup the output array for the nodes in layer l + 1
        h = np.zeros((w[l].shape[0],))
        # loop through the rows of the weight array
        for i in range(w[l].shape[0]):
            # setup the sum inside the activation function
            f_sum = 0
            # loop through the columns of the weight array
            for j in range(w[l].shape[1]):
                f_sum += w[l][i][j] * node_in[j]
            # add the bias
            f_sum += b[l][i]
            # finally use the activation function to calculate the
            # i-th output i.e. h1, h2, h3
            h[i] = f(f_sum)
    return h

def matrix_feed_forward_calc(n_layers, x, w, b):
    for l in range(n_layers - 1):
        if l == 0:
            node_in = x
        else:
            node_in = h
        z = w[l].dot(node_in) + b[l]
        h = f(z)
    return h

def algebraic_approach(h1, w12, b12, w23, b23):
    # compute layer2 output
    h2 = f(w12 @ h1 + b12)
    # compute layer3 output
    h3 = f(w23 @ h2 + b23)
    return h3[0, 0]

if __name__ == '__main__':
    PERFORMANCE_COUNT = 100000
    
    ###########################
    # Tutorial samples
    # input data
    w1 = np.array([
        [0.2, 0.2, 0.2],
        [0.4, 0.4, 0.4],
        [0.6, 0.6, 0.6]
        ])
    w2 = np.zeros((1, 3))
    w2[0, :] = np.array([0.5, 0.5, 0.5])
    b1 = np.array([0.8, 0.8, 0.8])
    b2 = np.array([0.2])
    #
    w = [w1, w2]
    b = [b1, b2]
    # a dummy input vector
    x = [1.5, 2.0, 3.0]
    
    ###########################
    # Algebraic approach
    # input data given in row major order
    H1 = (1.5, 2.0, 3.0)
    W12 = (0.2, 0.2, 0.2, 0.4, 0.4, 0.4, 0.6, 0.6, 0.6)
    B12 = (0.8, 0.8, 0.8)
    W23 = (0.5, 0.5, 0.5)
    B23 = (0.2,)
    # input = 3x1 (column) vector
    h1 = np.zeros(3 * 1).reshape(3, 1)
    h1[:, 0] = H1
    # layer1 -> layer2: w12 = 3x3 matrix, b12 = 3x1 (column) vector
    w12 = np.zeros(3 * 3).reshape(3, 3)
    w12[:, :] = [W12[i:i + 3] for i in range(0, 9, 3)]  # [W12[0:3],W12[3:6],W12[6:9]] and assignment in row major order
    b12 = np.zeros(3 * 1).reshape(3, 1)
    b12[:, 0] = B12
    # layer2 -> layer3: w23 = 1x3 matrix, b23 = 1x1 (column) vector
    w23 = np.zeros(1 * 3).reshape(1, 3)
    w23[0, :] = W23
    b23 = np.zeros(1 * 1).reshape(1, 1)
    b23[:, 0] = B23
    
    # COMPUTATION
    # algebraic approach
    tStart = time.perf_counter()
    for i in range(PERFORMANCE_COUNT):
        res = algebraic_approach(h1, w12, b12, w23, b23)
    tEnd = time.perf_counter()
    print('algebraic approach', res, tEnd - tStart, sep=' - ')
    # simple_looped_nn_calc
    tStart = time.perf_counter()
    for i in range(PERFORMANCE_COUNT):
        res = simple_looped_nn_calc(3, x, w, b)[0]
    tEnd = time.perf_counter()
    print('simple_looped_nn_calc', res, tEnd - tStart, sep=' - ')
    # matrix_feed_forward_calc
    tStart = time.perf_counter()
    for i in range(PERFORMANCE_COUNT):
        res = matrix_feed_forward_calc(3, x, w, b)[0]
    tEnd = time.perf_counter()
    print('matrix_feed_forward_calc', res, tEnd - tStart, sep=' - ')
