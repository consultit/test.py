'''
Created on Dec 27, 2018

@author: consultit
'''

import numpy as np

def printInfo(a):
    print(a, a.shape, a.ndim, a.dtype.name, a.itemsize, a.size, type(a), '\n', sep=' | ')
    print(' -------------------------------------------------------------------------- ')

if __name__ == '__main__':
    # 2.2.1
    a = np.arange(2 * 3 * 4).reshape(2, 3, 4)
    printInfo(a)
    # 2.2.2
    a = np.array([2, 3, 4])
    printInfo(a)
    a = np.array([2.2, 3.3, 4.4])
    printInfo(a)
    try:
        a = np.array(2, 3, 4)
    except Exception as e:
        print(e)
    a = np.array([[1, 2, 3],
                  [4, 5, 6]], dtype=complex)
    printInfo(a)
    a = np.zeros((2, 3, 4))
    printInfo(a)
    a = np.ones((2, 3, 4), dtype=np.int8)
    printInfo(a)
    a = np.empty((2, 3, 4))
    printInfo(a)
    a = np.arange(0, 10, 2)
    printInfo(a)
    a = np.arange(0, 1, 0.15)
    printInfo(a)
    a = np.linspace(0, 1, 7)
    printInfo(a)
    a = np.linspace(0, 2 * np.pi, 100)
    printInfo(a)
    # 2.2.3
    a = np.arange(2)
    printInfo(a)
    a = np.arange(3 * 2).reshape(3, 2)
    printInfo(a)
    a = np.arange(4 * 3 * 2).reshape(4, 3, 2)
    printInfo(a)
    a = np.arange(10000)
    printInfo(a)
    a = np.arange(10000).reshape(100, 100)
    printInfo(a)
    np.set_printoptions(threshold=np.nan)
    a = np.arange(10000).reshape(100, 100)
    printInfo(a)
    # 2.2.4 
    a = np.array([20.1, 21.1, 22.1])
    b = np.arange(3)
    printInfo(a - b)
    printInfo(b ** 2)
    printInfo(10 * np.sinc(a))
    printInfo(a < 21)
    printInfo(a * b)
    a = np.array([[1, 1], [2, 2]])
    b = np.array([[2, 2], [1, 1]])
    printInfo(a * b)
    a = np.arange(3 * 4).reshape(3, 4)
    b = np.arange(4 * 2).reshape(4, 2)
    printInfo(a @ b)
    printInfo(a.dot(b))
    a = np.ones((2, 3), dtype=int)
    a *= 3
    printInfo(a)
    b = np.random.random((2, 3))
    b += a
    printInfo(b)
    try:
        a += b
    except Exception as e:
        print(e)
    a = np.ones(3, dtype=np.int32)
    b = np.linspace(0, np.pi, 3)
    printInfo(a)
    printInfo(b)
    printInfo(a + b)
    a = np.exp((a + b) * 1j)
    printInfo(a)
    a = np.random.random((2, 3))
    printInfo(a)
    print(a.sum(), a.min(), a.max(), sep=' | ')
    a = np.arange(3 * 4).reshape(3, 4)
    printInfo(a)
    print(a.sum(axis=0), a.min(axis=1), a.cumsum(axis=1), sep=' | ')
    # 2.2.5
    a = np.linspace(1.0, np.e, 12).reshape(3, 4)
    printInfo(a)
    printInfo(np.exp(a))
    printInfo(np.sqrt(a))
    b = 3 * a
    printInfo(np.add(a, b))
    # 2.2.6
    a = np.arange(10) ** 3
    printInfo(a)
    print(a[3], a[3:7], a[3:9:2], a[::-2], sep=' | ')
    a[3:9:2] = -1000
    printInfo(a)
    for i in a:
        print(i ** (1 / 3))

    def f(x, y):
        return 10 * x + y

    a = np.fromfunction(f, (5, 4), dtype=np.int32)
    printInfo(a)
    print(a[1, 2], a[0:5, 1], a[:, 1], a[1:3, :], a[-1, -1], a[-1], a[-1, :], sep=' | ')
    a = np.array([[[ 0, 1, 2, 3],
                   [ 10, 11, 12, 13],
                   [ 20, 21, 22, 23]],
                   
                   [[100, 101, 102, 103],
                    [110, 111, 112, 113],
                    [120, 121, 122, 123]]])
    printInfo(a)
    printInfo(a[1, ...])
    printInfo(a[..., 3])
    printInfo(a[:, 1, :])
    for r in a:
        print(r)
    for e in a.flat:
        print(e)
    # 2.7.1
    a = np.array([[1.0, -2.0, 3.0],
                  [-4.0, 5.0, -6.0],
                  [7.0, -8.0, 9.0]])
    printInfo(a)
    printInfo(a.transpose())
    printInfo(np.linalg.inv(a))
    printInfo(np.eye(3))
    b = np.linspace(0.0, 1.0, 9).reshape(3, 3)
    printInfo(a @ b)
    print(a.trace())
    b = np.array([-5., 7., -9.])
    printInfo(np.linalg.solve(a, b))
    print(np.linalg.eig(a))
