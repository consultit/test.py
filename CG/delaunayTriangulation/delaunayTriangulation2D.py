'''
Created on Jun 7, 2020

@author: consultit
'''

from .geometricPrimitives2D import Triangle, Orient2D, \
    InCircle, Edge, GHOSTVERTEX, Vertex, DrawData


class DelaunayTriangulation(object):
    '''
    Delaunay triangulation algorithm.

    :param vertexList: the (random) vertices permutation
    :type vertexList: iterable

    .. seealso:: '3 Algorithms for Constructing Delaunay Triangulations .'
                Delaunay Mesh Generation, by Siu-Wing Cheng et al.,
                Chapman and Hall/CRC, 2016, pp. 55–73.
    '''

    def __init__(self, vertexList, drawData=None):
        '''
        Constructor
        '''
        # the (random and not empty) vertices permutation
        self._vertexList = list(vertexList)
        assert(self._vertexList)
        # data structure used by adjacent(): each item's value is a triangle
        # effectively belonging to the triangulation
        self._triangleByEdge = dict()
        # data structure used by adjacent2Vertex()
        self._vertexByVertex = dict()
        # data structures implementing conflict graph
        self._conflictTriangleByVertex = dict()
        self._conflictVerticesByTriangle = dict()
        # debug draw
        self._drawData = drawData
        # central point of triangulation
        self._vertexDrawData = None
        if self._drawData:
            c = (self._drawData.color[2], self._drawData.color[0],
                 self._drawData.color[1], self._drawData.color[3])
            t = self._drawData.thickness * 3
            r = self._drawData.render
            self._vertexDrawData = DrawData(c, t, r)
        # bootstrap flag
        self._bootStrapped = False

    def build(self, byStep=False):
        '''Builds the Delaunay triangulation.

        :param byStep: step-by-step flag (default: False)
        :type byStep: bool
        '''

        if not self._bootStrapped:
            self.bootstrap()
        # insert one vertex at a time
        if self._drawData:
            c = (self._vertexDrawData.color[0] * 0.8, self._vertexDrawData.color[1] * 0.1,
                 self._vertexDrawData.color[2] * 0.2, self._vertexDrawData.color[3])
            t = self._vertexDrawData.thickness * 4
            r = self._vertexDrawData.render
            vertexInsDrawData = DrawData(c, t, r)
        while self._vertexList:
            vertex = self._vertexList.pop(0)
            if self._drawData:
                _ = Vertex(vertex.x, vertex.y, vertexInsDrawData)
            if byStep:
                yield
            self.insertVertexAtConflict(vertex)

    @property
    def triangles(self):
        '''Returns the Delaunay Triangulation triangles.

        :return: the collection of all triangles of the triangulation
        :rtype: iterable
        '''

        return set(self._triangleByEdge.values())

    def bootstrap(self):
        '''Bootstraps the Delaunay triangulation algorithm.
        '''

        # choose three arbitrary vertices that are not collinear
        assert(len(self._vertexList) >= 3)
        v1 = self._vertexList.pop(0)
        v2 = self._vertexList.pop(0)
        v3 = None
        for i, v in enumerate(self._vertexList):
            if Orient2D(v1, v2, v) == 0:
                continue
            v3 = v
            self._vertexList.pop(i)
            break
        assert(v3)
        # build the triangle having those three vertices
        if Orient2D(v1, v2, v3) >= 0:
            triangle = Triangle(v1, v2, v3, self._drawData)
        else:
            triangle = Triangle(v1, v3, v2, self._drawData)
        self.addTriangle(triangle)
        # compute central point inside triangulation
        self._centralPoint = Vertex(
            (triangle.u.x + triangle.v.x + triangle.w.x) / 3,
            (triangle.u.y + triangle.v.y + triangle.w.y) / 3,
            drawData=self._vertexDrawData
        )
        # build three ghost triangles
        triGhost1 = Triangle(
            triangle.v, triangle.u, GHOSTVERTEX, self._drawData)
        self.addTriangle(triGhost1)
        #
        triGhost2 = Triangle(
            triangle.w, triangle.v, GHOSTVERTEX, self._drawData)
        self.addTriangle(triGhost2)
        #
        triGhost3 = Triangle(
            triangle.u, triangle.w, GHOSTVERTEX, self._drawData)
        self.addTriangle(triGhost3)
        # compute a conflict graph: a conflict graph records a conflict (w, τ)
        # such that w ∈ τ. If two triangles contain w, a solid triangle is
        # preferred over a ghost.
        for v in self._vertexList:
            if triangle.contains(v):
                self._conflictTriangleByVertex[v] = triangle
                vList = self._conflictVerticesByTriangle.get(triangle, None)
                if vList:
                    vList += [v]
                else:
                    self._conflictVerticesByTriangle[triangle] = [v]
                continue
            for t in (triGhost1, triGhost2, triGhost3):
                if self.ghostContains(t, v):
                    self._conflictTriangleByVertex[v] = t
                    vList = self._conflictVerticesByTriangle.get(t, None)
                    if vList:
                        vList += [v]
                    else:
                        self._conflictVerticesByTriangle[t] = [v]
                    break
        self._bootStrapped = True

    def insertVertexAtConflict(self, u):
        '''Inserts a vertex into a Delaunay triangulation.

        :param u: vertex to be inserted
        :type u: :class:`Vertex`
        '''

        # 1.
        vwx = self._conflictTriangleByVertex[u]
        v, w, x = vwx.u, vwx.v, vwx.w
        setattr(vwx, '_visited', True)
        # 2.
        D = dict({vwx: vwx})
        C = dict()
        # 3.
        vw = Edge(v, w)
        self.markCavity(u, vw, D, C)
        wx = Edge(w, x)
        self.markCavity(u, wx, D, C)
        xv = Edge(x, v)
        self.markCavity(u, xv, D, C)
        # 4.
        for vwx in D:
            self.redistributeList(u, vwx, C)
        # 5.
        for vwx in D:
            self.deleteTriangle(vwx)
        # 6.
        for vwx in C:
            self.addTriangle(vwx)

    def markCavity(self, u, vw, D, C):
        '''Called to identify the other deleted triangles and the new triangles.

        :param u: the vertex that is being inserted
        :type u: :class:`Vertex`
        :param vw: the edge to be tested
        :type vw: :class:`Edge`
        :param D: set of triangles to be deleted
        :type D: set (of :class:`Triangle)` (implemented as dict)
        :param C: set of triangles to be created
        :type C: set (of :class:`Triangle)` (implemented as dict)
        '''

        v, w = vw.u, vw.v
        # 1.
        x = self.adjacent(w, v)
        wvx = self._triangleByEdge.get(Edge(w, v))
        # 2.
        if hasattr(wvx, '_visited'):
            return
        # 3.
        if wvx.isGhost():
            inTest = InCircle(wvx.u, wvx.v, wvx.w, u) > 0
        else:
            inTest = InCircle(u, v, w, x) > 0
        if inTest:
            setattr(wvx, '_visited', True)
            D[wvx] = wvx
            vx = Edge(v, x)
            self.markCavity(u, vx, D, C)
            xw = Edge(x, w)
            self.markCavity(u, xw, D, C)
        # 4.
        else:
            uvw = Triangle(u, v, w, self._drawData)
            C[uvw] = uvw

    def redistributeList(self, u, vwx, C):
        '''Updates the conflict graph by redistributing a triangle’s vertices.

        :param u: the vertex that is being inserted
        :type u: :class:`Vertex`
        :param vwx: the triangle whose conflict list is to be tested
        :type vwx: :class:`Triangle`
        '''

        # 1.
        vList = self._conflictVerticesByTriangle.get(vwx, None)
        if not vList:
            # no vertex has vwx as conflicting triangle
            return
        for y in vList:
            if u == y:
                # u (being inserted) is in the conflict list of vwx: remove
                # from conflict graph and continue
                self._conflictTriangleByVertex.pop(y)
                continue
            # 1.(a)
            abc = vwx
            uba = None
            ray = Edge(u, y)
            # iterate over triangles along the ray uy
            while hasattr(abc, '_visited'):
                struck = False
                if abc == vwx:
                    # starting triangle: check all three edges
                    if self.intersect(ray, Edge(abc.u, abc.v)):
                        ab = Edge(abc.v, abc.u)
                        struck = True
                    elif self.intersect(ray, Edge(abc.v, abc.w)):
                        ab = Edge(abc.w, abc.v)
                        struck = True
                    elif self.intersect(ray, Edge(abc.w, abc.u)):
                        ab = Edge(abc.u, abc.w)
                        struck = True
                else:
                    # subsequent triangles along the ray: ab has been already
                    # checked, so check only the other two edges bc and ca
                    if ab.u == abc.u:
                        bc = Edge(abc.v, abc.w)
                        ca = Edge(abc.w, abc.u)
                    elif ab.u == abc.v:
                        bc = Edge(abc.w, abc.u)
                        ca = Edge(abc.u, abc.v)
                    else:
                        bc = Edge(abc.u, abc.v)
                        ca = Edge(abc.v, abc.w)
                    # checks
                    if self.intersect(ray, bc):
                        ab = Edge(bc.v, bc.u)
                        struck = True
                    elif self.intersect(ray, ca):
                        ab = Edge(ca.v, ca.u)
                        struck = True
                if not struck:
                    # we could reach here only when abc is ghost and no edge
                    # has been struck, in which case the vertex y lies in one
                    # of the newly created ghost triangles
                    for uvw in C:
                        if uvw.isGhost() and self.ghostContains(uvw, y):
                            uba = uvw
                            break
                    # uba found: exit iteration
                    break
                # get the triangle adjoining the struck edge ba
                abc = self._triangleByEdge[ab]
            # 1.(b)
            if not uba:
                b, a = ab.v, ab.u
                # temporary triangle used as a key to retrieve the one already
                # created and equal to it
                uba = C[Triangle(u, b, a)]
            self._conflictTriangleByVertex[y] = uba
            vList = self._conflictVerticesByTriangle.get(uba, None)
            if vList:
                vList += [y]
            else:
                self._conflictVerticesByTriangle[uba] = [y]
        # 2.
        vList = self._conflictVerticesByTriangle.pop(vwx)

    def intersect(self, ray, edge):
        '''Checks if a ray-edge intersection exists.

        If ray and edge are parallel or overlapping returns False: this should
        be fixed for a general purpose use.

        :param ray: the (possibly intersecting) ray
        :type ray: :class:`Edge`
        :param edge: the (possibly intersecting) edge 
        :type edge: :class:`Edge`
        :return: True if ray and edge intersect, False otherwise
        :rtype: bool
        '''

        # ray -> R = ray.u + r*(ray.v-ray.u), r>=1
        # edge -> E = edge.u + e*(edge.v-edge.u), 0<=e<=1
        # intersection when R=E with  r>=0 and 0<=e<=1
        hasGhost = False
        # Note: if edge has a ghost vertex: edge -> E = c + e*(X-c), e>=1, with
        # c=central point and X=non ghost vertex and intersection when R=E with
        # r>=0 and e>=1
        if edge.u == GHOSTVERTEX:
            hasGhost = True
            edge = Edge(self._centralPoint, edge.v)
        elif edge.v == GHOSTVERTEX:
            hasGhost = True
            edge = Edge(self._centralPoint, edge.u)
        Ax = ray.v.x - ray.u.x
        Ay = ray.v.y - ray.u.y
        Bx = -(edge.v.x - edge.u.x)
        By = -(edge.v.y - edge.u.y)
        Tx = edge.u.x - ray.u.x
        Ty = edge.u.y - ray.u.y
        det = Ax * By - Ay * Bx
        if det != 0.0:
            # avoid expensive operations
            # r = (Tx * By - Ty * Bx) / det
            # e = (Ax * Ty - Ay * Tx) / det
            rN = (Tx * By - Ty * Bx)
            eN = (Ax * Ty - Ay * Tx)
            if not hasGhost:
                # return (r >= 1) and ((0 <= e) and (e <= 1))
                rGEone = (rN >= det) if det > 0 else (rN <= det)  # (r >= 1)
                zeroLEe = (0 <= eN) if det > 0 else (0 >= eN)  # (0 <= e)
                eLEone = (eN <= det) if det > 0 else (eN >= det)  # (e <= 1)
                return rGEone and (zeroLEe and eLEone)
            else:
                # return (r >= 1) and (e >= 1)
                rGEone = (rN >= det) if det > 0 else (rN <= det)  # (r >= 1)
                eGEone = (eN >= det) if det > 0 else (eN <= det)  # (e >= 1)
                return rGEone and eGEone
        else:
            # ray and edge are parallel/overlapping
            return False

    def ghostContains(self, t, v):
        '''Check if a vertex should be considered inside a ghost triangle.

        This is used when redistributing conflicts: it treats the ghost edges as
        rays that point directly away from a central point in the interior of
        the triangulation. The ghost edges partition the portion of the plane
        outside the triangulation into unbounded, convex ghost 'triangles', each
        having one solid edge and two diverging rays in its boundary. Each
        uninserted vertex outside the triangulation chooses as its conflict the
        unbounded ghost triangle that contains it.

        :param t: the ghost triangle to check 
        :type t: :class:`Triangle`
        :param v: the vertex to check for containment
        :type v: :class:`Vertex`
        :return: True if vertex is inside the ghost triangle, False otherwise
        :rtype: bool        
        '''
        return ((InCircle(t.u, t.v, t.w, v) > 0) and
                (
            ((t.u == GHOSTVERTEX) and (Orient2D(self._centralPoint, t.v, v) <= 0) and (
                Orient2D(self._centralPoint, t.w, v) >= 0))
            or
            ((t.v == GHOSTVERTEX) and (Orient2D(self._centralPoint, t.w, v) <= 0) and (
                Orient2D(self._centralPoint, t.u, v) >= 0))
            or
            ((t.w == GHOSTVERTEX) and (Orient2D(self._centralPoint, t.u, v) <= 0) and (
                Orient2D(self._centralPoint, t.v, v) >= 0))
        )
        )

    def addTriangle(self, triangle):
        '''Add a positively oriented triangle to the triangulation.

        :param triangle: the triangle to be added
        :type triangle: :class:`Triangle`
        :return: True if the triangle was successfully added, False otherwise 
        :rtype: bool
        '''

        u, v, w = triangle.u, triangle.v, triangle.w
        # insert triangle for each (oriented) edge
        uvEdge = Edge(u, v)
        uvTriangle = self._triangleByEdge.get(uvEdge, None)
        if uvTriangle:
            return False
        vwEdge = Edge(v, w)
        vwTriangle = self._triangleByEdge.get(vwEdge, None)
        if vwTriangle:
            return False
        wuEdge = Edge(w, u)
        wuTriangle = self._triangleByEdge.get(wuEdge, None)
        if wuTriangle:
            return False
        # add data used by adjacent()
        self._triangleByEdge[uvEdge] = triangle
        self._triangleByEdge[vwEdge] = triangle
        self._triangleByEdge[wuEdge] = triangle
        # add data used by adjacent2Vertex()
        self._vertexByVertex[triangle.u] = v
        self._vertexByVertex[triangle.v] = w
        self._vertexByVertex[triangle.w] = u
        #
        return True

    def deleteTriangle(self, triangle):
        '''Delete a positively oriented triangle from the triangulation.

        :param triangle: the triangle to be deleted
        :type triangle: :class:`Triangle`
        :return: True if the triangle was successfully deleted, False otherwise 
        :rtype: bool
        '''

        u, v, w = triangle.u, triangle.v, triangle.w
        # remove triangle for each (oriented) edge
        uvEdge = Edge(u, v)
        uvTriangle = self._triangleByEdge[uvEdge]
        if not uvTriangle:
            return False
        vwEdge = Edge(v, w)
        vwTriangle = self._triangleByEdge.get(vwEdge, None)
        if not vwTriangle:
            return False
        wuEdge = Edge(w, u)
        wuTriangle = self._triangleByEdge.get(wuEdge, None)
        if not wuTriangle:
            return False
        assert(uvTriangle == vwTriangle == wuTriangle)
        # remove data used by adjacent2Vertex(): removal not needed
        # remove data used by adjacent()
        del self._triangleByEdge[uvEdge]
        del self._triangleByEdge[vwEdge]
        del self._triangleByEdge[wuEdge]
        return True

    def adjacent(self, u, v):
        '''Return a vertex w such that uvw is a positively oriented triangle.

        :param u, v: two vertices representing one of the edges of a (possibly
        existing) triangle
        :type u, v: :class:`Vertex`
        :return: a vertex w such that uvw is a positively oriented triangle, or
        None if no such triangle exists
        :rtype: a :class:`Vertex`
        '''

        uvTriangle = self._triangleByEdge.get(Edge(u, v))
        if not uvTriangle:
            return None
        if u == uvTriangle.u:
            return uvTriangle.w
        if u == uvTriangle.v:
            return uvTriangle.u
        if u == uvTriangle.w:
            return uvTriangle.v

    def adjacent2Vertex(self, u):
        '''Return vertices v, w such that uvw is a positively oriented triangle.

        :param u: one vertex belonging to a (possibly existing) triangle
        :type u: :class:`Vertex`
        :return: a pair vertex (v,w) such that uvw is a positively oriented
        triangle, or None if no such triangle exists
        :rtype: a pair (tuple) of :class:`Vertex`
        '''

        v = self._vertexByVertex.get(u, None)
        if not v:
            return None
        triangle = self._triangleByEdge.get(Edge(u, v), None)
        if not triangle:
            return None
        if triangle.u == u:
            return triangle.v, triangle.w
        elif triangle.v == u:
            return triangle.w, triangle.u
        elif triangle.w == u:
            return triangle.u, triangle.v

    def insertVertex(self, u, v, w, x):
        '''NOT USED.
        '''

        self.deleteTriangle(v, w, x)
        self.digCavity(u, v, w)
        self.digCavity(u, w, x)
        self.digCavity(u, x, v)

    def digCavity(self, u, v, w):
        '''NOT USED.
        '''

        x = self.adjacent(w, v)
        if not x:
            return
        if InCircle(u, v, w, x) > 0:
            self.deleteTriangle(w, v, x)
            self.digCavity(u, v, x)
            self.digCavity(u, x, w)
        else:
            self.addTriangle(u, v, w)
