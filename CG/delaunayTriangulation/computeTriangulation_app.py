'''
Created on Jul 22, 2020

@author: consultit
'''

from random import shuffle
import argparse
import textwrap
from direct.showbase.ShowBase import ShowBase
from test_py.CG.delaunayTriangulation.delaunayTriangulation2D import \
    DelaunayTriangulation as DT
from test_py.CG.delaunayTriangulation.geometricPrimitives2D import DrawData, \
    Vertex
from test_py.common.random import RandFloat


def createRandomizedVertices():
    '''create and randomize the vertices
    '''

    drawDataV = DrawData(thickness=6, render=app.render, color=(0, 0, 1, 0))
    halfExt = args.extent / 2.0
    vertexList = [Vertex(RandFloat(halfExt), RandFloat(halfExt), drawData=drawDataV)
                  for _ in range(args.num_vertices)]
    shuffle(vertexList)

    # debug: print the vertex list
    print('# vertex list')
    print('vertexList = [')
    for v in vertexList:
        print('\t' + str(v).rstrip(')') + ', drawData=drawDataV),')
    print(']')
    sortedVertexList = sorted(vertexList)
    print('# sorted vertex list')
    print('sorted vertexList = [')
    for v in sortedVertexList:
        print('\t' + str(v).rstrip(')') + ', drawData=drawDataV),')
    print(']')

    #
    return vertexList


def buildDT(dtGen):
    if dtGen[0]:
        try:
            next(dtGen[0])
        except StopIteration:
            print('Delaunay Triangulation built!')


def startDT(dt, dtGen, byStep, drawData):
    dt[0] = DT(createRandomizedVertices(), drawData)
    dtGen[0] = dt[0].build(byStep=byStep)


if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This script will build Delaunay Trianguleation of randomly chosen vertices
    
    Use:
        - 's' to (re)create a Delaunay Trianguleation
        - 'n' to start Delaunay Trianguleation build
    '''))
    # set up arguments
    parser.add_argument('-n', '--num-vertices', type=int, default=5,
                        help='the number of the vertices')
    parser.add_argument('-e', '--extent', type=float, default=4.0,
                        help='the extension of zero-centered vertex field')
    parser.add_argument('--by-step', action='store_true',
                        help='step-by-step flag')
    # parse arguments
    args = parser.parse_args()

    # do actions
    app = ShowBase()

    # create draw data
    drawData = DrawData(thickness=1.0, render=app.render)

    # create a new DelaunayTriangulation object
    dt = [None]
    dtGen = [None]
    app.accept('s', startDT, extraArgs=[dt, dtGen, args.by_step, drawData])

    # build the triangulation
    app.accept('n', buildDT, extraArgs=[dtGen])

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 20.0, 0.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
