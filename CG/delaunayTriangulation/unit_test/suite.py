'''
Created on May 16, 2019

@author: consultit
'''

from ....common.random import RandInt32, RandInt64, RandFloat, RandDouble


def clamp(my_value, min_value, max_value):
    return max(min(my_value, max_value), min_value)
