'''
Created on Jun 7, 2020

@author: consultit
'''

import unittest
from heapq import heappush, heappop
from test_py.CG.delaunayTriangulation.geometricPrimitives2D import Vertex, \
    Orient2D, InCircle, Triangle, GHOSTVERTEX
from test_py.CG.delaunayTriangulation.unit_test.suite import RandInt32, \
    RandFloat, clamp

NUMPOINTS = 100
RANDRANGE = 100


def setUpModule():
    pass


def tearDownModule():
    pass


class VertexTest(unittest.TestCase):

    def setUp(self):
        self.heap = []
        for _ in range(NUMPOINTS):
            x = RandInt32(RANDRANGE)
            y = RandInt32(RANDRANGE)
            heappush(self.heap, Vertex(float(x), float(y)))

    def tearDown(self):
        del self.heap

    def testOrder(self):
        self.assertTrue(Vertex(1, 1) == Vertex(1, 1))
        self.assertTrue(Vertex(1, 1) <= Vertex(1, 1))
        self.assertTrue(Vertex(1, 1) >= Vertex(1, 1))
        #
        self.assertTrue(Vertex(1, 1) < Vertex(2, 1))
        self.assertTrue(Vertex(1, 1) <= Vertex(2, 1))
        self.assertTrue(Vertex(1, 1) < Vertex(1, 2))
        self.assertTrue(Vertex(1, 1) <= Vertex(1, 2))
        #
        self.assertTrue(Vertex(2, 1) > Vertex(1, 1))
        self.assertTrue(Vertex(2, 1) >= Vertex(1, 1))
        self.assertTrue(Vertex(1, 2) > Vertex(1, 1))
        self.assertTrue(Vertex(1, 2) >= Vertex(1, 1))
        #
        u = Vertex(1, 1)
        v = Vertex(2, 1)
        w = Vertex(1.5, 1.5)
        self.assertEqual(min(u, v, w), u)
        self.assertEqual(max(u, v, w), v)

    def testOrder2(self):
        for i in range(NUMPOINTS):
            vertex = heappop(self.heap)
            if i == 0:
                prevX = vertex.x
                prevY = vertex.y
                continue
            self.assertTrue(prevX <= vertex.x)
            if prevX == vertex.x:
                self.assertTrue(prevY <= vertex.y)
            prevX = vertex.x
            prevY = vertex.y
            print(vertex)

    def testHash(self):
        # test for equality
        v = Vertex(1, 1)
        v1 = Vertex(1, 1)
        w = Vertex(1.1, 1.1)
        self.assertNotEqual(id(v), id(v1))
        self.assertEqual(v, v1)
        self.assertNotEqual(v, w)
        self.assertNotEqual(v1, w)
        self.assertEqual(w, w)
        self.assertEqual(hash(v), hash(v1))
        self.assertNotEqual(hash(v), hash(w))
        self.assertNotEqual(hash(v), hash(w))
        self.assertEqual(hash(w), hash(w))
        # test for hashable
        d = dict()
        d[v] = 'v'
        d[w] = 'w'
        self.assertEqual(d[v1], d[v])
        self.assertNotEqual(d[w], d[v])
        self.assertNotEqual(d[w], d[v1])
        d[v1] = 'v1'
        self.assertEqual(d[v1], d[v])
        self.assertNotEqual(d[w], d[v])
        self.assertNotEqual(d[w], d[v1])

    def testGhost(self):
        # test for ghost
        u = GHOSTVERTEX
        x = RandInt32(RANDRANGE)
        y = RandInt32(RANDRANGE)
        v = Vertex(float(x), float(y))
        x = RandInt32(RANDRANGE)
        y = RandInt32(RANDRANGE)
        w = Vertex(float(x), float(y))
        self.assertLess(v, u)
        self.assertGreater(u, v)
        self.assertEqual(v, min(u, v))
        self.assertEqual(u, max(u, v))
        self.assertEqual(w, min(u, w))
        self.assertEqual(u, max(u, w))
        self.assertEqual(u, max(u, v, w))


class TriangleTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testHash(self):
        # u0==w1==v2, v0==u1==w2, w0==v1==u2
        u0, v0, w0 = Vertex(1, 1), Vertex(2, 1), Vertex(1.5, 1.5)
        u1, v1, w1 = Vertex(2, 1), Vertex(1.5, 1.5), Vertex(1, 1)
        u2, v2, w2 = Vertex(1.5, 1.5), Vertex(1, 1), Vertex(2, 1)
        self.assertGreater(Orient2D(u0, v0, w0), 0.0)
        self.assertGreater(Orient2D(u1, v1, w1), 0.0)
        self.assertGreater(Orient2D(u2, v2, w2), 0.0)
        # test for equality
        t0 = Triangle(u0, v0, w0)
        t1 = Triangle(u1, v1, w1)
        t2 = Triangle(u2, v2, w2)
        self.assertEqual(t0, t1)
        self.assertEqual(t1, t2)
        self.assertEqual(t2, t0)
        # test for hashable
        u3, v3, w3 = Vertex(1.1, 1.1), Vertex(2.1, 1.1), Vertex(1.6, 1.6)
        t3 = Triangle(u3, v3, w3)
        self.assertGreater(Orient2D(u3, v3, w3), 0.0)
        d = dict()
        d[t3] = 't3'
        d[t0] = 't0'
        self.assertEqual(d[t3], d[t3])
        self.assertEqual(d[t0], d[t1])
        self.assertEqual(d[t1], d[t2])
        self.assertEqual(d[t2], d[t0])
        self.assertNotEqual(d[t3], d[t0])
        self.assertNotEqual(d[t3], d[t1])
        self.assertNotEqual(d[t3], d[t2])
        d[t1] = 't1'
        self.assertEqual(d[t0], d[t1])
        self.assertEqual(d[t1], d[t2])
        self.assertEqual(d[t2], d[t0])
        self.assertNotEqual(d[t3], d[t0])
        self.assertNotEqual(d[t3], d[t1])
        self.assertNotEqual(d[t3], d[t2])
        d[t2] = 't2'
        self.assertEqual(d[t0], d[t1])
        self.assertEqual(d[t1], d[t2])
        self.assertEqual(d[t2], d[t0])
        self.assertNotEqual(d[t3], d[t0])
        self.assertNotEqual(d[t3], d[t1])
        self.assertNotEqual(d[t3], d[t2])

    def testGhost(self):
        # test for ghost
        x1, y1, x2, y2 = float(RandInt32(RANDRANGE)), float(
            RandInt32(RANDRANGE)), float(RandInt32(RANDRANGE)), float(RandInt32(RANDRANGE))
        u, v, w = GHOSTVERTEX, Vertex(x1, y1), Vertex(x2, y2)
        t = Triangle(u, v, w)
        self.assertTrue(t.isGhost())

    def testContainment(self):
        # test for point containment
        u, v, w = Vertex(1, 1), Vertex(3, 1), Vertex(2, 3)
        t = Triangle(u, v, w)
        coords = [0, 0, 0]
        # inside or on
        for i in range(NUMPOINTS):
            idx = i % 3
            idx1 = (idx + 1) % 3
            idx2 = (idx + 2) % 3
            coords[idx] = clamp(0.5 + RandFloat(0.5), 0, 1)
            residue = clamp(1.0 - coords[idx], 0, 1)
            residue2 = residue / 2.0
            coords[idx1] = clamp(residue2 + RandFloat(residue2), 0, residue)
            residue -= coords[idx1]
            coords[idx2] = clamp(1.0 - coords[idx] - coords[idx1], 0, residue)
            p = Vertex(float(u.x) * coords[0] + float(v.x) * coords[1] + float(w.x) * coords[2],
                       float(u.y) * coords[0] + float(v.y) * coords[1] + float(w.y) * coords[2])
            self.assertTrue(t.contains(p))
        # outside
        for i in range(NUMPOINTS):
            idx = i % 3
            idx1 = (idx + 1) % 3
            idx2 = (idx + 2) % 3
            while True:
                coords[idx] = 0.5 + RandFloat(0.5)
                coords[idx1] = 0.5 + RandFloat(0.5)
                coords[idx2] = 1.0 - coords[idx] - coords[idx1]
                if not (((coords[idx] >= 0.0) and (coords[idx] <= 1.0)) and
                        ((coords[idx1] >= 0.0) and (coords[idx1] <= 1.0)) and
                        ((coords[idx2] >= 0.0) and (coords[idx2] <= 1.0))):
                    break
            p = Vertex(float(u.x) * coords[0] + float(v.x) * coords[1] + float(w.x) * coords[2],
                       float(u.y) * coords[0] + float(v.y) * coords[1] + float(w.y) * coords[2])
            self.assertFalse(t.contains(p))


class PredicateTest(unittest.TestCase):

    def setUp(self):
        self.a = Vertex(-0.0654, -0.877)
        self.b = Vertex(0.776, 0.0548)
        self.c = Vertex(-0.420, -1.11)

    def tearDown(self):
        del self.a
        del self.b
        del self.c

    def testOrient2D(self):
        self.assertGreater(Orient2D(self.a, self.b, self.c), 0.0)

    def testInCircle(self):
        d = Vertex(-0.623, 0.849)
        self.assertGreater(InCircle(self.a, self.b, self.c, d), 0.0)
        #
        a, b, c = Vertex(-1.09, 1.84), Vertex(0.858, -
                                              1.12), Vertex(-0.989, 1.86)
        d = Vertex(0.649, 1.50)
        self.assertLess(InCircle(a, b, c, d), 0.0)
        d = Vertex(1.28, -0.404)
        self.assertLess(InCircle(a, b, c, d), 0.0)
        #
        a, b, c = Vertex(1.15, -1.68), Vertex(1.81, 1.81), Vertex(1.34, 1.86)
        d = Vertex(-0.256, 1.55)
        self.assertLess(InCircle(a, b, c, d), 0.0)
        d = Vertex(-0.901, 0.823)
        self.assertLess(InCircle(a, b, c, d), 0.0)
        #
        a, b, c = Vertex(-1.89, -0.132), Vertex(-1.43, -
                                                0.643), Vertex(0.0134, 0.705)
        d = Vertex(-0.180, -0.485)
        self.assertLess(InCircle(a, b, c, d), 0.0)
        #
        a, b, c = Vertex(0.00710, 0.464), Vertex(
            0.512, -1.53), Vertex(1.10, -0.425)
        d = Vertex(-1.10, -0.655)
        self.assertLess(InCircle(a, b, c, d), 0.0)
        #
        a, b, c = Vertex(-1.05, 1.98), Vertex(-1.03,
                                              0.345), Vertex(0.214, 1.46)
        d = Vertex(-1.59, 1.05)
        self.assertLess(InCircle(a, b, c, d), 0.0)
        #
        a, b, c = Vertex(x=-1.13, y=2.58),Vertex(x=-1.06, y=2.49),Vertex(x=0.0965, y=2.54)
        d = Vertex(x=-1.31, y=2.74)
        self.assertLess(InCircle(a, b, c, d), 0.0)


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((VertexTest('testOrder'), VertexTest(
        'testOrder2'), VertexTest('testHash'), VertexTest('testGhost')))
    suite.addTests((TriangleTest('testHash'), TriangleTest(
        'testGhost'), TriangleTest('testContainment')))
    suite.addTests((PredicateTest('testOrient2D'),
                    PredicateTest('testInCircle')))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
