'''
Created on Jun 7, 2020

@author: consultit
'''

from decimal import Decimal as D, getcontext
from panda3d.core import LPoint3f, LineSegs, NodePath, GeomNode
from ...common.relational import Relational

# set precision
PRECISION = 28
getcontext().prec = 28

# draw data


class DrawData(object):
    def __init__(self, color=(1, 0.0, 1, 1), thickness=1.0, render=None):
        self.color = color
        self.thickness = thickness
        self.render = render


# geometric constructors

class Simplex(object):
    '''This class represents a generic simplex embedded into R2 (X,Y plane).
    '''


class Vertex(Simplex, Relational):
    '''This class represents a vertex: 0-simplex embedded into R2 (X,Y plane).

    Objects of this class can be lexicographically ordered: we first sort by
    x-coordinate, and if vertices have the same x-coordinate we sort them by
    y-coordinate.
    Object of this class can be used as a dictionary or set keys and, in this
    respect, they behaves like the (x, y) tuples.

    :param x: the vertex x coordinate
    :type x: float
    :param y: the vertex y coordinate
    :type y: float
    :param drawData: the data used for debug drawing
    :type drawData: :class:`DrawData`
    '''

    def __init__(self, x, y, drawData=None):
        self._x = D(x)
        self._y = D(y)
        # draw stuff
        self._draw = False
        if drawData:
            self._draw = True
            color, thickness, render = drawData.color, drawData.thickness, drawData.render
            # drawing stuff
            p = LPoint3f(float(self._x), 0, float(self._y))
            vertex = LineSegs('vertex')
            vertex.set_thickness(thickness)
            vertex.set_color(*color)
            self._pointnp = NodePath(
                GeomNode('point_GN_' + str(hash((self._x, self._y)))))
            # update transform
            geomnode = self._pointnp.node()
            vertex.move_to(p)
            if geomnode.get_num_geoms() > 0:
                geomnode.remove_geom(0)
            vertex.create(geomnode, True)
            if render:
                self._pointnp.reparent_to(render)

    def __del__(self):
        if self._draw:
            self._pointnp.remove_node()

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    # base (mandatory)
    def __eq__(self, other):
        return (self._x == other.x) and (self._y == other.y)

    # base (mandatory)
    # to be used as a dictionary key
    def __lt__(self, other):
        return ((self._x < other.x) or
                ((self._x == other.x) and (self._y < other.y)))

    def __hash__(self):
        return hash((self._x, self._y))

    # optional (for print)
    def __repr__(self):
        return 'Vertex(x=' + '{:.3}'.format(self._x) + ', y=' + '{:.3}'.format(self._y) + ')'

    def __str__(self):
        return self.__repr__()


Point = Vertex

GHOSTVERTEX = Vertex(D('Infinity'), D('Infinity'))


class Edge(Simplex):
    '''This class represents an edge: 1-simplex embedded into R2 (X,Y plane).

    Edge is oriented: uv != vu.
    Object of this class can be used as a dictionary or set keys and, in this
    respect, they behaves like the (u, v) tuples.

    :param u: the first edge vertex
    :type u: :class:`Vertex`
    :param v: the second edge vertex
    :type v: :class:`Vertex`
    :param drawData: the data used for debug drawing
    :type drawData: :class:`DrawData`
    '''

    def __init__(self, u, v, drawData=None):
        self._u, self._v = u, v
        # compute the uuid of this edge
        self._uuid = hash((self._u, self._v))
        # draw stuff
        self._draw = False
        if drawData:
            self._draw = True
            color, thickness, render = drawData.color, drawData.thickness, drawData.render
            # drawing stuff
            p0 = LPoint3f(float(self._u.x), 0, float(self._u.y))
            p1 = LPoint3f(float(self._v.x), 0, float(self._v.y))
            segment = LineSegs('segment')
            segment.set_thickness(thickness)
            segment.set_color(*color)
            self._segmentnp = NodePath(
                GeomNode('segment_GN_' + str(self._uuid)))
            # update transform
            geomnode = self._segmentnp.node()
            segment.move_to(p0)
            segment.draw_to(p1)
            if geomnode.get_num_geoms() > 0:
                geomnode.remove_geom(0)
            segment.create(geomnode, True)
            if render:
                self._segmentnp.reparent_to(render)

    def __del__(self):
        if self._draw:
            self._segmentnp.remove_node()

    @property
    def u(self):
        return self._u

    @property
    def v(self):
        return self._v

    @property
    def uuid(self):
        return self._uuid

    # to be used as a dictionary key
    def __eq__(self, other):
        return (self._u == other.u) and (self._v == other.v)

    def __hash__(self):
        return self._uuid

    # optional (for print)
    def __repr__(self):
        return 'Edge(' + str(self._u) + ', ' + str(self._v) + ')'

    def __str__(self):
        return self.__repr__()


class Triangle(Simplex):
    '''This class represents a triangle: 2-simplex embedded into R2 (X,Y plane).

    Triangle is oriented, that is specified as triple of vertices in
    counterclockwise order.
    The internal representation contains a triple of vertices ordered both
    counterclockwise (positive orientation) and in lexicographical order.
    Object of this class can be used as a dictionary or set keys and, in this
    respect, they behaves like the (u, v, w) lexicographically ordered tuples.

    :param u: the first triangle vertex
    :type u: :class:`Vertex`
    :param v: the second triangle vertex
    :type v: :class:`Vertex`
    :param w: the third triangle vertex
    :type w: :class:`Vertex`
    :param drawData: the data used for debug drawing
    :type drawData: :class:`DrawData`
    '''

    def __init__(self, u, v, w, drawData=None):
        if __debug__:
            if (u != GHOSTVERTEX) and (v != GHOSTVERTEX) and (w != GHOSTVERTEX):
                assert(Orient2D(u, v, w) >= 0.0)
        # the internal representation is unique regardless of the order
        # specified in the constructor, as long as the counterclockwise order
        # is respected
        minVertex = min(u, v, w)
        if minVertex == u:
            self._u, self._v, self._w = u, v, w
        elif minVertex == v:
            self._u, self._v, self._w = v, w, u
        elif minVertex == w:
            self._u, self._v, self._w = w, u, v
        if (self._u == GHOSTVERTEX) or (self._v == GHOSTVERTEX) or (self._w == GHOSTVERTEX):
            self._isGhost = True
        else:
            self._isGhost = False
        # compute the uuid of this triangle
        self._uuid = hash((self._u, self._v, self._w))
        # draw stuff
        self._draw = False
        if drawData:
            self._draw = True
            if not self._isGhost:
                self._drawEgdeUV = Edge(self._u, self._v, drawData)
                self._drawEgdeVW = Edge(self._v, self._w, drawData)
                self._drawEgdeWU = Edge(self._w, self._u, drawData)
            else:
                c = (drawData.color[0] * 0.7, drawData.color[1] * 0.7,
                     drawData.color[2] * 0.7, drawData.color[3])
                t = drawData.thickness
                r = drawData.render
                drawDataG = DrawData(c, t, r)
                if self._u == GHOSTVERTEX:
                    a, b = self._v, self._w
                elif self._v == GHOSTVERTEX:
                    a, b = self._w, self._u
                elif self._w == GHOSTVERTEX:
                    a, b = self._u, self._v
                Gx = a.y - b.y + D(0.5) * (a.x + b.x)
                Gy = b.x - a.x + D(0.5) * (a.y + b.y)
                G = Vertex(Gx, Gy)
                self._drawEgdeUV = Edge(a, b)
                self._drawEgdeVW = Edge(b, G, drawDataG)
                self._drawEgdeWU = Edge(G, a, drawDataG)

    def __del__(self):
        if self._draw:
            delattr(self, '_drawEgdeUV')
            delattr(self, '_drawEgdeVW')
            delattr(self, '_drawEgdeWU')

    @property
    def u(self):
        return self._u

    @property
    def v(self):
        return self._v

    @property
    def w(self):
        return self._w

    @property
    def uuid(self):
        return self._uuid

    def isGhost(self):
        return self._isGhost

    def contains(self, p):
        '''Checks if a vertex is inside or on the triangle.

        :param p: the vertex to be checked
        :type p: :class:`Vertex`
        :return: returns True if the vertex is inside or on the triangle, False
        otherwise
        respectively.
        :rtype: bool
        '''

        # compute the barycentric coordinates of p, based on these two
        # relations: P=aU+bV+cW and a+b+c=1
        D1 = self._v.x * self._w.y - self._w.x * self._v.y
        D2 = self._u.x * self._w.y - self._w.x * self._u.y
        D3 = self._u.x * self._v.y - self._v.x * self._u.y
        P1 = p.x * self._w.y - self._w.x * p.y
        P2 = p.x * self._v.y - self._v.x * p.y
        P3 = self._u.x * p.y - p.x * self._u.y
        P4 = self._v.x * p.y - p.x * self._v.y
        # compute D (determinant of the linear equation system)
        D = D1 - D2 + D3
        assert (D != 0)
        # compute coords: avoid expensive operations
        aN = (D1 - P1 + P2)
        bN = (P1 - D2 + P3)
        cN = (P4 - P3 + D3)
        #DInv = 1.0 / D
        #a = aN * DInv
        #b = bN * DInv
        #c = cN * DInv
        # check condition: 0<=a<=1 and 0<=b<=1 and 0<=c<=1
        if D > 0:
            return (
                ((aN >= 0.0) and (aN <= D)) and
                ((bN >= 0.0) and (bN <= D)) and
                ((cN >= 0.0) and (cN <= D))
            )
        else:
            # D < 0
            return (
                ((aN <= 0.0) and (aN >= D)) and
                ((bN <= 0.0) and (bN >= D)) and
                ((cN <= 0.0) and (cN >= D))
            )

    # to be used as a dictionary key
    def __eq__(self, other):
        return (self._u == other.u) and (self._v == other.v) and (self._w == other.w)

    def __hash__(self):
        return self._uuid

    # optional (for print)
    def __repr__(self):
        return 'Triangle(' + str(self._u) + ', ' + str(self._v) + ', ' + str(self._w) + ')'

    def __str__(self):
        return self.__repr__()

# geometric predicates


def Orient2D(a, b, c):
    '''Checks orientation of the three vertices.

    Returns a positive value if the vertices a, b,and c are arranged in
    counterclockwise order, a negative value if the vertices are in clockwise
    order, and zero if the vertices are collinear. Another interpretation is that
    Orient2D returns a positive value if a lies to the left of the line bc
    relative to an observer standing at b and facing c.
    The orientation test can be implemented as a matrix determinant that
    computes the signed area of the parallelogram determined by the vectors
    a - c and b - c.

    :param a,b,c: the three vertices
    :type a,b,c: :class:`Vertex`
    :return: a value indicating whether vertices are ccw ordered (>0), or are cw
    ordered (<0), or collinear (==0)
    :rtype: float
    '''
    return (a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x)


def InCircle(a, b, c, d):
    '''Checks if last vertex lies inside the unique circle through the first three counterclockwise ordered ones.

    Returns a positive value if d lies inside the unique (and possibly
    degenerate) circle through a, b, and c, assuming that the latter three
    vertices occur in counterclockwise order around the circle.
    Returns zero if and only if all four vertices lie on a common circle or
    line.
    If (a,b,c) represents a ghost triangle, returns a value positive or negative
    or zero if d is inside or outside or on the boundary of the outer plane
    respectively. 

    :param a,b,c,d: the four vertices
    :type a,b,c,d: :class:`Vertex`
    :return: a value indicating whether the fourth vertex in inside (>0), or
    outside (<0), or on (==0) the circumcircle of the triangle identified by the
    first ccw ordered vertices
    :rtype: float
    '''
    if a == GHOSTVERTEX:
        return Orient2D(b, c, d)
    elif b == GHOSTVERTEX:
        return Orient2D(c, a, d)
    elif c == GHOSTVERTEX:
        return Orient2D(a, b, d)
    ax, ay = a.x - d.x, a.y - d.y
    bx, by = b.x - d.x, b.y - d.y
    cx, cy = c.x - d.x, c.y - d.y
    return (
        ax * (by * (cx**2 + cy**2) - cy * (bx**2 + by**2)) -
        bx * (ay * (cx**2 + cy**2) - cy * (ax**2 + ay**2)) +
        cx * (ay * (bx**2 + by**2) - by * (ax**2 + ay**2))
    )
