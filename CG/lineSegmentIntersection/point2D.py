'''
Created on Mar 14, 2019

@author: consultit
'''

from panda3d.core import LPoint3f, LineSegs, NodePath, GeomNode
from ...common.relational import Relational
from .precision import EQ, LT, GT, ROUND


class Point2D(Relational):
    '''
    The point class in 2D over the X,Z plane. Can be used as event point.
    
    Objects of this kind maintain references to points in 2D over the X,Z plane 
    (LPoint3f) so that they can be sorted and compared in order to be used as keys 
    of an AVLTreeMap, which is an implementation of a balanced binary search tree.
    The order < on the event points is defined in this way:
        if e1 and e2 are two event points then we have e1 < e2 if and only if 
            e1.point.z > e2.point.z holds 
            or 
            e1.point.z = e2.point.z and e1.point.x < e2.point.x holds.
    '''
    
    def __init__(self, p, color=(1, 0.0, 1, 1), thickness=6.0, render=None):
        if isinstance(p, LPoint3f):
            self._p = p
        else:
            raise Exception(str(p) + ' is not an instance of an ' + str(LPoint3f) + 
                            ' or of a subclass thereof.')
        # drawing stuff
        self.point = LineSegs('point')
        self.point.set_thickness(thickness)
        self.point.set_color(*color)
        self.pointnp = NodePath(GeomNode('point_GN'))
        # update transform
        geomnode = self.pointnp.node()
        self.point.move_to(self.p)
        if geomnode.get_num_geoms() > 0:
            geomnode.remove_geom(0)
        self.point.create(geomnode, True)
        if render: self.pointnp.reparent_to(render)
        
    @property
    def p(self):
        return self._p
    
    # base (mandatory)
    def __eq__(self, other):
        # exact: (self._p.z == other.p.z) and (self._p.x == other.p.x)
        return EQ(self._p.z, other.p.z) and EQ(self._p.x, other.p.x)

    # base (mandatory)
    def __lt__(self, other):
        # exact: (self._p.z > other.p.z) or ((self._p.z == other.p.z) and (self._p.x < other.p.x))
        return GT(self._p.z, other.p.z) or (EQ(self._p.z, other.p.z) and LT(self._p.x, other.p.x))

    # optional (for print)
    def __repr__(self):
        return 'Point2D(x=' + str(ROUND(self._p.x)) + ', z=' + str(ROUND(self._p.z)) + ')'

    def __str__(self):
        return self.__repr__()
    
    # for set
    def __hash__(self):
        return hash(self._p.z)
