'''
Created on Apr 22, 2019

@author: consultit
'''

from test_py.CG.lineSegmentIntersection.lineSegmentIntersection import LineSegmentIntersection
from panda3d.core import LPoint3f
from test_py.CG.lineSegmentIntersection.segment2D import Segment2D
from test_py.CG.lineSegmentIntersection.point2D import Point2D
from test_py.CG.lineSegmentIntersection.utilities import drawGrid

# 2D plane (X,Z) with Y fixed
Y = 30.0
axisX = []
axisZ = []

points = None
point2Ds = None


def testDrawing(app):
    global Y, segment0, points
    pointColor = (1, 0.25, 0.75, 1)
    segmentColor = (1, 0, 0, 1)
    points = [
        LPoint3f(1.0, Y, 4.0),
        LPoint3f(6.0, Y, 7.0),
        LPoint3f(9.0, Y, 1.0)
    ]
    segment0 = Segment2D(Point2D(points[0], color=pointColor, render=app.render), 
                         Point2D(points[1], color=pointColor, render=app.render), 
                         color=segmentColor, render=app.render)

    def changeSeg(render, points):
        global segment0
        f = points.pop()
        points[:0] = [f]
        segment0 = Segment2D(Point2D(points[0], color=pointColor, render=app.render), 
                             Point2D(points[1], color=pointColor, render=app.render), 
                             color=segmentColor, render=render)
    
    app.accept('c', changeSeg, [app.render, points])


def testIntersections(app):
    global AB, CD, EF, point2Ds
    pointColor = (0.25, 1, 0.75, 1)
    point2Ds = {
        'A': Point2D(LPoint3f(2.0, Y, 2.0), color=pointColor, render=app.render),
        'B': Point2D(LPoint3f(5.0, Y, 4.0), color=pointColor, render=app.render),
        'C': Point2D(LPoint3f(2.0, Y, 6.0), color=pointColor, render=app.render),
        'D': Point2D(LPoint3f(5.0, Y, 1.0), color=pointColor, render=app.render),
        'E': Point2D(LPoint3f(16.0 / 5.0, Y, 4.0), color=pointColor, render=app.render),
        'F': Point2D(LPoint3f(0.0, Y, 28.0 / 3.0), color=pointColor, render=app.render),
    }

    AB = Segment2D(point2Ds['A'], point2Ds['B'], color=(1, 0.5, 0.5, 1), render=app.render)
    CD = Segment2D(point2Ds['C'], point2Ds['D'], color=(0.5, 1, 0.5, 1), render=app.render)
    EF = Segment2D(point2Ds['E'], point2Ds['F'], color=(0.5, 0.5, 1, 1), render=app.render)
     
    intersections = Segment2D.findIntersection(AB, CD)
    print(intersections[0], intersections[1], sep='|')
    intersections = Segment2D.findIntersection(CD, EF)
    print(intersections[0], intersections[1], sep='|')
    intersections = Segment2D.findIntersection(AB, EF)
    print(intersections[0], intersections[1], sep='|')


# # TESTs
if __name__ == '__main__':
    # main
    app = LineSegmentIntersection()
    drawGrid(app, Y, 10.0, 10)
    # test drawing
    segment0 = None  # needed to mantain a reference
    testDrawing(app)
    # test intersections
    AB = CD = EF = None  # needed to mantain references
    testIntersections(app)
    
    app.run()
