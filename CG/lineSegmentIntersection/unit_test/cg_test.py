'''
Created on May 16, 2019

@author: consultit
'''

import unittest
from test_py.CG.lineSegmentIntersection.unit_test import suite
from panda3d.core import LPoint3f
from test_py.CG.lineSegmentIntersection.segment2D import Segment2D
from test_py.CG.lineSegmentIntersection.point2D import Point2D
from test_py.CG.lineSegmentIntersection.precision import EPSILON
from ely.direct.data_structures_and_algorithms.ch11.avl_tree import AVLTreeMap as BalancedBynarySearchTree


def setUpModule():
    pass


def tearDownModule():
    pass


class Segment2DTEST(unittest.TestCase):

    def setUp(self):
        self.Y = 30.0
        self.point2Ds = {
            'A': Point2D(LPoint3f(2.0, self.Y, 2.0)),
            'B': Point2D(LPoint3f(5.0, self.Y, 4.0)),
            'C': Point2D(LPoint3f(2.0, self.Y, 6.0)),
            'D': Point2D(LPoint3f(5.0, self.Y, 1.0)),
            'E': Point2D(LPoint3f(16.0 / 5.0, self.Y, 4.0)),
            'F': Point2D(LPoint3f(0.0, self.Y, 28.0 / 3.0)),
        }
        self.points = [
            LPoint3f(1.0, self.Y, 4.0),
            LPoint3f(6.0, self.Y, 7.0),
            LPoint3f(9.0, self.Y, 1.0)
        ]

    def tearDown(self):
        del self.Y
        del self.point2Ds
        del self.points

    def testIntersections(self):
        AB = Segment2D(self.point2Ds['A'], self.point2Ds['B'],
                       color=(1, 0.5, 0.5, 1))
        CD = Segment2D(self.point2Ds['C'], self.point2Ds['D'],
                       color=(0.5, 1, 0.5, 1))
        EF = Segment2D(self.point2Ds['E'], self.point2Ds['F'],
                       color=(0.5, 0.5, 1, 1))

        intersections = Segment2D.findIntersection(AB, CD)
        self.assertEqual(intersections[0], 1), print(intersections[1])
        intersections = Segment2D.findIntersection(CD, EF)
        self.assertEqual(intersections[0], 2), print(intersections[1])
        intersections = Segment2D.findIntersection(AB, EF)
        self.assertEqual(intersections[0], 0), print(intersections[1])

    def testContainments(self):
        segment = Segment2D(self.point2Ds['A'], self.point2Ds['B'],
                            color=(1, 0.5, 0.5, 1))
        pointU = self.point2Ds['B']
        pointL = self.point2Ds['A']
        pointI = Point2D(pointL.p + (pointU.p - pointL.p) * 0.637)
        pointC = self.point2Ds['C']
        self.assertEqual(segment.contains(pointU), (True, False, False))
        self.assertEqual(segment.contains(pointL), (False, True, False))
        self.assertEqual(segment.contains(pointI), (False, False, True))
        self.assertEqual(segment.contains(pointC), (False, False, False))


class Segment2DSortingTEST(unittest.TestCase):

    def setUp(self):
        self.Y = 30.0

    def tearDown(self):
        del self.Y

    def _doCheckOrder(self, Q):
        intersectFound = False
        while not intersectFound:
            p00 = Point2D(LPoint3f(suite.RandFloat(10.0),
                                   self.Y, suite.RandFloat(10.0)))
            p01 = Point2D(LPoint3f(suite.RandFloat(10.0),
                                   self.Y, suite.RandFloat(10.0)))
            p10 = Point2D(LPoint3f(suite.RandFloat(10.0),
                                   self.Y, suite.RandFloat(10.0)))
            p11 = Point2D(LPoint3f(suite.RandFloat(10.0),
                                   self.Y, suite.RandFloat(10.0)))
            seg0 = Segment2D(p00, p01)
            seg1 = Segment2D(p10, p11)
            res = Segment2D.findIntersection(seg0, seg1)
            if res[0] == 1:
                # handle only single intersection
                intersectFound = True
                intersectPoint = res[1][0]
        # compute geometry layout:
        # - coordinate z of intersection point is lower than those of both segments' upper points
        # - segment with higher x comes first (ie is lower according to the ordering relation)
        sortedSegs = []
        if seg0.dirX > seg1.dirX:
            sortedSegs.append(seg0)
            sortedSegs.append(seg1)
        else:
            sortedSegs.append(seg1)
            sortedSegs.append(seg0)
        # set intersection point and insert segments
        seg0.sweepLineIntersect = intersectPoint
        seg1.sweepLineIntersect = intersectPoint
        Q[seg0] = None
        Q[seg1] = None
        # check the order
        for seg, segQ in zip(sortedSegs, Q):
            self.assertTrue(seg is segQ)
        # delete items
        del Q[seg0]
        del Q[seg1]

    def testOrder(self):
        # Given 2 segments, find intersection point and set it for both
        Q = BalancedBynarySearchTree()
        NUM = 100
        for i in range(NUM):
            self._doCheckOrder(Q)


class Point2DSortingTEST(unittest.TestCase):

    def setUp(self):
        self.Y = 30.0

    def tearDown(self):
        del self.Y

    def testInsertAlmostEqual(self):
        # Inserting a key in a Tree equal to an existing one will
        # replace the associated value but not the key itself
        Q = BalancedBynarySearchTree()
        # A are "equal" to A1
        A = Point2D(LPoint3f(2.0, self.Y, 0.0))
        A1 = Point2D(LPoint3f(2.0 * (1.0 + 0.9 * EPSILON), self.Y, 0.0))
        # A are not "equal" to B
        B = Point2D(LPoint3f(2.0 * (1.0 + 1.01 * EPSILON), self.Y, 0.0))
        Q[A] = A.p.x
        Q[A1] = A1.p.x
        Q[B] = B.p.x
        self.assertEqual(len(Q), 2)
        # A key is not replaced by A1
        posA = Q.find_position(A)
        self.assertEqual(posA.key().p.x, A.p.x), print(
            posA.key().p.x, A.p.x, sep=' = ')
        self.assertNotEqual(posA.key().p.x, A1.p.x), print(
            posA.key().p.x, A1.p.x, sep=' != ')
        # the associated value Q[A] is replaced by Q[A1]
        self.assertEqual(Q[A], Q[A1]), print(Q[A], Q[A1], sep=' = ')

    def testKeyContainment(self):
        # Inserting a key in a Tree equal to an existing one will
        # replace the associated value but not the key itself
        Q = BalancedBynarySearchTree()
        # A are "equal" to A1
        A = Point2D(LPoint3f(2.0, self.Y, 0.0))
        A1 = Point2D(LPoint3f(2.0 * (1.0 + 0.9 * EPSILON), self.Y, 0.0))
        Q[A] = None
        Q[A1] = None
        self.assertEqual(len(Q), 1)
        self.assertTrue(A in Q)
        self.assertTrue(A1 in Q)

    def testValuePreservation(self):
        # Inserting a key in a Tree equal to an existing one will
        # replace the associated value but not the key itself
        Q = BalancedBynarySearchTree()
        # A are "equal" to A1
        A = Point2D(LPoint3f(2.0, self.Y, 0.0))
        A1 = Point2D(LPoint3f(2.0 * (1.0 + 0.9 * EPSILON), self.Y, 0.0))
        Q[A] = []
        if A1 in Q:
            Q[A1].append('A')
        self.assertEqual(Q[A][0], 'A')
        self.assertEqual(Q[A1][0], 'A')


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((Segment2DTEST('testIntersections'),
                    Segment2DTEST('testContainments')))
    suite.addTest(Segment2DSortingTEST('testOrder'))
    suite.addTest(Point2DSortingTEST('testInsertAlmostEqual'))
    suite.addTest(Point2DSortingTEST('testKeyContainment'))
    suite.addTest(Point2DSortingTEST('testValuePreservation'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
