'''
Created on Jul 20, 2019

@author: consultit
'''

from panda3d.core import LPoint3f
from .segment2D import Segment2D
from .point2D import Point2D

# 2D plane (X,Z) with Y fixed
axisX = []
axisZ = []


def drawGrid(app, Y, width, num=10, gridColor=(0.6, 0.6, 0.0, 1),
             colorX=(1, 0, 0, 1), colorZ=(0, 0, 1, 1)):
    delta = width / num
    #
    axisX.append(Segment2D(Point2D(LPoint3f(-width, Y, 0.0)), Point2D(LPoint3f(width, Y, 0.0)),
                      color=colorX, render=app.render))
    for i in range(1, num + 1):
        axisX.append(Segment2D(Point2D(LPoint3f(-width, Y, i * delta)),
                               Point2D(LPoint3f(width, Y, i * delta)),
                      color=gridColor, render=app.render))
        axisX.append(Segment2D(Point2D(LPoint3f(-width, Y, -i * delta)),
                               Point2D(LPoint3f(width, Y, -i * delta)),
                      color=gridColor, render=app.render))
    #
    axisZ.append(Segment2D(Point2D(LPoint3f(0.0, Y, -width)), Point2D(LPoint3f(0.0, Y, width)),
                      color=colorZ, render=app.render))
    for i in range(1, num + 1):
        axisZ.append(Segment2D(Point2D(LPoint3f(i * delta, Y, -width)),
                               Point2D(LPoint3f(i * delta, Y, width)),
                      color=gridColor, render=app.render))
        axisZ.append(Segment2D(Point2D(LPoint3f(-i * delta, Y, -width)),
                               Point2D(LPoint3f(-i * delta, Y, width)),
                      color=gridColor, render=app.render))
