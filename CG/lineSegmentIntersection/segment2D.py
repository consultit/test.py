'''
Created on Mar 13, 2019

@author: consultit
'''

from panda3d.core import LineSegs, NodePath, GeomNode, LPoint3f
from ...common.relational import Relational
from .point2D import Point2D
from .precision import EQ, LT, GT, EPSILON


class Segment2D(Relational):
    '''
    The segment class in 2D over the X,Z plane.
    '''
     
    def __init__(self, point0, point1, color=(0.0, 1, 1, 1), thickness=2.0, render=None):
        if isinstance(point0, Point2D):
            self._point0 = point0
        else:
            raise Exception(str(point0) + ' is not an instance of an ' + str(Point2D) + ' or of a subclass thereof.')   
        if isinstance(point1, Point2D):
            self._point1 = point1
        else:
            raise Exception(str(point1) + ' is not an instance of an ' + str(Point2D) + ' or of a subclass thereof.')
        self._reorder()
        # drawing stuff
        self.segment = LineSegs('segment')
        self.segment.set_thickness(thickness)
        self.segment.set_color(*color)
        self.segmentnp = NodePath(GeomNode('segment_GN'))
        # update transform
        geomnode = self.segmentnp.node()
        self.segment.move_to(self.point0.p)
        self.segment.draw_to(self.point1.p)
        if geomnode.get_num_geoms() > 0:
            geomnode.remove_geom(0)
        self.segment.create(geomnode, True)
        if render: self.segmentnp.reparent_to(render)
        # compute the X axis component of the (unitary) direction vector from lower to upper
        self.dirX = (self._upper.p - self._lower.p).normalized().x
        # sweep line intersection point
        self.sweepLineIntersect = None
        
    def __del__(self):
        self.segmentnp.remove_node()
                
    @property
    def point0(self):
        return self._point0
     
    @property
    def point1(self):
        return self._point1
             
    def _reorder(self):
        if self._point0 < self._point1:
            self._upper, self._lower = (self._point0, self._point1)
        else:
            self._upper, self._lower = (self._point1, self._point0)

    @property
    def upper(self):
        return self._upper
     
    @property
    def lower(self):
        return self._lower
    
    def contains(self, point):
        if not isinstance(point, Point2D):
            raise Exception(str(point) + ' is not an instance of an ' + str(Point2D) + ' or of a subclass thereof.')   
        isUpper = False
        isLower = False
        isInside = False
        if self._upper == point:
            isUpper = True
        elif self._lower == point:
            isLower = True
        elif self._isBetween(self._upper, self._lower, point):
            # if point belongs to the segment then: 
            # point = point0 + t*(point1 - point0) with 0<=t<=1
            isInside = True
        return (isUpper, isLower, isInside)
    
    def _isBetween(self, a, b, c):
        return EQ((c.p.x - a.p.x) * (b.p.z - a.p.z), (b.p.x - a.p.x) * (c.p.z - a.p.z))

    @staticmethod
    def findIntersection(S0, S1):
        '''
        Finds the intersection point(s) between S0 and S1 Segment2Ds.
        
        Input: the Segment2Ds S0 and S1.
        Output: the return value is a pair = (INT, I), where I is a two element list.
            If the value of INT is 0 there is no intersection and I=[None, None]; if it 
            is 1 there is a unique intersection point and I=[point, None]; if it is 2 
            the two segments overlap and the intersection is itself a segment, in this
            case I=[point1,point2] where point1 and point2 are the extremes of the 
            intersection segment.
            
        See: “7.1 Linear Components.” Geometric Tools for Computer Graphics, 
            by Philip J. Schneider and David H. Eberly, Morgan Kaufmann Publishers, 
            2003, pp. 241–245.
        '''
        
        I = [None] * 2
        if not isinstance(S0, Segment2D):
            print(str(S0) + ' is not an instance of an ' + str(Segment2D) + ' or of a subclass thereof.') 
            return (0, I)
        if not isinstance(S1, Segment2D):
            print(str(S1) + ' is not an instance of an ' + str(Segment2D) + ' or of a subclass thereof.') 
            return (0, I)

        P0 = S0.point0.p
        D0 = S0.point1.p - S0.point0.p
        P1 = S1.point0.p
        D1 = S1.point1.p - S1.point0.p
        # segments: P0 + s * D0 for s in [0, 1], P1 + t * D1 for t in [0,1]
        E = P1 - P0
        kross = D0.x * D1.z - D0.z * D1.x
        sqrKross = kross * kross
        sqrLen0 = D0.length_squared()  # D0.X * D0.X + D0.Y * D0.Y
        sqrLen1 = D1.length_squared()  # D1.X * D1.X + D1.Y * D1.Y

        if sqrKross > EPSILON * sqrLen0 * sqrLen1:
            # lines of the segments are not parallel
            s = (E.x * D1.z - E.z * D1.x) / kross
            if s < 0 or s > 1: 
                # intersection of lines is not a point on segment P0 + s * D0
                return (0, I)
        
            t = (E.x * D0.z - E.z * D0.x) / kross
            if t < 0 or t > 1: 
                # intersection of lines is not a point on segment P1 + t * D1
                return (0, I)
        
            # intersection of lines is a point on each segment
            I[0] = Point2D(P0 + D0 * s)
            return (1, I)
        
        # lines of the segments are parallel
        sqrLenE = E.length_squared()  # E.X * E.X + E.Y * E.Y
        kross = E.x * D0.z - E.z * D0.x
        sqrKross = kross * kross
        
        if sqrKross > EPSILON * sqrLen0 * sqrLenE:
            # lines of the segments are different
            return (0, I)
        
        # Lines of the segments are the same. Need to test for overlap of
        # segments.
        s0 = D0.dot(E) / sqrLen0
        s1 = s0 + D0.dot(D1) / sqrLen0
        smin = min(s0, s1)
        smax = max(s0, s1)
        imax, w = Segment2D._findIntersectionI(0.0, 1.0, smin, smax)
        for i in range(imax):
            I[i] = Point2D(P0 + D0 * w[i])
        return (imax, I)

    @staticmethod
    def findIntersectionSweepLine(S, p):
        '''
        Finds the intersection point(s) between S and a sweep line passing at p.
        
        Input: the Segment2D S0 and the Point2D p.
        Output: the return value is a pair = (INT, I), where I is a two element list.
            If the value of INT is 0 there is no intersection and I=[None, None]; if it 
            is 1 there is a unique intersection point and I=[point, None]; if it is 2 
            the segment overlaps the sweep line and the intersection is the segment itself,
            in this case I=[S.upper,S.lower].
        '''
        I = [None] * 2
        if not isinstance(S, Segment2D):
            print(str(S) + ' is not an instance of an ' + str(Segment2D) + ' or of a subclass thereof.') 
            return (0, I)
        if not isinstance(p, Point2D):
            print(str(p) + ' is not an instance of an ' + str(Point2D) + ' or of a subclass thereof.') 
            return (0, I)
        
        if LT(S.upper.p.z, p.p.z) or GT(S.lower.p.z, p.p.z):
            # Sweep line above or below S
            return (0, I)
        if EQ(S.upper.p.z, p.p.z):
            if EQ(S.upper.p.z, S.lower.p.z):
                # Sweep line and S overlap
                return (2, [S.upper, S.lower])
            else:
                # Sweep line intersects S at S.upper
                return (1, [S.upper, None])
        if EQ(S.lower.p.z, p.p.z):
            # Sweep line intersects S at S.lower
            return (1, [S.lower, None])
        
        # Sweep line and S intersect at an internal point
        x = (S.point0.p.x + (S.point1.p.x - S.point0.p.x) * 
                                (p.p.z - S.point0.p.z) / (S.point1.p.z - S.point0.p.z))
        return (1, [Point2D(LPoint3f(x, p.p.y, p.p.z)), None])

    @staticmethod
    def _findIntersectionI(u0, u1, v0, v1):
        '''
        Find the intersection(s) between two intervals.
        
        Input: 2 intervals [u0, u1] and [v0, v1], where u0 < u1 and v0 < v1.
        Output: the return value is a pair = (INT, w), where w is a two element list.
            If the value of INT is 0 there is no intersection and I=[None, None]; 
            if it is 1 there is a unique intersection w=[w0, None]; if it is 2 
            the two intervals overlap and the intersection is itself an interval, 
            in this case w=[w0,w1] where w1 and w2 are the extremes of the intersection
            interval.
        '''
        
        w = [None] * 2
        # original: if u1 < v0 or u0 > v1:
        if LT(u1, v0) or GT(u0, v1):
            return (0, w)

        # original: if u1 > v0:
        if GT(u1, v0):
            if LT(u0, v1): 
                if LT(u0, v0): 
                    w[0] = v0 
                else: 
                    w[0] = u0
                if GT(u1, v1): 
                    w[1] = v1 
                else: 
                    w[1] = u1
                return (2, w)
            else:
                # u0 == v1
                w[0] = u0
                return (1, w)
        
        else:
            # u1 == v0
            w[0] = u1
            return (1, w)

    @property
    def sweepLineIntersect(self):
        return self._sweepLineIntersect
     
    @sweepLineIntersect.setter
    def sweepLineIntersect(self, point):
        if  isinstance(point, Point2D) or (point == None):
            self._sweepLineIntersect = point
        else:
            raise Exception(str(point) + ' is not an instance of an Point2D or of a subclass thereof.')
    
    # base (mandatory)
    def __eq__(self, other):
        # exact: (self._sweepLineIntersect.x == other.sweepLineIntersect.x) and 
        # (self.dirX == other.dirX)
        return (EQ(self._sweepLineIntersect.p.x, other.sweepLineIntersect.p.x) 
                and EQ(self.dirX, other.dirX))
    
    # base (mandatory)
    def __lt__(self, other):
        # exact: (self._sweepLineIntersect.x < other.sweepLineIntersect.x) or 
        # ((self._sweepLineIntersect.x == other.sweepLineIntersect.x) and (self.dirX > other.dirX))
        # Note: when there is the same intersection point with the sweep line, 
        # self.dirX > other.dirX means that 'self' will intersect a sweep  line, just below 
        # this one before 'other' (ie x coordinate of the intersection point is lower)
        return (LT(self._sweepLineIntersect.p.x, other.sweepLineIntersect.p.x) or 
                (EQ(self._sweepLineIntersect.p.x, other.sweepLineIntersect.p.x) and GT(self.dirX, other.dirX)))
    
    # optional (for print)
    def __repr__(self):
        return 'Segment2D(' + str(self._point0) + ', ' + str(self._point1) + ')'

    # optional (for print)
    def __str__(self):
        return self.__repr__()
