'''
Created on Sep 1, 2019

@author: consultit
'''

# tolerance
from math import floor, log10, isclose
EPSILON = 1e-5


# see: https://exality.com/convert-floats-to-engineering-notation/
def frexp10(x):
    '''
    Return mantissa and exponent (base 10), similar to base-2 frexp()
    :param x: floating point number
    :return: tuple (mantissa, exponent)
    '''
    if x <= 0.0:
        return 0.0, 0
    exp = floor(log10(x))
    return x / 10 ** exp, exp


ndigits = abs(frexp10(EPSILON)[1])


def ROUND(x):
    return round(x, ndigits)


def EQ(x, y):
    # return abs(x - y) < EPSILON
    return isclose(x, y, rel_tol=EPSILON, abs_tol=EPSILON)


def LT(x, y):
    # return (x - y) < EPSILON
    return (x - y) < 0.0


def GT(x, y):
    # return (x - y) > EPSILON
    return (x - y) > 0.0
