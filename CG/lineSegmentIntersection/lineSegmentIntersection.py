'''
Created on Feb 16, 2019

@author: consultit
'''

from direct.showbase.ShowBase import ShowBase
from ely.direct.data_structures_and_algorithms.ch11.avl_tree import AVLTreeMap
from .segment2D import Segment2D


class BalancedBynarySearchTree(AVLTreeMap):
    
    def __repr__(self):
        return '{' + ''.join([str(k) + ':' + str(self.__getitem__(k)) + ', ' for k in self]).rstrip(', ') + '}'


class LineSegmentIntersection(ShowBase):
    '''
    Implements 'Line Segment Intersection' algorithm in 2D over the X,Z plane.
    
    Given a sequence S of n closed 2D segments in the plane, all intersection 
    points among the segments in S are reported using a 'plane sweep algorithm'.
    The generic horizontal sweep line l is defined by z=CONST.
       
    See: 
        - 'Berg, Mark De. Computational Geometry: Algorithms and Applications. 
           Berlin: Springer, 2008. chapter 2 - Line Segment2D Intersection'
        - 'Schneider, Philip J., and David H. Eberly. Geometric Tools for Computer
           Graphics. Morgan Kaufmann Publishers, 2010. Chapter 7 - Intersection in 2D'
    '''
 
    def __init__(self, *args, **kwargs):
        super(LineSegmentIntersection, self).__init__(*args, **kwargs)
        
    def findIntersections(self, S):
        '''
        Finds the intersection points among a set (list) of Segment2Ds.
        
        Input: a sequence S of line Segment2Ds in the plane X,Z.
        Output: a list of pairs = (point, segmentList), with 'point' (Point2D) 
            as an intersection point, and 'segmentList' as the list of segments
            (Segment2D) intersecting at point.
        '''
        
        # inizialize intersection dict = {p0:[s01, s02, ...], p1:[s11, s12, ...], ...]
        self.intersections = []
        # 1. Initialize an empty event queue Q. Next, insert the segment endpoints into 
        # Q; when an upper endpoint is inserted, the corresponding segment should 
        # be stored with it.
        # Note: Q's keys are points, and corresponding values are lists of segments
        self.Q = BalancedBynarySearchTree()
        for segment in S:
            # handle upper
            if not (segment.upper in self.Q):
                # endpoint not present: initialize
                self.Q[segment.upper] = [segment]
            else:
                # endpoint already present: update segment list
                self.Q[segment.upper].append(segment)
            # handle lower
            if not (segment.lower in self.Q):
                # endpoint not present: initialize
                self.Q[segment.lower] = []

        # 2. Initialize an empty status structure T.
        # Note: T's keys are segments, and corresponding values are None
        self.T = BalancedBynarySearchTree()
        # 3. while Q is not empty
        while not self.Q.is_empty():
            # 4. Determine the next event point p in Q and delete it.
            pPos = self.Q.first()
            # 5. HANDLEEVENTPOINT(p)
            self._handleEventPoint(pPos.key())
            self.Q.delete(pPos)
        
        # return result
        return self.intersections
            
    def _handleEventPoint(self, p):
              
        # 1. Let U(p) be the set of segments whose upper endpoint is p; these segments
        # are stored with the event point p. (For horizontal segments, the upper
        # endpoint is by deﬁnition the left endpoint.)
        Up = self.Q[p]
                
        # 2. Find all segments stored in T that contain p; they are adjacent in T. Let
        # L(p) denote the subset of segments found whose lower endpoint is p, and
        # let C(p) denote the subset of segments found that contain p in their interior.
        Lp = []
        Cp = []
        for segment in self.T:
            if p == segment.lower:
                Lp.append(segment)
            elif segment.contains(p)[2]:
                Cp.append(segment)
        
        # Note: L(p), U(p), and C(p) are disjoint
        # 3. if L(p)∪U(p)∪C(p) contains more than one segment 
        LUCp = Lp + Up + Cp
        if len(LUCp) > 1:
            # 4. then Report p as an intersection, together with L(p), U(p), and C(p).
            self.intersections.append((p, LUCp))
        
        # 5. Delete the segments in L(p)∪C(p) from T.
        LCp = Lp + Cp
        for segment in LCp:
            pPos = self.T.find_position(segment)
            self.T.delete(pPos)
        
        # 5.5 Remove all remaining segments from T, update their intersections 
        # with the sweep line and then reinsert them into T 
        Ts = []
        while not self.T.is_empty():
            firstP = self.T.first()
            Ts.append(firstP.key())
            self.T.delete(firstP)
        for segment in Ts:
            intersection = Segment2D.findIntersectionSweepLine(segment, p)
            segment.sweepLineIntersect = intersection[1][0]
            self.T[segment] = None
        
        # 6. Insert the segments in U(p)∪C(p) into T. The order of the segments in T
        # should correspond to the order in which they are intersected by a sweep
        # line just below p. If there is a horizontal segment, it comes last among all
        # segments containing p.
        # 7. (∗ Deleting and re-inserting the segments of C(p) reverses their order. ∗)
        UCp = Up + Cp
        for segment in UCp:
            # set sweepLineIntersect (to compute order)
            segment.sweepLineIntersect = p
            self.T[segment] = None
        
        # 8. if U(p)∪C(p) == 0
        if len(UCp) == 0:
            # 9. then Let sl and sr be the left and right neighbors of p in T.
            # Note: since C(p) and U(p) are empty and since p is an event point, 
            # we have that L(p) is necessarily non-empty
            sl = self.T.find_lt(Lp[0])
            sr = self.T.find_gt(Lp[0])
            # 10. FINDNEWEVENT(sl , sr, p)
            if sl and sr:
                self._findNewEvent(sl, sr, p)
        else:
            UCpTail = UCp[1:]
            # 11. else Let s' be the leftmost segment of U(p)∪C(p) in T.
            sSQuot = UCp[0]
            for s in UCpTail:
                if s < sSQuot:
                    sSQuot = s
            # 12. Let sl be the left neighbor of s' in T.
            sl = self.T.find_lt(sSQuot)
            # 13. FINDNEWEVENT(sl , s', p)
            if sl:
                self._findNewEvent(sl[0], sSQuot, p)
            # 14. Let s" be the rightmost segment of U(p)∪C(p) in T.
            sDQuot = UCp[0]
            for s in UCpTail:
                if s > sDQuot:
                    sDQuot = s
            # 15. Let sr be the right neighbor of s" in T.
            sr = self.T.find_gt(sDQuot)
            # 16. FINDNEWEVENT(s", sr, p)
            if sr:
                self._findNewEvent(sDQuot, sr[0], p)
        
    def _findNewEvent(self, sl, sr, p):
        # 1. if sl and sr intersect below the sweep line, or on it and to the right of the
        # current event point p, and the intersection is not yet present as an event in Q
        intersect = Segment2D.findIntersection(sl, sr)
        # check intersection(s)
        if intersect[0] == 0:
            # no intersection
            pIntersect = None
        elif intersect[0] == 1:
            # one intersection
            pIntersect = intersect[1][0]
        else:  # intersect[0] == 2
            # segments overlap
            pIntersect = intersect[1][0] if intersect[1][0] > intersect[1][1] else intersect[1][1]
        if pIntersect and (pIntersect > p) and (not(pIntersect in self.Q)):
            # 2. then Insert the intersection point as an event into Q.
            self.Q[pIntersect] = []
