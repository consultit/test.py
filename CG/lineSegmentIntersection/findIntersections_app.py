'''
Created on Jul 20, 2019

@author: consultit
'''

from test_py.CG.lineSegmentIntersection.lineSegmentIntersection import LineSegmentIntersection
from panda3d.core import LPoint3f
from test_py.CG.lineSegmentIntersection.segment2D import Segment2D
from test_py.CG.lineSegmentIntersection.point2D import Point2D
from test_py.CG.lineSegmentIntersection.utilities import drawGrid
from test_py.common.random import RandFloat

# 2D plane (X,Z) with Y fixed
Y = 30.0
gridWidth = 20.0
gridSepNum = 20
# segments
segsNum = 10


def createRandomSegments(render, segsNum, Y, xMin, xMax, zMin, zMax):
    segments = []
    wX = abs(xMax - xMin) / 2.0
    mX = abs(xMax + xMin) / 2.0
    wZ = abs(zMax - zMin) / 2.0
    mZ = abs(zMax + zMin) / 2.0
    pointData = []
    for _ in range(segsNum):
        color = (0.5 + RandFloat(0.5), 0.5 +
                 RandFloat(0.5), 0.5 + RandFloat(0.5), 1)
        x0, y0, z0 = mX + RandFloat(wX), Y, mZ + RandFloat(wZ)
        point0 = Point2D(LPoint3f(x0, y0, z0))
        x1, y1, z1 = mX + RandFloat(wX), Y, mZ + RandFloat(wZ)
        point1 = Point2D(LPoint3f(x1, y1, z1))
        pointData.append(((x0, y0, z0), (x1, y1, z1)))
        segments.append(Segment2D(point0, point1, color=color, render=render))
    print(pointData)
    return segments


def getSimpleS(app):
    color = (0.5 + RandFloat(0.5), 0.5 +
             RandFloat(0.5), 0.5 + RandFloat(0.5), 1)
    return [
        Segment2D(Point2D(LPoint3f(-10.0, Y, -10.0)), Point2D(LPoint3f(10.0, Y, 5.0)),
                  color=color, render=app.render),
        #     Segment2D(Point2D(LPoint3f(0.0, Y, -15.0)), Point2D(LPoint3f(-15.0, Y, 15.0)),
        #                       color=color, render=app.render),
        #     Segment2D(Point2D(LPoint3f(-15.0, Y, 3.0)), Point2D(LPoint3f(15.0, Y, 3.0)),
        #                       color=color, render=app.render),
        Segment2D(Point2D(LPoint3f(-15.0, Y, 1.0)), Point2D(LPoint3f(15.0, Y, 4.0)),
                  color=color, render=app.render),
        Segment2D(Point2D(LPoint3f(-15.0, Y, -1.0)), Point2D(LPoint3f(15.0, Y, 3.0)),
                  color=color, render=app.render),
        Segment2D(Point2D(LPoint3f(-15.0, Y, -3.0)), Point2D(LPoint3f(15.0, Y, 2.0)),
                  color=color, render=app.render),
    ]


def createFromPointData(render):
    pointData = [
        ((13.061428639529854, 30.0, -11.262537793467446),
         (-6.332911286322056, 30.0, -11.527818992448019)),
        ((-5.635394393917754, 30.0, 10.148034698290056),
         (7.021839621925552, 30.0, -14.702023746604214)),
        ((10.062903416768133, 30.0, 5.056633211812773),
         (2.86478767438326, 30.0, -17.446826104518678)),
    ]
    segments = []
    for p0, p1 in pointData:
        color = (0.5 + RandFloat(0.5), 0.5 +
                 RandFloat(0.5), 0.5 + RandFloat(0.5), 1)
        point0 = Point2D(LPoint3f(*p0))
        point1 = Point2D(LPoint3f(*p1))
        segments.append(Segment2D(point0, point1, color=color, render=render))
    print(pointData)
    return segments


if __name__ == '__main__':
    # main
    app = LineSegmentIntersection()
    drawGrid(app, Y, gridWidth, gridSepNum)

    # create segments
    limits = (-gridWidth, gridWidth) * 2
    S = createRandomSegments(app.render, segsNum, Y, *limits)
#     S = getSimpleS(app)
#     S = createFromPointData(app.render)

    # find the intersections
    intersections = app.findIntersections(S)
    # draw intersection points
    for i in intersections:
        Point2D(i[0].p, color=(1, 1, 0.5, 1),
                thickness=10.0, render=app.render)

    # setup camera
    trackball = app.trackball.node()
    trackball.set_pos(0.0, 50.0, 0.0)
    trackball.set_hpr(0.0, 0.0, 0.0)
    # run
    app.run()
