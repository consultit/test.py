'''
Created on 22/apr/2012

@author: consultit
@note: code derived from http://www.panda3d.org/forums/viewtopic.php?t=11657
'''

from direct.showbase import DirectObject
from panda3d.core import LVecBase3f, LMatrix4f, LVector3f, \
                        BitMask32, LPoint3f, WindowProperties
from direct.task import Task
from direct.gui.OnscreenImage import OnscreenImage


# #   First person camera controller, 'free view'/'FPS' style.
#    
#    Simple camera mouse look and WASD key controller 
#    shift to go faster,
#    r and f keys move camera up/down, 
#    q and e keys rotate camera,
#    hit enter to start/stop controls.
#    If physicsWorld != None, the camera becomes a kinematic body.
#    If chasedNode != None, the camera chases the given node.
class FirstPersonCamera(DirectObject.DirectObject):
    '''
    First person camera controller.
    '''
    
    # # Constructor
    # @param gameaApp: the game application to which this controller
    # applies, that should be ShowBase derived.
    # @param camera: the camera to which this controller applies
    # @param speed: base camera speed (with shift 10 times this value)
    # @param physicsWorld: the physics world (Bullet) for which the 
    # camera becomes a kinematic body
    # @param chasedNode: the node path to follow (chase)
    # @param chasedNodeRadius: the node path max radius (to avoid 
    # collisions with camera)
    def __init__(self, gameApp, camera, speed=1.0, chasedNode=None,
                chasedNodeRadius=0.0, physicsWorld=None, toggleEvent='enter'):
        '''
        Constructor
        '''
        
        self.gameApp = gameApp
        self.camera = camera
        self.worldNP = self.gameApp.render
        self.running = False 
        self.centX = self.gameApp.win.get_properties().get_x_size() // 2 
        self.centY = self.gameApp.win.get_properties().get_y_size() // 2
        self.nearRadius = self.camera.get_child(0).node().get_lens().get_near() * 3.0
        # check if chase camera
        if chasedNode != None:
            self.chasing = True
            self.chasedNode = chasedNode
            # node tracking the desired camera position
            self.chaseCamPos = LVector3f(0.0, -self.nearRadius - 5.0 * chasedNodeRadius,
                                         max(self.nearRadius, chasedNodeRadius))
            self.chaseCamNode = self.chasedNode.attachNewNode('chaseCamNode')
            self.chaseCamNode.setPos(self.chaseCamPos)
            # min distance between camera and chased node
            self.chaseMinDistance = self.nearRadius * 1.3 + chasedNodeRadius
            # node the camera looks at
            self.lookAtNode = self.chasedNode.attachNewNode('lookAtPoint')
            self.lookAtNode.setPos(LVector3f(0.0, chasedNodeRadius, 0.0))
        else:
            self.chasing = False
        
        # key controls 
        self.forward = False 
        self.backward = False 
        self.speed = speed
        self.left = False 
        self.right = False 
        self.up = False 
        self.down = False 
        self.rollLeft = False 
        self.rollRight = False 
        
        # sensitivity settings 
        self.movSens = 2
        self.movSensFast = self.movSens * 5
        self.rollSens = 50 
        self.sensX = self.sensY = 0.2       

        # Bullet world
        self.physicsWorld = physicsWorld
        self.collideMask = BitMask32(BitMask32.allOn())

        # press enter to get this camera controller
        self.toggleEvent = toggleEvent
        self.accept(self.toggleEvent, self.toggle)
        
    # # Camera cleanup
    def removeCamera(self):
        if self.chasing:
            self.chaseCamNode.removeNode()
            self.lookAtNode.removeNode()
        self.ignoreAll()
        
    # # Destructor
    def __del__(self):
        print ('Instance of FirstPersonCamera Removed ')

    # # Get camera collide mask
    def getCollideMask(self):
        return self.collideMask

    # # Place the chase camera
    # (see OgreBulletDemos)
    def placeChaseCamera(self, desiredWorldCamPos, targetPos, deltaTime):
        kReductFactor = 1.0 * deltaTime;
        # get actual world camera position
        actualWorldCamPos = self.camera.getPos(self.worldNP)
        # calculate difference between desiredWorldCamPos and camera position
        deltaPos = actualWorldCamPos - desiredWorldCamPos
        # converge deltaPos.lenght toward zero: proportionally to deltaPos.lenght
        if deltaPos.lengthSquared() > 0.0:
            deltaPos -= deltaPos * kReductFactor
        # move camera to new position
        newPos = desiredWorldCamPos + deltaPos
        NewTargetDir = newPos - targetPos
        if NewTargetDir.length() < self.chaseMinDistance:
            NewTargetDir.normalize()
            newPos = targetPos + NewTargetDir * self.chaseMinDistance 
            
        # correct camera height if camera is kinematic (not in OgreBulletDemos)
        if self.physicsWorld != None:
            pTo = LPoint3f(newPos.getX(), newPos.getY(), -1000000.0)
            result = self.physicsWorld.rayTestClosest(newPos, pTo)
            if newPos.getZ() - result.getHitPos().getZ() < self.nearRadius * 2.0:
                newPos.setZ(result.getHitPos().getZ() + self.nearRadius * 2.0)
        # set position
        self.camera.setPos(self.worldNP, newPos)
    
    # # Camera rotation task 
    def cameraTask(self, task):
        global globalClock
        dt = globalClock.getDt()
         
        # handle mouse look 
        md = self.gameApp.win.getPointer(0)        
        x = md.getX() 
        y = md.getY()
        
        if self.gameApp.win.movePointer(0, self.centX, self.centY):
            self.camera.setH(self.worldNP, self.camera.getH(self.worldNP)
                             -(x - self.centX) * self.sensX) 
            self.camera.setP(self.camera, self.camera.getP(self.camera) 
                             -(y - self.centY) * self.sensY) 
 
        # if chase camera do:
        if (self.chasing):
            # update chase camera position and orientation (see OgreBulletDemos)
            # get desired world camera position
            desiredWorldCamPos = self.chaseCamNode.getPos(self.worldNP)
            self.placeChaseCamera(desiredWorldCamPos, self.chasedNode.getPos(self.worldNP), dt)
            # look at self.lookAtNode
            self.camera.lookAt(self.lookAtNode)

        # handle keys: 
        if self.forward == True: 
            self.camera.setY(self.camera, self.camera.getY(self.camera) 
                             +self.movSens * self.speed * dt)
        if self.backward == True:
            self.camera.setY(self.camera, self.camera.getY(self.camera) 
                             -self.movSens * self.speed * dt) 
        if self.left == True:
            self.camera.setX(self.camera, self.camera.getX(self.camera) 
                             -self.movSens * self.speed * dt) 
        if self.right == True:
            self.camera.setX(self.camera, self.camera.getX(self.camera) 
                             +self.movSens * self.speed * dt) 
        if self.up == True:
            self.camera.setZ(self.worldNP, self.camera.getZ(self.worldNP) 
                             +self.movSens * self.speed * dt) 
        if self.down == True:
            self.camera.setZ(self.worldNP, self.camera.getZ(self.worldNP) 
                             -self.movSens * self.speed * dt)           
        if self.rollLeft == True:
            self.camera.setR(self.camera, self.camera.getR(self.camera) 
                             -self.rollSens * dt)
        if self.rollRight == True:
            self.camera.setR(self.camera, self.camera.getR(self.camera) 
                             +self.rollSens * dt)
        #    
        return Task.cont 

    # # Start to control the camera
    def start(self):
        self.gameApp.disableMouse()
        # hide mouse cursor, comment these 3 lines to see the cursor 
        props = WindowProperties() 
        props.setCursorHidden(True) 
        self.gameApp.win.requestProperties(props) 
        # reset mouse to start position: 
        self.gameApp.win.movePointer(0, self.centX, self.centY)             
        self.gameApp.taskMgr.add(self.cameraTask, 'HxMouseLook::cameraTask')        
        # Task for changing direction/position 
        self.accept('w', setattr, [self, 'forward', True])
        self.accept('shift-w', setattr, [self, 'forward', True])
        self.accept('w-up', setattr, [self, 'forward', False]) 
        self.accept('s', setattr, [self, 'backward', True]) 
        self.accept('shift-s', setattr, [self, 'backward', True]) 
        self.accept('s-up', setattr, [self, 'backward', False])
        self.accept('a', setattr, [self, 'left', True]) 
        self.accept('shift-a', setattr, [self, 'left', True]) 
        self.accept('a-up', setattr, [self, 'left', False]) 
        self.accept('d', setattr, [self, 'right', True]) 
        self.accept('shift-d', setattr, [self, 'right', True]) 
        self.accept('d-up', setattr, [self, 'right', False]) 
        self.accept('r', setattr, [self, 'up', True])
        self.accept('shift-r', setattr, [self, 'up', True]) 
        self.accept('r-up', setattr, [self, 'up', False])
        self.accept('f', setattr, [self, 'down', True])
        self.accept('shift-f', setattr, [self, 'down', True])
        self.accept('f-up', setattr, [self, 'down', False]) 
        self.accept('q', setattr, [self, 'rollLeft', True]) 
        self.accept('q-up', setattr, [self, 'rollLeft', False]) 
        self.accept('e', setattr, [self, 'rollRight', True]) 
        self.accept('e-up', setattr, [self, 'rollRight', False])
        self.accept('shift', setattr, [self, 'speed', 10.0 * self.speed])
        self.accept('shift-up', setattr, [self, 'speed', 1.0 * self.speed])
        
        # set chase camera position
        if self.chasing:
            self.camera.setPos(self.chasedNode, self.chaseCamPos)
            self.camera.lookAt(self.lookAtNode)
        else:
            startP = self.camera.getP(self.worldNP)
            startR = self.camera.getR(self.worldNP)
            startH = self.camera.getH(self.worldNP)
            timePR = 0.5
            intervalPR = self.camera.hprInterval(timePR, LVecBase3f(startH, 0, 0),
                                 startHpr=LVecBase3f(startH, startP, startR),
                                 other=self.worldNP)
            intervalPR.start()

#         # setup kinematic body (for collision): needs Bullet FIXME
#         if self.physicsWorld != None:
#             # setup collisions
#             # Create a collision shape for this camera.
#             # and attach it to the camera.
#             cameraShape = BulletSphereShape(self.nearRadius)
#             # create a Kinematic body for the camera
#             cameraBody = BulletRigidBodyNode('cameraBody')
#             cameraBody.setKinematic(True)
#             cameraBody.setDeactivationEnabled(False)
#             cameraBody.setRestitution(0.1)
#             cameraBody.setFriction(0.8)
#             cameraBody.addShape(cameraShape)     
#             self.physicsWorld.attachRigidBody(cameraBody)
#             # create rigid body node path: the camera drives the kinematic body
#             self.cameraBodyNP = self.camera.attachNewNode(cameraBody)
#             # set collision mask
#             self.cameraBodyNP.setCollideMask(self.collideMask)

        if not self.chasing:
            # create target image
            self.target = OnscreenImage(image='plusnode.png',
                                        pos=(0, 0, 0),
                                        scale=0.02)

    # # Stop to control the camera  
    def stop(self): 
        self.gameApp.taskMgr.remove('HxMouseLook::cameraTask') 
        
        mat = LMatrix4f(self.camera.getTransform(self.worldNP).getMat())
        mat.invertInPlace()
        self.camera.setMat(LMatrix4f.identMat())
        self.gameApp.mouseInterfaceNode.setMat(mat)
        self.gameApp.enableMouse() 
        props = WindowProperties() 
        props.setCursorHidden(False) 
        self.gameApp.win.requestProperties(props)        
         
        self.forward = False 
        self.backward = False 
        self.left = False 
        self.right = False 
        self.up = False 
        self.down = False 
        self.rollLeft = False 
        
        self.ignore('w') 
        self.ignore('shift-w') 
        self.ignore('w-up') 
        self.ignore('s') 
        self.ignore('shift-s') 
        self.ignore('s-up') 
        self.ignore('a')
        self.ignore('shift-a') 
        self.ignore('a-up') 
        self.ignore('d')
        self.ignore('shift-d') 
        self.ignore('d-up') 
        self.ignore('r')
        self.ignore('shift-r')
        self.ignore('r-up') 
        self.ignore('f')
        self.ignore('shift-f')
        self.ignore('f-up') 
        self.ignore('q') 
        self.ignore('q-up') 
        self.ignore('e') 
        self.ignore('e-up')
        self.ignore('shift')
        self.ignore('shift-up')
        
#         # un-setup kinematic (and collisions) FIXME
#         if self.physicsWorld != None:
#             # remove rigid body
#             self.physicsWorld.removeRigidBody(self.cameraBodyNP.node())
#             # remove rigid body node path
#             self.cameraBodyNP.removeNode()
        
        if not self.chasing:
            # delete target
            self.target.destroy()
       
    # # Call to start/stop control system 
    def toggle(self): 
        if(self.running): 
            self.stop() 
            self.running = False 
        else: 
            self.start() 
            self.running = True 
