'''
Created on 07 dic 2015

@author: consultit
'''

from direct.particles.ParticleEffect import ParticleEffect

class TimedBody(object):
    '''
    classdocs
    '''
    def __init__(self, timeLife, world, particleEffectFile):
        self.nodePath = None
        self.world = world
        self.timeLife = timeLife
        self.time = 0.0
        self.smoke = ParticleEffect()
        self.smoke.load_config(particleEffectFile)

    def update(self, dTime):
        self.time += dTime
        if (self.time > self.timeLife) and self.nodePath != None:
            self.smoke.cleanup()
            self.world.destroy_rigid_body(self.nodePath)
            return False
        return True

        