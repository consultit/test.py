'''
Created on 07 dic 2015

@author: consultit
'''
from TimedBody import TimedBody
from FirstPersonCamera import FirstPersonCamera
from panda3d.core import LPoint3f, LVector3f, AudioSound, NodePath, \
                LVector4f, BitMask32
from direct.gui.DirectGui import DirectDialog
from direct.directtools.DirectGeometry import LineNodePath
from direct.particles.ParticleEffect import ParticleEffect
import random, sys
import ely.libtools
from ely.physics import GamePhysicsManager, BT3RayTestResult, BT3RigidBody, \
    BT3SoftBody


# class for utilities
class Utilities(object):
    '''Class grouping some (mostly) unrelated utilities'''

    def __init__(self):
        '''Constructor'''
        
        self.wantExitDiag = None
        self.askCameraDiag = None
        self.cameraType = None
        self.counter = 0
    
    def setGlobals(self,
        app=None,
        bulletWorld=None,
        pandaActor=None,
        numPandas=None,
        pandas=None,
        pandaTimedBodies=None,
        pandaMass=None,
        pandaRadius=None,
        pandaMaskBullet=None,
        firePandaKey=None,
        fireImpulse=None,
        fireCCD=None,
        soundFirePanda=None,
        particleEffectFile=None,
        hitSoundsDict=None,
        joint0Pos=None,
        geometricShootKey=None,
        shootParticleEffectFile=None,
        shootFrom=None,
        shootLength=None,
        shootSound=None,
        gunSightPos=None
        ):
        '''Set utilities' globals'''
        
        self.app = app
        self.bulletWorld = bulletWorld
        self.pandaActor = pandaActor
        self.numPandas = numPandas
        self.pandas = pandas
        self.pandaTimedBodies = pandaTimedBodies
        self.pandaMass = pandaMass
        self.pandaRadius = pandaRadius
        self.pandaMaskBullet = pandaMaskBullet
        self.firePandaKey = firePandaKey
        self.fireImpulse = fireImpulse
        self.fireCCD = fireCCD
        self.soundFirePanda = soundFirePanda
        self.particleEffectFile = particleEffectFile
        self.hitSoundsDict = hitSoundsDict
        self.joint0Pos = joint0Pos
        self.geometricShootKey = geometricShootKey
        self.shootFrom = shootFrom
        self.shootLength = shootLength
        self.shootSound = shootSound
        self.gunSightPos = gunSightPos
        # We are going to be drawing some lines between the anchor points and the joints
        self.lines = LineNodePath(parent=self.app.render,
                                  thickness=3.0,
                                  colorVec=LVector4f(1, 0, 0, 1))
        # setup for a geometric shoot callback
        self.shootGraphicsEffect = ParticleEffect()
        self.shootGraphicsEffect.loadConfig(shootParticleEffectFile)
        self.shootGraphicsEffect.start(self.app.render,
                                  self.app.render.attachNewNode('shootBaseParticleRendererNodePath'))
        # hide shootFrom (gun)
        self.shootFrom.hide()    
    
    @staticmethod
    def getDimensions(model, modelScale=1.0):
        '''Get dimensions of a model'''
        
        minP = LPoint3f()
        maxP = LPoint3f()
        model.calcTightBounds(minP, maxP)
        if modelScale != None:
            modelDims = LVector3f(abs(maxP.getX() - minP.getX()),
                              abs(maxP.getY() - minP.getY()),
                              abs(maxP.getZ() - minP.getZ())) * modelScale
        else:
            modelDims = LVector3f(abs(maxP.getX() - minP.getX()),
                              abs(maxP.getY() - minP.getY()),
                              abs(maxP.getZ() - minP.getZ()))
        #
        modelCenter = LPoint3f(maxP.getX() - minP.getX(),
                           maxP.getY() - minP.getY(),
                           maxP.getZ() - minP.getZ()) / 2.0
        modelRadius = max(modelDims.getX(),
                          modelDims.getY(),
                          modelDims.getZ()) / 2.0
        if modelScale != None:
            model.setScale(modelScale)
        return (modelDims, modelCenter, modelRadius)
    
    def firePanda(self, soundFirePanda):
        '''Shoot a panda'''
        
        self.counter += 1
        p1 = self.numPandas + self.counter
        panda = NodePath('firedPanda' + str(p1))
        self.pandaActor.instance_to(panda)
        # get camera direction
        dirN = LVector3f(self.app.camera.get_net_transform().get_mat().get_row3(1))
        dirN.normalize()
        panda.set_pos(self.app.camera.get_pos() + 
                      dirN * self.app.cam.node().get_lens().get_near() * 10)
        panda.set_h(self.app.camera, 0)
        #
        physicsMgr = GamePhysicsManager.get_global_ptr()
        physicsMgr.set_parameters_defaults(GamePhysicsManager.RIGIDBODY)
        # create && get a reference to a BT3RigidBody
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'mass',
                                       self.pandaMass)
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'group_mask',
                                       self.pandaMaskBullet)
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'collide_mask',
                                       self.pandaMaskBullet)
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'up_axis',
                                       'no_up')            
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'shape_type',
                                       'sphere')
        pandaBodyNP = physicsMgr.create_rigid_body('firedPandaBody' + str(p1))
        self.pandas.append(pandaBodyNP)
        pandaBody = pandaBodyNP.node()
        # setup
        pandaBody.set_owner_object(panda)
        pandaBody.setup()
        # change some attrs
        pandaBody.collision_object_attrs.friction = 0.8
        pandaBody.collision_object_attrs.restitution = 0.1
        # check if ccd should be enabled 
        if self.fireCCD:
            pandaBody.collision_object_attrs.ccd_motion_threshold = self.fireImpulse / 100.0
            pandaBody.collision_object_attrs.ccd_swept_sphere_radius = self.pandaRadius
        # timed body
        lifeTime = random.uniform(10.0, 15.0)
        timedBody = TimedBody(lifeTime, self.bulletWorld, self.particleEffectFile)
        timedBody.nodePath = pandaBodyNP
        timedBody.smoke.start(pandaBodyNP, self.app.render)
        self.pandaTimedBodies.append(timedBody)
        #
        pandaBody.apply_impulse(dirN * self.fireImpulse, LPoint3f.zero())
        # emit sound
        soundFirePanda.play()

    def contactAdded(self, name, node1, node2):
        '''Contact added callback'''
        
        self.genericEventNotify(name, node1, node2)
        name1 = node1.get_name()
        name2 = node2.get_name()
        # check firedPanda against door, hangingPandas, character, vehicle, firedPanda
        if 'firedPanda' in name1:
            hitNameT = name2
        elif 'firedPanda' in name2:
            hitNameT = name1
        else:
            return
        #
        if hitNameT:
            # rstrip
            hitName = hitNameT.rstrip('0123456789')
            # emit sound
            if self.hitSoundsDict[hitName] and (self.hitSoundsDict[hitName].status != AudioSound.PLAYING):
                self.hitSoundsDict[hitName].play()
            
    def contactDestroyed(self, name, node1, node2):
        '''Contact destroyed callback'''
        
        self.genericEventNotify(name, node1, node2)

    def genericEventNotify(self, name, object0, object1):
        '''generic event notify'''
    
        print ('got ' + name + ' between ' + object0.get_name() + 
               ' and ' + object1.get_name())

    def drawLines(self, pandas, numPandas, joint0Pos):
        '''Draws lines between the pandas'''
        
        self.lines.reset()
        lineList = []
        for p in range(numPandas):
            if p == 0:
                p1 = joint0Pos
                p2 = pandas[p].get_pos(self.app.render)
            else:
                p1 = pandas[p - 1].get_pos(self.app.render)
                p2 = pandas[p].get_pos(self.app.render)
            lineList.append((p1, p2))
        self.lines.drawLines(lineList)
        self.lines.create()
    
    def simulationTask(self, task):
        '''Main physics simulation task'''
        
        global globalClock
        # get time step
        timeStep = globalClock.getDt()
        #
        self.drawLines(self.pandas, self.numPandas, self.joint0Pos)
        # update timed bodies
        for body in reversed(self.pandaTimedBodies):
            if not body.update(timeStep):
                self.pandaTimedBodies.remove(body)
        #
        return task.cont

    def geometricShoot(self):
        '''Perform a geometric shoot (no physics)'''
        
        physicsMgr = GamePhysicsManager.get_global_ptr()
        # get the mouse watcher
        mwatcher = self.app.mouseWatcherNode
        if mwatcher.has_mouse():
            # emit a sound
            if self.shootSound.status != AudioSound.PLAYING:
                self.shootSound.play()
            # Get to and from pos in camera coordinates
            if self.gunSightPos is None:
                pMouse = mwatcher.get_mouse()
            else:
                pMouse = self.gunSightPos
            # 
            pFrom, pTo = (LPoint3f(), LPoint3f())
            if self.app.camLens.extrude(pMouse, pFrom, pTo):
                # Transform to global coordinates
                pFrom = self.app.render.get_relative_point(self.app.camera, pFrom)
                pTo = self.app.render.get_relative_point(self.app.camera, pTo)
                # get shoot direction
                dirN = pTo - pFrom
                dirN.normalize()
                # cast a ray to detect a body
                result = physicsMgr.ray_test(pFrom, pTo, BT3RayTestResult.CLOSEST,
                                             BitMask32.allOn(), BitMask32.allOn())
                # set shoot from position
                pFrom = self.shootFrom.get_pos(self.app.render)
                # get the body under the mouse
                if result.has_hit:
                    # possible hit objects:
                    # - BT3RigidBody
                    # - BT3CharacterController
                    # - BT3Vehicle
                    # - BT3Constraint
                    # - BT3SoftBody
                    # - BT3Ghost
                    # correct pTo to the hit point position
                    pTo = result.get_hit_point(0)
                    # check if hit body is a rigid body
                    hitNode = result.get_hit_object(0).node()
                    if hitNode.is_of_type(BT3RigidBody.get_class_type()):
                        # apply an impulse to the hit object
                        hitNode.apply_impulse(dirN * 150.0, LPoint3f.zero())
                    elif hitNode.is_of_type(BT3SoftBody.get_class_type()):
                        # apply an force to the hit object
                        nodeIdx = hitNode.get_closest_node_index(pTo)
                        hitNode.add_velocity(dirN * 100.0, nodeIdx)                        
                    # emit a sound
                    # rstrip
                    hitName = hitNode.get_name().rstrip('0123456789')
                    # emit sound
                    if self.hitSoundsDict[hitName] and (self.hitSoundsDict[hitName].status != AudioSound.PLAYING):
                        self.hitSoundsDict[hitName].play()

                # emit particle effect
                for particle in self.shootGraphicsEffect.getParticlesList():
                    if result.has_hit:
                        pToShoot = pTo
                    else:
                        pToShoot = pFrom + dirN * self.shootLength
                    particle.emitter.setEndpoint1(pFrom)
                    particle.emitter.setEndpoint2(pToShoot)
                    # induce labor: force particles' birth
                    particle.induceLabor()

    @staticmethod
    def toggleDebug(debugNP):
        '''Toggle debug physics node on/off'''
        
        if debugNP.isHidden():
            debugNP.show()
        else:
            debugNP.hide()
    
    # camera and GUI methods
    def chooseCamera(self, arg):
        '''Choose camera type'''
        
        # cleanup old camera if any
        if self.cameraType != None:
            self.cameraType.removeCamera()
            self.cameraType = None
        # remove firing if any
        if self.app.isAccepting(self.firePandaKey) and \
                self.app.isAccepting('shift-' + self.firePandaKey):
            self.app.ignore(self.firePandaKey)
            self.app.ignore('shift-' + self.firePandaKey)
        # remove geometric shooting if any
        if self.app.isAccepting(self.geometricShootKey) and \
                self.app.isAccepting('shift-' + self.geometricShootKey):
            self.app.ignore(self.geometricShootKey)
            self.app.ignore('shift-' + self.geometricShootKey)
            self.shootFrom.hide()
        if arg == 'firing':
            self.cameraType = FirstPersonCamera(self.app,
                                       self.app.camera, speed=10,
                                       physicsWorld=self.bulletWorld)
            # fire
            self.app.accept(self.firePandaKey, self.firePanda, extraArgs=[self.soundFirePanda])
            self.app.accept('shift-' + self.firePandaKey, self.firePanda, extraArgs=[self.soundFirePanda])
            # geometric shoot
            self.app.accept(self.geometricShootKey, self.geometricShoot)
            self.app.accept('shift-' + self.geometricShootKey, self.geometricShoot)
            self.shootFrom.show()            
        elif arg == 'trackAuto':
            self.cameraType = FirstPersonCamera(self.app, self.app.camera, speed=10,
                                       physicsWorld=self.bulletWorld,
                                       chasedNode=self.vehicleBodyNP,
                                       chasedNodeRadius=self.vehicleRadius)
        elif arg == 'trackCube':
            self.cameraType = FirstPersonCamera(self.app, self.app.camera, speed=10,
                                       physicsWorld=self.bulletWorld,
                                       chasedNode=self.characterNP,
                                       chasedNodeRadius=self.characterRadius * 2)
        elif arg == 'default':
            self.cameraType = None
        else:
            self.cameraType = None
        #
        if self.cameraType != None:
            self.cameraType.toggle()
        self.askCameraDiag.cleanup()
        self.askCameraDiag = None
    
    def askCamera(self):
        '''Ask for which camera type'''
        
        self.askCameraDiag = DirectDialog(dialogName='askCameraDiag',
                            text='Choose camera type\nPress <enter> to enable/disable',
                            buttonTextList=['Firing', 'Track auto', 'Track cube', 'Default'],
                            buttonValueList=['firing', 'trackAuto', 'trackCube', 'default'],
                            suppressKeys=1, suppressMouse=1, command=self.chooseCamera)
    
    def testExit(self, arg, cameraWasRunning, askCameraWasUp):
        '''Test if user want really to exit'''
        
        if arg == 'exit':
            sys.exit()
        elif arg == 'cameraType':
            self.askCamera()
            self.wantExitDiag.cleanup()
            self.wantExitDiag = None
        elif arg == 'cancel':
            if self.cameraType != None:
                if cameraWasRunning:
                    self.cameraType.toggle()
            if askCameraWasUp:
                self.askCamera()
            self.wantExitDiag.cleanup()
            self.wantExitDiag = None
        else:
            return          
    
    def wantExit(self):
        '''User exit request callback'''
        
        cameraWasRunning = False
        if self.cameraType != None:
            cameraWasRunning = self.cameraType.running
            if self.cameraType.running:
                self.cameraType.toggle()
        askCameraWasUp = False        
        if self.askCameraDiag != None:
            askCameraWasUp = True
            self.askCameraDiag.cleanup()
        self.wantExitDiag = DirectDialog(dialogName='ExitOrApplication',
                                     text='Exit or choose camera type?',
                                     buttonTextList=['Exit', 'Camera Type', 'Cancel'],
                                     buttonValueList=['exit', 'cameraType', 'cancel'],
                                     suppressKeys=1, suppressMouse=1,
                                     command=self.testExit,
                                     extraArgs=[cameraWasRunning, askCameraWasUp])
    
