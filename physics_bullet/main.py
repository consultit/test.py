'''
Created on 07 dic 2015

@author: consultit
'''

from Utilities import Utilities
from Scene import Scene

from panda3d.core import BitMask32, LPoint3f, LVector3f, LMatrix4f, \
                load_prc_file_data, load_prc_file, NodePath
from direct.showbase.ShowBase import ShowBase
import random, time
import ely.libtools
from ely.physics import GamePhysicsManager
import argparse, textwrap

# debug flag
toggleDebugFlag = [False]


def toggleDebugDraw(toggleDebugFlag):
    '''toggle debug draw'''
    
    toggleDebugFlag[0] = not toggleDebugFlag[0]
    GamePhysicsManager.get_global_ptr().debug(toggleDebugFlag[0])


groupMask = BitMask32(BitMask32.all_on())
collideMask = BitMask32(BitMask32.all_on())

load_prc_file('config.prc')
load_prc_file_data('', 'threading-model Cull/Draw')
# load_prc_file_data('', 'want-directtools #t')
# load_prc_file_data('', 'want-tk #t')
# http://www.panda3d.org/forums/viewtopic.php?t=10231&postdays=0&postorder=asc&start=540
# bullet forum:http://www.bulletphysics.org/Bullet/phpBB3/viewtopic.php?p=27489#p27489
load_prc_file_data('', 'bullet-enable-contact-events true')

if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent('''
    This is the physics_bullet script.
    '''))    
    # set up arguments
    parser.add_argument('-d', '--data-dir', type=str, action='append',
                        help='the data dir(s)')
    # parse arguments
    args = parser.parse_args()
    # do actions
    ELY_DATADIRS = args.data_dir
    # set data dir(s)
    if ELY_DATADIRS:
        for dataDir in ELY_DATADIRS:
            load_prc_file_data('', 'model-path ' + dataDir)
            
    # Setup our application
    app = ShowBase()
    
    # Setup our physics world
    physicsMgr = GamePhysicsManager(10, app.render, groupMask, collideMask)
    # set reference node to render
    physicsMgr.reference_node_path = app.render
        
    # create an Utilities object
    utilities = Utilities()

    # Set Global values     
    pandaActor = app.loader.load_model('smiley')
    pandaScale = 1.0
    pandaDims , pandaCenter, pandaRadius = Utilities.getDimensions(pandaActor,
                                                                   pandaScale)
    numPandas = 30
    pandas = []
    pandaShape = 'sphere'
    pandaMass = '1.5'
    pandaMaskBullet = 'all_on'
    pandaTimedBodies = []
    particleEffectFile = '../Models/smoke.ptf'
    soundFirePanda = app.loader.load_sfx('fire_panda.wav')
    hitSoundsDict = {
    'doorBody' : app.loader.loadSfx('hit_door.wav'),
    'hangingPandaBody' : app.loader.load_sfx('hit_panda.wav'),
    'firedPandaBody' : app.loader.load_sfx('hit_panda.wav'),
    'SceneRigidBody' : app.loader.load_sfx('hit_character.wav'),
    }
    pandaPos = LPoint3f(0, 0, 20 + (4 * pandaRadius) * numPandas)
    pandaMaskTarget = '0x4'
    firePandaKey = 'u'
    fireImpulse = 1000
    fireCCD = True
    joint0Pos = pandaPos + LVector3f(0, 0, 2 * pandaRadius)
    togglePhysicsDebugKey = 'f1'
    pickingKey = 'p'
    geometricShootKey = 'mouse1'
    shootParticleEffectFile = '../particles/shoot.ptf'
    shootFrom = None  # see forward
    shootLength = 500.0
    shootSound = app.loader.load_sfx('gunshot.wav')
    gunSightPos = None
    
    # some corrections
    for _, hitSound in hitSoundsDict.items():
        hitSound.set_volume(0.9)
    shootSound.set_volume(0.6)
        
    # get a Scene object
    scene = Scene(app)

    # create environment
    scene.createEnvironment()

    # create a door FIXME
    doorBodyNP, doorDims = scene.createADoor(LPoint3f(46.0, 3.0, 16.6))
    
    # create hanging pandas
    scene.createHangingPandas(pandaMass, pandaRadius, pandaMaskTarget,
                        pandaShape, numPandas, pandas,
                        pandaPos, pandaActor)
    
    # initialize random
    random.seed(time.time())

    # The task for our simulation
    app.taskMgr.add(utilities.simulationTask, 'Game Simulation')
    
    # DEBUG DRAWING: make the debug reference node paths sibling of the reference node
    physicsMgr.reference_node_path_debug.reparent_to(app.render)
    app.accept(togglePhysicsDebugKey, toggleDebugDraw, [toggleDebugFlag])
    # enable object picking
    physicsMgr.enable_picking(app.render, NodePath.any_path(app.camNode),
                              app.mouseWatcher.node(), pickingKey, pickingKey + '-up')
    # enable collision notify event: BTRigidBody_BTRigidBody_Collision
    physicsMgr.enable_throw_event(GamePhysicsManager.COLLISIONNOTIFY, True, 0.1,
                                  'COLLISION')
    app.accept('COLLISION', utilities.contactAdded, ['COLLISION'])
    app.accept('COLLISIONOff', utilities.contactDestroyed, ['COLLISIONOff'])
    # The task for our simulation
    physicsMgr.start_default_update()
    
    # enable particle system
    app.enable_particles()
    
    # additional setup for a geometric shoot
    gun = app.loader.load_model('revolver')
    gun.setScale(0.075)
    gun.setPos(0.25, app.cam.node().getLens().getNear() * 1.5, -0.3)
    gun.setHpr(15.0, 15.0, 0.0)
    gun.reparentTo(app.camera)
    gunTip = gun.attachNewNode('gunTip')
    gunTip.setPos(0.0, 4.0, 0.0)
    shootFrom = gunTip

    # GUI manage exit request and global variables
    app.win.setCloseRequestEvent('close_request_event')    
    app.accept('close_request_event', utilities.wantExit)
    app.accept('escape', utilities.wantExit)

    # setup camera
    app.camera.set_pos(0.0, -200.0, 100.0)
    app.camera.look_at(joint0Pos + LVector3f(0, 0, 0))
    mat = LMatrix4f(app.camera.getTransform().getMat())
    mat.invert_in_place()
    app.mouseInterfaceNode.set_mat(mat)
    utilities.askCamera()

    # show usage
    print ('Type: \n' + 
        '\t' + togglePhysicsDebugKey + ' to toggle physics debug\n' + 
        '\t' + geometricShootKey + ' to geometrically shoot a body (in firing mode)\n' + 
        '\t' + firePandaKey + ' to fire pandas (in firing mode)\n'
        '\t' + pickingKey + ' to pick bodies with mouse\n'
        )

    # set Utilities' globals
    utilities.setGlobals(
        app=app,
        bulletWorld=physicsMgr,
        pandaActor=pandaActor,
        numPandas=numPandas,
        pandas=pandas,
        pandaTimedBodies=pandaTimedBodies,
        pandaMass=pandaMass,
        pandaRadius=pandaRadius,
        pandaMaskBullet=pandaMaskBullet,
        firePandaKey=firePandaKey,
        fireImpulse=fireImpulse,
        fireCCD=fireCCD,
        soundFirePanda=soundFirePanda,
        particleEffectFile=particleEffectFile,
        hitSoundsDict=hitSoundsDict,
        joint0Pos=joint0Pos,
        geometricShootKey=geometricShootKey,
        shootParticleEffectFile=shootParticleEffectFile,
        shootFrom=shootFrom,
        shootLength=shootLength,
        shootSound=shootSound,
        gunSightPos=gunSightPos)

    # start
    app.run()
