'''
Created on 08 dic 2015

@author: consultit
'''

from panda3d.core import LPoint3f, LVector3f, NodePath, \
                BitMask32, CardMaker, TextureStage, \
                GeomVertexFormat, GeomNode, NurbsCurveEvaluator, RopeNode, \
    LVecBase3f, TransformState
from ely.libtools import ValueList_NodePath, ValueList_LPoint3f, \
    ValueList_LVector3f
from ely.physics import GamePhysicsManager, BT3Constraint, CollisionObjectAttrs
from direct.showbase.InputStateGlobal import inputState
from Utilities import Utilities
import math


# scene objects' creation class
class Scene(object):
    '''Scene objects' creation class'''
    
    def __init__(self, app):
        '''Constructor'''
        
        self.app = app
    
    def createEnvironment(self):
        '''Create collision environment'''

        physicsMgr = GamePhysicsManager.get_global_ptr()
        # # triangle mesh
        pos = LVector3f(0.0, 0.0, 0.0)
        hpr = LVecBase3f(45.0, 0.0, 0.0)
        sceneNP = self.loadTerrainLowPoly('SceneNP', 512.0, 512.0)
        # set sceneNP transform
        sceneNP.set_pos_hpr(pos, hpr)
        # create scene's rigid_body (attached to the reference node)
        sceneRigidBodyNP = physicsMgr.create_rigid_body('SceneRigidBody')
        # get a reference to the scene's rigid_body
        sceneRigidBody = sceneRigidBodyNP.node()   
        # set some parameters
        sceneRigidBody.set_shape_type(GamePhysicsManager.TRIANGLE_MESH)
        # set mass=0 before setup() for static (and kinematic) bodies
        sceneRigidBody.set_mass(0.0)
        # set the object for rigid body
        sceneRigidBody.set_owner_object(sceneNP)
        # setup
        sceneRigidBody.setup()
        # change some attrs
        sceneRigidBody.collision_object_attrs.friction = 0.8
        sceneRigidBody.collision_object_attrs.restitution = 0.1
        # add a skybox
        skybox = self.app.loader.load_model('skybox')
        # make big enough to cover whole terrain, else there'll be problems with the water reflections
        skybox.set_z(0)
        skybox.set_scale(1000)
        skybox.set_bin('background', 1)
        skybox.set_depth_write(0)
        skybox.set_light_off()
        skybox.reparent_to(self.app.render)
        # Music
        self.backgroundMusic = self.app.loader.load_music('music.ogg')
        self.backgroundMusic.set_loop(True)
        self.backgroundMusic.play()
        self.backgroundMusic.set_volume(0.4)
        self.backgroundMusic.set_balance(0.0)
        self.backgroundMusic.set_play_rate(1.0)

    def loadTerrainLowPoly(self, name, widthScale=128, heightScale=64.0,
                           texture='dry-grass.png'):
        '''load terrain low poly stuff'''
        
        terrainNP = self.app.loader.load_model('terrain-low-poly.egg')
        terrainNP.set_name(name)
        terrainNP.set_transform(TransformState.make_identity())
        terrainNP.set_scale(widthScale, widthScale, heightScale)
        tex = self.app.loader.load_texture(texture)
        terrainNP.set_texture(tex)
        return terrainNP

    def createHangingPandas(self, pandaMass,
                        pandaRadius, pandaMaskTarget,
                        pandaShape, numPandas, pandas,
                        pandaPos, pandaActor):
        '''Create hanging pandas'''
        
        physicsMgr = GamePhysicsManager.get_global_ptr()
        physicsMgr.set_parameters_defaults(GamePhysicsManager.CONSTRAINT)
        # Create bodies and set masses for pandas   
        for p in range(numPandas):
            panda = NodePath('pandaNP' + str(p))
            pandaActor.instance_to(panda)
            pos = pandaPos - LVector3f(0, 0, (4 * pandaRadius) * p)
            panda.set_pos(pos)
            # create && get a reference to a BT3RigidBody
            physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'mass',
                                           pandaMass)
            physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'group_mask',
                                           pandaMaskTarget)
            physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'collide_mask',
                                           pandaMaskTarget)
            physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'up_axis',
                                           'no_up')            
            physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'shape_type',
                                           pandaShape)
            pandaRBNP = physicsMgr.create_rigid_body('hangingPandaBody' + str(p))
            pandas.append(pandaRBNP)         
            pandaRB = pandaRBNP.node()
            # setup
            pandaRB.set_owner_object(panda)
            pandaRB.setup()
            # change some attrs
            pandaRB.collision_object_attrs.friction = 0.9          
            pandaRB.collision_object_attrs.set_activation_state(
                CollisionObjectAttrs.DISABLE_DEACTIVATION_STATE)
            # create the constraint 
            cs = physicsMgr.create_constraint('constraint' + str(p)).node()
            # set the constraint type
            cs.set_constraint_type(BT3Constraint.POINT2POINT)
            # use world references
            cs.set_use_world_reference(True)
            # set the constraint between this and the previous object, if any
            # set the objects
            objects = ValueList_NodePath()
            objects.add_value(pandas[p])
            # set the pivot
            points = ValueList_LPoint3f()
            points.add_value(pos + LVector3f(0, 0, 2 * pandaRadius))
            if p == 0:
                objects.add_value(NodePath())
            else:
                # p > 0
                objects.add_value(pandas[p - 1])
            cs.set_owner_objects(objects)
            cs.set_pivot_references(points)
            cs.setup()
            cs.constraints_attrs.debug_draw_size = 1.0

    def createADoor(self, pos):
        '''Create a door'''
         
        doorMask = 'all_on'
        door = self.app.loader.load_model('door01')
        # get dimensions of door
        doorScale = 3.0
        doorDims, _, _ = Utilities.getDimensions(door, doorScale)
        door.set_pos(pos + LVector3f(0, 0, doorDims.getZ() / 2.0 + 1.0))
        #
        doorMass = '2.0'
        physicsMgr = GamePhysicsManager.get_global_ptr()
        physicsMgr.set_parameters_defaults(GamePhysicsManager.RIGIDBODY)
        # create && get a reference to a BT3RigidBody
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'mass',
                                       doorMass)
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'group_mask',
                                       doorMask)
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'collide_mask',
                                       doorMask)
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'up_axis',
                                       'no_up')            
        physicsMgr.set_parameter_value(GamePhysicsManager.RIGIDBODY, 'shape_type',
                                       'box')
        doorBodyNP = physicsMgr.create_rigid_body('doorBody')
        doorBody = doorBodyNP.node()
        # setup
        doorBody.set_owner_object(door)
        doorBody.setup()
        # change some attrs
        doorBody.collision_object_attrs.set_activation_state(
            CollisionObjectAttrs.DISABLE_DEACTIVATION_STATE)
        doorBody.collision_object_attrs.friction = 0.8
        doorBody.collision_object_attrs.restitution = 0.1
        # add hinge constraint
        physicsMgr.set_parameters_defaults(GamePhysicsManager.CONSTRAINT)
        doorHingeNP = physicsMgr.create_constraint('doorHinge')
        doorHinge = doorHingeNP.node()
        doorHinge.set_constraint_type(BT3Constraint.HINGE)
        doorHinge.set_use_world_reference(False)
        #
        objects = ValueList_NodePath([doorBodyNP, NodePath()])
        doorPivot = LPoint3f(-doorDims.get_x() / 4.0 + 0.4, 0, 0)
        points = ValueList_LPoint3f([doorPivot])
        doorAxis = LVector3f(0, 0, 1)
        axes = ValueList_LVector3f([doorAxis])
        #
        doorHinge.owner_objects = objects
        doorHinge.pivot_references = points
        doorHinge.axis_references = axes
        doorHinge.setup()
        doorHinge.constraints_attrs.debug_draw_size = 2.0
        doorHinge.constraints_attrs.set_limit(-math.pi / 4.0, math.pi / 4.0,
                                              _softness=0.9,
                                              _biasFactor=0.3,
                                              _relaxationFactor=1.0)
        return (doorBodyNP, doorDims)
