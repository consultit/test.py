'''
Created on Mar 16, 2019

@author: consultit
'''

# 2014-Game Programming Pattern

from direct.showbase.ShowBase import ShowBase
from panda3d.core import load_prc_file_data, LPoint3f, LVecBase3f
from flyweight import TreeModel, Tree
import data
import os
from random import random


class World(object):
    '''
    World class.
    '''
    
    def __init__(self, app, fieldW, treeSharedModel):
        
        self.app = app
        self.fieldW = abs(fieldW)
        # create the tree shared model(s)
        self.treeSharedModel = TreeModel(app.loader.load_model(treeSharedModel))

    def createTrees(self, instNum, angW, deltaH):
        
        for i in range(instNum):
            pos = LPoint3f(self._fRand(self.fieldW), self._fRand(self.fieldW), 0.0)
            hpr = LVecBase3f(self._fRand(angW), 0.0, 0.0)
            scale = 1.0 - self._fRand(abs(deltaH))
            treeInst = Tree('tree_' + str(i), self.treeSharedModel, pos, hpr, scale)
            treeInst.instance.reparent_to(self.app.render)
    
    def _fRand(self, lim):
        return (2.0 * random() - 1.0) * lim


class MyApp(ShowBase):
    
    def __init__(self, modelPath=None, *args, **kwargs):
        load_prc_file_data('', 'model-path ' + modelPath)
        super(MyApp, self).__init__(*args, **kwargs)


if __name__ == '__main__':
    dataDir = os.path.dirname(data.__file__)
    app = MyApp(dataDir)
    #
    world = World(app, fieldW=1000.0, treeSharedModel='eucalyptus')
    world.createTrees(instNum=2000, angW=360, deltaH=0.2)
    #
    app.run()
