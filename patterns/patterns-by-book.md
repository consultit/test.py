#### Game Development Patterns

1. **Component** [4](#4)
2. **Component Object Model** [2](#2),[7](#7)
3. **Data Locality** [4](#4)
4. **Dirty Flag** [4](#4)
5. **Double Buffer** [4](#4)
6. **Event Queue** [4](#4)
7. **Game Loop** [4](#4)
8. **Manager** [2](#2)
9. **Object Pool** [4](#4),[7](#7)
10. **Russian doll** [2](#2)
11. **Service Locator** [4](#4)
12. **Spatial Partition** [4](#4)
13. **Strawman** [2](#2)
14. **Update Method** [4](#4)

#### Creational Patterns

1. **Abstract Factory** [1](#1),[3](#3),[5](#5),[6](#6),[8](#8),[9](#9)
2. **Builder** [1](#1),[5](#5),[8](#8),[9](#9)
3. **Factory** [2](#2),[6](#6),[7](#7) - *[python]*
4. **Factory Method** [1](#1),[3](#3),[5](#5),[6](#6),[8](#8),[9](#9) - *[python]*
5. **Prototype** [1](#1),[2](#2),[4](#4),[5](#5),[7](#7),[8](#8),[9](#9)
6. **Singleton** [1](#1),[2](#2),[3](#3),[4](#4),[5](#5),[6](#6),[7](#7),[8](#8),[9](#9) - *[python]*

#### Structural Patterns

1. **Adapter** [1](#1),[5](#5),[8](#8),[9](#9)
2. **Bridge** [1](#1),[5](#5),[9](#9)
3. **Composite** [1](#1),[9](#9)
4. **Decorator** [1](#1),[5](#5),[7](#7),[8](#8),[9](#9) - *[python, c++]*
5. **Facade** [1](#1),[3](#3),[5](#5),[6](#6),[8](#8),[9](#9) - *[python]*
6. **Flyweight** [1](#1),[4](#4),[5](#5),[7](#7),[9](#9) - *[python]*
7. **Proxy** [1](#1),[3](#3),[5](#5),[6](#6),[8](#8),[9](#9)

#### Behavioral Patterns

1. **Bytecode** [4](#4)
2. **Chain of Responsibility** [1](#1),[5](#5),[8](#8),[9](#9) - *[python, c++]*
3. **Command** [1](#1),[3](#3),[4](#4),[5](#5),[6](#6),[7](#7),[8](#8),[9](#9) - *[python]*
4. **Interpreter** [1](#1),[5](#5),[8](#8),[9](#9)
5. **lterator** [1](#1),[5](#5),[8](#8),[9](#9)
6. **Mediator** [1](#1),[9](#9)
7. **Memento** [1](#1),[5](#5),[9](#9)
8. **Observer (Publisher/Subscriber)** [1](#1),[3](#3),[4](#4),[5](#5),[6](#6),[7](#7),[8](#8),[9](#9) - *[python]*
9. **State** [1](#1),[4](#4),[5](#5),[6](#6),[7](#7),[8](#8),[9](#9)
10. **Strategy** [1](#1),[5](#5),[7](#7),[8](#8),[9](#9) - *[python]*
11. **Subclass Sandbox** [4](#4)
12. **Template Method** [1](#1),[3](#3),[5](#5),[6](#6),[8](#8),[9](#9)
13. **Visitor** [1](#1),[2](#2),[8](#8),[9](#9)
14. **Type Object** [4](#4)

#### Architectural Patterns

1. **Model-View-Controller** [3](#3),[5](#5),[6](#6),[8](#8)
2. **Microservices** [5](#5)

#### Books

##### [1]
>[_2004-Holub on Patterns Learning Design Patterns by Looking at Code_](https://drive.google.com/open?id=1WnRcS_2aD_mDiY7Y5VRZj7FA99de7lPt)
##### [2]
>[_2004-Object-Oriented Game Development_](https://drive.google.com/open?id=1b46g9fdkQuNu4eMXSCCmJ-dOmo2Fyvkx)
##### [3]
>[_2013-Learning Python Design Patterns_](https://drive.google.com/open?id=1s0sW3PQN_e9Exmpur8ZYjG_P1tz27ib4)
##### [4]
>[_2014-Game Programming Pattern_](https://drive.google.com/open?id=1aRXBeIPyff6PCYMw54-NKmr0mUO3cgxu)
##### [5]
>[_2018-Mastering Python Design Patterns_](https://drive.google.com/file/d/13fm7tz2qSa5wbSatH8OFJWctAkzFn72-/view?usp=sharing)
##### [6]
>[_2016-Learning Python Design Patterns_](https://drive.google.com/open?id=1Vf-LIcN0sORhl3NGFbi6u0A13p3l5Oy0)
##### [7]
>[_2017-Game Development Patterns and Best Practices_](https://drive.google.com/open?id=1T4FTF9wj5B-Vbm29riqHaJUQF35nlADV)
##### [8]
>[_2017-Practical Python Design Patterns_](https://drive.google.com/open?id=1nD6PP5uekCSnjC_K_TtXCkZRWuEwrMk3)
##### [9]
>[_Huston - Design Patterns_](http://www.vincehuston.org/dp/)
