'''
Created on Jan 20, 2019

@author: consultit
'''

# 2016-Learning Python Design Patterns

from abc import ABCMeta, abstractmethod


# The Simple Factory pattern
class Animal(metaclass=ABCMeta):

    @abstractmethod
    def do_say(self):
        raise NotImplementedError


class Dog(Animal):

    def do_say(self):
        print('DOG!')


class Cat(Animal):

    def do_say(self):
        print('CAT!')


class ForestFactory(object):

    def make_sound(self, object_type):
        return eval(object_type)().do_say()

# # The Factory Method pattern
# Creator
class Section(metaclass=ABCMeta):

    @abstractmethod
    def describe(self):
        raise NotImplementedError


class PersonalSection(Section):

    def describe(self):
        return 'Personal Section'

        
class AlbumSection(Section):

    def describe(self):
        return 'Album Section'


class PatentSection(Section):

    def describe(self):
        return 'Patent Section'

        
class PublicationSection(Section):

    def describe(self):
        return 'Publication Section'


# Product
class Profile(metaclass=ABCMeta):

    def __init__(self):
        self.sections = []
        self.createProfile()
    
    @abstractmethod
    def createProfile(self):
        raise NotImplementedError
    
    def getSections(self):
        return self.sections
    
    def addSections(self, section):
        self.sections.append(section)


class linkedin(Profile):

    def createProfile(self):
        self.addSections(PersonalSection())
        self.addSections(PatentSection())
        self.addSections(PublicationSection())


class facebook(Profile):

    def createProfile(self):
        self.addSections(PersonalSection())
        self.addSections(AlbumSection())

# # The Abstract Factory pattern
# Factories
class PizzaFactory(metaclass=ABCMeta):
    
    @abstractmethod
    def createVegPizza(self):
        raise NotImplementedError

    @abstractmethod
    def createNonVegPizza(self):
        raise NotImplementedError


# 1
class IndianPizzaFactory(PizzaFactory):
    
    def createVegPizza(self):
        return deluxveggiePizza()
    
    def createNonVegPizza(self):
        return chickenPizza()


# 2
class USPizzaFactory(PizzaFactory):

    def createVegPizza(self):
        return mexicanvegPizza()

    def createNonVegPizza(self):
        return hamPizza()


# Products
# 1
class VegPizza(metaclass=ABCMeta):
    
    @abstractmethod
    def prepare(self, VegPizza):
        raise NotImplementedError


class deluxveggiePizza(VegPizza):

    def prepare(self):
        print("Prepare ", type(self).__name__)


class mexicanvegPizza(VegPizza):

    def prepare(self):
        print("Prepare ", type(self).__name__)


# 2
class NonVegPizza(metaclass=ABCMeta):

    @abstractmethod
    def serve(self, VegPizza):
        raise NotImplementedError


class chickenPizza(NonVegPizza):

    def serve(self, VegPizza):
        print(type(self).__name__, " is served with Chicken on ", type(VegPizza).__name__)


class hamPizza(NonVegPizza):
    
    def serve(self, VegPizza):
        print(type(self).__name__, " is served with Ham on ", type(VegPizza).__name__)


# Client
class PizzaStore:
    
    def __init__(self):
        pass
    
    def makePizza(self, veg, nonVeg=None):
        
        VegPizza = None
        NonVegPizza = None
        if veg == 'deluxveggiePizza':
            VegPizza = IndianPizzaFactory().createVegPizza()
        elif veg == 'mexicanvegPizza':
            VegPizza = USPizzaFactory().createVegPizza()
        else:
            print('Sorry, we''haven''t this kind of vegan pizza!')
            return
        if nonVeg:
            if nonVeg == 'chickenPizza':
                NonVegPizza = IndianPizzaFactory().createNonVegPizza()
            elif nonVeg == 'hamPizza':
                NonVegPizza = USPizzaFactory().createNonVegPizza()
            else:
                print('Sorry, we''haven''t this kind of pizza!')
        VegPizza.prepare()
        if NonVegPizza: NonVegPizza.serve(VegPizza)
