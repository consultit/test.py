'''
Created on Mar 16, 2019

@author: consultit
'''

# 2014-Game Programming Pattern

from panda3d.core import NodePath

# Flyweight Pattern 1
class TreeModel(object):
    '''
    TreeModel class.
    
    The intrinsic (context-free) state of a Tree, shared among 
    several Tree instances.
    '''

    def __init__(self, model):
        
        self._model = model
        
    @property
    def model(self):
        
        return self._model

    
class Tree(object):
    '''
    Tree class.
    
    The estrinsic state of a Tree, unique to a given Tree instance.
    '''

    def __init__(self, name, treeModel, pos, hpr, scale):
        
        self._instance = NodePath(name)
        treeModel.model.instance_to(self._instance)
        self._instance.set_pos(pos)
        self._instance.set_hpr(hpr)
        self._instance.set_scale(scale)
        
    @property
    def instance(self):
        
        return self._instance
        
