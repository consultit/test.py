'''
Created on Jun 18, 2019

@author: consultit
'''

import unittest


def setUpModule():
    pass

    
def tearDownModule():
    pass


class FirstTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testA(self):
        pass
    
    def testB(self):
        pass


class SecondTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testA(self):
        pass
    
    def testB(self):
        pass

        
def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((FirstTEST('testA'), FirstTEST('testB')))
    suite.addTest(SecondTEST('testA'))
    suite.addTest(SecondTEST('testB'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
