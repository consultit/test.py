'''
Created on Aug 01, 2019

@author: consultit
'''

import unittest
from test_py.patterns.singleton import SingletonMETA


def setUpModule():
    pass

    
def tearDownModule():
    pass


class Base(object):
    
    def __init__(self, *args, **kwargs):
        for arg in args:
            print('Base', str(arg), sep='-')
        for kwarg in kwargs:
            print('Base', str(kwarg) + ':' + str(kwargs[kwarg]), sep='-')

    def method(self):
        return str(self.__class__.__name__) + '.method'

            
class Derived(Base, metaclass=SingletonMETA):

    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        for arg in args:
            print('Derived', str(arg), sep='-')
        for kwarg in kwargs:
            print('Derived', str(kwarg) + ':' + str(kwargs[kwarg]), sep='-')

            
class OnlyOne(metaclass=SingletonMETA):
    '''OnlyOne'''


class SingletonMETATEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testA(self):
        d = Derived('a', b='b')
        d1 = Derived('a1', b1='b1')
        self.assertEqual(d, d1)
        self.assertEqual(d, Derived.getGlobalPtr())
        self.assertEqual('Derived.method', Derived.getGlobalPtr().method())
    
    def testB(self):
        self.assertRaises(KeyError, OnlyOne.getGlobalPtr)

        
def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((SingletonMETATEST('testA'), SingletonMETATEST('testB')))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
