'''
Created on Jun 15, 2019

@author: consultit
'''

from chainOfResponsibility import ConsoleLogger, LogLevel, EmailLogger, \
    FileLogger

if __name__ == '__main__':
    '''
    Building the chain of responsibility.
    '''
    logger = ConsoleLogger([LogLevel.ALL])
    email_logger = logger.set_next(
        EmailLogger([LogLevel.FUNCTIONAL_MESSAGE, LogLevel.FUNCTIONAL_ERROR])
    )
    # As we don't need to use file logger instance anywhere later
    # We will not set any value for it.
    email_logger.set_next(
        FileLogger([LogLevel.WARNING, LogLevel.ERROR])
    )

    # ConsoleLogger will handle this part of code since the message
    # has a log level of all
    logger.message('Entering function ProcessOrder().', LogLevel.DEBUG)
    logger.message('Order record retrieved.', LogLevel.INFO)

    # ConsoleLogger and FileLogger will handle this part since file logger
    # implements WARNING and ERROR
    logger.message(
        'Customer Address details missing in Branch DataBase.',
        LogLevel.WARNING
    )
    logger.message(
        'Customer Address details missing in Organization DataBase.',
        LogLevel.ERROR
    )

    # ConsoleLogger and EmailLogger will handle this part as they implement
    # functional error
    logger.message(
        'Unable to Process Order ORD1 Dated D1 for customer C1.',
        LogLevel.FUNCTIONAL_ERROR
    )
    logger.message('OrderDispatched.', LogLevel.FUNCTIONAL_MESSAGE)
