'''
Created on Jan 19, 2019

@author: consultit
'''

# 2016-Learning Python Design Patterns


class Singleton(object):
    '''
    Classic singleton.
     
    Access only through constructor.
    '''
     
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super().__new__(cls)
            print('Singleton created:', cls.instance, sep=' ')
        return cls.instance
     
    def method(self):
        print('method of', self.__class__.instance, sep=' ')

        
class SingletonLazy(object):
    '''
    Lazy instantiation singleton.
    
    Access only through getInstance() class method.
    '''
    
    __instance = None
    
    def __init__(self):
        if not SingletonLazy.__instance:
            print('__init__ method called...')
        else:
            print('Instance already created and initialized:', self.getInstance(), sep=' ')
       
    @classmethod
    def getInstance(cls):
        if not cls.__instance:
            cls.__instance = SingletonLazy()
        return cls.__instance
    
    def method(self):
        print('method of', self.__class__.__instance, sep=' ')

# inspired by https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python

class SingletonMETA(type):
    '''
    Singleton implemented as metaclass.
    
    See: https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python .
    '''

    _instances = {}
    
    def __init__(cls, clsname, bases, dct):
        if not hasattr(cls, 'getGlobalPtr'):
            setattr(cls, 'getGlobalPtr', classmethod(lambda _cls: _cls._instances[_cls]))

    def __call__(self, *args, **kwargs):
        '''
        Constructor
        '''
        if self not in self._instances:
            self._instances[self] = super(SingletonMETA, self).__call__(*args, **kwargs)
        return self._instances[self]
