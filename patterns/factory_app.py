'''
Created on Jan 20, 2019

@author: consultit
'''

from factory import ForestFactory, PizzaStore

if __name__ == '__main__':
    # The Simple Factory pattern
    ff = ForestFactory()
    animal = True
    while animal:
        animal = input('Which animal?')
        if animal: 
            ff.make_sound(animal)
        print()
        
    # The Factory Method pattern
    profile_type = True
    while profile_type:
        profile_type = input('Which Profile you''d like to create? [LinkedIn or FaceBook]')
        if profile_type:
            profile = eval(profile_type.lower())()
            print('Creating Profile...', type(profile).__name__)
            print('Profile has sections:', [section.describe() for section in profile.getSections()])
        print()

    # The Abstract Factory pattern
    store = PizzaStore()
    pizzaVeg = True
    while pizzaVeg:
        pizzaVeg = input('Which vegan pizza?')
        if pizzaVeg:
            veg = pizzaVeg.lower() + 'Pizza'
            nonVeg = None
            pizzaNonVeg = input('Which non vegan pizza?')
            if pizzaNonVeg: nonVeg = pizzaNonVeg.lower() + 'Pizza'
            store.makePizza(veg, nonVeg)
        print()
