'''
Created on Apr 27, 2019

@author: consultit
'''

from decorator import IAwithX, IAwithXY, IAwithXYZ, DA, X, Y, Z

if __name__ == '__main__':
    # http://www.vincehuston.org/dp/decorator.html
    print('Inheritance:')
    anX = IAwithX()    
    anXY = IAwithXY()   
    anXYZ = IAwithXYZ()  
    anX.do_it()    
    print()
    anXY.do_it()
    print()
    anXYZ.do_it()
    print()
    print('Decorator:')
    anX = X(DA())    
    anXY = Y(X(DA()))   
    anXYZ = Z(Y(X(DA())))
    anX.do_it()    
    print()
    anXY.do_it()
    print()
    anXYZ.do_it()
    print()
