'''
Created on Apr 16, 2019

@author: consultit
'''

from strategy import ProtocolHandler, KeepalivePool
from socket import AF_INET, AF_UNIX, SOCK_STREAM, SOCK_DGRAM

if __name__ == '__main__':
    # 2004-Holub on Patterns: Learning Design Patterns by Looking at Code
    protHandler = ProtocolHandler()
    protHandler.process(AF_INET, SOCK_DGRAM)
    protHandler.setPoolingStrategy(KeepalivePool())
    protHandler.process(AF_INET, SOCK_DGRAM)
    protHandler.process(AF_UNIX, SOCK_STREAM)
    protHandler.process(AF_INET, SOCK_DGRAM)
    
