'''
Created on Mar 9, 2019

@author: consultit
'''

# 2014-Game Programming Pattern

from abc import ABCMeta, abstractmethod


class Command(metaclass=ABCMeta):

    @abstractmethod
    def execute(self, actor):
        raise NotImplementedError

    
class JumpCommand(Command):
    
    def execute(self, actor):
        print('JumpCommand for ' + str(actor))


class FireCommand(Command):
    
    def execute(self, actor):
        print('FireCommand for ' + str(actor))

    
class SwapCommand(Command):
    
    def execute(self, actor):
        print('SwapCommand for ' + str(actor))


class NullCommand(Command):
    
    def execute(self, actor):
        print('NullCommand')


class Action(metaclass=ABCMeta):

    @abstractmethod
    def execute(self):
        raise NotImplementedError

    @abstractmethod
    def undo(self):
        raise NotImplementedError


class MoveAction(Action):
    
    def __init__(self, item):
        self.item = item
    
    def execute(self):
        self.prevPos = self.item.pos
        self.item.pos += 1
        print('position:', self.item.pos, hex(id(self)), str(self.item), sep=' ')

    def undo(self):
        self.item.pos = self.prevPos
        print('position:', self.item.pos, hex(id(self)), str(self.item), sep=' ')


class RotateAction(Action):

    def __init__(self, item):
        self.item = item
    
    def execute(self):
        self.prevAngle = self.item.angle
        self.item.angle += 1
        print('rotation:', self.item.angle, hex(id(self)), str(self.item), sep=' ')

    def undo(self):
        self.item.angle = self.prevAngle
        print('rotation:', self.item.angle, hex(id(self)), str(self.item), sep=' ')
