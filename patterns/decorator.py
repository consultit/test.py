'''
Created on Apr 27, 2019

@author: consultit
'''

# http://www.vincehuston.org/dp/decorator.html

from abc import ABCMeta, abstractmethod


# Before: Inheritance run amok
class IA(object):
    
    def do_it(self):
        print('A', end='')

        
class IAwithX(IA):
    
    def do_it(self):
        IA.do_it(self)
        self._doX()
        
    def _doX(self):
        print('X', end='')

        
class IAwithY(IA):
    
    def do_it(self):
        IA.do_it(self)
        self._doY()
        
    def _doY(self):
        print('Y', end='')


class IAwithZ(IA):
    
    def do_it(self):
        IA.do_it(self)
        self._doZ()
        
    def _doZ(self):
        print('Z', end='')


class IAwithXY(IAwithX, IAwithY):
    
    def do_it(self):
        IAwithX.do_it(self)
        IAwithY._doY(self)

        
class IAwithXYZ(IAwithX, IAwithY, IAwithZ):
    
    def do_it(self):
        IAwithX.do_it(self)
        IAwithY._doY(self)
        IAwithZ._doZ(self)

# After: Replacing inheritance with wrapping-delegation
#
# Discussion.  Use aggregation instead of inheritance to
# implement embellishments to a "core" object.  Client
# can dynamically compose permutations, instead of the
# architect statically wielding multiple inheritance.


# Interface
class I(metaclass=ABCMeta):
    
    @abstractmethod
    def do_it(self):
        raise NotImplementedError


# Core
class DA(I):

    def do_it(self):
        print('A', end='')


# Base Decorator
class D(I):

    def __init__(self, inner):
        self.m_wrappee = inner
        
    def do_it(self):
        self.m_wrappee.do_it()


# Derived Decorator(s)
class X(D):

    def do_it(self):
        D.do_it(self)
        print('X', end='')


class Y(D):

    def do_it(self):
        D.do_it(self)
        print('Y', end='')


class Z(D):

    def do_it(self):
        D.do_it(self)
        print('Z', end='')

# 2015-Mastering Python Design Patterns
