'''
Created on Apr 16, 2019

@author: consultit
'''

# 2004-Holub on Patterns: Learning Design Patterns by Looking at Code

from abc import ABCMeta, abstractmethod
from socket import socket


# Strategy (interface)
class SocketPool(metaclass=ABCMeta):

    @abstractmethod
    def allocate(self, family, sockType):
        raise NotImplementedError

    @abstractmethod
    def release(self, s):
        raise NotImplementedError


# Concrete Strategy
class SimplePool(SocketPool):
    
    def allocate(self, family, sockType):
        return socket(family, sockType)

    def release(self, s):
        s.close()


# Concrete Strategy
class KeepalivePool(SocketPool):
    
    def __init__(self):
        self._connections = {}

    def __del__(self):
        for k in self._connections:
            self._connections[k].close()
   
    def allocate(self, family, sockType):
        connection = self._connections.get(str(int(family)) + '_' + str(int(sockType)))
        if not connection:
            connection = socket(family, sockType)
        return connection

    def release(self, s):
        self._connections[str(int(s.family)) + '_' + str(int(s.type))] = s


# Context
class ProtocolHandler(object):

    def __init__(self):
        self.pool = SimplePool()
        
    def process(self, family, sockType):
        inSock = self.pool.allocate(family, sockType)
        print('Using: ', str(inSock))
        self.pool.release(inSock)

    def setPoolingStrategy(self, p):
        self.pool = p
