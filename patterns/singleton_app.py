'''
Created on Jan 19, 2019

@author: consultit
'''

from singleton import Singleton, SingletonLazy

if __name__ == '__main__':
    Singleton().method()
    s = Singleton()
    s1 = Singleton.instance
    print(Singleton() is s, s is s1)
    #
    sl = SingletonLazy()
    SingletonLazy.getInstance()
    sl1 = SingletonLazy()
    sl1.method()
    print(sl, SingletonLazy.getInstance(), sl1, sep=' ')
