'''
Created on Mar 9, 2019

@author: consultit
'''

# 2014-Game Programming Pattern

from direct.showbase.ShowBase import ShowBase
from command import Command, JumpCommand, FireCommand, SwapCommand, NullCommand, \
    Action, MoveAction, RotateAction
import string


class InputHandler(object):
   
    class InputHandlerException(Exception):
        pass
    
    def __init__(self, app):
        if not app:
            raise self.InputHandlerException()
        self.commandLog = []
        self.app = app
        # make all events be handled by NullCommand
        self.handlers = {event:NullCommand() for event in string.ascii_lowercase + string.digits}
        for event in self.handlers:
            self.app.accept(event, self.handleInput, [event, None])
        # set undo/redo action handlers (hardcoded)
        self.actionList = []
        self.app.accept('control-z', self.undoAction)
        self.app.accept('shift-control-z', self.redoAction)
        self.currentAction = -1
            
    def setCommand(self, event, command, actor):
        cmd = command if actor else NullCommand()
        self.app.accept(event, self.handleInput, [event, actor])
        self.handlers[event] = cmd
        
    def handleInput(self, event, actor):
        command = self.handlers[event]
        if not isinstance(command, Command): return
        
        # execute the command
        command.execute(actor)
        # log the command
        self.commandLog.append((str(command.__class__), str(actor)))

    def printCommandsSoFar(self):
        print('Commands so far')
        for item in self.commandLog:
            print(item)
        self.commandLog[:] = []
        
    def setAction(self, event, actionClass, item):
        if issubclass(actionClass, Action):
            self.handlers[event] = (actionClass, item)
            self.app.accept(event, self.handleAction, [event])
        else:
            self.setCommand(event, None, None)

    def handleAction(self, event):
        actionClassItem = self.handlers[event]
        if len(actionClassItem) != 2: return
        if not issubclass(actionClassItem[0], Action): return
        
        # discard current action's redo list (if any)
        if self.currentAction < len(self.actionList) - 1:
            self.actionList[self.currentAction + 1:] = []
        # register and execute the action
        self.actionList.append(actionClassItem[0](actionClassItem[1]))
        self.actionList[-1].execute()
        # update current action
        self.currentAction += 1

    def undoAction(self):
        if self.currentAction >= 0:
            # undo action
            self.actionList[self.currentAction].undo()
            # update current action
            self.currentAction -= 1

    def redoAction(self):
        if self.currentAction < len(self.actionList) - 1:
            # redo action
            self.actionList[self.currentAction].execute()
            # update current action
            self.currentAction += 1


class MyApp(ShowBase):
    
    def __init__(self, *args, **kwargs):
        super(MyApp, self).__init__(*args, **kwargs)


class GameActor(object):
    
    def __init__(self, name):
        self.name = name
        self._pos = 0
        self._angle = 0
        
    def __str__(self):
        return self.name
    
    @property
    def pos(self):
        return self._pos
    
    @pos.setter
    def pos(self, pos):
        self._pos = pos
        
    @property
    def angle(self):
        return self._angle
    
    @angle.setter
    def angle(self, angle):
        self._angle = angle


if __name__ == '__main__':
    app = MyApp()
    inp = InputHandler(app)
    # some actors
    a0 = GameActor('a0')
    a1 = GameActor('a1')
    # set commands
    inp.setCommand('j', JumpCommand(), a0)
    inp.setCommand('f', FireCommand(), a0)
    inp.setCommand('s', SwapCommand(), a0)   
    inp.setCommand('q', JumpCommand(), a1)
    inp.setCommand('w', FireCommand(), a1)
    inp.setCommand('e', SwapCommand(), a1)
    app.accept('page_up', lambda: inp.printCommandsSoFar())
    
    # set action
    actions = [MoveAction, RotateAction]
    inp.setAction('a', actions[0], a0)
    
    def toggleAction():
        tmp = actions[0]
        actions[0] = actions[1]
        actions[1] = tmp
        inp.setAction('a', actions[0], a0)
        print('action: ', actions[0], ' ')
        
    app.accept('page_down', toggleAction)   
    app.run()
