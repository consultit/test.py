'''
Created on Jun 15, 2019

@author: consultit
'''

from abc import ABCMeta, abstractmethod
from enum import Enum


class LogLevel(Enum):
    '''
    Log Levels Enum.
    '''
    NONE = 0
    INFO = 1
    DEBUG = 2
    WARNING = 3
    ERROR = 4
    FUNCTIONAL_MESSAGE = 5
    FUNCTIONAL_ERROR = 6
    ALL = 7


class Logger(metaclass=ABCMeta):
    '''
    Abstract handler in chain of responsibility pattern.
    '''
    
    def __init__(self, levels):
        '''
        Initialize new logger

        Args:
            levels (list[str]): List of log levels.
        '''
        self.log_levels = []

        for level in levels:
            self.log_levels.append(level)
        
        self.next = None
            
    def set_next(self, next_logger):
        '''
        Set next responsible logger in the chain.

        Args:
            next_logger (Logger): Next responsible logger.

        Returns:
            Logger: Next responsible logger.
        '''
        self.next = next_logger
        return self.next
            
    def message(self, msg, severity):
        '''
        Message writer handler.

        Args:
            msg (str): Message string.
            severity (LogLevel): Severity of message as log level enum.
        '''
        if LogLevel.ALL in self.log_levels or severity in self.log_levels:
            self.write_message(msg)

        if self.next is not None:
            self.next.message(msg, severity)
    
    @abstractmethod
    def write_message(self, msg):
        '''
        Abstract method to write a message.

        Args:
            msg (str): Message string.

        Raises:
            NotImplementedError
        '''
        raise NotImplementedError

    
class ConsoleLogger(Logger):

    def write_message(self, msg):
        '''
        Overrides parent's abstract method to write to console.

        Args:
            msg (str): Message string.
        '''
        print('Writing to console:', msg)

        
class EmailLogger(Logger):
    '''
    Overrides parent's abstract method to send an email.

    Args:
        msg (str): Message string.
    '''

    def write_message(self, msg):
        print('Sending via email:', msg)


class FileLogger(Logger):
    '''
    Overrides parent's abstract method to write a file.

    Args:
        msg (str): Message string.
    '''

    def write_message(self, msg):
        print('Writing to log file:', msg)
