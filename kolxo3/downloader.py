'''
Created on Nov 27, 2018

@author: consultit
'''

from html.parser import HTMLParser
import argparse, textwrap
from urllib.request import urlopen
from urllib.parse import urlparse, unquote
from os.path import split, join
import sys
from urllib.error import HTTPError, URLError, ContentTooShortError
import os
from time import sleep

class Kolxo3Downloader(HTMLParser):
    '''Kolxo3 downloader'''

    def set_param(self, htmlFilePath, toDir, lapse):
        self.htmlFilePath = htmlFilePath
        self.toDir = toDir
        self.lapse = lapse
        
    def handle_startendtag(self, tag, attrs):
        if tag == 'a':
            for attr in attrs:
                if attr[0] == 'href':
                    url = attr[1]
                    self.download(url, self.lapse)
                
    def handle_starttag(self, tag, attrs):
        self.handle_startendtag(tag, attrs)

    def download(self, url, lapse):
        try:
            fileName = unquote(join(self.toDir, split(urlparse(url).path)[1]))
            if os.path.exists(fileName):
                print('downloading...', unquote(url), sep=' ')
                print('\t', url, ' already downloaded: skipping...', sep='')
                return                
            print('waiting...')
            sleep(lapse)
            print('downloading...', unquote(url), sep=' ')
            response = urlopen(url)
            data = response.read()
            print('\tsaving...', fileName, sep=' ')
            toFile = open(fileName, 'wb')
            toFile.write(data)
            toFile.close()
        except (URLError, HTTPError, ContentTooShortError) as err:
            print('\t', err, sep='')
        except KeyboardInterrupt:
            print('\tExiting...:')
            sys.exit(0)
        except:
            print('\tUnexpected error:', sys.exc_info()[0],sep=' ')

if __name__ == '__main__':
    # create the arguments' parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description=textwrap.dedent('''
    Dowloader of kolxo3 books.
      
    '''))
    # set up arguments
    parser.add_argument('htmlfile', metavar='FILE.html', type=str, help='the html file to parse')
    parser.add_argument('-d', '--dir', type=str, default='.', help='the directory where to save files into')
    parser.add_argument('-l', '--lapse', type=int, default=0, help='the time interval between downloads')
    # parse arguments
    args = parser.parse_args()
    
    # do actions
    htmlFilePath = args.htmlfile
    toDir = args.dir
    lapse = args.lapse
    # open file
    htmlFile = open(htmlFilePath, 'r')
    htmlFileStr = htmlFile.read()
    # create a downloader
    downloader = Kolxo3Downloader()
    downloader.set_param(htmlFilePath, toDir, lapse)
    # parse the file
    downloader.feed(htmlFileStr)
    