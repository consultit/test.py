'''
Created on Feb 3, 2020

@author: consultit
'''

from setuptools import setup, find_packages
import os


def get_version(version_tuple):
    # additional handling of a,b,rc tags, this can
    # be simpler depending on your versioning scheme
    if not isinstance(version_tuple[-1], int):
        return '.'.join(map(str, version_tuple[:-1])) + version_tuple[-1]
    return '.'.join(map(str, version_tuple))


# path to the packages __init__ module in project
# source tree
init = os.path.join(os.path.dirname(__file__), '__init__.py')
version_line = list(filter(lambda l: l.startswith('VERSION'), open(init)))[0]

# VERSION is a tuple so we need to eval 'version_line'.
# We could simply import it from the package but we
# cannot be sure that this package is importable before
# installation is done.
VERSION = get_version(eval(version_line.split('=')[-1]))


def read_md(file_path):
    with open(file_path, 'r') as f:
        return f.read()


README = os.path.join(os.path.dirname(__file__), 'README.md')

PACKAGES = ['test_py'] + ['test_py.' + pkg for pkg in find_packages(
        exclude=['*.unit_test', '*.test', '*.test.*'])]

setup(
    name='test_py',
    version=VERSION,
    author='consultit',
    author_email='consultit25@gmail.com',
    maintainer='consultit',
    maintainer_email='consultit25@gmail.com',
    url='https://gitlab.com/consultit/test.py.git',
    description='Python Test Programs',
    long_description=read_md(README),
    long_description_content_type='text/markdown',
    license='GPL3',
    package_dir={'test_py': ''},
    packages=PACKAGES,
    include_package_data=True,
    install_requires=['panda3d (>=1.10)'],
    provides=['test_py'],
)
