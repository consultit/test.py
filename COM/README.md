#Coding Style Guide

Coding styles used in this project. These styles follow the *PEP8* recommendations, more or less.

* Names of private data members must be prefixed by **_** (*underscore*).
* Names of private function members must be prefixed by **_** (*underscore*).
* Public instance variables must be accessible through *@property*(s).
* Public methods must check parameters' types (possibly using *utilities.CheckArgType*) and raise **GameException** on check failure.

#Data driven configuration rules

#####XML/JSON Parameters separator characters
*  **:** (*colon*) used to separate multiple elements on the same line (ex. *texture1:texture2:...*)
*  **@** (*at*) used to correspond concrete values to more abstract elements (ex. *evTypeN@evValueN*)
*  **$** (*dollar*) used to separate an optional element (ex. *evTypeN@evValueN]$[callbackName]*)
*  **,** (*comma*) used to separate generic options

*NOTE*: currently there is no escape mechanism, so you **cannot** use the above characters 
inside a parameter identifier.

#####XML/JSON parameters types
A parameter is *compound*  when it is composed by substrings separated by __:__ .<br>
A parameter is *single*  when can be specified only once.<br>
A parameter is *multiple*  when can be specified more than once.<br>
