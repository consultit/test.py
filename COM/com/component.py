'''
Created on May 12, 2019

@author: consultit
'''

from abc import ABCMeta, abstractmethod
from collections.abc import Callable
from ..tools.utilities import parse_compound_string, DEFAULT_CALLBACK_NAME, \
    ParameterTable, CheckArgType, replace_character, BaseType, EventTable, \
    EventTypeCallbackTable, EventTypeTable, NameCallbackPair, StringSequence, \
    Event, EventType, load_library_dictionary, COMLogger
from ..managers.gameDataInfo import GameDataInfo
from enum import IntEnum
from direct.showbase.ShowBase import ShowBase
from panda3d.core import GraphicsWindow


# Data types
class ComponentId(BaseType):
    '''
    Component instance identifier type.
    '''

    @staticmethod
    def valueType():
        return str

    def __add__(self, other):
        assert CheckArgType((other, ComponentId))
        return ComponentId(self._value + other.value)


class ComponentType(BaseType):
    '''
    Component type.
    '''

    @staticmethod
    def valueType():
        return str


class ComponentFamilyType(BaseType):
    '''
    Component family type.
    '''

    @staticmethod
    def valueType():
        return str


class Component(metaclass=ABCMeta):
    '''
    Base abstract class to provide a common interface for all Components.

    Components are organized into sets of similar behavior called families.
    Each Component has a type and belongs to a family and is derived from this
    base class. Any Object can have only one Component of each family type.
    Each Component can be updated directly at each game loop tick (or frame).
    Each Component can respond to Panda events (stimuli) by registering
    or unregistering callbacks for them with the global EventHandler.
    An Object can respond to events only through its owned Components.
    For any owned Component type, an Object type can optionally declare, the set
    of event types which that Component type has to respond to.
    This means that each event-type, declared for a Component type,
    is defined  on 'per ObjectTemplate (i.e. Object type) basis'.
    Panda events are specified as values of these event types on a
    'per Object basis'.
    By default, when an event-type is defined, any event specified
    as value of the event-type, is associated to a callback function
    with this signature:

        void <EVENTTYPE>_<COMPONENTTYPE>_<OBJECTTYPE>(const Event* event, void* data)

    This means that, by default, Components of the same type that
    belong to any Object of a given type, will respond to these events
    with the same callback function.
    Any event can be ignored on a 'per Object basis' by not specifying it in
    parameters. Moreover, both the event-type values and the callback function
    can be overridden by parameters on a 'per Object basis'.
    The callback functions are loaded at runtime.
    (See: GameDataInfo.CALLBACKS.
    If a callback function doesn't exist or if any error occurs, the default
    callback (referenced by the macro DEFAULT_CALLBACK_NAME) is used.
    To check if a (name,value) pair is a declared legitimate event-type call:

        ObjectTemplate::is_component_type_parameter_value()

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *event_types*    |multiple| - | each specified as 'evType1[:evType2:...:evTypeN]' into ObjectTemplate definition (as ComponentTemplates' parameters)
    | *events*         |multiple| - | each specified as 'evType1@evValue1[:evType2@evValue2:...:evTypeN@evValueN]$[callbackName]' into Object definition

    Note: in 'events' any of evValues or  callbackNamecan be empty
        (meaning we want the defaults value).
    Note: Inside the strings representing the above mentioned predefined
        callback function names, any '-' will be replaced by '_'.
    Note: parts inside of [] are optional.
    '''

    def __init__(self, tmpl):
        '''
        Constructor
        '''
        # The Template used to construct this Component (read only after
        # creation).
        self._tmpl = tmpl
        # Unique identifier for this Component (read only after creation).
        self._componentId = None
        # The Object this Component is a member of (read only after Component
        # creation).
        self._ownerObject = None
        # Free Component flag.
        self._freeComponent = False
        # Event management data types and variables (for managing library of
        # event callbacks). Table of events keyed by event types:
        # {str(eventType):str(event)}
        self._eventTable = EventTable()
        # Table of event types keyed by events: {str(event):str(eventType)}
        self._eventTypeTable = EventTypeTable()
        # Table of <name,callback> pairs keyed by event type names:
        # {str(eventType): (str(callbackName), callback)}
        self._callbackTable = EventTypeCallbackTable()
        # Helper flags.
        self._callbackLib = None
        self._callbacksLoaded = False
        self._callbacksRegistered = False
        #
        self._do_cleanup_event_tables()

    @abstractmethod
    def _reset(self):
        '''
        Resets all Component data members.
        '''

    @abstractmethod
    def _initialize(self):
        '''
        Allows a Component to be initialized.

        This can be done after creation but 'before' insertion into an Object.
        This method for all derived classes are called only by methods of the
        respective ComponentTemplate derived class.
        This method for all derived classes are called (indirectly) only
        by ObjectTemplateManager methods during its creation and before
        it is publicly available to other threads.
        '''

    @property
    def component_id(self):
        '''
        Gets the Component unique identifier.

        Return: The Component unique identifier.
        '''
        return self._componentId

    def _set_component_id(self, componentId):
        '''
        Sets the Component unique identifier.

        This method for all derived classes are called only by methods of the
        respective ComponentTemplate derived class.
        This method for all derived classes are called (indirectly) only
        by ObjectTemplateManager methods during its creation and before
        it is publicly available to other threads.
        Param componentId: The Component unique identifier.
        '''
        self._componentId = componentId

    def _add_to_object_setup(self):
        '''
        On addition to Object setup.

        Gives a Component the ability to do any required setup just
        'after' this Component has been added to an Object. Optional.
        This method for all derived classes are called only by
        ObjectTemplateManager methods during its creation and before it is
        publicly available to other threads.
        _add_to_object_setup() is the setup common to all Component's types,
        while _on_add_to_object_setup() is the specialized setup implemented by
        a derived Component type.
        '''
        # setup event tables (if any)
        self._do_setup_event_tables()
        # load event callbacks (if any)
        self._do_load_event_callbacks()
        # call _on_add_to_object_setup
        self._on_add_to_object_setup()
        # register event callbacks (if any)
        self._register_event_callbacks()

    @abstractmethod
    def _on_add_to_object_setup(self):
        '''
        On addition to object setup.
        '''

    def _remove_from_object_cleanup(self):
        '''
        On removal from Object cleanup.

        Gives a Component the ability to do any required cleanup just
        'before' this Component will be removed from an Object. Optional.
        _remove_from_object_cleanup() is the cleanup common to all Component's
        types, while _on_remove_from_object_cleanup() is the specialized cleanup
        implemented by a derived Component type.
        '''
        # unregister event callbacks (if any)
        self._unregister_event_callbacks()
        # call _on_remove_from_object_cleanup
        self._on_remove_from_object_cleanup()
        # unload event callbacks (if any)
        self._do_unload_event_callbacks()
        # cleanup event tables (if any)
        self._do_cleanup_event_tables()

    @abstractmethod
    def _on_remove_from_object_cleanup(self):
        '''
        On removal from object cleanup.
        '''

    def _add_to_scene_setup(self):
        '''
        On Object addition to scene setup.

        Gives a Component the ability to do any required setup just
        'after' the Object, this Component belongs to, has been added
        to the game scene and set up. Optional.
        This method for all derived classes are called only by
        ObjectTemplateManager methods during its creation and before it is
        publicly available to other threads.
        A possible request registration to any 'Game*Manager' for updating
        'must' be done in this method as the last thing.
        _add_to_scene_setup() is the setup common to all Component types, while
        _on_add_to_scene_setup() is the specialized setup implemented by a
        derived Component type.
        '''
        self._on_add_to_scene_setup()

    @abstractmethod
    def _on_add_to_scene_setup(self):
        '''
        On additional to scene setup.
        '''

    def _remove_from_scene_cleanup(self):
        '''
        On Object removal from scene cleanup.

        Gives a Component the ability to do any required cleanup just
        'before' this Component will be removed from the game scene. Optional.
        A possible request for cancellation from any 'Game*Manager' for updating
        'must' be done in this method as the first thing (before locking the
        mutex). _remove_from_scene_cleanup() is the cleanup common to all
        Component's types, while _on_remove_from_scene_cleanup() is the
        specialized cleanup implemented by a derived Component type.
        '''
        self._on_remove_from_scene_cleanup()

    @abstractmethod
    def _on_remove_from_scene_cleanup(self):
        '''
        On remove from scene cleanup.
        '''

    @property
    def component_type(self):
        '''
        Gets the type of this Component.

        Return: The id of this Component.
        '''
        return self._tmpl.component_type

    @property
    def component_family_type(self):
        '''
        Gets the family type of this Component.

        Return: The family type of this Component.
        '''
        return self._tmpl.component_family_type

    # # TODO: remove !? ##
    class Result(IntEnum):
        '''
        The result type a Component method can return.

        To signal the status of result of the operation or of the Component.
        Derived Component can derive from this to add other result status,
        by starting the enum from COMPONENT_RESULT_END+1.
        '''
        OK = 0
        ERROR = 1
        DESTROYING = 2

    def update(self, data):
        '''
        Updates the state of the Component.

        Param data: Generic data.
        '''
        pass

    @property
    def owner_object(self):
        '''
        Gets the owner Object.

        Return: The owner Object.
        '''
        return self._ownerObject

    def _set_owner_object(self, ownerObject):
        '''
        Sets the owner Object.

        This method for all derived classes is called only by
        ObjectTamplateManager methods.
        Param owner_object: The owner Object.
        '''
        self._ownerObject = ownerObject

    def get_event_type(self, event):
        '''
        Return the type of an event.

        Param event: The event.
        Return: The type of the event.
        '''
        assert CheckArgType((event, Event))
        result = Event()
        if event in self._eventTypeTable:
            result = self._eventTypeTable[event]
        return result

    def set_free_flag(self, freeComponent):
        '''
        Set the Component as free.

        Param freeComponent: The free flag.
        '''
        assert CheckArgType((freeComponent, bool))
        self._freeComponent = freeComponent

    def _register_event_callbacks(self):
        '''
        Helper functions to register/unregister events' callbacks.

        This interface can be called by the derived Components to setup
        and register/unregister callbacks for events this Component
        should respond to.
        Functions _register_event_callbacks/_unregister_event_callbacks can
        be used, after the Component has been added to Object, to
        add/remove callbacks to/from the global EventHandler.
        All these functions can be called multiple time in a safe way.
        Note: Because Component's events are shared by all Object of the same
        type, and because their types are stored into the Object Template, these
        functions should be called only after the Component's owner Object
        has been set, for example into _add_to_object_setup Component method.
        '''
        # if callbacks already registered do nothing
        if self._callbacksRegistered or (not self._callbacksLoaded):
            return
        # register every handler
        # {str(eventType): (str(callbackName), callback)}.items() == (evType, nameClbk)
        for evType, nameClbk in self._callbackTable.items():
            # event = mEventTable[items[0]], items[1][1]==handler
            # nameClbk.callback is always defined: if not specified, it falls
            # back to the 'default callback'.
            eventValue = self._eventTable[evType].value
            self._tmpl.panda_framework.accept(eventValue, nameClbk.callback,
                                              extraArgs=[eventValue, self])
        # handlers registered
        self._callbacksRegistered = True

    def _unregister_event_callbacks(self):
        # if callbacks not already registered do nothing
        if not self._callbacksRegistered:
            return
        # Unregister the handlers
        # {str(eventType): (str(callbackName), callback)}.items() = (evType, nameClbk)
        for evType, _ in self._callbackTable.items():
            # event = mEventTable[items[0]], items[1][1]==handler
            self._tmpl.panda_framework.ignore(self._eventTable[evType].value)
        # handlers unregistered
        self._callbacksRegistered = False

    def _do_setup_event_tables(self):
        '''
        Helper functions to setup/cleanup events' tables and to load/unload events' callbacks.
        '''
        # get events specifications on a per Object basis
        eventList = self._tmpl.parameter_list('events')
        for itemEvent in eventList:
            # any 'events' string is a 'compound' one, i.e. could have the form:
            # 'evType1@evValue1:evType2@evValue2:...:evTypeN@evValueN$callbackName'
            # parse string as a ((evType,evValue)s,callback) pair
            typeValuesCallback = parse_compound_string(itemEvent, '$')
            # check if there is (at least) a pair
            if len(typeValuesCallback) >= 2:
                # set the callback name as the second element, that could be
                # empty string
                callbackName = typeValuesCallback[1]
                # parse first element as (evType,evValue) pair list
                typeValues = parse_compound_string(typeValuesCallback[0], ':')
                for itemTypeValue in typeValues:
                    # get event-type and event value
                    typeValue = parse_compound_string(itemTypeValue, '@')
                    # check in order:
                    # - if there is (at least) a pair and
                    # - if there is non empty 'event value' (== typeValue[1])
                    if (len(typeValue) >= 2) and typeValue[1]:
                        # check:
                        # if (component is free and
                        #     there is non empty 'event-type' (== typeValue[0]))
                        # or
                        # (if component is not free and
                        #     there is a valid 'event-type' (== typeValue[0]))
                        if (
                            (self._freeComponent and typeValue[0])
                            or
                            ((not self._freeComponent) and (self._ownerObject.object_tmpl.is_component_type_parameter_value(
                                'event_types', typeValue[0], self.component_type)))
                        ):

                            # insert event keyed by eventType
                            self._eventTable[EventType(
                                typeValue[0])] = Event(typeValue[1])
                            # insert eventType keyed by event
                            self._eventTypeTable[Event(
                                typeValue[1])] = EventType(typeValue[0])
                            # insert the <callbackName,None> pair keyed by
                            # eventType
                            self._callbackTable[EventType(typeValue[0])] = NameCallbackPair(
                                callbackName, None)

    def _do_cleanup_event_tables(self):
        self._eventTable.clear()
        self._eventTypeTable.clear()
        self._callbackTable.clear()

    def _do_load_event_callbacks(self):
        # if callbacks already loaded do nothing
        if self._callbacksLoaded:
            return
        # load the event callbacks library
        callbackLibName = self._tmpl._gameDataInfo[GameDataInfo.CALLBACKS]
        self._callbackLib = load_library_dictionary(callbackLibName,
                                                    'callbacks', globals())
        if not self._callbackLib:
            return
        # check the default callback
        pDefaultCallback = self._callbackLib.get(DEFAULT_CALLBACK_NAME, None)
        if ((not pDefaultCallback) or (not isinstance(pDefaultCallback, Callable))):
            COMLogger.debug('Cannot find default callback ' +
                            DEFAULT_CALLBACK_NAME)
            # Close the event callbacks library
            self._callbackLib = None
            return
        # load every callback, as specified in callbacks' name table
        for evType, nameClbk in self._callbackTable.items():
            if nameClbk.name:
                # the callback name has been specified as parameter
                callbackName = nameClbk.name
            else:
                # the callback name has not been specified as parameter:
                # try the name: <EVENTTYPE>_<COMPONENTTYPE>_<OBJECTTYPE>
                callbackNameTmp = (str(evType) + '_'
                                   + str(self.component_type) + '_'
                                   + str(self.owner_object.object_tmpl.object_type))
                # replace hyphens
                callbackName = replace_character(callbackNameTmp, '-', '_')
            # reset errors
            # load the callback
            pCallback = self._callbackLib.get(callbackName, None)
            if (not pCallback) or (not isinstance(pCallback, Callable)):
                COMLogger.debug('Cannot load callback ' + callbackName)
                # set default callback for this event
                newPair = NameCallbackPair(self._callbackTable[evType].name,
                                           pDefaultCallback)
                self._callbackTable[evType] = newPair
                # continue with the next event
                continue
            # set callback for this event
            newPair = NameCallbackPair(self._callbackTable[evType].name,
                                       pCallback)
            self._callbackTable[evType] = newPair
        # callbacks loaded
        self._callbacksLoaded = True

    def _do_unload_event_callbacks(self):
        # if callbacks not loaded do nothing
        if not self._callbacksLoaded:
            return
        # Close the event callbacks library
        if not self._callbackLib:
            COMLogger.debug('Error closing library: ' + str(
                self._tmpl._gameDataInfo[GameDataInfo.CALLBACKS]))
        else:
            self._callbackLib = None
        # callbacks unloaded
        self._callbacksLoaded = False

    def __repr__(self):
        return self._componentId.__repr__() + ' (' + self.__class__.__name__ + ')'

    def __str__(self):
        return self._componentId.__str__() + ' (' + self.__class__.__name__ + ')'


class ComponentTemplate(metaclass=ABCMeta):
    '''
    Abstract base class modeling templates used to create Components.
    '''

    def __init__(self, panda_framework, window_framework, gameDataInfo):
        '''
        Constructor
        '''
        assert CheckArgType((panda_framework, ShowBase),
                            (window_framework, GraphicsWindow))
        # Parameter table.
        self._parameterTable = ParameterTable()
        # The PandaFramework .
        self._pandaFramework = panda_framework
        # The WindowFramework .
        self._windowFramework = window_framework
        # gameDataInfo = GameManager.get_global_ptr().data_info[GameDataInfo.CALLBACKS]
        self._gameDataInfo = gameDataInfo
        #
        self.set_parameters_defaults()

    @abstractmethod
    def _make_component(self, compId):
        '''
        Creates the Component of a given type and family type.

        Param compId: The Component identifier.
        Return: The Component just created, None if Component cannot be created.
        '''

    @property
    @abstractmethod
    def component_type(self):
        '''
        Gets the type of the Component created.

        Return: The type of the Component created.
        '''

    @property
    @abstractmethod
    def component_family_type(self):
        '''
        Gets the family type of the Component created.

        Return: The family type of the Component created.
        '''

    @abstractmethod
    def set_parameters_defaults(self):
        '''
        Sets the Component parameters to their default values.

        For the Component this template is designed to create,
        this function sets the parameters to their default values.
        '''

    def set_parameters(self, parameterTable):
        '''
        Sets the Component parameters to custom values.

        For the Component this template is designed to create,
        this function sets the parameters to custom values.
        These parameters overwrite (and/or are added to) the parameters
        defaults set by set_parameters_defaults.
        Param parameterTable: The table of (parameter,valueList).
        '''
        assert CheckArgType((parameterTable, ParameterTable))
        # insert new parameters and update existing ones into
        # _parameterTable from parameterTable's parameters
        for key in parameterTable:
            self._parameterTable[key] = StringSequence(parameterTable[key])

    def parameter(self, paramName):
        '''
        Gets the parameter single value associated to the Component.

        Param paramName: The name of the parameter.
        Return: The value of the parameter, empty string if none exists.
        '''
        assert CheckArgType((paramName, str))
        valueList = self._parameterTable.get(paramName, None)
        return valueList[0] if valueList else str()

    def parameter_list(self, paramName):
        '''
        Gets the parameter multiple values associated to the Component.

        Param paramName: The name of the parameter.
        Return: The value list of the parameter, empty list if none exists.
        '''
        assert CheckArgType((paramName, str))
        return self._parameterTable.get(paramName, StringSequence())

    @property
    def parameter_table(self):
        '''
        Gets a reference to the entire parameter table.

        Return: The parameter table.
        '''
        return self._parameterTable

    def parameter_table_copy(self):
        '''
        Returns a (deep) copy of the entire parameter table.

        Return: A copy of the parameter table.
        '''
        paramTabelDeepCopy = ParameterTable()
        for param, stringSeq in self._parameterTable.items():
            paramTabelDeepCopy[param] = StringSequence(stringSeq)
        return paramTabelDeepCopy

    @property
    def panda_framework(self):
        '''
        Gets/sets the PandaFramework.

        Return: A reference to the PandaFramework.
        '''
        return self._pandaFramework

    @property
    def window_framework(self):
        '''
        Gets/sets the WindowFramework.

        Return: A reference to the WindowFramework.
        '''
        return self._windowFramework

    def __repr__(self):
        return self.component_type.__repr__() + ' Template'

    def __str__(self):
        return self.component_type.__str__() + ' Template'
