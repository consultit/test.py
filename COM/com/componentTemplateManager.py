'''
Created on Jul 31, 2019

@author: consultit
'''

from ..tools.singleton import Singleton
from ..tools.utilities import BaseTable, IdType, CheckArgType
from .component import ComponentType, ComponentTemplate, ComponentId


class _ComponentTemplateTable(BaseTable):
    '''
    Table of Component templates indexed by Component type.

    dict{ComponentType:ComponentTemplate,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(ComponentType, ComponentTemplate, mapping)


class ComponentTemplateManager(metaclass=Singleton):
    '''
    Singleton manager that handles all the ComponentTemplates.
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self._componentTemplates = _ComponentTemplateTable()
        # The unique id for created components.
        self._id = IdType()

    def _do_create_component(self, compType, freeComponent=False):
        '''
        Creates a Component given its type.

        Param compType: The Component type.
        Param freeComponent: If this is a free Component or if it belongs
        to an Object Template, default is false.
        Return: The just created Component, or None on failure (for any reason).
        '''
        if compType not in self._componentTemplates:
            return None
        # new unique id
        newCompId = ComponentId(compType.value) + \
            ComponentId(self._get_id().value)
        # create component
        newComp = self._componentTemplates[compType]._make_component(newCompId)
        if not newComp:
            return None
        newComp.set_free_flag(freeComponent)
        return newComp

    def add_component_template(self, componentTmpl):
        '''
        Adds a ComponentTemplate for a the Component type it can create.

        It will add the Component Template to the internal table and if a
        Template for that Component type already existed it'll be replaced
        by this new Template (and its ownership released by the manager).
        Param componentTmpl: The Component Template to add.
        Return: None if there wasn't a Template for that Component, otherwise
        the previous Template.
        '''
        assert CheckArgType((componentTmpl, ComponentTemplate))
        compType = componentTmpl.component_type
        # get (and remove) a previous component template for that component,
        # if any
        previousCompTmpl = self._componentTemplates.pop(compType, None)
        # insert the new component template (possibly replacing a previous
        # component template)
        self._componentTemplates[compType] = componentTmpl
        return previousCompTmpl

    def remove_component_template(self, compType):
        '''
        Removes the ComponentTemplate given the Component type it can create.

        Param compType: The Component type.
        Return: True if the Component Template existed, False otherwise.
        '''
        assert CheckArgType((compType, ComponentType))
        # Removing component template for type compType
        if self._componentTemplates.pop(compType, None) == None:
            return False
        return True

    def get_component_template(self, compType):
        '''
        Gets the ComponentTemplate given the Component type it can create.

        Param compType: The Component type.
        Return: The Component Template, None if none exists.
        '''
        assert CheckArgType((compType, ComponentType))
        return self._componentTemplates.get(compType, None)

    def reset_component_template_params(self, compType):
        '''
        Resets the parameters of the given Component type, to their default parameters.

        Component configuration parameters are handled by the corresponding
        ComponentTemplate.
        Param compType: The Component type.
        '''
        assert CheckArgType((compType, ComponentType))
        compTmpl = self._componentTemplates.get(compType, None)
        if compTmpl:
            compTmpl.set_parameters_defaults()

    def reset_component_templates_params(self):
        '''
        Resets all ComponentTemplates to their default parameters.
        '''
        for compTmpl in self._componentTemplates.values():
            compTmpl.set_parameters_defaults()

    def _get_id(self):
        '''
        Returns an unique id for created Components.

        Return: An unique id.
        '''
        return self._id.incr()
