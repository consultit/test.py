'''
Created on Jul 31, 2019

@author: consultit
'''

from ..tools.singleton import Singleton
from ..tools.utilities import BaseTable, IdType, CheckArgType, ParameterTable
from .object import ObjectId, ObjectType, ObjectTemplate, Object, \
    ComponentParameterTableMap
from .componentTemplateManager import ComponentTemplateManager
from .component import ComponentType


class _ObjectTemplateTable(BaseTable):
    '''
    ObjectTemplate table typedef.

    dict{ObjectType:ObjectTemplate,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(ObjectType, ObjectTemplate, mapping)


class _ObjectTable(BaseTable):
    '''
    Object table typedef.

    dict{ObjectId:Object,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(ObjectId, Object, mapping)


class ObjectTemplateManager(metaclass=Singleton):
    '''
    Singleton manager that handles all the ObjectTemplates.

    This manager has the additional responsibility to create game Objects
    and to maintain a table of (references to) all created Objects, indexed by
    ObjectId.\n
    Thread-safe during utilization.
    '''

    def __init__(self):
        '''
        Constructor.
        '''
        # Table of ObjectTemplates indexed by their name.
        self._objectTemplates = _ObjectTemplateTable()
        # The table of created game Objects.
        self._createdObjects = _ObjectTable()
        # The unique identifier for created Objects.
        self._id = IdType()

    def add_object_template(self, objectTmpl):
        '''
        Adds an ObjectTemplate for a given Object type it can create.

        It will add the ObjectTemplate to the internal table and, if a
        template for that Object type (i.e. having the same name) already
        existed, it'll be replaced by this new template (and its ownership
        released by the manager).
        Param objectTmpl: The ObjectTemplate to add.
        Return: None if there wasn't a template for that Object, otherwise
        the previous template.
        '''
        assert CheckArgType((objectTmpl, ObjectTemplate))
        objType = objectTmpl.object_type
        # get (and remove) a previous object template for that object, if any
        previousObjTmpl = self._objectTemplates.pop(objType, None)
        # insert the new object template (possibly replacing a previous object
        # template)
        self._objectTemplates[objType] = objectTmpl
        return previousObjTmpl

    def remove_object_template(self, objType):
        '''
        Removes the ObjectTemplate given the Object type it can create.

        Param objType: The Object type.
        Return: True if the ObjectTemplate existed, false otherwise.
        '''
        assert CheckArgType((objType, ObjectType))
        # Removing object template for type objType
        if self._objectTemplates.pop(objType, None) == None:
            return False
        return True

    def get_object_template(self, objType):
        '''
        Gets the ObjectTemplate given the Object type it can create.

        Param objType: The Object type.
        Return: The ObjectTemplate.
        '''
        assert CheckArgType((objType, ObjectType))
        return self._objectTemplates.get(objType, None)

    def create_object(self, objType, objId=ObjectId(),
                      objectParams=ParameterTable(),
                      compParamTableMap=ComponentParameterTableMap(),
                      storeParams=False, owner=None):
        '''
        Creates a game Object.

        The Object creation can be performed 'ONLY' by calling this function.\n
        The type is needed to select the correct ObjectTemplate.\n
        Param objType: The Object type.
        Param objId: The Object identifier.
        Param objectParams: Object parameter table used to setup the
            Object in the game scene.
        Param compParamTableMap: Map of Components' parameter tables, indexed by
            Component type, used to initialize the owned Components of the
            Object.
        Param storeParams: Whether to store Object and Components' parameters
            into the created Object and before it will be added to the game
            scene (and before being initialized).
        Param owner: The owner Object
        Return: The just created Object, or None if the Object cannot be created.
        '''
        assert CheckArgType((objType, ObjectType),
                            (objId, ObjectId),
                            # (objectParams, ParameterTable),
                            (compParamTableMap, ComponentParameterTableMap),
                            (storeParams, bool),
                            # (owner, NodePath)
                            )
        # check if it is an already created Object
        oldObject = self.get_created_object(objId)
        if oldObject:
            return oldObject
        # retrieve the ObjectTemplate
        objectTmpl = self._objectTemplates.get(objType, None)
        if not objectTmpl:
            return None
        # create the new Object
        if objId == ObjectId():
            newId = ObjectId(objType.value) + ObjectId(self._get_id().value)
        else:
            newId = objId
        # create the new Object
        newObj = Object(newId, objectTmpl)
        # set the owner if any
        newObj.owner = owner
        # get the ComponentTemplate ordered list
        compTmplList = objectTmpl.component_templates
        # iterate in order over the ordered list and add Components in the same
        # order
        for compTmpl in compTmplList:
            # use ComponentTemplateManager to create Component
            compType = compTmpl.component_type
            # we have to initialize parameters of this Component
            # set Component' parameters to their default values
            ComponentTemplateManager.get_global_ptr().reset_component_template_params(compType)
            # set parameters for this Component...
            paramTable = compParamTableMap.get(compType, None)
            if paramTable:
                # ...if not empty
                compTmpl.set_parameters(paramTable)
            # create the Component
            newComp = ComponentTemplateManager.get_global_ptr()._do_create_component(compType)
            # return None on error
            if not newComp:
                return None
            # add the new Component into the Object if there
            # isn't another one with the same family type
            if not newObj._do_add_component(newComp):
                continue
            # set the Component owner Object
            newComp._set_owner_object(newObj)
            # give a chance to Component to setup itself when being added to
            # Object.
            newComp._add_to_object_setup()
        # we have to initialize parameters of this Object template
        # initialize parameters' Object template to defaults
        objectTmpl._do_set_parameters_defaults()
        # set parameters for this Object template...
        if objectParams:
            # ...if not empty
            objectTmpl.set_parameters(objectParams)
        # store Object and Components' parameters into
        # the created Object and before it will be added to the
        # scene (and initialized).
        if storeParams or (objectTmpl.parameter('store_params') == 'true'):
            # need to make deep copy of both tables
            objectParamsCopy = objectTmpl.parameter_table_copy()
            compParamTableMapCopy = ComponentParameterTableMap()
            for compTmpl in compTmplList:
                compParamTableMapCopy[compTmpl.component_type] = compTmpl.parameter_table_copy(
                )
            newObj._do_store_parameters(
                objectParamsCopy, compParamTableMapCopy)
        # give a chance to Object to setup itself when being added to scene.
        newObj._on_add_to_scene_setup()
        # give a chance to components to setup themselves when being
        # added to scene in order of insertion. Thread safe.
        for familyTypeComp in newObj._do_get_components():
            familyTypeComp.component._add_to_scene_setup()
        # Now the Object is completely existent so insert
        # it in the table of created objects.
        self._createdObjects[newId] = newObj
        return newObj

    def add_component_to_object(self, objId, compType,
                                componentParams=ParameterTable()):
        '''
        Adds/replaces a Component of the given type to an existing Object with
        the given Object identifier.

        Note: you can add any Component of any given type, even if the
            ObjectTemplate doesn't contain it.
        Note: the new Component will replace a pre-existing Component of the 
            same family type, if any.
        Note: event types of this new Component need not to be the same as the
            declared ones in ObjectTemplate, if any.\n
        Param objId: The given Object identifier.
        Param compType: The given Component type.
        Param componentParams: Parameters used to initialize the Component.
        Return: True if successfully added, false otherwise.
        '''
        assert CheckArgType((objId, ObjectId),
                            (compType, ComponentType),
                            (componentParams, ParameterTable))
        # check if it is effectively an already created Object
        objectT = self.get_created_object(objId)
        if not objectT:
            return False
        # get the ComponentTemplate
        compTmpl = ComponentTemplateManager.get_global_ptr().get_component_template(compType)
        # we have to initialize parameters of this Component
        # set Component' parameters to their default values
        compTmpl.set_parameters_defaults()
        # set parameters for this Component...
        if componentParams:
            # ...if not empty
            compTmpl.set_parameters(componentParams)
        # create the new free Component
        newComp = ComponentTemplateManager.get_global_ptr(
        )._do_create_component(compType, True)
        if not newComp:
            return False
        compFamilyType = newComp.component_family_type
        # check if there are an old Component
        oldComp = objectT.get_component(compFamilyType)
        if oldComp:
            # on removal from scene old Component cleanup
            oldComp._remove_from_scene_cleanup()
            # on removal from Object old Component cleanup
            oldComp._remove_from_object_cleanup()
            # set the old Component owner to None
            oldComp._set_owner_object(None)
            # remove old Component from Object
            objectT._do_remove_component(oldComp)
        # add the new Component to the Object
        objectT._do_add_component(newComp)
        # set the new Component owner
        newComp._set_owner_object(objectT)
        # on addition to Object new Component setup
        newComp._add_to_object_setup()
        # on addition to scene new Component setup
        newComp._add_to_scene_setup()
        #
        return True

    def remove_component_from_object(self, objId, compType):
        '''
        Removes a Component, if any, with the given type, from an existing
        Object with the given identifier.

        Note: the Component is not removed if the ObjectTemplate contains
            one with the same family type.\n
        Param objId: The given Object identifier.
        Param compType: The given Component type.
        Return: True if successfully removed, false otherwise.
        '''
        assert CheckArgType((objId, ObjectId),
                            (compType, ComponentType))
        # check if it is effectively an already created Object
        objectT = self.get_created_object(objId)
        if not objectT:
            return False
        # get the Component by type
        oldComp = objectT.get_component(compType)
        # return false if there's not such a Component
        if not oldComp:
            return False
        # return false if this Object is designed to have a Component of this
        # family type
        compFamilyType = oldComp.component_family_type
        if objectT.object_tmpl.get_component_template(compFamilyType):
            return False
        # remove actually the Component:
        # on removal from scene old Component cleanup
        oldComp._remove_from_scene_cleanup()
        # on removal from object old Component cleanup
        oldComp._remove_from_object_cleanup()
        # set the old Component owner to None
        oldComp._set_owner_object(None)
        # remove old Component from Object
        objectT._do_remove_component(oldComp)
        #
        return True

    def get_created_object(self, objId):
        '''
        Gets a created Object by its identifier.

        Return: A reference to the created Object, None if it doesn't exist or
            on error.
        '''
        assert CheckArgType((objId, ObjectId))
        return self._createdObjects.get(objId, None)

    @property
    def created_objects(self):
        '''
        Gets a list of all created Objects.

        Return: A list of references to the created Objects.
        '''
        return [idObj[1] for idObj in self._createdObjects.items()]

    def destroy_object(self, objId):
        '''
        Destroys a created Object by its identifier.

        Return: True if successful, False otherwise.
        '''
        assert CheckArgType((objId, ObjectId))
        objectT = self._createdObjects.get(objId, None)
        if not objectT:
            return False
        # Removing object with objId
        # get a copy of Object components because original will be modified
        # and to avoid dropping of reference count
        objectComponents = objectT._do_get_components()
        # on removal from scene components cleanup in reverse order of
        # insertion
        for famTypeComp in reversed(objectComponents):
            famTypeComp.component._remove_from_scene_cleanup()
        # on removal from scene Object cleanup
        objectT._on_remove_from_scene_cleanup()
        # remove components in reverse order of insertion
        for famTypeComp in reversed(objectComponents):
            # on removal from Object Component cleanup
            famTypeComp.component._remove_from_object_cleanup()
            # set the old Component owner to None
            famTypeComp.component._set_owner_object(None)
            # remove old Component from Object
            objectT._do_remove_component(famTypeComp.component)
        # on Object removal cleanup
        objectT._on_remove_object_cleanup()
        # remove Object from the table of created objects.
        self._createdObjects.pop(objId)
        return True

    def destroy_all_objects(self, owned=True):
        '''
        Destroys all created Objects.

        Param owned: if True destroys all created Objects, if False owned
            Objects are not destroyed.
        '''
        # get not owned objects
        notOwnedObjectIds = []
        for objId in self._createdObjects:
            if not self._createdObjects[objId].owner:
                # Object has no owner: insert id
                notOwnedObjectIds.append(objId)
        # remove not owned objects
        for objId in notOwnedObjectIds:
            self.destroy_object(objId)

        if owned:
            # remove any remaining (zombie) objects: should be no one
            for objId in list(self._createdObjects.keys()):
                self.destroy_object(objId)

    def _get_id(self):
        '''     
        Return an unique id for created Objects.

        Return: The unique id.
        '''
        return self._id.incr()
