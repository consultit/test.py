'''
Created on May 26, 2019

@author: consultit
'''

from collections import namedtuple
from panda3d.core import NodePath, LPoint3f, LVecBase3f, GraphicsWindow
from ..tools.utilities import ParameterTable, BaseSequence, StringSequence, \
    ParameterNameValue, strtof, BaseTable, CheckArgType, BaseType, \
    parse_compound_string, load_library_dictionary, COMLogger
from .component import ComponentType, ComponentFamilyType, ComponentTemplate
from ..managers.gameDataInfo import GameDataInfo
from . import objectTemplateManager
from direct.showbase.ShowBase import ShowBase
from COM.components.scene.nodePathWrapper import NodePathWrapperTemplate


class ObjectId(BaseType):
    '''
    Object instance identifier type.
    '''

    @staticmethod
    def valueType():
        return str

    def __add__(self, other):
        assert CheckArgType((other, ObjectId))
        return ObjectId(self._value + other.value)


class ComponentParameterTableMap(BaseTable):
    '''
    dict{ComponentType:ParameterTable,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(ComponentType, ParameterTable, mapping)


_FamilyTypeComponentPair = namedtuple('_FamilyTypeComponentPair',
                                      ['familyType', 'component'])


class _FamilyTypeComponentList(BaseSequence):
    '''
    _FamilyTypeComponentPair sequence.
    '''

    def __init__(self, sequence=None):
        super().__init__(_FamilyTypeComponentPair, sequence)

    def __str__(self):
        res = '['
        for pair in self:
            res += '(' + str(pair.familyType) + \
                ', ' + str(pair.component) + '),'
        res = res.rstrip(',')
        return res + ']'


class Object(object):
    '''
    The Object is the basic entity that can exist in the game world.

    A Object should 'compose', as far as possible, the different features
    (graphics, sound, ai, physics, animation, etc...), supplied by Panda3d
    game engine. When a feature is missing or considered of low quality,
    other external libraries may be used.
    Each Object has an embedded NodePath.
    An Object is an aggregation of Components: a “has a,” or whole/part,
    relationship where neither class owns the other.
    An Object can have any number of Components of different types, but only
    one can belong to any family type: two Components with different types
    belonging to the same family type, cannot coexist into an Object.
    Objects can be initialized after they are added to the game scene, through
    an initialization function, whose name can be specified as parameter
    or by a default which is the string '<OBJECTID>_initialization',
    loaded at runtime.
    (See: GameDataInfo.INITIALIZATIONS).


    **XML Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *parent*          |single| -           | values: objectId,none
    | *store_params*    |single| *false*     | -
    | *is_steady*       |single| *false*     | -
    | *pos*             |single| 0.0,0.0,0.0 | -
    | *rot*             |single| 0.0,0.0,0.0 | -
    | *init_func*       |single| -           | -

    Note: The Object Model is based on the article 'Game Object
    Component System' by Chris Stoy in 'Game Programming Gems 6' book.
    Note: parts inside of [] are optional.
    '''

    def __init__(self, objId, tmpl):
        '''
        Constructor
        '''
        # Object and Components' parameters tables.
        self._objTmplParams = ParameterTable()
        self._compTmplParams = ComponentParameterTableMap()
        # The ObjectTemplate used to construct this Object.
        self._tmpl = tmpl
        # Unique identifier for this Object (read only after creation).
        self._objectId = objId
        # The owner of this Object (responsible for its lifetime) or None.
        self._owner = None
        # The component list: actually a _FamilyTypeComponentPair sequence
        self._components = _FamilyTypeComponentList()
        # Steady flag: if this Object doesn't move in the game world.
        # Various Components can set or get this value to implement
        # some optimization.
        self._isSteady = False
        # Handle of the library of initialization functions.
        self._initializationLib = None
        # Helper flag.
        self._initializationsLoaded = False
        # Initialization function name (optional).
        self._inititializationFuncName = ''
        # Initialization function.
        self._initializationFunction = None
        # The underlying NodePath (read-only after creation & before
        # destruction). By default set node path to not empty
        self._nodePath = NodePath(str(self._objectId))
        self._do_reset()

    def _do_reset(self):
        '''
        Resets all Object data members.
        '''
        self._components.clear()
        self._isSteady = False

    def _do_add_component(self, component):
        '''
        Adds a Component to this Object.

        A Component is added only if there is not another with
        its same family type.
        Param component: The new Component to add.
        Return: True if successfully removed, False otherwise.
        '''
        compFamilyType = component.component_family_type
        for familyComponentPair in self._components:
            if familyComponentPair.familyType == compFamilyType:
                return False
        # insert the new Component into the list at the back end
        self._components.append(
            _FamilyTypeComponentPair(compFamilyType, component))
        #
        return True

    def _do_remove_component(self, component):
        '''
        Removes a Component from this Object.

        Component is removed only if it's there.
        Param component: The Component to remove.
        Return: True if successfully removed, false otherwise.
        '''
        compType = component.component_type
        for idx, familyComponentPair in enumerate(self._components):
            if ((familyComponentPair.component.component_type == compType) and
                    (familyComponentPair.component == component)):
                # erase Component
                self._components.pop(idx)
                return True
        return False

    def _do_get_components(self):
        '''
        Gets a reference to the Component list.

        The list is ordered by insertion: i.e Component's
        position in the list reflects the Component's
        (relative) insertion order.
        Return: A reference to the Component list.
        '''
        return self._components

    def _on_remove_object_cleanup(self):
        '''
        On remove Object cleanup.

        Gives the Object the ability to perform cleanup and any
        required finalization before being definitively destroyed.
        Called only by ObjectTemplateManager.destroy_object().
        '''
        #
        self._do_reset()
        # unload initialization functions
        self._do_unload_initialization_functions()

    def _on_add_to_scene_setup(self):
        '''
        On addition to scene setup.

        Gives the Object the ability to perform any required
        setup just 'after' its addition to the game scene.
        Called only by ObjectTemplateManager.create_object().
        Note: this method is called exclusively by the Object creation thread,
        i.e. before it will be publicly accessible, so no other thread can
        access the Object during its execution, then it doesn't need to hold the
        mutex.
        '''
        # Called only by ObjectTemplateManager.create_object(). Thread safe.
        # Is static (by default false)
        self._isSteady = True if self._tmpl.parameter(
            'is_steady') == 'true' else False
        # set up into scene only if NodePath is not empty
        if not self._nodePath.is_empty():
            # Parent (by default none)
            parentId = ObjectId(self._tmpl.parameter('parent'))
            # find parent into the created objects
            parentObj = objectTemplateManager.ObjectTemplateManager.get_global_ptr(
            ).get_created_object(parentId)
            if parentObj:
                # reparent to parent
                self._nodePath.reparent_to(parentObj.node_path)
            elif parentId.value == 'none':
                # reparent to empty node
                self._nodePath.reparent_to(NodePath())
            #
            # position
            param = self._tmpl.parameter('pos')
            paramValuesStr = parse_compound_string(param, ',')
            valueNum = len(paramValuesStr)
            if valueNum < 3:
                paramValuesStr += ['0.0'] * (3 - valueNum)
            pos = LPoint3f()
            for idx in range(3):
                pos[idx] = strtof(paramValuesStr[idx])
            self._nodePath.set_pos(pos)
            # rotation
            param = self._tmpl.parameter('rot')
            paramValuesStr = parse_compound_string(param, ',')
            valueNum = len(paramValuesStr)
            if valueNum < 3:
                paramValuesStr += ['0.0'] * (3 - valueNum)
            rot = LVecBase3f()
            for idx in range(3):
                rot[idx] = strtof(paramValuesStr[idx])
            self._nodePath.set_hpr(rot)
        # set initialization function (if any)
        # get initialization function name
        self._inititializationFuncName = self._tmpl.parameter('init_func')
        # load initialization functions library.
        self._do_load_initialization_functions()
        # execute the initialization function (if any)
        if self._initializationsLoaded:
            # load initialization function (if any): <OBJECTID>_initialization
            functionName = self._inititializationFuncName if self._inititializationFuncName else str(
                self._objectId) + '_initialization'
            self._initializationFunction = self._initializationLib.get(
                functionName, None)
            if not self._initializationFunction:
                COMLogger.debug('No initialization function ' + functionName)

    def _on_remove_from_scene_cleanup(self):
        '''
        On remove from scene cleanup.

        Gives the Object the ability to perform any required
        cleanup just 'before' its removal from the game scene.
        Called only by ObjectTemplateManager.destroy_object().
        Note: this method is called by the Object destruction thread,
        i.e. when it is be publicly accessible, so other thread can access
        the Object during its execution, then it does need to hold the mutex.
        '''
        # clean up into scene only if NodePath is not empty
        if not self._nodePath.is_empty():
            # set default pos/hpr
            self._nodePath.set_hpr(0.0, 0.0, 0.0)
            self._nodePath.set_pos(0.0, 0.0, 0.0)
            # detach node path from its parent
            self._nodePath.detach_node()
        # set steady to false
        self._isSteady = False

    def _do_store_parameters(self, objTmplParams, compTmplParams):
        '''
        This methods will store, in an internal storage, the Object and Components' parameters.

        Both Object and Components' parameters, are stored into the
        corresponding templates.
        Note: This method is intended to be used during the Object
        creation if the Object is susceptible to be cloned with clones
        having (approximately) the same parameters values, so they are
        available at once.
        Param objTmplParams: The Object parameters' table.
        Param compTmplParams: The Components' parameters' table.
        '''
        self._objTmplParams = objTmplParams
        self._compTmplParams = compTmplParams

    def get_component(self, compOrFamilyType):
        '''
        Get a Component.

        Gets the Component of a given type or of a given family type.
        Param compOrFamilyType: The type or family type of the Component.
        Return: The Component if it exists, None otherwise.
        '''
        assert(CheckArgType((compOrFamilyType, ComponentType)) or
               CheckArgType((compOrFamilyType, ComponentFamilyType)))
        if CheckArgType((compOrFamilyType, ComponentType)):
            for idx, familyComponentPair in enumerate(self._components):
                if familyComponentPair.component.component_type == compOrFamilyType:
                    return self._components[idx].component
        elif CheckArgType((compOrFamilyType, ComponentFamilyType)):
            for idx, familyComponentPair in enumerate(self._components):
                if familyComponentPair.familyType == compOrFamilyType:
                    return self._components[idx].component
        return None

    @property
    def num_components(self):
        '''
        Returns the number of Components.

        Return: The number of Components.
        '''
        return len(self._components)

    def world_setup(self):
        '''
        On game world creation setup.

        Gives an Object the ability to perform any given
        initialization after the whole world creation.
        '''
        # this method is thread safe because
        # mInitializationFunction is read-only when called
        if self._initializationFunction:
            # call initialization function
            self._initializationFunction(self, self._tmpl.parameter_table,
                                         self._tmpl.panda_framework, self._tmpl.window_framework)

    @property
    def object_id(self):
        '''
        Gets a the identifier of this Object.

        Return: The id of this Object.
        '''
        return self._objectId

    @property
    def object_type(self):
        '''
        Gets the type of this Component.

        Return: The id of this Component.
        '''
        return self._tmpl.object_type

    @property
    def node_path(self):
        '''
        NodePath getter function.
        '''
        return self._nodePath

    @node_path.setter
    def node_path(self, nodePath):
        '''
        NodePath setter function.
        '''
        assert CheckArgType((nodePath, NodePath))
        self._nodePath = nodePath

    @property
    def object_tmpl(self):
        '''
        Gets a reference to the ObjectTemplate.

        Return: The a reference to the ObjectTemplate.
        '''
        return self._tmpl

    @property
    def is_steady(self):
        '''
        Gets the Object steady-ness.

        This flag represents if this Object doesn't move in the world.
        Various _components can set or get this value to implement
        some optimization. By default false (i.e. Object is dynamic).
        '''
        return self._isSteady

    @property
    def stored_obj_tmpl_params(self):
        '''
        Get the Object parameter table.

        This table is stored into their corresponding ObjectTemplate.
        Return: The Object parameter table.
        '''
        return self._objTmplParams

    @property
    def stored_comp_tmpl_params(self):
        '''
        Get the Components' parameter tables.

        These tables are stored into their corresponding ComponentTemplates.
        Return: The Components' parameter tables.
        '''
        return self._compTmplParams

    def free_parameters(self):
        '''
        This method will free the stored Object and Components' parameters.

        These parameters are stored in tables into their corresponding
        ObjectTemplate and ComponentTemplates.
        Note: This method is intended to be used when done with the parameters'
            storage.
        '''
        self._objTmplParams.clear()
        self._compTmplParams.clear()

    @property
    def owner(self):
        '''
        Gets this Object's owner, i.e. the Object responsible of its lifetime (if any).
        '''
        return self._owner

    @owner.setter
    def owner(self, owner):
        '''
        Sets this Object's owner, i.e. the Object responsible of its lifetime (if any).

        Param owner: The owner Object. None if no one.
        '''
        assert (CheckArgType((owner, Object)) or (owner == None))
        self._owner = owner

    def _do_load_initialization_functions(self):
        '''
        Helper function to load initialization functions.
        '''
        # if initializations loaded do nothing
        if self._initializationsLoaded:
            return
        # load the initialization functions library
        initializationLibName = self._tmpl._gameDataInfo[GameDataInfo.INITIALIZATIONS]
        self._initializationLib = load_library_dictionary(initializationLibName,
                                                          'initializations', globals())
        if not self._initializationLib:
            return
        # initializations loaded
        self._initializationsLoaded = True

    def _do_unload_initialization_functions(self):
        '''
        Helper function to unload initialization functions.
        '''
        # if initializations not loaded do nothing
        if not self._initializationsLoaded:
            return
        # Close the initialization functions library
        if not self._initializationLib:
            COMLogger.debug('Error closing library: ' +
                            str(self._tmpl._gameDataInfo[GameDataInfo.INITIALIZATIONS]))
        else:
            self._initializationLib = None
        # initializations unloaded
        self._initializationsLoaded = False

    def __repr__(self):
        return self._objectId.__repr__() + ' (' + self.object_type.__repr__() + ')'

    def __str__(self):
        return self._objectId.__str__() + ' (' + self.object_type.__str__() + ')'


class ObjectType(BaseType):
    '''
    Object type.

    This type identifies the name of ObjectTemplate that create these type of
    Objects.
    '''

    @staticmethod
    def valueType():
        return str


class _ComponentTemplateList(BaseSequence):
    '''
    Type used for the ordered list of the ComponentTemplates.
    '''

    def __init__(self, sequence=None):
        super().__init__(ComponentTemplate, sequence)


class ObjectTemplate(object):
    '''
    Class modeling templates used to create Objects.
    '''

    def __init__(self, objType, objectTmplMgr, panda_framework,
                 window_framework, gameDataInfo):
        '''
        Constructor.

        Param objType: The object type of this template.
        Param objectTmplMgr: The ObjectTemplateManager.
        Param panda_framework: The PandaFramework.
        Param window_framework: The WindowFramework.
        '''
        assert CheckArgType((objectTmplMgr, objectTemplateManager.ObjectTemplateManager),
                            (objType, ObjectType),
                            (panda_framework, ShowBase),
                            (window_framework, GraphicsWindow))
        # Name (ObjectType) identifying this Object template.
        self._objType = objType
        # Ordered list of ComponentTemplates for all the owned Components.
        self._componentTemplates = _ComponentTemplateList()
        # The ObjectTemplateManager.
        self._objectTmplMgr = objectTmplMgr
        # Parameter table.
        self._parameterTable = ParameterTable()
        # The PandaFramework.
        self._pandaFramework = panda_framework
        # The WindowFramework.
        self._windowFramework = window_framework
        # gameDataInfo = GameManager.get_global_ptr().data_info[GameDataInfo.INITIALIZATIONS]
        self._gameDataInfo = gameDataInfo
        # reset parameters
        self._do_set_parameters_defaults()
        # The table of ParameterTables indexed by Component type.
        # These represent the allowed common attributes shared by each
        # Component of a given type, belonging to any Object of a given type.
        self._componentParameterTables = ComponentParameterTableMap()

    @property
    def object_type(self):
        '''
        Gets the name (i.e. the Object type) of ObjectTemplate.

        Return: The name of this ObjectTemplate.
        '''
        return self._objType

    @property
    def component_templates(self):
        '''
        Gets the ComponentTemplate list.

        This ObjectTemplate has an internal (ordered by insertion)
        list of references to ComponentTamplates, corresponding
        to Components owned by Objects it defines.
        Return: The ComponentTemplate list.
        '''
        return self._componentTemplates

    def add_component_template(self, componentTmpl):
        '''
        Adds a ComponentTemplate.

        This ObjectTemplate has an internal (ordered by insertion)
        list of references to ComponentTemplates, corresponding
        to Components owned by Objects it defines.
        Param componentTmpl: The ComponentTemplate.
        '''
        assert CheckArgType((componentTmpl, ComponentTemplate))
        self._componentTemplates.append(componentTmpl)

    def get_component_template(self, compOrFamilyType):
        '''
        Gets a ComponentTemplate contained into this ObjectTemplate

        This ObjectTemplate has an internal (ordered by insertion)
        list of references to ComponentTamplates, corresponding
        to Components owned by Objects it defines.

        Gets a ComponentTemplate corresponding to a Component of a given type
        or of a given family type.
        Param compOrFamilyType: The type or family type of the Component.
        Return: The Component template, None if it doesn't exist.
        '''
        assert (CheckArgType((compOrFamilyType, ComponentType)) or
                CheckArgType((compOrFamilyType, ComponentFamilyType)))
        if CheckArgType((compOrFamilyType, ComponentType)):
            for componentTemplate in self._componentTemplates:
                if componentTemplate.component_type == compOrFamilyType:
                    return componentTemplate
        elif CheckArgType((compOrFamilyType, ComponentFamilyType)):
            for componentTemplate in self._componentTemplates:
                if componentTemplate.component_family_type == compOrFamilyType:
                    return componentTemplate
        return None

    def clear_component_templates(self):
        '''
        Clears the table of all ComponentTemplates of this ObjectTemplate.
        '''
        self._componentTemplates.clear()

    def _do_set_parameters_defaults(self):
        '''
        Sets the Object parameters to their default values.

        For the Object this template is designed to create,
        this function sets the parameters to their default values.
        '''
        # _parameterTable must be the first cleared
        self._parameterTable.clear()
        # sets the (mandatory) parameters to their default values.
        self._parameterTable.insert(ParameterNameValue('is_steady', 'false'))
        self._parameterTable.insert(
            ParameterNameValue('store_params', 'false'))
        self._parameterTable.insert(ParameterNameValue('pos', '0.0,0.0,0.0'))
        self._parameterTable.insert(ParameterNameValue('rot', '0.0,0.0,0.0'))
        self._parameterTable.insert(ParameterNameValue('init_func', ''))

    def set_parameters(self, parameterTable):
        '''
        Sets the Object parameters to custom values.

        For the Object this template is designed to create,
        this function sets the parameters to custom values.
        These parameters overwrite (and/or are added to) the parameters
        defaults set by _do_set_parameters_defaults.
        Param parameterTable: The table of (parameter,value).
        '''
        assert CheckArgType((parameterTable, ParameterTable))
        # insert new parameters and update existing ones into
        # _parameterTable from parameter_table's parameters
        for key in parameterTable:
            self._parameterTable[key] = StringSequence(parameterTable[key])

    def parameter(self, paramName):
        '''
        Gets the parameter single value associated to the Object.

        Param paramName: The name of the parameter.
        Return: The value of the parameter, empty string if none exists.
        '''
        assert CheckArgType((paramName, str))
        valueList = self._parameterTable.get(paramName, None)
        return valueList[0] if valueList else ''

    def parameter_list(self, paramName):
        '''
        Gets the parameter multiple values associated to the Object.

        Param paramName: The name of the parameter.
        Return: The value list  of the parameter, empty list if none exists.
        '''
        assert CheckArgType((paramName, str))
        return self._parameterTable.get(paramName, [])

    @property
    def parameter_table(self):
        '''
        Gets a reference to the entire parameter table.

        Return: The parameter table.
        '''
        return self._parameterTable

    def parameter_table_copy(self):
        '''
        Returns a (deep) copy of the entire parameter table.

        Return: A copy of the parameter table.
        '''
        paramTabelDeepCopy = ParameterTable()
        for param, stringSeq in self._parameterTable.items():
            paramTabelDeepCopy[param] = StringSequence(stringSeq)
        return paramTabelDeepCopy

    @property
    def panda_framework(self):
        '''
        Gets/sets the PandaFramework.

        Return: A reference to the PandaFramework.
        '''
        return self._pandaFramework

    @property
    def window_framework(self):
        '''
        Gets/sets the WindowFramework.

        Return: A reference to the WindowFramework.
        '''
        return self._windowFramework

    def add_component_type_parameter(self, parameterName, parameterValue,
                                     compType):
        '''
        Adds a common multiple-valued parameter to a Component type of this Object.

        Any parameter value is interpreted as a 'compound' one, i.e. as having
        the form: 'value1:value2:...:valueN' and each valueX is stored in a
        list as returned by component_type_parameter_values().\n
        Param parameterName: The parameter name.
        Param parameterValue: The parameter value.
        Param compType: The Component type the parameter is related to.
        '''
        # any parameter value is a 'compound' one, i.e. could have the form:
        # 'value1:value2:...:valueN'
        assert CheckArgType((parameterName, str),
                            (parameterValue, str),
                            (compType, ComponentType))
        valueSeq = StringSequence(parse_compound_string(parameterValue, ':'))
        paramTable = self._componentParameterTables.get(compType, None)
        if not paramTable:
            self._componentParameterTables[compType] = ParameterTable(
                {parameterName: valueSeq})
        elif not parameterName in paramTable:
            paramTable[parameterName] = valueSeq
        else:
            paramTable[parameterName] += valueSeq

    def is_component_type_parameter_value(self, name, value, compType):
        '''
        Checks whether a (name, value) pair is a (parameter, value) allowed for a certain Component type of this Object.

        Param name: The name to check.
        Param value: The value to check.
        Param compType: The Component type.
        Return: True if the name/value pair match an allowed parameter/value
            for a given Component, false otherwise.
        '''
        assert CheckArgType((name, str),
                            (value, str),
                            (compType, ComponentType))
        #
        paramTable = self._componentParameterTables.get(compType, None)
        if paramTable:
            valueList = paramTable.get(name, None)
            if valueList and (value in valueList):
                return True
        #
        return False

    def component_type_parameter_values(self, paramName, compType):
        '''
        Gets all values of the common parameter associated to a certain Component type of the Object.

        Param paramName: The name of the parameter.
        Param compType: The Component type.
        Return: The value list of the parameter, empty list if none exists.
        '''
        assert CheckArgType((paramName, str),
                            (compType, ComponentType))
        #
        paramTable = self._componentParameterTables.get(compType, None)
        if paramTable:
            valueList = paramTable.get(paramName, None)
            if valueList:
                return valueList
        return StringSequence()

    def __repr__(self):
        return self.object_type.__repr__() + ' Template'

    def __str__(self):
        return self.object_type.__str__() + ' Template'
