'''
Created on May 12, 2019

@author: consultit
'''

import re
from collections import namedtuple
from collections.abc import MutableSequence, MutableMapping, Sequence, \
    Mapping, Callable
from .relational import Relational
from abc import abstractmethod
from numbers import Number
from panda3d.core import GeomVertexArrayFormat, InternalName, Geom, \
    GeomVertexFormat, GeomVertexData, GeomVertexRewriter, LPoint3f, LVector3f, \
    GeomTriangles, GeomNode, NodePath
import os
import logging


# Data Types
class GameException(Exception):
    pass


class BaseType(Relational):
    '''
    Base type.

    Designed to be used as an abstract base for wrappers of an built-in type.
    '''

    @staticmethod
    @abstractmethod
    def valueType():
        '''
        '''

    def __init__(self, value=None):
        '''
        Constructor
        '''
        self.value = value if value != None else self.valueType()()

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        typeConv = CheckTypeConversion(value, self.valueType())
        assert typeConv
        self._value = typeConv.value

    def assign(self, other):
        assert CheckArgType((other, BaseType))
        self.value = other.value

    # override base
    def __eq__(self, other):
        if isinstance(other, self.valueType()):
            return self._value.__eq__(other)
        return self._value.__eq__(other.value)

    # override base
    def __lt__(self, other):
        if isinstance(other, self.valueType()):
            return self._value.__lt__(other)
        return self._value.__lt__(other.value)

    def __hash__(self):
        return self._value.__hash__()

    def __repr__(self):
        return self._value.__repr__()

    def __str__(self):
        return self._value.__str__()

    def __bool__(self):
        return bool(self._value)


class BaseTable(Relational, MutableMapping):
    '''
    Base table.
    '''

    def __init__(self, keyType, valueType, mapping):
        assert CheckArgType((keyType, type), (valueType, type))
        assert (CheckArgType((mapping, Mapping)) or (mapping == None))
        self._keyType = keyType
        self._valueType = valueType
        self._storage = dict()
        if CheckArgType((mapping, Mapping)):
            self.update(mapping)

    def __setitem__(self, key, value):
        assert CheckArgType((key, self._keyType), (value, self._valueType))
        self._storage.__setitem__(key, value)

    def __getitem__(self, key):
        return self._storage.__getitem__(key)

    def __delitem__(self, key):
        self._storage.__delitem__(key)

    def __iter__(self):
        return self._storage.__iter__()

    def __len__(self):
        return self._storage.__len__()

    def __repr__(self):
        return self._storage.__repr__()

    def __str__(self):
        return self._storage.__str__()

    # base (mandatory)
    def __eq__(self, other):
        return self._storage.__eq__(other._storage)

    # base (mandatory)
    def __lt__(self, other):
        raise NotImplementedError('\'<, >, <=, >=\' not supported between instances of ' +
                                  self.__class__.__name__ + ' and ' + other.__class__.__name__)


class BaseSequence(MutableSequence, Relational):
    '''
    Base sequence.
    '''

    def __init__(self, valueType, sequence):
        assert CheckArgType((valueType, type))
        assert (CheckArgType((sequence, Sequence)) or (sequence == None))
        self._valueType = valueType
        self._storage = list()
        if CheckArgType((sequence, Sequence)):
            self.extend(sequence)

    def __setitem__(self, index, value):
        assert CheckArgType((value, self._valueType))
        self._storage.__setitem__(index, value)

    def __getitem__(self, index):
        return self._storage.__getitem__(index)

    def __delitem__(self, index):
        self._storage.__delitem__(index)

    def __len__(self):
        return self._storage.__len__()

    def insert(self, index, value):
        assert CheckArgType((value, self._valueType))
        return self._storage.insert(index, value)

    def __repr__(self):
        return self._storage.__repr__()

    def __str__(self):
        return self._storage.__str__()

    # base (mandatory)
    def __eq__(self, other):
        return self._storage.__eq__(other._storage)

    # base (mandatory)
    def __lt__(self, other):
        return self._storage.__lt__(other._storage)


class StringSequence(BaseSequence):
    '''
    String sequence.
    '''

    def __init__(self, sequence=None):
        super().__init__(str, sequence)


class FloatSequence(BaseSequence):
    '''
    Float sequence.
    '''

    def __init__(self, sequence=None):
        super().__init__(float, sequence)


class ParameterTable(BaseTable):
    '''
    Table for parameters management.

    dict{string:StringSequence,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(str, StringSequence, mapping)

    def insert(self, nameValue):
        assert CheckArgType((nameValue, ParameterNameValue))
        if nameValue.name in self:
            self[nameValue.name].append(nameValue.value)
        else:
            self[nameValue.name] = StringSequence([nameValue.value])


ParameterNameValue = namedtuple('ParameterNameValue', ['name', 'value'])


class Event(BaseType):
    '''
    Event.
    '''

    @staticmethod
    def valueType():
        return str


class EventType(BaseType):
    '''
    Event type.
    '''

    @staticmethod
    def valueType():
        return str


class EventTable(BaseTable):
    '''
    Table of events keyed by event types.

    dict{EventType:Event,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(EventType, Event, mapping)


class EventTypeTable(BaseTable):
    '''
    Table of event types keyed by events.

    dict{Event:EventType,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(Event, EventType, mapping)


NameCallbackPair = namedtuple('NameCallbackPair', ['name', 'callback'])


class EventTypeCallbackTable(BaseTable):
    '''
    Table of <name,callback> pairs keyed by event-type names.

    dict{EventType:NameCallbackPair,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(EventType, NameCallbackPair, mapping)


class CallableTable(BaseTable):
    '''
    Table of callbacks keyed by names.

    dict{string:callable,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(str, Callable, mapping)


class IdType(BaseType):
    '''
    Type of the generated object counter.
    '''

    @staticmethod
    def valueType():
        return int

    def incr(self):
        self._value += 1
        return self

    def decr(self):
        self._value -= 1
        return self


class CheckArgType(object):

    def __init__(self, *objectTypeList):
        self._msg = str()
        self._resTable = dict()
        for objectT, typeT in objectTypeList:
            objectStr = str(objectT)
            typeStr = str(typeT)
            if not isinstance(objectT, typeT):
                self._msg += (objectStr + ' is not an instance of ' +
                              typeStr + ' or of a subclass thereof.\n')
                self._resTable[objectStr] = False
            else:
                self._resTable[objectStr] = True

    def __bool__(self):
        return not self._msg

    def __str__(self):
        return self._msg

    def __getitem__(self, objectT):
        return self._resTable[str(objectT)]


class CheckTypeConversion(object):

    def __init__(self, value, valueType):
        self._msg = str()
        self._value = None
        try:
            self._value = valueType(value)
        except:
            self._msg += ('Cannot convert \'' + str(value) +
                          '\' to type \'' + str(valueType) + '\'')

    def __bool__(self):
        return not self._msg

    def __str__(self):
        return self._msg

    @property
    def value(self):
        return self._value


class Orderable(Relational):
    '''
    Struct that can be ordered based on a priority field.
    '''

    def __init__(self, prio=int(), ptr=None, msg=''):
        self._prio = prio
        self._ptr = ptr
        self._msg = str(msg)

    @property
    def prio(self):
        return self._prio

    @prio.setter
    def prio(self, prio):
        assert CheckArgType((prio, Number))
        self._prio = int(prio)

    @property
    def ptr(self):
        return self._ptr

    @ptr.setter
    def ptr(self, ptr):
        self._ptr = ptr

    @property
    def msg(self):
        return self._msg

    @msg.setter
    def msg(self, msg):
        self._msg = str(msg)

    # base (mandatory)
    def __eq__(self, other):
        return self._prio == other.prio

    # base (mandatory)
    def __lt__(self, other):
        return self._prio < other.prio

    def __repr__(self):
        return self._prio.__repr__() + ':' + self._msg

    def __str__(self):
        return self._prio.__str__() + ':' + self._msg


# Data
# callback stuff
DEFAULT_CALLBACK_NAME = 'default_callback__'


# Functions
def COMPONENT_STANDARD_NAME(comp):
    '''
    Define a standard name usable by components.

    Standard name = 'ObjectId_ObjectType_ComponentId_ComponentType'
    '''
    return (comp.owner_object.object_id.value + '_' +
            comp.owner_object.object_tmpl.object_type.value + '_' +
            comp.component_id.value + '_' +
            comp.component_type.value)


def erase_character(source, character):
    return source.replace(character, '')


def replace_character(source, character, replacement):
    return source.replace(character, replacement)


def parse_compound_string(srcCompoundString, separator):
    # erase blanks
    compoundString = re.sub('\n|\t| ', '', srcCompoundString)
    # parse
    substrings = compoundString.split(separator)
    # remove empty strings
    substrings = StringSequence(substrings)
    #
    return substrings


def strtof(strVal):
    try:
        val = float(strVal)
    except:
        val = 0.0
    return val


def strtol(strVal):
    try:
        val = int(strVal)
    except:
        val = 0
    return val


def load_library_dictionary(libName, dictName, globalsDict):
    '''
    Execs the libName in the context of globals and puts dictName in it.
    '''
    thisGlobalsDict = {
        '_this_library_dir': os.path.dirname(os.path.abspath(libName)),
        **globalsDict
    }
    try:
        exec(open(libName).read(), thisGlobalsDict)
        if dictName in thisGlobalsDict:
            library = CallableTable(thisGlobalsDict[dictName])
        else:
            COMLogger.debug('dictionary \'' + dictName + '\' not defined in ' +
                            libName)
            return None
    except Exception as e:
        COMLogger.debug('Error loading library: ' + libName + ' - ' + str(e))
        return None
    return library


def procedurallyGenerating3DModels(vertices, triangles, ntriangles,
                                   name='gnode', color=(0, 0, 1, 1),
                                   scale=1.0):
    # # Defining your own GeomVertexFormat
    array = GeomVertexArrayFormat()
    array.add_column(InternalName.make('vertex'), 3, Geom.NT_float32,
                     Geom.C_point)
    array.add_column(InternalName.make('normal'), 3, Geom.NT_float32,
                     Geom.C_normal)
    #
    unregistered_format = GeomVertexFormat()
    unregistered_format.add_array(array)
    #
    format1 = GeomVertexFormat.register_format(unregistered_format)
    # # Creating and filling a GeomVertexData
    vdata = GeomVertexData(name + 'VD', format1, Geom.UH_static)
    #
    numVerts = 0
    i = 0
    for i in range(0, ntriangles * 3, 1):
        numVerts = max(triangles[i], numVerts)
    numVerts += 1
    vdata.set_num_rows(numVerts)
    #
    vertexRW = GeomVertexRewriter(vdata, 'vertex')
    normalRW = GeomVertexRewriter(vdata, 'normal')
    #
    # vertices in panda3d space reset normal
    for i in range(0, numVerts * 3, 3):
        p3dvert = LPoint3f(vertices[i], vertices[i + 1], vertices[i + 2])
        vertexRW.add_data3f(p3dvert)
        normalRW.add_data3f(LVector3f.zero())
    # # Creating the GeomPrimitive objects and compute normals per vertex
    # # note: for each face the normal is computed and added to each normal of
    # # its vertices normals are not normalized at this stage, so a vertex
    # # normal is more influenced by triangles with wider area. All vertex
    # # normals are normalized all together in a second step.
    prim = GeomTriangles(Geom.UH_static)
    for i in range(0, ntriangles * 3, 3):
        i0 = triangles[i]
        i1 = triangles[i + 1]
        i2 = triangles[i + 2]
        prim.add_vertex(i0)
        prim.add_vertex(i1)
        prim.add_vertex(i2)
        # compute face normal (not normalized): (p1-p0)X(p2-p0)
        vertexRW.set_row(i0)
        p0 = vertexRW.get_data3f()
        vertexRW.set_row(i1)
        p1 = vertexRW.get_data3f()
        vertexRW.set_row(i2)
        p2 = vertexRW.get_data3f()
        faceNorm = (p1 - p0).cross(p2 - p0)
        # add faceNorm to each vertex normal (not normalized)
        normalRW.set_row(i0)
        n0 = normalRW.get_data3f()
        normalRW.set_row(i0)
        normalRW.set_data3f(n0 + faceNorm)
        #
        normalRW.set_row(i1)
        n1 = normalRW.get_data3f()
        normalRW.set_row(i1)
        normalRW.set_data3f(n1 + faceNorm)
        #
        normalRW.set_row(i2)
        n2 = normalRW.get_data3f()
        normalRW.set_row(i2)
        normalRW.set_data3f(n2 + faceNorm)
    prim.close_primitive()
    # normalize all vertex normals
    normalRW.set_row(0)
    for i in range(0, numVerts, 1):
        normalRW.set_row(i)
        n = LVector3f(normalRW.get_data3f())
        n.normalize()
        normalRW.set_row(i)
        normalRW.set_data3f(n)
    # Putting your new geometry in the scene graph
    geom = Geom(vdata)
    geom.add_primitive(prim)
    node = GeomNode(name)
    node.add_geom(geom)
    #
    modelNP = NodePath.any_path(node)
    modelNP.set_color(color)
    modelNP.set_scale(scale)
    return modelNP


# logging
COMLogger = logging.getLogger('COM_logger')
COMLogger.setLevel(logging.DEBUG)
# create console handler
consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter(
    '%(pathname)s - %(funcName)s - %(levelname)s - %(message)s')
consoleHandler.setFormatter(formatter)
# add the handlers to the logger
COMLogger.addHandler(consoleHandler)
