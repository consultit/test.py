'''
Created on Aug 1, 2019

@author: consultit
'''


class Singleton(type):
    '''
    Singleton implemented as metaclass.

    See: https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python .
    '''

    _instances = {}

    def __init__(cls, *args, **kwargs):
        '''
        Constructor
        '''
        if not hasattr(cls, 'get_global_ptr'):
            setattr(cls, 'get_global_ptr', classmethod(
                lambda _cls: _cls._instances[_cls]))

    def __call__(self, *args, **kwargs):
        if self not in self._instances:
            self._instances[self] = super(
                Singleton, self).__call__(*args, **kwargs)
        return self._instances[self]
