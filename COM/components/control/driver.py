'''
Created on Aug 30, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...tools.utilities import StringSequence
from ely.libtools import ValueList_string
from ely.control import GameControlManager as elyMgr
from panda3d.core import TransformState


class Driver(Component):
    '''
    Driver Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | - | - | - | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3Driver = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.DRIVER)
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3Driver
        for paramName, paramValue in self._tmpl.parameter_table.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        self._p3Driver = elyMgr.get_global_ptr().create_driver(
            self._compId.value)
        # set tag of underlying Ely component to the object
        self._p3Driver.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3Driver.clear_tag('_owner_object')
        #
        elyMgr.get_global_ptr().destroy_driver(self._p3Driver)
        self._p3Driver = None

    def _on_add_to_scene_setup(self):
        self._ownerObjectParent = self._ownerObject.node_path.get_parent()
        # move the transform of the object to this node path
        self._p3Driver.set_transform(
            self._ownerObject.node_path.node().get_transform())
        self._ownerObject.node_path.set_transform(
            TransformState.make_identity())
        # reparent the object node path to this
        self._ownerObject.node_path.reparent_to(self._p3Driver)

    def _on_remove_from_scene_cleanup(self):
        # move the transform of this node path to the object
        self._ownerObject.node_path.set_transform(
            self._p3Driver.node().get_transform())
        self._p3Driver.set_transform(TransformState.make_identity())
        self._ownerObject.node_path.reparent_to(self._ownerObjectParent)

    @property
    def p3Driver(self):
        return self._p3Driver


class DriverTemplate(ComponentTemplate):
    '''
    Driver Template.
    '''

    def _make_component(self, compId):
        newComp = Driver(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(Driver.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Control')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.DRIVER):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.DRIVER, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.DRIVER)
