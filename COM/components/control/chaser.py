'''
Created on Aug 30, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...com.object import ObjectId
from ...com.objectTemplateManager import ObjectTemplateManager
from ...tools.utilities import StringSequence
from ely.libtools import ValueList_string
from ely.control import GameControlManager as elyMgr
from panda3d.core import TransformState


class Chaser(Component):
    '''
    Chaser Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *chased_object*      |single| - | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3Chaser = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.CHASER)
        # chased object
        self._chasedId = ObjectId(self._tmpl.parameter('chased_object'))
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3sound3d
        for paramName, paramValue in self._tmpl.parameter_table.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        self._p3Chaser = elyMgr.get_global_ptr().create_chaser(
            self._compId.value)
        # set tag of underlying Ely component to the object
        self._p3Chaser.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3Chaser.clear_tag('_owner_object')
        #
        elyMgr.get_global_ptr().destroy_chaser(self._p3Chaser)
        self._p3Chaser = None

    def _on_add_to_scene_setup(self):
        self._ownerObjectParent = self._ownerObject.node_path.get_parent()
        # move the transform of the object to this node path
        self._p3Chaser.set_transform(
            self._ownerObject.node_path.node().get_transform())
        self._ownerObject.node_path.set_transform(
            TransformState.make_identity())
        # reparent the object node path to this
        self._ownerObject.node_path.reparent_to(self._p3Chaser)
        # set the (node path of) object chased by this component
        # that object is supposed to be already created,
        # set up and added to the created objects table
        # if not, this component chases nothing.
        chasedObject = ObjectTemplateManager.get_global_ptr().get_created_object(
            self._chasedId)
        if chasedObject:
            self._p3Chaser.node().chased_object = chasedObject.node_path

    def _on_remove_from_scene_cleanup(self):
        # move the transform of this node path to the object
        self._ownerObject.node_path.set_transform(
            self._p3Chaser.node().get_transform())
        self._p3Chaser.set_transform(TransformState.make_identity())
        self._ownerObject.node_path.reparent_to(self._ownerObjectParent)

    @property
    def p3Chaser(self):
        return self._p3Chaser


class ChaserTemplate(ComponentTemplate):
    '''
    Chaser Template.
    '''

    def _make_component(self, compId):
        newComp = Chaser(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(Chaser.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Control')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.CHASER):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.CHASER, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.CHASER)
