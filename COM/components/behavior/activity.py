'''
Created on Sep 23, 2019

@author: consultit
'''

from collections import namedtuple
from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from direct.fsm.FSM import FSM
from ...tools.utilities import StringSequence, parse_compound_string, \
    BaseTable, load_library_dictionary, BaseType, EventType, COMLogger
from ...managers.gameBehaviorManager import GameBehaviorManager
from ...managers.gameDataInfo import GameDataInfo


class State(BaseType):
    '''
    State type.
    '''

    @staticmethod
    def valueType():
        return str


StatePair = namedtuple('StatePair', ['stateFrom', 'stateTo'])
StateEventTypePair = namedtuple('StateEventTypePair', ['state', 'eventType'])


def ActivityFSM___init__(self):
    super(self.__class__, self).__init__('ActivityFSM')


def ActivityFSM_add_state(self, state, enterFunc, exitFunc, filterFunc):
    if not state:
        return
    if enterFunc:
        setattr(self.__class__, 'enter' + state.value, enterFunc)
    if exitFunc:
        setattr(self.__class__, 'exit' + state.value, exitFunc)
    if filterFunc:
        setattr(self.__class__, 'filter' + state.value, filterFunc)


def ActivityFSM_add_from_to_func(self, stateFrom, stateTo, pFromToFunction):
    if (not stateFrom) or (not stateTo):
        return
    if pFromToFunction:
        setattr(self.__class__, 'from' + stateFrom + 'To' +
                stateTo, pFromToFunction)


ActivityFSM_methods = {
    '__init__': ActivityFSM___init__,
    'add_state': ActivityFSM_add_state,
    'add_from_to_func': ActivityFSM_add_from_to_func
}


class TransitionTable(BaseTable):
    '''
    Transition table type declarations.

    dict{StateEventTypePair:State,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(StateEventTypePair, State, mapping)


# private type definitions
_TransitionFuncNameTriple = namedtuple('_TransitionFuncNameTriple', ['enter',
                                                                     'exit',
                                                                     'filter'])


class _TransitionFromToFuncName(BaseType):
    '''
    _TransitionFromToFuncName type.
    '''

    @staticmethod
    def valueType():
        return str


class _StateTransitionNameTripleMap(BaseTable):
    '''
    Transition functions' names keyed by state.

    dict{State:_TransitionFuncNameTriple,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(State, _TransitionFuncNameTriple, mapping)


class _StatePairFromToMap(BaseTable):
    '''
    FromTo transition functions' names keyed by state pair.

    dict{StatePair:_TransitionFromToFuncName,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(StatePair, _TransitionFromToFuncName, mapping)


class Activity(Component):
    '''
    Component representing the activity of an object.

    It is composed of a embedded fsm (i.e. FSM) representing
    the object game states (i.e. strings) and a 'transition table' which
    specify transitions from state to state upon event types reception.
    Moreover it gives an object instance the chance to do custom update through
    a function loaded at runtime from a dynamic linked library
    (See: GameDataInfo.INSTANCE_UPDATES).
    .
    All objects of the same type share the same activity component's states.
    A state transition can be request by delegating its embedded FSM
    interface functions, like in this sample code:

        activity.fsm.request(nextState, *args)

    Given an object with type <OBJECTYPE>, for any state <STATE> defined
    three names of transition functions are defined by default
    on a 'per ObjectTemplate (i.e. Object type)' basis:
    - Enter_<STATE>_<OBJECTYPE>
    - Exit_<STATE>_<OBJECTYPE>
    - Filter_<STATE>_<OBJECTYPE>
    These names can be overridden by parameters on a 'per Object' basis.
    Furthermore for a pair of state <STATEA>, <STATEB> a 'FromTo' transition
    function name can be defined, by default, on a 'per ObjectTemplate
    (i.e. Object type)' basis:
    - <STATEA>_FromTo_<STATEB>_<OBJECTYPE> 
    This name can be overridden by parameters on a 'per Object'
    basis.
    The transition functions are loaded at runtime from a dynamic linked library
    (See: GameDataInfo.TRANSITIONS).
    Inside these routine the Activity 'activity' argument passed refers to this
    component.
    Note: To make the embedded FSM work correctly, the names of the states must
        start with a capital letter and contain only alphanumeric characters and
        the underscore (that is, contained in the set [A-Za-z0-9_]).
    See: FSM for details.

    The transition table is queried for the <NEXT STATE> given the pair
    (<CURRENT STATE>,<EVENT TYPE>), so any of its elements is specified as
    <CURRENT STATE>,<EVENT TYPE>@<NEXT STATE>
    which means that when this component is in <CURRENT STATE>, and receive
    an event of type <EVENT TYPE>, will make a transition on <NEXT STATE>.
    So a common programming pattern, is:
    - specify the states
    - specify the event types that cause state transitions
    - specify the transition table elements (triples)
    - in the event's callback query the fsm for the current state and, given
    the type of this event, ask the transition table for the next state to go.
    Transition table elements, besides for states and event types, are defined
    on a 'per ObjectTemplate (i.e. Object type)' basis.

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *states*             |multiple| - | each one specified as 'state1[:state2:...:stateN]' into ObjectTemplate definition
    | *from_to*            |multiple| - | each one specified as 'state11@state21[:state12@state22:...:state1N@state2N]' into ObjectTemplate definition
    | *states_transition*  |multiple| - | each one specified as 'state1[:state2:...:stateN]$enterName,exitName,filterName' into Object definition
    | *from_to_transition* |multiple| - | each one specified as 'state11@state21[:state12@state22:...:state1N@state2N]$fromToName' into Object definition
    | *transition_table*   |multiple| - | each one specified as 'current_state1,event_type1@next_state1[:current_state2,event_type2@next_state2:...:current_stateN,event_typeN@next_stateN]' into ObjectTemplate definition
    | *instance_update*    |single| - | -

    Note: in 'states_transition' and 'from_to_transition' any of
    enterName, exitName, filterName, fromToName could be empty (meaning
    that we want the defaults).

    Note: parts inside [] are optional.
    '''

    def __init__(self, tmpl):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        # ActivityFSM
        self._reset()

    def _reset(self):
        # The underlying FSM (read-only after creation & before destruction).
        if hasattr(self, '_fsm_inst') and self._fsm_inst:
            self._fsm_inst.cleanup()
        self._fsm_inst = None
        self._fsm_class = None
        # Transition functions library management.
        # State transitions.
        self._stateTransitionListParam = StringSequence()
        # FromTo transitions.
        self._fromToTransitionListParam = StringSequence()
        # Temporary helper data/functions.
        self._stateTransitionTable = _StateTransitionNameTripleMap()
        self._statePairFromToTable = _StatePairFromToMap()
        # Handles, typedefs, for managing library of transition functions.
        self._transitionLib = None
        # Helper flag.
        self._transitionsLoaded = False
        # Transition table management.
        self._transitionTable = TransitionTable()
        # Update management data types, variables and functions.
        # Handles, typedefs, for managing library of instances' update
        # functions.
        self._instanceUpdateLib = None
        # Instance update function.
        self._instanceUpdate = None
        # Instance update function name.
        self._instanceUpdateName = str()
        #
        self._stateTransitionListParam.clear()
        self._fromToTransitionListParam.clear()
        self._transitionTable.clear()

    def _initialize(self):
        result = True
        # Create the underlying FSM class (uniquely related to this component)
        self._fsm_class = type(self._componentId.value + '_FSM', (FSM,),
                               ActivityFSM_methods)
        # Instantiate the underlying FSM class
        self._fsm_inst = self._fsm_class()
        # State transitions.
        self._stateTransitionListParam = self._tmpl.parameter_list(
            'states_transition')
        # FromTo transitions.
        self._fromToTransitionListParam = self._tmpl.parameter_list(
            'from_to_transition')
        # update function name
        self._instanceUpdateName = self._tmpl.parameter('instance_update')
        #
        return result

    def _on_add_to_object_setup(self):
        # setup the wrapped NodePath
        # setup the FSM
        self._do_setup_fsm_data()
        # setup the Transition Table
        self._do_setup_transition_table_data()
        # load transitions library
        self._do_load_transition_functions()
        # load instance update function (if any)
        if self._instanceUpdateName:
            self._do_load_instance_update()
        # set reference to the object id in underlying fsm component
        setattr(self._fsm_inst, '_owner_object', self._ownerObject.object_id)

    def _on_remove_from_object_cleanup(self):
        # remove reference to the object id in underlying fsm component
        delattr(self._fsm_inst, '_owner_object')
        # set the object node path to the old one
        self._reset()
        # unload instance update function (if any)
        if self._instanceUpdateName:
            self._do_unload_instance_update()
        # unload transitions library
        self._do_unload_transition_functions()

    def _on_add_to_scene_setup(self):
        if self._instanceUpdateName and self._instanceUpdate:
            # add to behavior update
            GameBehaviorManager.get_global_ptr().add_to_behavior_update(self)

    def _on_remove_from_scene_cleanup(self):
        if self._instanceUpdateName and self._instanceUpdate:
            # remove from behavior update
            GameBehaviorManager.get_global_ptr().remove_from_behavior_update(
                self)

    @property
    def fsm(self):
        '''
        FSM reference getter function.
        '''
        return self._fsm_inst

    def update(self, data):
        '''
        Updates this component.

        Will be called automatically by a behavior manager update.
        Param data: The custom data.
        '''
        if (not self._instanceUpdate):
            return
        # update should be called if and only if self._instanceUpdate != NULL
        self._instanceUpdate(data, self)

    @property
    def transition_table(self):
        '''
        Transition table getter.
        '''
        return self._transitionTable

    def _do_setup_fsm_data(self):
        '''
        Setup FSM helper data.
        '''
        # clear helper data
        self._stateTransitionTable.clear()
        self._statePairFromToTable.clear()
        # fill up transition functions' names table
        # with null string for transition function names
        states = self._ownerObject.object_tmpl.component_type_parameter_values(
            'states', self.component_type)
        for state in states:
            # a valid state has a not empty value
            if state:
                # set empty transitions' names for each state
                self._stateTransitionTable[State(state)] = _TransitionFuncNameTriple(
                    '', '', '')
        # override transition functions on a per Object basis
        for statesTransStr in self._stateTransitionListParam:
            # any 'states_transition' string is a 'compound' one, i.e. could
            # have the form:
            #  'state1:state2:...:stateN$enterName,exitName,filterName'
            # parse string as a (stateList,transitionNameTriple) pair
            stateListFuncTriple = parse_compound_string(statesTransStr, '$')
            # override only if there is (at least) a (stateList,FuncTriple)
            # pair
            if len(stateListFuncTriple) >= 2:
                # parse second element as a func-name triple
                funcNameList = parse_compound_string(
                    stateListFuncTriple[1], ',')
                # set only if there is (at least) a triple
                if len(funcNameList) >= 3:
                    # any one could be empty
                    transitionNameTriple = _TransitionFuncNameTriple(
                        funcNameList[0], funcNameList[1], funcNameList[2])
                    # parse first element as a state list
                    states = parse_compound_string(stateListFuncTriple[0], ':')
                    for state in states:
                        # insert only if it is a valid state
                        if state in self._stateTransitionTable:
                            # set transitions' names for each state
                            self._stateTransitionTable[State(
                                state)] = transitionNameTriple
        # fill up FromTo transition functions' names table
        # with null string for fromTo transition function names
        # only for valid fromTo transitions
        fromTos = self._ownerObject.object_tmpl.component_type_parameter_values(
            'from_to', self.component_type)
        for fromTo in fromTos:
            # parse element as state pair
            statePair = parse_compound_string(fromTo, '@')
            # a valid fromTo transition must have valid states (==
            # statePair[i])
            if (self._ownerObject.object_tmpl.is_component_type_parameter_value(
                'states', statePair[0], self.component_type)
                and
                statePair[0]
                and
                self._ownerObject.object_tmpl.is_component_type_parameter_value(
                'states', statePair[1], self.component_type)
                and
                    statePair[1]):
                # insert empty name into the FromTo functions' names table
                self._statePairFromToTable[StatePair(statePair[0],
                                                     statePair[1])] = _TransitionFromToFuncName('')
        # override FromTo transition functions on a per Object basis
        for fromToTrans in self._fromToTransitionListParam:
            # any 'from_to_transition' string is a 'compound' one, i.e.
            # could have the form:
            #  'state11@state21:state12@state22:...:state1N@state2N$fromToName'
            # parse string as a (statePairs,fromToName) pair
            statePairsFromTo = parse_compound_string(fromToTrans, '$')
            # override only if there is (at least) a (statePair,FromToName)
            # pair
            if len(statePairsFromTo) >= 2:
                # set second element as FromToName (could be empty)
                fromToName = statePairsFromTo[1]
                # parse first element as a state pair list
                statePairs = parse_compound_string(statePairsFromTo[0], ':')
                for statePair in statePairs:
                    # parse element as a state pair
                    statePair = parse_compound_string(statePair, '@')
                    # insert only if it is a valid fromTo transition
                    if (StatePair(statePair[0], statePair[1]) in
                            self._statePairFromToTable):
                        # insert into the FromTo functions' names table
                        self._statePairFromToTable[StatePair(
                            statePair[0], statePair[1])] = _TransitionFromToFuncName(fromToName)
        # clear all no more needed 'Param' variables
        self._stateTransitionListParam.clear()
        self._fromToTransitionListParam.clear()

    def _do_load_transition_functions(self):
        '''
        Helper function to load transition functions.
        '''
        # if no states or transitions loaded do nothing
        if (len(self._stateTransitionTable) == 0) or self._transitionsLoaded:
            return
        # load the transition functions library
        transitionLibName = self._tmpl._gameDataInfo[GameDataInfo.TRANSITIONS]
        self._transitionLib = load_library_dictionary(transitionLibName,
                                                      'transitions', globals())
        if not self._transitionLib:
            return
        # for each state load transition functions' names
        for state, transTable in self._stateTransitionTable.items():
            # set enter functionName
            if transTable.enter:
                # set enter function name as specified (if any)
                enterFuncName = transTable.enter
            else:
                # set enter function name as 'Enter_<STATE>_<OBJECTYPE>' (if
                # any)
                enterFuncName = ('Enter' + '_' + state.value + '_' +
                                 self._ownerObject.object_tmpl.object_type.value)
            pEnterFunction = self._transitionLib.get(enterFuncName, None)
            if not pEnterFunction:
                COMLogger.debug('Cannot load ' + enterFuncName +
                                ' (enter function)')
                pEnterFunction = None
            # set exit functionName
            if transTable.exit:
                # set exit function name as specified (if any)
                exitFuncName = transTable.exit
            else:
                # set exit function name as 'Exit_<STATE>_<OBJECTYPE>' (if any)
                exitFuncName = ('Exit' + '_' + state.value + '_' +
                                self._ownerObject.object_tmpl.object_type.value)
            pExitFunction = self._transitionLib.get(exitFuncName, None)
            if not pExitFunction:
                COMLogger.debug('Cannot load ' + exitFuncName +
                                ' (exit function)')
                pExitFunction = None
            # set filter functionName
            if transTable.filter:
                # set filter function name as specified (if any)
                filterFuncName = transTable.filter
            else:
                # set filter function name as 'Filter_<STATE>_<OBJECTYPE>' (if
                # any)
                filterFuncName = ('Filter' + '_' + state.value + '_' +
                                  self._ownerObject.object_tmpl.object_type.value)
            pFilterFunction = self._transitionLib.get(filterFuncName, None)
            if not pFilterFunction:
                COMLogger.debug('Cannot load ' + filterFuncName +
                                ' (filter function)')
                pFilterFunction = None
            # eventually add the state with the current transition functions
            self._fsm_inst.add_state(state, pEnterFunction, pExitFunction,
                                     pFilterFunction)
        # load FromTo transition functions if any
        for statePair, fromTo in self._statePairFromToTable.items():
            # set states
            stateFrom = statePair.stateFrom
            stateTo = statePair.stateTo
            # set FromTo functionName
            if fromTo:
                # set FromTo function name as specified (if any)
                fromToFuncName = str(fromTo)
            else:
                # set FromTo function name as '<STATEA>_FromTo_<STATEB>_<OBJECTYPE>'
                # (if any)
                fromToFuncName = (stateFrom + '_FromTo_' + stateTo + '_' +
                                  self._ownerObject.object_tmpl.object_type.value)
            pFromToFunction = self._transitionLib.get(fromToFuncName, None)
            if not pFromToFunction:
                COMLogger.debug('Cannot load ' + fromToFuncName +
                                ' (fromTo function)')
                pFromToFunction = None
            # add the FromTo function
            self._fsm_inst.add_from_to_func(
                stateFrom, stateTo, pFromToFunction)
        # clear no more needed helper data
        self._stateTransitionTable.clear()
        self._statePairFromToTable.clear()
        # transitions loaded
        self._transitionsLoaded = True

    def _do_unload_transition_functions(self):
        '''
        Helper function to unload transition functions.
        '''
        # if transitions not loaded do nothing
        if not self._transitionsLoaded:
            return
        # Close the transition functions library
        if not self._transitionLib:
            COMLogger.debug('Error closing library: ' + str(
                self._tmpl._gameDataInfo[GameDataInfo.TRANSITIONS]))
        else:
            self._transitionLib = None
        # transitions unloaded
        self._transitionsLoaded = False

    def _do_setup_transition_table_data(self):
        '''
        Setup Transition Table helper data.
        '''
        # fill up transition table
        currEventTypeNextList = self._ownerObject.object_tmpl.component_type_parameter_values(
            'transition_table', self.component_type)
        for currEventTypeNextStr in currEventTypeNextList:
            # each parameter value is of the form:
            #     'current_state,event_type@next_state'
            currEventTypeNext = parse_compound_string(
                currEventTypeNextStr, '@')
            if len(currEventTypeNext) >= 2:
                currEventType = parse_compound_string(
                    currEventTypeNext[0], ',')
                if len(currEventType) >= 2:
                    if (self._ownerObject.object_tmpl.is_component_type_parameter_value(
                            'states', currEventType[0], self.component_type)
                        and
                        self._ownerObject.object_tmpl.is_component_type_parameter_value(
                            'states', currEventTypeNext[1], self.component_type)
                        and
                        self._ownerObject.object_tmpl.is_component_type_parameter_value(
                            'event_types', currEventType[1], self.component_type)):
                        # insert element into the transition table
                        self._transitionTable[
                            StateEventTypePair(currEventType[0],
                                               EventType(currEventType[1]))] = State(currEventTypeNext[1])

    def _do_load_instance_update(self):
        '''
        Helper function to load instance update function.
        '''
        # if instance updates already loaded do nothing
        if self._instanceUpdate:
            return
        # load the instance updates library
        instanceUpdateLibName = self._tmpl._gameDataInfo[GameDataInfo.INSTANCE_UPDATES]
        self._instanceUpdateLib = load_library_dictionary(instanceUpdateLibName,
                                                          'instance_updates', globals())
        if not self._instanceUpdateLib:
            return
        # set the instance update function (if any)
        if self._instanceUpdateName:
            self._instanceUpdate = self._instanceUpdateLib.get(
                self._instanceUpdateName, None)
            if not self._instanceUpdate:
                COMLogger.debug('Cannot find instance update function ' +
                                self._instanceUpdateName)
                # self._instanceUpdate == None
                # cannot load instance update function
                return

    def _do_unload_instance_update(self):
        '''
        Helper function to unload instance update function.
        '''
        # if instance updates not loaded do nothing
        if not self._instanceUpdate:
            return
        # Close the event instance updates library
        if not self._instanceUpdateLib:
            COMLogger.debug('Error closing library: ' +
                            str(self._tmpl._gameDataInfo[GameDataInfo.INSTANCE_UPDATES]))
        else:
            self._instanceUpdateLib = None
        # unset instance update
        self._instanceUpdate = None


class ActivityTemplate(ComponentTemplate):
    '''
    Activity Template.
    '''

    def _make_component(self, compId):
        newComp = Activity(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(Activity.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Behavior')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # sets the (mandatory) parameters to their default values:
        # no mandatory parameters
