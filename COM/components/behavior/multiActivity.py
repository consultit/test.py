'''
Created on Feb 26, 2020

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from .activity import Activity
from ...tools.utilities import BaseTable, parse_compound_string, StringSequence
from ...com.object import ObjectId
from ...com.objectTemplateManager import ObjectTemplateManager


class ActivityTable(BaseTable):
    '''
    Activity table type declarations.

    dict{ObjectId:Activity,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(ObjectId, Activity, mapping)


class MultiActivity(Component):
    '''
    Component representing multiple activities of an object.

    It is a container of multiple Activity components.
    Each Activity must belong to an Object, and it is stored in a dictionary
    with the key equal to its ObjectId.
    It could be useful to implement concurrent and/or hierarchical FSMs, for
    example. 

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *activities*  |multiple| - | each one specified as 'activityObj1[:activityObj2:...:activityObjN]'

    Note: parts inside [] are optional.
    '''

    def __init__(self, tmpl):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        # ActivityFSM
        self._reset()

    def _reset(self):
        # The activity table.
        self._activities = ActivityTable()

    def _initialize(self):
        result = True
        # activity object names
        self._activityObjNames = StringSequence()
        for activityObjNamesParam in self._tmpl.parameter_list('activities'):
            self._activityObjNames += parse_compound_string(
                activityObjNamesParam, ':')
        #
        return result

    def _on_add_to_object_setup(self):
        pass

    def _on_remove_from_object_cleanup(self):
        pass

    def _on_add_to_scene_setup(self):
        # fill the activities dictionary
        for activityObjName in self._activityObjNames:
            activityObjId = ObjectId(activityObjName)
            activityObj = ObjectTemplateManager.get_global_ptr().get_created_object(
                activityObjId)
            if activityObj:
                activityComp = activityObj.get_component(ComponentFamilyType(
                    'Behavior'))
                if (activityComp and
                        (activityComp.component_type == ComponentType('Activity'))):
                    self._activities[activityObjId] = activityComp
                    # set reference to this component in activityComp
                    setattr(activityComp, '_multi_activity', self)

    def _on_remove_from_scene_cleanup(self):
        for activityComp in self._activities.values():
            # remove reference to this component in activityComp
            delattr(activityComp, '_multi_activity')

    @property
    def activities(self):
        '''
        Activities reference getter function.
        '''
        return self._activities


class MultiActivityTemplate(ComponentTemplate):
    '''
    MultiActivity Template.
    '''

    def _make_component(self, compId):
        newComp = MultiActivity(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(MultiActivity.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Behavior')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # sets the (mandatory) parameters to their default values:
        # no mandatory parameters
