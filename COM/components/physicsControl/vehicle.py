'''
Created on Sep 6, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...tools.utilities import StringSequence, parse_compound_string
from ...com.object import ObjectId
from ...com.objectTemplateManager import ObjectTemplateManager
from ely.libtools import ValueList_string
from ely.physics import GamePhysicsManager as elyMgr
from panda3d.core import NodePath


class Vehicle(Component):
    '''
    Vehicle Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *wheel_model*        |single| - | specified as 'modelF[:modelR]'

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3Vehicle = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.VEHICLE)
        # chased object
        paramStr = parse_compound_string(self._tmpl.parameter('wheel_model'),
                                         ':')
        if len(paramStr) > 0:
            self._wheelModelF = ObjectId(paramStr[0])
            self._wheelModelR = ObjectId(paramStr[1]) if len(
                paramStr) > 1 else ObjectId(paramStr[0])
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3Vehicle
        for paramName, paramValue in self._tmpl._parameterTable.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        self._p3Vehicle = elyMgr.get_global_ptr().create_vehicle(
            self._compId.value)
        # owner object must have an already constructed RigidBody component
        rigidBodyComp = self._ownerObject.get_component(
            ComponentFamilyType('Physics'))
        assert rigidBodyComp and (
            rigidBodyComp.component_type == ComponentType('RigidBody'))
        self._p3Vehicle.node().owner_object = rigidBodyComp.p3RigidBody
        # set wheel model(s)
        wheelModelF = ObjectTemplateManager.get_global_ptr().get_created_object(
            self._wheelModelF)
        if wheelModelF:
            self._p3Vehicle.node().set_wheel_model(wheelModelF.node_path, True)
        wheelModelR = ObjectTemplateManager.get_global_ptr().get_created_object(
            self._wheelModelR)
        if wheelModelR:
            self._p3Vehicle.node().set_wheel_model(wheelModelR.node_path, False)
        # set tag of underlying Ely component to the object
        self._p3Vehicle.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3Vehicle.clear_tag('_owner_object')
        #
        self._p3Vehicle.node().owner_object = NodePath()
        elyMgr.get_global_ptr().destroy_vehicle(
            self._p3Vehicle)
        self._p3Vehicle = None

    def _on_add_to_scene_setup(self):
        self._p3Vehicle.node().setup()

    def _on_remove_from_scene_cleanup(self):
        self._p3Vehicle.node().cleanup()

    @property
    def p3Vehicle(self):
        return self._p3Vehicle


class VehicleTemplate(ComponentTemplate):
    '''
    Vehicle Template.
    '''

    def _make_component(self, compId):
        newComp = Vehicle(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(Vehicle.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('PhysicsControl')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.VEHICLE):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.VEHICLE, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.VEHICLE)
