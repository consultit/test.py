'''
Created on Sep 6, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...tools.utilities import StringSequence
from ely.libtools import ValueList_string
from ely.physics import GamePhysicsManager as elyMgr
from panda3d.core import NodePath


class CharacterController(Component):
    '''
    CharacterController Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | - | - | - | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3CharacterController = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.CHARACTERCONTROLLER)
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3CharacterController
        for paramName, paramValue in self._tmpl._parameterTable.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        self._p3CharacterController = elyMgr.get_global_ptr().create_character_controller(
            self._compId.value)
        # owner object must have an already constructed Ghost component
        ghostComp = self._ownerObject.get_component(
            ComponentFamilyType('Physics'))
        assert ghostComp and (ghostComp.component_type ==
                              ComponentType('Ghost'))
        self._p3CharacterController.node().owner_object = ghostComp.p3Ghost
        # set tag of underlying Ely component to the object
        self._p3CharacterController.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3CharacterController.clear_tag('_owner_object')
        #
        self._p3CharacterController.node().owner_object = NodePath()
        elyMgr.get_global_ptr().destroy_character_controller(
            self._p3CharacterController)
        self._p3CharacterController = None

    def _on_add_to_scene_setup(self):
        self._p3CharacterController.node().setup()

    def _on_remove_from_scene_cleanup(self):
        self._p3CharacterController.node().cleanup()

    @property
    def p3CharacterController(self):
        return self._p3CharacterController


class CharacterControllerTemplate(ComponentTemplate):
    '''
    CharacterController Template.
    '''

    def _make_component(self, compId):
        newComp = CharacterController(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(CharacterController.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('PhysicsControl')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.CHARACTERCONTROLLER):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.CHARACTERCONTROLLER, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.CHARACTERCONTROLLER)
