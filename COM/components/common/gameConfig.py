'''
Created on Sep 29, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...tools.utilities import StringSequence, parse_compound_string, strtof
from ...managers.gameGUIManager import GameGUIManager
from ...managers.gamePhysicsManager import GamePhysicsManager
from ...managers.gameDataInfo import GameDataInfo
from pathlib import Path


class GameConfig(Component):
    '''
    Component representing the overall game configuration at startup.

    Game configuration is done through both xml parameters and its methods.

    *Collision notify*
    This configuration allows throwing events when objects collide. It is
    performed by the GamePhysicsManager and, by default, it's disabled.
    It can be enabled at startup by specifying
    'collision_notify@<EVENTNAME>@<FREQUENCY>' in 'thrown_events'
    xml parameter, with <EVENTNAME> ignored (so it could be empty).
    See: GamePhysicsManager documentation for details.

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *thrown_events*      |single| - | specified as 'event1@[event_name1]@[frequency1][:...[:eventN@[event_nameN]@[frequencyN]]]' with eventX = collision_notify
    | *gui_main_menu*      |single| - | -
    | *gui_exit_menu*      |single| - | -
    | *gui_font_paths*     |multiple| - | each one specified as 'font_path1[:font_path2:...:font_pathN]']

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._reset()

    def _reset(self):
        # Gui font paths parameters.
        self._guiMainMenuParam = str()
        self._guiExitMenuParam = str()
        self._guiFontPathListParam = StringSequence()
        # Throwing notify collision events.
        # Helper.
        self._thrownEventsParam = str()
        #
        self._guiFontPathListParam.clear()

    def _initialize(self):
        result = True

        # gui main menu
        self._guiMainMenuParam = self._tmpl.parameter('gui_main_menu')
        # gui exit menu
        self._guiExitMenuParam = self._tmpl.parameter('gui_exit_menu')
        # gui font paths
        self._guiFontPathListParam = self._tmpl.parameter_list(
            'gui_font_paths')
        # thrown events
        self._thrownEventsParam = self._tmpl.parameter('thrown_events')
        #
        return result

    def _do_get_file_path(self, fileName):
        pathFound = fileName
        for path in self._tmpl._gameDataInfo[GameDataInfo.DATA_DIRS].split(','):
            filePath = Path(path, fileName)
            if filePath.is_file():
                pathFound = str(filePath)
                break
        return pathFound

    def _on_add_to_object_setup(self):
        elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
        if elyGUIMgr:
            from ely.libtools import ValueList_string
            # gui main menu if any
            if self._guiMainMenuParam:
                elyGUIMgr.set_gui_main_menu_path(self._do_get_file_path(
                    self._guiMainMenuParam))
            # gui exit menu if any
            if self._guiExitMenuParam:
                elyGUIMgr.set_gui_exit_menu_path(self._do_get_file_path(
                    self._guiExitMenuParam))
            # set gui font paths if any
            fontPaths = ValueList_string()
            for fontPathListPram in self._guiFontPathListParam:
                # any 'font_path' string is a 'compound' one, i.e. could have the form:
                #  'font_path1[:font_path2:...:font_pathN]'
                fontPathList = parse_compound_string(fontPathListPram, ':')
                for fontPath in fontPathList:
                    fontPaths.add_value(fontPath)
            elyGUIMgr.set_gui_font_paths(fontPaths)
            # setup gui
            elyGUIMgr.gui_setup()
        # set thrown events if any
        elyPhysicsMgr = GamePhysicsManager.get_global_ptr()._elyMgr
        if elyPhysicsMgr and self._thrownEventsParam:
            # events specified
            # event1@[event_name1]@[frequency1][:...[:eventN@[event_nameN]@[frequencyN]]]
            paramValuesStr1 = parse_compound_string(
                self._thrownEventsParam, ':')
            valueNum1 = len(paramValuesStr1)
            for idx1 in range(valueNum1):
                # eventX@[event_nameX]@[frequencyX]
                paramValuesStr2 = parse_compound_string(
                    paramValuesStr1[idx1], '@')
                if len(paramValuesStr2) >= 3:
                    # get the event type to throw
                    if paramValuesStr2[0] == 'collision_notify':
                        eventType = elyPhysicsMgr.COLLISIONNOTIFY
                        # get name
                        eventName = paramValuesStr2[1]
                        # get frequency
                        eventFrequency = strtof(paramValuesStr2[2])
                        # enable the event
                        elyPhysicsMgr.enable_throw_event(
                            eventType, True, eventFrequency, eventName)
                    else:
                        # paramValuesStr2[0] is not a suitable event:
                        # continue with the next event
                        continue
        # clear all no more needed 'Param' variables
        self._thrownEventsParam = str()

    def _on_remove_from_object_cleanup(self):
        elyGUIMgr = GameGUIManager.get_global_ptr()._elyMgr
        if elyGUIMgr:
            # cleanup gui
            elyGUIMgr.gui_cleanup()

    def _on_add_to_scene_setup(self):
        pass

    def _on_remove_from_scene_cleanup(self):
        pass


class GameConfigTemplate(ComponentTemplate):
    '''
    GameConfig Template.
    '''

    def _make_component(self, compId):
        newComp = GameConfig(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(GameConfig.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Common')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # sets the (mandatory) parameters to their default values:
        # no mandatory parameters
