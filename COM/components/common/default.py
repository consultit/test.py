'''
Created on Sep 29, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType


class Default(Component):
    '''
    Component representing a minimum default implementation.

    It could be useful for attaching events' callbacks.

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | - | - | - | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._reset()

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        #
        return result

    def _on_add_to_object_setup(self):
        pass

    def _on_remove_from_object_cleanup(self):
        pass

    def _on_add_to_scene_setup(self):
        pass

    def _on_remove_from_scene_cleanup(self):
        pass


class DefaultTemplate(ComponentTemplate):
    '''
    Default Template.
    '''

    def _make_component(self, compId):
        newComp = Default(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(Default.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Common')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # sets the (mandatory) parameters to their default values:
        # no mandatory parameters
