'''
Created on Sep 4, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...com.object import ObjectId
from ...com.objectTemplateManager import ObjectTemplateManager
from ...tools.utilities import StringSequence
from .steerPlugIn import SteerPlugIn
from ely.libtools import ValueList_string
from ely.ai import GameAIManager as elyMgr
from panda3d.core import TransformState


class SteerVehicle(Component):
    '''
    SteerVehicle Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *add_to_plugin*      |single| - | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3SteerVehicle = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.STEERVEHICLE)
        # add to plugin object
        self._pluginId = ObjectId(self._tmpl.parameter('add_to_plugin'))
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3SteerVehicle
        for paramName, paramValue in self._tmpl.parameter_table.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        # handle add_to_plugin
        plugInObject = ObjectTemplateManager.get_global_ptr().get_created_object(
            self._pluginId)
        if plugInObject:
            compAI = plugInObject.get_component(ComponentFamilyType('AI'))
            if isinstance(compAI, SteerPlugIn):
                elyMgr.get_global_ptr().set_parameter_value(
                    elyMgr.STEERVEHICLE, 'add_to_plugin',
                    compAI.p3SteerPlugIn.node().get_name())
        #
        self._p3SteerVehicle = elyMgr.get_global_ptr().create_steer_vehicle(
            self._compId.value)
        # set tag of underlying Ely component to the object
        self._p3SteerVehicle.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3SteerVehicle.clear_tag('_owner_object')
        #
        elyMgr.get_global_ptr().destroy_steer_vehicle(self._p3SteerVehicle)
        self._p3SteerVehicle = None

    def _on_add_to_scene_setup(self):
        self._ownerObjectParent = self._ownerObject.node_path.get_parent()
        # remove steer vehicle from plug-in, if any
        plugIn = self._p3SteerVehicle.node().steer_plug_in
        if plugIn != None:
            plugIn.remove_steer_vehicle(self._p3SteerVehicle)
        # move the transform of the object to this node path
        self._p3SteerVehicle.set_transform(
            self._ownerObject.node_path.node().get_transform())
        self._ownerObject.node_path.set_transform(
            TransformState.make_identity())
        # reparent the object node path to this
        self._ownerObject.node_path.reparent_to(self._p3SteerVehicle)
        # re-add steer vehicle to plug-in, if any
        if plugIn != None:
            plugIn.add_steer_vehicle(self._p3SteerVehicle)

    def _on_remove_from_scene_cleanup(self):
        # move the transform of this node path to the object
        self._ownerObject.node_path.set_transform(
            self._p3SteerVehicle.node().get_transform())
        self._p3SteerVehicle.set_transform(TransformState.make_identity())
        self._ownerObject.node_path.reparent_to(self._ownerObjectParent)

    @property
    def p3SteerVehicle(self):
        return self._p3SteerVehicle


class SteerVehicleTemplate(ComponentTemplate):
    '''
    SteerVehicle Template.
    '''

    def _make_component(self, compId):
        newComp = SteerVehicle(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(SteerVehicle.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('AI')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.STEERVEHICLE):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.STEERVEHICLE, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.STEERVEHICLE)
