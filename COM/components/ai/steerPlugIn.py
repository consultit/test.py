'''
Created on Sep 4, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...com.object import ObjectId
from ...com.objectTemplateManager import ObjectTemplateManager
from ...tools.utilities import StringSequence, parse_compound_string
from ely.libtools import ValueList_string
from ely.ai import GameAIManager as elyMgr
from panda3d.core import LVector3f, LPoint3f, LVecBase3f


class SteerPlugIn(Component):
    '''
    SteerPlugIn Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | - | - | - | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3SteerPlugIn = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # save and reset 'obstacles' parameters
        self._obstParamList = self._tmpl.parameter_table.pop('obstacles', None)
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.STEERPLUGIN)
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3SteerVehicle
        for paramName, paramValue in self._tmpl.parameter_table.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        self._p3SteerPlugIn = elyMgr.get_global_ptr().create_steer_plug_in(
            self._compId.value)
        # set tag of underlying Ely component to the object
        self._p3SteerPlugIn.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3SteerPlugIn.clear_tag('_owner_object')
        #
        elyMgr.get_global_ptr().destroy_steer_plug_in(self._p3SteerPlugIn)
        self._p3SteerPlugIn = None

    def _on_add_to_scene_setup(self):
        # add obstacles, if any
        self._obstRefs = []
        if self._obstParamList:
            for obstParam in self._obstParamList:
                for obstSpec in parse_compound_string(obstParam, ':'):
                    objId, shape, sFS = parse_compound_string(obstSpec, '@')
                    obj = ObjectTemplateManager.get_global_ptr().get_created_object(
                        ObjectId(objId))
                    if obj:
                        obsRef = self.p3SteerPlugIn.node().add_obstacle_geometry(
                            shape, sFS, *self._compute_obstacle_geometry(obj.node_path))
                        if obsRef >= 0:
                            self._obstRefs.append(obsRef)
            self._obstParamList = None

    def _compute_obstacle_geometry(self, objectNP):
        refNP = elyMgr.get_global_ptr().reference_node_path
        # get "tight" dimensions of model
        minP = LPoint3f()
        maxP = LPoint3f()
        modelDims = LVecBase3f()
        modelDeltaCenter = LVector3f()
        objectNP.calc_tight_bounds(minP, maxP, refNP)
        #
        delta = maxP - minP
        deltaCenter = -(minP + delta / 2.0)
        modelDims.set(abs(delta.get_x()), abs(
            delta.get_y()), abs(delta.get_z()))
        modelDeltaCenter.set(deltaCenter.get_x(), deltaCenter.get_y(),
                             deltaCenter.get_z())
        modelRadius = max(max(modelDims.get_x(), modelDims.get_y()),
                          modelDims.get_z()) / 2.0
        # correct obstacle's parameters
        position = objectNP.get_pos(refNP)
        forward = refNP.get_relative_vector(objectNP, LVector3f.forward())
        up = refNP.get_relative_vector(objectNP, LVector3f.up())
        side = refNP.get_relative_vector(objectNP, LVector3f.right())
        width = modelDims.get_x()
        height = modelDims.get_z()
        depth = modelDims.get_y()
        radius = modelRadius
        return (width, height, depth, radius, side, up, forward,
                position + LPoint3f(0.0, 0.0, height / 2.0))

    def _on_remove_from_scene_cleanup(self):
        # remove obstacles, if any
        for obsRef in self._obstRefs:
            self.p3SteerPlugIn.node().remove_obstacle(obsRef)

    @property
    def p3SteerPlugIn(self):
        return self._p3SteerPlugIn


class SteerPlugInTemplate(ComponentTemplate):
    '''
    SteerPlugIn Template.
    '''

    def _make_component(self, compId):
        newComp = SteerPlugIn(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(SteerPlugIn.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('AI')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.STEERPLUGIN):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.STEERPLUGIN, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.STEERPLUGIN)
