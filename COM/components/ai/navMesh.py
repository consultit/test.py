'''
Created on Sep 4, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...tools.utilities import StringSequence
from ely.libtools import ValueList_string
from ely.ai import GameAIManager as elyMgr
from panda3d.core import NodePath


class NavMesh(Component):
    '''
    NavMesh Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | - | - | - | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3NavMesh = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.NAVMESH)
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3SteerVehicle
        for paramName, paramValue in self._tmpl.parameter_table.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        self._p3NavMesh = elyMgr.get_global_ptr().create_nav_mesh(
            self._compId.value)
        self._p3NavMesh.node().owner_object = self._ownerObject.node_path
        # set tag of underlying Ely component to the object
        self._p3NavMesh.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3NavMesh.clear_tag('_owner_object')
        #
        self._p3NavMesh.node().owner_object = NodePath()
        elyMgr.get_global_ptr().destroy_nav_mesh(self._p3NavMesh)
        self._p3NavMesh = None

    def _on_add_to_scene_setup(self):
        self._p3NavMesh.node().setup()

    def _on_remove_from_scene_cleanup(self):
        self._p3NavMesh.node().cleanup()

    @property
    def p3NavMesh(self):
        return self._p3NavMesh


class NavMeshTemplate(ComponentTemplate):
    '''
    NavMesh Template.
    '''

    def _make_component(self, compId):
        newComp = NavMesh(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(NavMesh.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('AI')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.NAVMESH):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.NAVMESH, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.NAVMESH)
