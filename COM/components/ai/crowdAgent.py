'''
Created on Sep 4, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...com.object import ObjectId
from ...com.objectTemplateManager import ObjectTemplateManager
from ...tools.utilities import StringSequence
from .navMesh import NavMesh
from ely.libtools import ValueList_string
from ely.ai import GameAIManager as elyMgr
from panda3d.core import TransformState


class CrowdAgent(Component):
    '''
    CrowdAgent Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *add_to_navmesh*      |single| - | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3CrowdAgent = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.CROWDAGENT)
        # add to navmesh object
        self._navmeshId = ObjectId(self._tmpl.parameter('add_to_navmesh'))
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3SteerVehicle
        for paramName, paramValue in self._tmpl.parameter_table.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        # handle add_to_navmesh
        navMeshObject = ObjectTemplateManager.get_global_ptr().get_created_object(
            self._navmeshId)
        if navMeshObject:
            compAI = navMeshObject.get_component(ComponentFamilyType('AI'))
            if isinstance(compAI, NavMesh):
                elyMgr.get_global_ptr().set_parameter_value(
                    elyMgr.CROWDAGENT, 'add_to_navmesh',
                    compAI.p3NavMesh.node().get_name())
        self._p3CrowdAgent = elyMgr.get_global_ptr().create_crowd_agent(
            self._compId.value)
        # set tag of underlying Ely component to the object
        self._p3CrowdAgent.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3CrowdAgent.clear_tag('_owner_object')
        #
        elyMgr.get_global_ptr().destroy_crowd_agent(self._p3CrowdAgent)
        self._p3CrowdAgent = None

    def _on_add_to_scene_setup(self):
        self._ownerObjectParent = self._ownerObject.node_path.get_parent()
        # remove crowd agent from nav mesh, if any
        navMesh = self._p3CrowdAgent.node().nav_mesh
        if navMesh != None:
            navMesh.remove_crowd_agent(self._p3CrowdAgent)
        # move the transform of the object to this node path
        self._p3CrowdAgent.set_transform(
            self._ownerObject.node_path.node().get_transform())
        self._ownerObject.node_path.set_transform(
            TransformState.make_identity())
        # reparent the object node path to this
        self._ownerObject.node_path.reparent_to(self._p3CrowdAgent)
        # re-add crowd agent to nav mesh, if any
        if navMesh != None:
            navMesh.add_crowd_agent(self._p3CrowdAgent)

    def _on_remove_from_scene_cleanup(self):
        # move the transform of this node path to the object
        self._ownerObject.node_path.set_transform(
            self._p3CrowdAgent.node().get_transform())
        self._p3CrowdAgent.set_transform(TransformState.make_identity())
        self._ownerObject.node_path.reparent_to(self._ownerObjectParent)

    @property
    def p3CrowdAgent(self):
        return self._p3CrowdAgent


class CrowdAgentTemplate(ComponentTemplate):
    '''
    CrowdAgent Template.
    '''

    def _make_component(self, compId):
        newComp = CrowdAgent(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(CrowdAgent.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('AI')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.CROWDAGENT):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.CROWDAGENT, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.CROWDAGENT)
