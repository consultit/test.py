'''
Created on Mar 16, 2020

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...tools.utilities import parse_compound_string, strtof, CheckArgType, \
    COMLogger, ParameterNameValue, BaseTable
from ...managers.gameDataInfo import GameDataInfo
from ely.direct.animTransition import AnimTransitionBase
from panda3d.core import LVecBase3f
from direct.actor.Actor import Actor
from pathlib import Path


class AnimTransitionTable(BaseTable):
    '''
    Table of AnimTransitions keyed by names.

    dict{str:AnimTransitionBase,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(str, AnimTransitionBase, mapping)


class ActorModel(Component):
    '''
    Component representing an Actor.

    This component has an Actor as inner NodePath.
    The 'parameters file' (corresponding to the XML/JSON Param 'param_file'),
    should be the name of a python script that defines (optionally) one global
    variable for each parameter of the Actor constructor with the same name and
    type. Missing variables are set to default values.

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *param_file*         |single| - | -
    | *scale*              |single| 1.0 | specified as 'scaleX[,scaleY,scaleZ]'
    | *flatten_mode*       |single| *strong* | values: none,light,medium,strong

    Note: parts inside [] are optional.
    '''

    def __init__(self, tmpl):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._reset()

    def _reset(self):
        # The NodePath associated to this actorModel.
        self._actor = None
        # Main parameters.
        # ParamFile.
        self._paramFile = None
        # Scaling.
        self._scale = LVecBase3f()
        # flatten mode
        self._flatten_mode = 'strong'

    def _initialize(self):
        result = True
        # actor parameters file.
        # search file into data dirs
        paramFileName = self._tmpl.parameter('param_file')
        for path in self._tmpl._gameDataInfo[GameDataInfo.DATA_DIRS].split(','):
            filePath = Path(path, paramFileName)
            if filePath.is_file():
                self._paramFile = filePath
                break
        # scale
        param = self._tmpl.parameter('scale')
        paramValuesStr = parse_compound_string(param, ',')
        valueNum = len(paramValuesStr)
        if (valueNum > 0) and (valueNum < 3):
            paramValuesStr += [paramValuesStr[0]] * (3 - valueNum)
        elif valueNum < 3:
            paramValuesStr += ['1.0'] * (3 - valueNum)
        for idx in range(3):
            value = strtof(paramValuesStr[idx])
            self._scale[idx] = abs(value)
        # flatten mode
        self._flatten_mode = self._tmpl.parameter('flatten_mode')
        #
        return result

    def _on_add_to_object_setup(self):
        # build actorModel
        COMLogger.debug('\'' + self._ownerObject.object_id.value + '\'.\'' +
                        self.component_id.value + '\'._on_add_to_object_setup')
        # read parameter file data
        exec(open(self._paramFile).read(), globals())
        # create the Actor
        global actorData
        self._actor = Actor(
            models=actorData.get('models', None),
            anims=actorData.get('anims', None),
            other=actorData.get('other', None),
            copy=actorData.get('copy', True),
            lodNode=actorData.get('lodNode', None),
            flattenable=actorData.get('flattenable', True),
            setFinal=actorData.get('setFinal', False),
            mergeLODBundles=actorData.get('mergeLODBundles', None),
            allowAsyncBind=actorData.get('allowAsyncBind', None),
            okMissing=actorData.get('okMissing', None)
        )
        # create the anim transitions
        self._animTransitions = None
        animTransitionsData = globals().get('animTransitionsData', None)
        if animTransitionsData:
            self._animTransitions = self._anim_transitions_setup(
                animTransitionsData)
        # set tag of underlying node path to the object
        self._actor.set_tag(
            '_owner_object', self._ownerObject.object_id.value)
        # set the object node path
        self._oldObjectNodePath = self._ownerObject.node_path
        self._ownerObject.node_path = self._actor
        # flatten if requested
        if self._flatten_mode == 'light':
            self._actor.flatten_light()
        elif self._flatten_mode == 'medium':
            self._actor.flatten_medium()
        elif self._flatten_mode != 'none':
            self._actor.flatten_strong()
        # set scaling to object node path
        self._ownerObject.node_path.set_scale(self._scale)
        # delete unused globals
        del actorData
        if animTransitionsData:
            del animTransitionsData

    def _on_remove_from_object_cleanup(self):
        # reset the object node path
        self._ownerObject.node_path = self._oldObjectNodePath
        # remove anim transitions if any
        self._animTransitions = None
        # remove tag of underlying node path to the object
        self._actor.clear_tag('_owner_object')
        # Remove node path
        self._actor.remove_node()
        #
        self._reset()

    def _on_add_to_scene_setup(self):
        pass

    def _on_remove_from_scene_cleanup(self):
        pass

    @property
    def actor(self):
        '''
        Gets the node path associated to this ActorModel.
        '''
        return self._actor

    @actor.setter
    def actor(self, actor):
        '''
        Sets the node path associated to this ActorModel.
        '''
        assert CheckArgType((actor, Actor))
        self._actor = actor

    @property
    def anim_transitions(self):
        '''
        Gets the node path associated to this ActorModel.
        '''
        return self._animTransitions

    @anim_transitions.setter
    def anim_transitions(self, animTransitions):
        '''
        Sets the node path associated to this ActorModel.
        '''
        assert (CheckArgType((animTransitions, AnimTransitionTable)) or
                (animTransitions == None))
        self._animTransitions = animTransitions

    def _anim_transitions_setup(self, animTransitionsData):
        '''
        Sets up all AnimTransitions, if any.
        '''
        if not animTransitionsData:
            return None
        animTransitions = AnimTransitionTable()
        for aTName in animTransitionsData:
            animTransType = animTransitionsData[aTName].get('type', None)
            if ((not animTransType) or
                    (not issubclass(animTransType, AnimTransitionBase))):
                continue
            animTrans = animTransType(self._tmpl.panda_framework)
            # setup animTrans
            animTrans.animCtrlFrom = animTransitionsData[aTName].get(
                'animCtrlFrom', None)(self._actor)
            animTrans.animCtrlTo = animTransitionsData[aTName].get(
                'animCtrlTo', None)(self._actor)
            animTrans.animToFrame = animTransitionsData[aTName].get(
                'animToFrame', 0)
            animTrans.blendType = animTransitionsData[aTName].get(
                'blendType', None)
            animTrans.frameBlend = animTransitionsData[aTName].get(
                'frameBlend', None)
            animTrans.partName = animTransitionsData[aTName].get(
                'partName', None)
            animTrans.playRate = animTransitionsData[aTName].get(
                'playRate', 1.0)
            animTrans.playRateRatios = animTransitionsData[aTName].get(
                'playRateRatios', ())
            animTrans.usePose = animTransitionsData[aTName].get(
                'usePose', False)
            animTrans.loop = animTransitionsData[aTName].get(
                'loop', True)
            duration = animTransitionsData[aTName].get(
                'duration', None)
            if duration:
                animTrans.duration = duration
            doneEvent = animTransitionsData[aTName].get(
                'doneEvent', None)
            if doneEvent:
                animTrans.doneEvent = doneEvent
            # store aninTrans
            animTransitions[aTName] = animTrans
        return animTransitions if animTransitions else None


class ActorModelTemplate(ComponentTemplate):
    '''
    ActorModel Template.
    '''

    def _make_component(self, compId):
        newComp = ActorModel(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(ActorModel.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Scene')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # sets the (mandatory) parameters to their default values:
        self._parameterTable.insert(ParameterNameValue('scale', '1.0'))
        self._parameterTable.insert(ParameterNameValue(
            'flatten_mode', 'strong'))
