'''
Created on Sep 18, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...tools.utilities import ParameterNameValue, COMPONENT_STANDARD_NAME, \
    parse_compound_string, strtof
from ...com.object import ObjectId
from ...com.objectTemplateManager import ObjectTemplateManager
from panda3d.core import NodePath, LVecBase3f


class InstanceOf(Component):
    '''
    Component wrapping a predefined NodePath (e.g. render, camera etc...).

    On error default NodePath is render.

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *instance_of*        |single| - | -
    | *scale*              |single| 1.0 | specified as 'scalex[,scaley,scalez]'
    | *flatten_mode*       |single| *strong* | values: none,light,medium,strong

    Note: parts inside [] are optional.
    '''

    def __init__(self, tmpl):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._reset()

    def _reset(self):
        # The NodePath associated to this model.
        self._nodePath = NodePath()
        # Instance of object id.
        self._instanceOfId = ObjectId()
        # The instanced object.
        self._instancedObject = None
        # Scaling.
        self._scale = LVecBase3f()
        # flatten mode
        self._flatten_mode = 'strong'

    def _initialize(self):
        result = True
        # instance of object id
        self._instanceOfId = ObjectId(self._tmpl.parameter('instance_of'))
        # scale
        param = self._tmpl.parameter('scale')
        paramValuesStr = parse_compound_string(param, ',')
        valueNum = len(paramValuesStr)
        if (valueNum > 0) and (valueNum < 3):
            paramValuesStr += [paramValuesStr[0]] * (3 - valueNum)
        elif valueNum < 3:
            paramValuesStr += ['1.0'] * (3 - valueNum)
        for idx in range(3):
            value = strtof(paramValuesStr[idx])
            self._scale[idx] = abs(value)
        # flatten mode
        self._flatten_mode = self._tmpl.parameter('flatten_mode')
        #
        return result

    def _on_add_to_object_setup(self):
        # setup initial state
        # Component standard name:
        # ObjectId_ObjectType_ComponentId_ComponentType
        name = COMPONENT_STANDARD_NAME(self)
        self._nodePath = NodePath(name)
        # set tag of underlying node path to the object
        self._nodePath.set_tag(
            '_owner_object', self._ownerObject.object_id.value)
        # get that object this component is an instance of
        # that object is supposed to be already created,
        # set up and added to the created objects table
        # if not this component is instance of nothing.
        self._instancedObject = ObjectTemplateManager.get_global_ptr().get_created_object(
            self._instanceOfId)
        if self._instancedObject:
            sceneComponent = self._instancedObject.get_component(
                ComponentFamilyType('Scene'))
            # an instanceable object should have a model component
            if (sceneComponent and
                    (sceneComponent.component_type == ComponentType('Model'))):
                sceneComponent.node_path.instance_to(self._nodePath)
        # set the object node path
        self._oldObjectNodePath = self._ownerObject.node_path
        self._ownerObject.node_path = self._nodePath
        # flatten if requested
        if self._flatten_mode == 'light':
            self._nodePath.flatten_light()
        elif self._flatten_mode == 'medium':
            self._nodePath.flatten_medium()
        elif self._flatten_mode != 'none':
            self._nodePath.flatten_strong()
        # set scaling to object node path
        self._ownerObject.node_path.set_scale(self._scale)

    def _on_remove_from_object_cleanup(self):
        # detach the first child of this instance of node path (if any)
        if self._instancedObject and (self._nodePath.get_num_children() > 0):
            #  \see NodePath::instance_to() documentation.
            self._nodePath.get_child(0).detach_node()
        # reset the object node path
        self._ownerObject.node_path = self._oldObjectNodePath
        # remove tag of underlying node path to the object
        self._nodePath.clear_tag('_owner_object')
        # Remove node path
        self._nodePath.remove_node()
        #
        self._reset()

    def _on_add_to_scene_setup(self):
        pass

    def _on_remove_from_scene_cleanup(self):
        pass

    @property
    def node_path(self):
        '''
        Gets the node path associated to this InstanceOf.
        '''
        return self._nodePath


class InstanceOfTemplate(ComponentTemplate):
    '''
    InstanceOf Template.
    '''

    def _make_component(self, compId):
        newComp = InstanceOf(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(InstanceOf.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Scene')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # sets the (mandatory) parameters to their default values:
        self._parameterTable.insert(ParameterNameValue('scale', '1.0'))
        self._parameterTable.insert(ParameterNameValue(
            'flatten_mode', 'strong'))
