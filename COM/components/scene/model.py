'''
Created on Sep 7, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...tools.utilities import StringSequence, FloatSequence, BaseSequence, \
    BaseTable, parse_compound_string, strtof, strtol, ParameterNameValue, \
    COMPONENT_STANDARD_NAME, procedurallyGenerating3DModels, CheckArgType, \
    COMLogger
from ...tools.torusMesh import gIndices, gVertices, NUM_TRIANGLES
from panda3d.core import NodePath, LVecBase3f, RopeNode, Texture, \
    AnimControlCollection, PartBundle, AnimBundle, TexturePool, Filename, \
    PartGroup, AnimBundleNode, PartBundleNode, CardMaker, GeomNode, SheetNode, \
    TextureStage


class _AnimBundles(BaseSequence):
    '''
    AnimBundle sequence.
    '''

    def __init__(self, sequence=None):
        super().__init__(AnimBundle, sequence)


class _Anims(BaseTable):
    '''
    Table of _AnimBundles by str.

    dictstr:_AnimBundles,...
    '''

    def __init__(self, mapping=None):
        super().__init__(str, _AnimBundles, mapping)


class _PartBundles(BaseSequence):
    '''
    PartBundle sequence.
    '''

    def __init__(self, sequence=None):
        super().__init__(PartBundle, sequence)


class _Parts(BaseTable):
    '''
    Table of _PartBundles by str.

    dictstr:_PartBundles,...
    '''

    def __init__(self, mapping=None):
        super().__init__(str, _PartBundles, mapping)


class Model(Component):
    '''
    Component representing the model and animations of an object.

    In relation to animations', this component accepts one model file, and
    a number of animation files it takes into account only the first
    joint's hierarchy (PartBundle) found the model file, and the animations
    (AnimBundle) found into the animation files (or model file) are bound to
    this only joint's hierarchy.
    Each animation can be referred by its associated file name. If more
    animations exist in any file, the next ones are suffixed  with '.N'
    where N=1,2,... .

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *from_file*          |single| *true* | -
    | *scale*              |single| 1.0 | specified as 'scaleX[,scaleY,scaleZ]'
    | *model_file*         |single| - | can have this form: [anim_name1@anim_name2@...@anim_nameN@]model_filename
    | *flatten_mode*       |single| *strong* | values: none,light,medium,strong
    | *anim_files*         |multiple| - | each one specified as 'anim_name1@anim_file1[:anim_name2@anim_file2:...:anim_nameN@anim_fileN]']
    | *model_type*         |single| - | values: card,rope_node,sheet_node,geom_node
    | *card_points*        |single| -1.0,1.0,-1.0,1.0 | specified as 'left,right,bottom,top'
    | *rope_render_mode*   |single| *tube* | -
    | *rope_uv_mode*       |single| *parametric* | -
    | *rope_normal_mode*   |single| *none* | -
    | *rope_num_subdiv*    |single| 4 | -
    | *rope_num_slices*    |single| 8 | -
    | *rope_thickness*     |single| 0.4 | -
    | *sheet_num_u_subdiv* |single| 2 | -
    | *sheet_num_v_subdiv* |single| 2 | -
    | *texture_file*       |single| - | -
    | *texture_uscale*     |single| 1.0 | -
    | *texture_vscale*     |single| 1.0 | -

    Note: parts inside [] are optional.
    '''

    def __init__(self, tmpl):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._reset()

    def _reset(self):
        # The NodePath associated to this model.
        self._nodePath = NodePath()
        # Flag indicating if component is set up from a file or procedurally.
        self._fromFile = False
        # Main parameters.
        # Model.
        self._modelNameParam = str()
        # Animations.
        self._animFileListParam = StringSequence()
        # Scaling.
        self._scale = LVecBase3f()
        # flatten mode
        self._flatten_mode = 'strong'
        # Type of model procedurally generated.
        self._modelTypeParam = str()
        # Card parameters.
        self._cardPoints = FloatSequence()
        # Rope parameters.
        self._ropeRenderMode = RopeNode.RM_tube
        self._ropeUVMode = RopeNode.UV_none
        self._ropeNormalMode = RopeNode.NM_none
        self._ropeNumSubdiv = 0
        self._ropeNumSlices = 0
        self._ropeThickness = 0.0
        # Sheet parameters.
        self._sheetNumUSubdiv = 0
        self._sheetNumVSubdiv = 0
        # Texture.
        self._textureImage = Texture()
        # Texture scale.
        self._textureUscale = 1.0
        self._textureVscale = 1.0
        # Animations' related data/functions.
        # The list of animations associated with this model.
        self._animations = AnimControlCollection()
        self._animations.clear_anims()
        self._firstPartBundle = None

    def _initialize(self):
        result = True
        # check if model is from file
        self._fromFile = (False if self._tmpl.parameter('from_file') == 'false'
                          else True)
        # model
        self._modelNameParam = self._tmpl.parameter('model_file')
        # more animations
        self._animFileListParam = self._tmpl.parameter_list('anim_files')
        # if model not from file then get which type
        self._modelTypeParam = self._tmpl.parameter('model_type')
        # scale
        param = self._tmpl.parameter('scale')
        paramValuesStr = parse_compound_string(param, ',')
        valueNum = len(paramValuesStr)
        if (valueNum > 0) and (valueNum < 3):
            paramValuesStr += [paramValuesStr[0]] * (3 - valueNum)
        elif valueNum < 3:
            paramValuesStr += ['1.0'] * (3 - valueNum)
        for idx in range(3):
            value = strtof(paramValuesStr[idx])
            self._scale[idx] = abs(value)
        # flatten mode
        self._flatten_mode = self._tmpl.parameter('flatten_mode')
        #
        if not self._fromFile:
            # card points
            param = self._tmpl.parameter('card_points')
            paramValuesStr = parse_compound_string(param, ',')
            valueNum = len(paramValuesStr)
            if valueNum < 4:
                paramValuesStr += ['0.0'] * (4 - valueNum)
            for idx in range(4):
                self._cardPoints.append(strtof(paramValuesStr[idx]))
            # rope render mode
            param = self._tmpl.parameter('rope_render_mode')
            if param == 'thread':
                self._ropeRenderMode = RopeNode.RM_thread
            elif param == 'tape':
                self._ropeRenderMode = RopeNode.RM_tape
            elif param == 'billboard':
                self._ropeRenderMode = RopeNode.RM_billboard
            else:
                self._ropeRenderMode = RopeNode.RM_tube
            # rope uv mode
            param = self._tmpl.parameter('rope_uv_mode')
            if param == 'parametric':
                self._ropeUVMode = RopeNode.UV_parametric
            elif param == 'distance':
                self._ropeUVMode = RopeNode.UV_distance
            elif param == 'distance2':
                self._ropeUVMode = RopeNode.UV_distance2
            else:
                self._ropeUVMode = RopeNode.UV_none
            # rope normal mode
            param = self._tmpl.parameter('rope_normal_mode')
            if param == 'vertex':
                self._ropeNormalMode = RopeNode.NM_vertex
            else:
                self._ropeNormalMode = RopeNode.NM_none
            # rope num subdiv
            valueInt = strtol(self._tmpl.parameter('rope_num_subdiv'))
            self._ropeNumSubdiv = abs(valueInt)
            # rope num slices
            valueInt = strtol(self._tmpl.parameter('rope_num_slices'))
            self._ropeNumSlices = abs(valueInt)
            # rope thickness
            value = strtof(self._tmpl.parameter('rope_thickness'))
            self._ropeThickness = abs(value)
            # sheet num u subdiv
            valueInt = strtol(self._tmpl.parameter('sheet_num_u_subdiv'))
            self._sheetNumUSubdiv = abs(valueInt)
            # sheet num v subdiv
            valueInt = strtol(self._tmpl.parameter('sheet_num_v_subdiv'))
            self._sheetNumVSubdiv = abs(valueInt)
            # texture
            self._textureImage = TexturePool.load_texture(
                Filename(self._tmpl.parameter('texture_file')))
            # texture uscale
            value = strtof(self._tmpl.parameter('texture_uscale'))
            self._textureUscale = abs(value)
            # texture vscale
            value = strtof(self._tmpl.parameter('texture_vscale'))
            self._textureVscale = abs(value)
        #
        return result

    def _on_add_to_object_setup(self):
        # build model
        if self._fromFile:
            COMLogger.debug('\'' + self._ownerObject.object_id.value + '\'.\'' +
                            self.component_id.value + '\'._on_add_to_object_setup')
            #  some declarations
            parts = _Parts()
            anims = _Anims()
            parts.clear()
            anims.clear()
            # setup model (with possible animations)
            # _modelNameParam can have this form: [anim_name1@anim_name2@
            #  ...@anim_nameN@]model_filename ([] means optional)
            animsFileNames = parse_compound_string(self._modelNameParam, '@')
            if not animsFileNames:
                animsFileNames.append('')
            # use the last element (model file name)
            # remove last element
            modelFileName = animsFileNames.pop()
            self._nodePath = self._tmpl.panda_framework.loader.load_model(
                Filename(modelFileName), loaderOptions=None, noCache=None,
                allowInstance=False, okMissing=True, callback=None,
                extraArgs=[], priority=None, blocking=None)
            if not self._nodePath:
                # On error loads our favorite blue model.
                COMLogger.debug('\tCannot load \'' + modelFileName + '\'')
                self._nodePath = procedurallyGenerating3DModels(
                    gVertices, gIndices, NUM_TRIANGLES,
                    COMPONENT_STANDARD_NAME(self) + '_torus',
                    (0, 0, 1, 1), self._scale)
            # find all the bundles into mNodePath.node
            self._do_r_find_bundles(self._nodePath, anims, parts)
            # check if there is at least one PartBundle
            for partBundles in parts.values():
                for partBundle in partBundles:
                    if self._firstPartBundle == None:
                        # set the first PartBundle
                        self._firstPartBundle = partBundle
                        COMLogger.debug('\tFirst PartBundle: \'' +
                                        partBundle.get_name() + '\'')
                    else:
                        COMLogger.debug('\tNext PartBundle: \'' +
                                        partBundle.get_name() + '\'')
            # proceeds with animations only if there is at least one PartBundle
            if self._firstPartBundle is not None:
                # check if there are some _AnimBundles within the model file
                # and bind them to the first PartBundle
                j = 1
                for animBundles in anims.values():
                    for animBundle in animBundles:
                        if animsFileNames:
                            # anim file names specified not finished:
                            # use the first name
                            # remove first name
                            animName = animsFileNames.pop(0)
                        else:
                            # anim names finished
                            animName = modelFileName + '.' + str(j)
                            j += 1
                        COMLogger.debug('\tBinding animation \'' +
                                        animBundle.get_name() + '\' (from \'' +
                                        modelFileName + '\') with name \'' +
                                        animName + '\'')
                        control = (self._firstPartBundle.bind_anim(animBundle,
                                                                   PartGroup.HMF_ok_wrong_root_name |
                                                                   PartGroup.HMF_ok_part_extra |
                                                                   PartGroup.HMF_ok_anim_extra))
                        self._animations.store_anim(control, animName)
                # setup more animations (if any)
                for animFileParam in self._animFileListParam:
                    # any 'anim_files' string is a 'compound' one, i.e. could
                    # have the form:
                    #  'anim_name1@anim_file1:anim_name2@anim_file2:...:anim_nameN@anim_fileN'
                    nameFileList = parse_compound_string(animFileParam, ':')
                    for nameFile in nameFileList:
                        # an empty anim_name@anim_file is ignored
                        if nameFile:
                            parts.clear()
                            anims.clear()
                            # get anim name and anim file name
                            nameFilePair = parse_compound_string(nameFile, '@')
                            # check only if there is a pair
                            if len(nameFilePair) == 2:
                                # anim name == nameFilePair[0]
                                # anim file name == nameFilePair[1]
                                # get the AnimBundle node path
                                animNP = self._tmpl.panda_framework.loader.load_model(
                                    Filename(nameFilePair[1]), loaderOptions=None,
                                    noCache=None, allowInstance=False,
                                    okMissing=True, callback=None,
                                    extraArgs=[], priority=None, blocking=None)
                                # find all the bundles into animNP.node
                                self._do_r_find_bundles(animNP, anims, parts)
                                for animBundles in anims.values():
                                    for j, animBundle in enumerate(animBundles):
                                        if j > 0:
                                            animName = nameFilePair[0] + \
                                                '.' + str(j)
                                        else:
                                            animName = nameFilePair[0]
                                        COMLogger.debug('\tBinding animation \'' +
                                                        animBundle.get_name() +
                                                        '\' (from \'' + nameFilePair[1] +
                                                        '\') with name \'' + animName + '\'')
                                        control = (self._firstPartBundle.bind_anim(animBundle,
                                                                                   PartGroup.HMF_ok_wrong_root_name |
                                                                                   PartGroup.HMF_ok_part_extra |
                                                                                   PartGroup.HMF_ok_anim_extra))
                                        self._animations.store_anim(
                                            control, animName)
        else:
            # not from file: model is programmatically generated
            if self._modelTypeParam == 'card':
                # card (e.g. finite plane)
                if ((self._cardPoints[1] - self._cardPoints[0]) *
                        (self._cardPoints[3] - self._cardPoints[2]) == 0.0):
                    self._cardPoints[0] = -1.0
                    self._cardPoints[1] = 1.0
                    self._cardPoints[2] = -1.0
                    self._cardPoints[3] = 1.0
                # Component standard name:
                #     ObjectId_ObjectType_ComponentId_ComponentType
                card = CardMaker(COMPONENT_STANDARD_NAME(self) + '_card')
                card.set_frame(self._cardPoints[0], self._cardPoints[1],
                               self._cardPoints[2], self._cardPoints[3])
                self._nodePath = NodePath(card.generate())
                self._nodePath.set_p(-90.0)
            elif self._modelTypeParam == 'rope_node':
                # rope_node: this is a generic RopeNode to which
                # a NurbsCurveEvaluator could be associated.
                # Component standard name:
                #     ObjectId_ObjectType_ComponentId_ComponentType
                ropeNode = RopeNode(COMPONENT_STANDARD_NAME(self) +
                                    '_rope_node')
                ropeNode.set_render_mode(self._ropeRenderMode)
                ropeNode.set_uv_mode(self._ropeUVMode)
                ropeNode.set_normal_mode(self._ropeNormalMode)
                ropeNode.set_num_subdiv(self._ropeNumSubdiv)
                ropeNode.set_num_slices(self._ropeNumSlices)
                ropeNode.set_thickness(self._ropeThickness)
                self._nodePath = NodePath(ropeNode)
            elif self._modelTypeParam == 'sheet_node':
                # sheet_node: this is a generic SheetNode to which
                # a NurbsSurfaceEvaluator could be associated.
                # Component standard name:
                #     ObjectId_ObjectType_ComponentId_ComponentType
                sheetNode = SheetNode(COMPONENT_STANDARD_NAME(self) +
                                      '_sheet_node')
                sheetNode.set_num_u_subdiv(self._sheetNumUSubdiv)
                sheetNode.set_num_v_subdiv(self._sheetNumVSubdiv)
                self._nodePath = NodePath(sheetNode)
            elif self._modelTypeParam == 'geom_node':
                # geom_node: this is a generic GeomNode to which
                # one or more Geoms could be added.
                # Component standard name:
                #     ObjectId_ObjectType_ComponentId_ComponentType
                geomNode = GeomNode(COMPONENT_STANDARD_NAME(self) +
                                    '_geom_node')
                self._nodePath = NodePath(geomNode)
            else:
                # On error loads our favorite blue model.
                self._nodePath = procedurallyGenerating3DModels(
                    gVertices, gIndices, NUM_TRIANGLES,
                    COMPONENT_STANDARD_NAME(self) + '_torus',
                    (0, 0, 1, 1), self._scale)
        # Rename the node
        # Component standard name:
        #     ObjectId_ObjectType_ComponentId_ComponentType
        name = COMPONENT_STANDARD_NAME(self)
        self._nodePath.set_name(name)
        # set tag of underlying node path to the object
        self._nodePath.set_tag(
            '_owner_object', self._ownerObject.object_id.value)
        # set the object node path
        self._oldObjectNodePath = self._ownerObject.node_path
        self._ownerObject.node_path = self._nodePath
        # flatten if requested
        if self._flatten_mode == 'light':
            self._nodePath.flatten_light()
        elif self._flatten_mode == 'medium':
            self._nodePath.flatten_medium()
        elif self._flatten_mode != 'none':
            self._nodePath.flatten_strong()
        # set scaling to object node path
        self._ownerObject.node_path.set_scale(self._scale)
        # if not from file, perform texturing (if any)
        if not self._fromFile:
            textureStage0 = TextureStage(COMPONENT_STANDARD_NAME(self) +
                                         '_TextureStage0')
            self._nodePath.set_tex_scale(textureStage0, self._textureUscale,
                                         self._textureVscale)
            if self._textureImage != None:
                self._nodePath.set_texture(
                    textureStage0, self._textureImage, 1)
        # clear all no more needed 'Param' variables
        self._modelNameParam = str()
        self._animFileListParam.clear()
        self._modelTypeParam = str()

    def _on_remove_from_object_cleanup(self):
        # reset the object node path
        self._ownerObject.node_path = self._oldObjectNodePath
        # remove tag of underlying node path to the object
        self._nodePath.clear_tag('_owner_object')
        # Remove node path
        self._nodePath.remove_node()
        #
        self._reset()

    def _on_add_to_scene_setup(self):
        pass

    def _on_remove_from_scene_cleanup(self):
        pass

    @property
    def node_path(self):
        '''
        Gets the node path associated to this Model.
        '''
        return self._nodePath

    @node_path.setter
    def node_path(self, nodePath):
        '''
        Sets the node path associated to this Model.
        '''
        assert CheckArgType((nodePath, NodePath))
        self._nodePath = nodePath

    @property
    def animations(self):
        '''
        Gets a reference to AnimControlCollection of this Model.

        Thread safe.
        Return: A reference to AnimControlCollection of this Model.
        '''
        return self._animations

    @property
    def part_bundle(self):
        '''
        Gets a reference to the PartBundle animations are bound to.

        Return: A reference to the PartBundle animations are bound to.
        '''
        return self._firstPartBundle

    def _do_r_find_bundles(self, nodePath, anims, parts):
        '''
        A support function for auto_bind().

        Walks through the hierarchy and finds all of the _PartBundles
        and _AnimBundles.
        Param node: The panda node.
        Param anims: Out parameter: the table of AnimBundle sets indexed
            by their names.
        Param parts: Out parameter: the table of AnimBundle sets indexed
            by their names.
        '''
        # on empty node return
        if nodePath.is_empty():
            return
        node = nodePath.node()
        if isinstance(node, AnimBundleNode):
            bn = node
            bundle = bn.get_bundle()
            bundleName = bundle.get_name()
            if bundleName in anims:
                anims[bundleName] += _AnimBundles([bundle])
            else:
                anims[bundleName] = _AnimBundles([bundle])
        elif isinstance(node, PartBundleNode):
            bn = node
            num_bundles = bn.get_num_bundles()
            for i in range(num_bundles):
                bundle = bn.get_bundle(i)
                bundleName = bundle.get_name()
                if bundleName in parts:
                    parts[bundleName] += _PartBundles([bundle])
                else:
                    parts[bundleName] = _PartBundles([bundle])
        for child in node.get_children():
            self._do_r_find_bundles(NodePath(child), anims, parts)


class ModelTemplate(ComponentTemplate):
    '''
    Model Template.
    '''

    def _make_component(self, compId):
        newComp = Model(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(Model.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Scene')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # sets the (mandatory) parameters to their default values:
        self._parameterTable.insert(ParameterNameValue('from_file', 'true'))
        self._parameterTable.insert(ParameterNameValue('scale', '1.0'))
        self._parameterTable.insert(ParameterNameValue(
            'flatten_mode', 'strong'))
        self._parameterTable.insert(ParameterNameValue(
            'card_points', '-1.0,1.0,-1.0,1.0'))
        self._parameterTable.insert(ParameterNameValue(
            'rope_render_mode', 'tube'))
        self._parameterTable.insert(ParameterNameValue(
            'rope_uv_mode', 'parametric'))
        self._parameterTable.insert(ParameterNameValue('rope_normal_mode',
                                                       'none'))
        self._parameterTable.insert(ParameterNameValue('rope_num_subdiv', '4'))
        self._parameterTable.insert(ParameterNameValue('rope_num_slices', '8'))
        self._parameterTable.insert(
            ParameterNameValue('rope_thickness', '0.4'))
        self._parameterTable.insert(ParameterNameValue(
            'sheet_num_u_subdiv', '2'))
        self._parameterTable.insert(ParameterNameValue(
            'sheet_num_v_subdiv', '2'))
        self._parameterTable.insert(
            ParameterNameValue('texture_uscale', '1.0'))
        self._parameterTable.insert(
            ParameterNameValue('texture_vscale', '1.0'))
