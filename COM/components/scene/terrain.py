'''
Created on Sep 29, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...com.object import ObjectId
from ...tools.utilities import ParameterNameValue, COMPONENT_STANDARD_NAME, \
    strtof, strtol
from ...com.objectTemplateManager import ObjectTemplateManager
from ...managers.gameSceneManager import GameSceneManager
from ...managers.gameDataInfo import GameDataInfo
from panda3d.core import GeoMipTerrain, PNMImage, LPoint3f, NodePath, Filename, \
    TexturePool, TextureStage, TransformState
from math import floor, ceil
from pathlib import Path


class Terrain(Component):
    '''
    Component representing the terrain model of an object.

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *heightfield_file*   |single| - | -
    | *height_scale*       |single| 1.0 | -
    | *width_scale*        |single| 1.0 | -
    | *do_scale*           |single| *true* | hack for rigid body
    | *block_size*         |single| 64 | -
    | *near_percent*       |single| 0.1 | -
    | *far_percent*        |single| 1.0 | -
    | *brute_force*        |single| *true* | -
    | *auto_flatten*       |single| *off* | one of medium,light,strong,off
    | *focal_point*        |single| *camera* | -
    | *minimum_level*      |single| 0 | -
    | *texture_file*       |single| - | -
    | *texture_uscale*     |single| 1.0 | -
    | *texture_vscale*     |single| 1.0 | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._reset()

    def _reset(self):
        # The underlying GeoMipTerrain (read-only after creation & before
        # destruction).
        self._terrain = None
        # Main parameters.
        # Heightfield image. Default empty (black) image with 513x513
        # dimensions.
        self._heightField = PNMImage(513, 513)
        # Scale.
        self._heightScale = 1.0
        self._widthScale = 1.0
        self._doScale = True
        # LOD.
        self._nearPercent = 0.0
        self._farPercent = 1.0
        # Block size.
        self._blockSize = 0
        # Auto flatten.
        self._flattenMode = GeoMipTerrain.AFM_off
        # Minimum level.
        self._minimumLevel = 0
        # Texture.
        self._textureImage = None
        # Texture scale.
        self._textureUscale = 1.0
        self._textureVscale = 1.0
        # Focal point stuff.
        # The object to be focal point .
        self._focalPointObject = ObjectId()
        # The focal point NodePath.
        # see https:# www.panda3d.org/forums/viewtopic.php?t=5384
        self._focalPointNP = NodePath()
        self._terrainRootNetPos = LPoint3f()
        # Flag if brute force is enabled.
        self._bruteForce = False

    def _initialize(self):
        result = True
        # height scale
        value = strtof(self._tmpl.parameter('height_scale'))
        self._heightScale = abs(value)
        # width scale
        value = strtof(self._tmpl.parameter('width_scale'))
        self._widthScale = abs(value)
        # do scale
        self._doScale = False if self._tmpl.parameter(
            'do_scale') == 'false' else True
        # LOD
        # near percent [0.0, 1.0]
        value = strtof(self._tmpl.parameter('near_percent'))
        self._nearPercent = value - \
            floor(value) if value >= 0.0 else ceil(value) - value
        # far percent [0.0, 1.0]
        value = strtof(self._tmpl.parameter('far_percent'))
        self._farPercent = value - \
            floor(value) if value >= 0.0 else ceil(value) - value
        if self._nearPercent > self._farPercent:
            self._farPercent = self._nearPercent
        # block size
        valueInt = strtol(self._tmpl.parameter('block_size'))
        self._blockSize = abs(valueInt)
        # brute force
        self._bruteForce = False if self._tmpl.parameter(
            'brute_force') == 'false' else True
        # auto flatten
        autoFlatten = self._tmpl.parameter('auto_flatten')
        if autoFlatten == 'medium':
            self._flattenMode = GeoMipTerrain.AFM_medium
        elif autoFlatten == 'light':
            self._flattenMode = GeoMipTerrain.AFM_light
        elif autoFlatten == 'strong':
            self._flattenMode = GeoMipTerrain.AFM_strong
        else:
            self._flattenMode = GeoMipTerrain.AFM_off
        # focal point
        self._focalPointObject = ObjectId(self._tmpl.parameter('focal_point'))
        # minimum level
        valueInt = strtol(self._tmpl.parameter('minimum_level'))
        self._minimumLevel = abs(valueInt)
        # heightfield file
        # search image into data dirs
        heightfieldFileName = self._tmpl.parameter('heightfield_file')
        for path in self._tmpl._gameDataInfo[GameDataInfo.DATA_DIRS].split(','):
            filePath = Path(path, heightfieldFileName)
            if filePath.is_file():
                # ##heightField = PNMImage(Filename(dataDirs[1] + '/heightfield.png'))
                self._heightField = PNMImage(Filename(filePath))
                break
        # texture
        self._textureImage = TexturePool.load_texture(
            Filename(self._tmpl.parameter('texture_file')))
        # texture uscale
        value = strtof(self._tmpl.parameter('texture_uscale'))
        self._textureUscale = abs(value)
        # texture vscale
        value = strtof(self._tmpl.parameter('texture_vscale'))
        self._textureVscale = abs(value)
        #
        return result

    def _on_add_to_object_setup(self):
        # create the current terrain
        # terrain definition
        # Component standard name:
        # ObjectId_ObjectType_ComponentId_ComponentType
        name = COMPONENT_STANDARD_NAME(self)
        self._terrain = GeoMipTerrain(name)
        # set height field
        self._terrain.set_heightfield(self._heightField)
        self._terrain.get_root().set_transform(TransformState.make_identity())
        # sizing
        environmentWidthX = (
            self._heightField.get_x_size() - 1) * self._widthScale
        environmentWidthY = (
            self._heightField.get_y_size() - 1) * self._widthScale
        environmentWidth = (environmentWidthX + environmentWidthY) / 2.0
        # set terrain properties effectively
        self._terrain.set_block_size(self._blockSize)
        self._terrain.set_near(self._nearPercent * environmentWidth)
        self._terrain.set_far(self._farPercent * environmentWidth)
        # other properties
        terrainLODmin = min(self._minimumLevel, self._terrain.get_max_level())
        self._terrain.set_min_level(terrainLODmin)
        self._terrain.set_auto_flatten(self._flattenMode)
        self._terrain.set_bruteforce(self._bruteForce)
        if self._doScale:
            self._terrain.get_root().set_sx(self._widthScale)
            self._terrain.get_root().set_sy(self._widthScale)
            self._terrain.get_root().set_sz(self._heightScale)
        # terrain texturing
        textureStage0 = TextureStage(
            COMPONENT_STANDARD_NAME(self) + '_TextureStage0')
        self._terrain.get_root().set_tex_scale(textureStage0,
                                               self._textureUscale, self._textureVscale)
        if self._textureImage:
            self._terrain.get_root().set_texture(
                textureStage0, self._textureImage, 1)
        # set tag of underlying node path to the object
        self._terrain.get_root().set_tag(
            '_owner_object', self._ownerObject.object_id.value)
        # set the object node path
        self._oldObjectNodePath = self._ownerObject.node_path
        self._ownerObject.node_path = self._terrain.get_root()
        # set the focal point
        objTmplMgr = ObjectTemplateManager.get_global_ptr()
        createdObject = objTmplMgr.get_created_object(self._focalPointObject)
        if not createdObject:
            # set render as focal point
            createdObject = objTmplMgr.getCreatedObject(ObjectId('render'))
        self._focalPointNP = createdObject.node_path
        # Generate the terrain
        self._terrain.generate()

    def _on_remove_from_object_cleanup(self):
        # reset the object node path
        self._ownerObject.node_path = self._oldObjectNodePath
        # remove tag of underlying node path to the object
        self._terrain.get_root().clear_tag('_owner_object')
        self._terrain.get_root().remove_node()
        self._reset()

    def _on_add_to_scene_setup(self):
        # save the net pos of terrain root
        self._terrainRootNetPos = self._terrain.get_root().get_net_transform().get_pos()
        # Add to the scene manager update if not brute force
        if not self._bruteForce:
            # Add to the scene manager update if not brute force
            GameSceneManager.get_global_ptr().add_to_scene_update(self)

    def _on_remove_from_scene_cleanup(self):
        # check if not brute force and if game scene manager exists
        if not self._bruteForce:
            # remove from the scene manager update
            GameSceneManager.get_global_ptr().remove_from_scene_update(self)

    def update(self, data):
        '''
        Updates the terrain associated to this component.

        Will be called automatically by an scene manager update.
        Param data: The custom data.
        '''
        # set focal point
        # see https://discourse.panda3d.org/t/geomipterrain-issues/4997
        focalPointNetPos = self._focalPointNP.get_net_transform().get_pos()
        self._terrain.set_focal_point(
            focalPointNetPos - self._terrainRootNetPos)
        # update every frame
        self._terrain.update()

    @property
    def width_scale(self):
        '''
        Gets width scale.
        '''
        return self._widthScale

    @property
    def height_scale(self):
        '''
        Gets height scale.
        '''
        return self._heightScale

    @property
    def geo_mip_terrain(self):
        '''
        GeoMipTerrain reference getter & conversion function.
        '''
        return self._terrain


class TerrainTemplate(ComponentTemplate):
    '''
    Terrain Template.
    '''

    def _make_component(self, compId):
        newComp = Terrain(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(Terrain.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Scene')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # sets the (mandatory) parameters to their default values:
        self._parameterTable.insert(ParameterNameValue('height_scale', '1.0'))
        self._parameterTable.insert(ParameterNameValue('width_scale', '1.0'))
        self._parameterTable.insert(ParameterNameValue('do_scale', 'true'))
        self._parameterTable.insert(ParameterNameValue('block_size', '64'))
        self._parameterTable.insert(ParameterNameValue('near_percent', '0.1'))
        self._parameterTable.insert(ParameterNameValue('far_percent', '1.0'))
        self._parameterTable.insert(ParameterNameValue('brute_force', 'true'))
        self._parameterTable.insert(ParameterNameValue('auto_flatten', 'off'))
        self._parameterTable.insert(
            ParameterNameValue('focal_point', 'camera'))
        self._parameterTable.insert(ParameterNameValue('minimum_level', '0'))
        self._parameterTable.insert(
            ParameterNameValue('texture_uscale', '1.0'))
        self._parameterTable.insert(
            ParameterNameValue('texture_vscale', '1.0'))
