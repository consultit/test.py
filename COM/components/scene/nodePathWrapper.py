'''
Created on Sep 17, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from panda3d.core import NodePath


class NodePathWrapper(Component):
    '''
    Component wrapping a predefined NodePath (e.g. render, camera etc...).

    On error default NodePath is render.

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *node_path*           |single| - | -

    Note: parts inside [] are optional.
    '''

    def __init__(self, tmpl):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._reset()

    def _reset(self):
        # The NodePath associated to this node path wrapper.
        self._nodePath = NodePath()
        # The wrapped NodePath.
        self._wrappedNodePathParam = str()
        # Old owner object node path.
        self._oldObjectNodePath = NodePath()

    def _initialize(self):
        result = True
        # setup the wrapped NodePath
        self._wrappedNodePathParam = self._tmpl.parameter('node_path')
        #
        return result

    def _on_add_to_object_setup(self):
        # setup the wrapped NodePath
        if self._wrappedNodePathParam == 'render2d':
            self._nodePath = self._tmpl.panda_framework.render2d
        elif self._wrappedNodePathParam == 'aspect2d':
            self._nodePath = self._tmpl.panda_framework.aspect2d
        elif self._wrappedNodePathParam == 'camera':
            self._nodePath = self._tmpl.panda_framework.camera
        else:
            # default is 'render'
            self._nodePath = self._tmpl.panda_framework.render
        # set tag of underlying node path to the object
        self._nodePath.set_tag(
            '_owner_object', self._ownerObject.object_id.value)
        # set the object node path
        self._oldObjectNodePath = self._ownerObject.node_path
        self._ownerObject.node_path = self._nodePath
        # clear all no more needed 'Param' variables
        self._wrappedNodePathParam = str()

    def _on_remove_from_object_cleanup(self):
        # reset the object node path
        self._ownerObject.node_path = self._oldObjectNodePath
        # remove tag of underlying node path to the object
        self._nodePath.clear_tag('_owner_object')
        #
        self._reset()

    def _on_add_to_scene_setup(self):
        pass

    def _on_remove_from_scene_cleanup(self):
        pass

    @property
    def node_path(self):
        '''
        Gets the node path associated to this NodePathWrapper.
        '''
        return self._nodePath


class NodePathWrapperTemplate(ComponentTemplate):
    '''
    NodePathWrapper Template.
    '''

    def _make_component(self, compId):
        newComp = NodePathWrapper(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(NodePathWrapper.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Scene')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # sets the (mandatory) parameters to their default values:
        # no mandatory parameters
