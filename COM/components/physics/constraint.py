'''
Created on Sep 20, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...com.object import ObjectId
from ...com.objectTemplateManager import ObjectTemplateManager
from ...tools.utilities import StringSequence
from .rigidBody import RigidBody
from ely.libtools import ValueList_string, ValueList_NodePath
from ely.physics import GamePhysicsManager as elyMgr


class Constraint(Component):
    '''
    Constraint Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *object_a*       |single| - | -
    | *object_b*       |single| - | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3Constraint = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.CONSTRAINT)
        # object_a/object_b
        self._objectAId = ObjectId(self._tmpl.parameter('object_a'))
        self._objectBId = ObjectId(self._tmpl.parameter('object_b'))
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3Ghost
        for paramName, paramValue in self._tmpl._parameterTable.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        self._p3Constraint = elyMgr.get_global_ptr().create_constraint(
            self._compId.value)
        # set constraint's owner objects
        # get object_a/object_b for the underlying Ely component
        objTmplMgr = ObjectTemplateManager.get_global_ptr()
        objectA = objTmplMgr.get_created_object(self._objectAId)
        if objectA:
            compA = objectA.get_component(ComponentFamilyType('Physics'))
            if isinstance(compA, RigidBody):
                objectANP = compA.p3RigidBody
        #
        objectB = objTmplMgr.get_created_object(self._objectBId)
        if objectB:
            compB = objectB.get_component(ComponentFamilyType('Physics'))
            if isinstance(compB, RigidBody):
                objectBNP = compB.p3RigidBody
        #
        objects = ValueList_NodePath([objectANP, objectBNP])
        self._p3Constraint.node().set_owner_objects(objects)
        # set tag of underlying Ely component to the object
        self._p3Constraint.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3Constraint.clear_tag('_owner_object')
        #
        self._p3Constraint.node().set_owner_objects(ValueList_NodePath())
        elyMgr.get_global_ptr().destroy_constraint(self._p3Constraint)
        self._p3Constraint = None

    def _on_add_to_scene_setup(self):
        self._p3Constraint.node().setup()

    def _on_remove_from_scene_cleanup(self):
        self._p3Constraint.node().cleanup()

    @property
    def p3Constraint(self):
        return self._p3Constraint


class ConstraintTemplate(ComponentTemplate):
    '''
    Constraint Template.
    '''

    def _make_component(self, compId):
        newComp = Constraint(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(Constraint.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Physics')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.CONSTRAINT):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.CONSTRAINT, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.CONSTRAINT)
