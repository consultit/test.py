'''
Created on Sep 6, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...com.object import ObjectId
from ...com.objectTemplateManager import ObjectTemplateManager
from ...tools.utilities import StringSequence
from panda3d.core import NodePath
from ely.libtools import ValueList_string
from ely.physics import GamePhysicsManager as elyMgr


class Ghost(Component):
    '''
    Ghost Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *use_shape_of*       |single| - | -

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3Ghost = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.GHOST)
        # use shape of object Id
        self._useShapeOfId = ObjectId(self._tmpl.parameter('use_shape_of'))
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3Ghost
        for paramName, paramValue in self._tmpl._parameterTable.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        self._p3Ghost = elyMgr.get_global_ptr().create_ghost(
            self._compId.value)
        self._p3Ghost.node().owner_object = self._ownerObject.node_path
        # set tag of underlying Ely component to the object
        self._p3Ghost.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3Ghost.clear_tag('_owner_object')
        #
        self._p3Ghost.node().owner_object = NodePath()
        elyMgr.get_global_ptr().destroy_ghost(self._p3Ghost)
        self._p3Ghost = None

    def _on_add_to_scene_setup(self):
        useShapeOfObject = ObjectTemplateManager.get_global_ptr().get_created_object(
            self._useShapeOfId)
        if useShapeOfObject:
            ghostComp = useShapeOfObject.get_component(ComponentType('Ghost'))
            if ghostComp:
                self._p3Ghost.node().use_shape_of = ghostComp.p3Ghost
        #
        self._p3Ghost.node().setup()

    def _on_remove_from_scene_cleanup(self):
        self._p3Ghost.node().cleanup()

    @property
    def p3Ghost(self):
        return self._p3Ghost


class GhostTemplate(ComponentTemplate):
    '''
    Ghost Template.
    '''

    def _make_component(self, compId):
        newComp = Ghost(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(Ghost.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Physics')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.GHOST):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.GHOST, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.GHOST)
