'''
Created on Sep 6, 2019

@author: consultit
'''

from ...com.component import Component, ComponentTemplate, ComponentType, \
    ComponentFamilyType
from ...tools.utilities import StringSequence, parse_compound_string
from ...managers.gameDataInfo import GameDataInfo
from ely.libtools import ValueList_string
from ely.physics import GamePhysicsManager as elyMgr
from panda3d.core import NodePath
from pathlib import Path


class SoftBody(Component):
    '''
    SoftBody Component.

    The XML/JSON parameters are those of the underlying Ely component, and also
    the following:

    **XML/JSON Param(s)**:
    param | type | default | note
    ------|------|---------|-----
    | *tetgen_ele_face_node*    |single| - | specified as 'ele_file:face_file:node_file'

    Note: parts inside [] are optional.\n
    '''

    def __init__(self, tmpl, compId):
        '''
        Constructor
        '''
        super().__init__(tmpl)
        self._compId = compId
        self._p3SoftBody = None

    def _reset(self):
        pass

    def _initialize(self):
        result = True
        # set parameters before creation
        self._set_parameters_before_creation(elyMgr.get_global_ptr(),
                                             elyMgr.SOFTBODY)
        # tetgen ele face node
        tetgenFileNames = parse_compound_string(
            self._tmpl.parameter('tetgen_ele_face_node'), ':')
        self.tetgenFilePathNames = ValueList_string()
        if len(tetgenFileNames) > 2:
            for fileName in tetgenFileNames[:3]:
                for path in self._tmpl._gameDataInfo[GameDataInfo.DATA_DIRS].split(','):
                    filePath = Path(path, fileName)
                    if filePath.is_file():
                        self.tetgenFilePathNames.add_value(str(filePath))
                        break
            if len(self.tetgenFilePathNames) < 3:
                self.tetgenFilePathNames.clear()
        #
        return result

    def _set_parameters_before_creation(self, mgr, compType):
        # TODO: skip parameters not handled by inner p3Ghost
        for paramName, paramValue in self._tmpl._parameterTable.items():
            if len(paramValue) == 1:
                mgr.set_parameter_value(compType, paramName, paramValue[0])
            else:
                valueList = ValueList_string()
                for value in paramValue:
                    valueList.add_value(value)
                mgr.set_parameter_values(compType, paramName, valueList)

    def _on_add_to_object_setup(self):
        self._p3SoftBody = elyMgr.get_global_ptr().create_soft_body(
            self._compId.value)
        self._p3SoftBody.node().owner_object = self._ownerObject.node_path
        # set tetgen data (if any)
        if self.tetgenFilePathNames:
            self._p3SoftBody.node().set_tetgen_ele_face_node(
                self.tetgenFilePathNames, True)
        # set tag of underlying Ely component to the object
        self._p3SoftBody.set_tag(
            '_owner_object', self._ownerObject.object_id.value)

    def _on_remove_from_object_cleanup(self):
        # remove tag of underlying Ely component to the object
        self._p3SoftBody.clear_tag('_owner_object')
        #
        self._p3SoftBody.node().owner_object = NodePath()
        elyMgr.get_global_ptr().destroy_soft_body(self._p3SoftBody)
        self._p3SoftBody = None

    def _on_add_to_scene_setup(self):
        self._p3SoftBody.node().setup()

    def _on_remove_from_scene_cleanup(self):
        self._p3SoftBody.node().cleanup()

    @property
    def p3SoftBody(self):
        return self._p3SoftBody


class SoftBodyTemplate(ComponentTemplate):
    '''
    SoftBody Template.
    '''

    def _make_component(self, compId):
        newComp = SoftBody(self, compId)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(SoftBody.__name__)

    @property
    def component_family_type(self):
        return ComponentFamilyType('Physics')

    def parameter_table_copy(self):
        '''
        Override the base ComponentTemplate's method.
        '''
        paramTabelDeepCopy = super().parameter_table_copy()
        for param in elyMgr.get_global_ptr().get_parameter_name_list(elyMgr.SOFTBODY):
            values = list(elyMgr.get_global_ptr().get_parameter_values(
                elyMgr.SOFTBODY, param))
            paramTabelDeepCopy[param] = StringSequence(values)
        return paramTabelDeepCopy

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        elyMgr.get_global_ptr().set_parameters_defaults(elyMgr.SOFTBODY)
