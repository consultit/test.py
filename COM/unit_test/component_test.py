'''
Created on Jun  2, 2019

@author: consultit
'''

import unittest
from COM.com.component import ComponentType, ComponentFamilyType, \
    ComponentId, Component, ComponentTemplate
from COM.tools.utilities import ParameterTable, StringSequence, \
    ParameterNameValue, CheckArgType
from COM.managers.gameManager import GameManager


def setUpModule():
    global app
    app = GameManager()

    
def tearDownModule():
    global app
    del app


class ComponentRealTemplate(ComponentTemplate):

    def _make_component(self, compId):
        assert CheckArgType((compId, ComponentId))
        newComp = ComponentReal(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(ComponentReal.__name__)
 
    @property
    def component_family_type(self):
        return ComponentFamilyType('ComponentRealFamily')

    def set_parameters_defaults(self):
        
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # several ways to set parameters and corresponding values
        self._parameterTable['p1'] = StringSequence(['v11'])
        self._parameterTable['p2'] = StringSequence(['v21', 'v22'])
        self._parameterTable.insert(ParameterNameValue('p3', 'v31'))
        self._parameterTable.insert(ParameterNameValue('p1', 'v12'))


class ComponentReal(Component):
    
    def _reset(self):
        pass
    
    def _initialize(self):
        return True
    
    def _on_add_to_object_setup(self):
        return

    def _on_remove_from_object_cleanup(self):
        return
    
    def _on_add_to_scene_setup(self):
        return
    
    def _on_remove_from_scene_cleanup(self):
        return
    

class ComponentTemplateTEST(unittest.TestCase):

    def setUp(self):
        self.paramTable = ParameterTable({
            'pA1':StringSequence(['vA11', 'vA12']),
            'p2':StringSequence(['w21', 'w22'])})

    def tearDown(self):
        del self.paramTable

    def test(self):
        compTmpl = ComponentRealTemplate(app, app.window_framework, 'clbkLibName')
        compTmpl.set_parameters(self.paramTable)
        self.paramTable.update(compTmpl.parameter_table)
        self.assertEqual(compTmpl.parameter_table, self.paramTable)
        self.assertTrue(compTmpl.parameter_table is not self.paramTable)
        self.assertEqual(compTmpl.parameter('paramName'), str())
        self.assertEqual(compTmpl.parameter_list('paramName'), StringSequence())
        self.assertEqual(compTmpl.parameter('p1'), 'v11')
        self.assertEqual(compTmpl.parameter_list('p2'), StringSequence(['w21', 'w22']))
        self.assertEqual(compTmpl.panda_framework, app)
        self.assertEqual(compTmpl.window_framework, app.window_framework)


class ComponentTEST(unittest.TestCase):

    def setUp(self):
        self.tmpl = ComponentRealTemplate(app, app.window_framework, 'clbkLibName')

    def tearDown(self):
        del self.tmpl

    def test(self):
        compReal = self.tmpl._make_component(ComponentId('CompRealId'))
        self.assertEqual(compReal.component_id, 'CompRealId')
        self.assertEqual(compReal.component_type, 'ComponentReal')
        self.assertEqual(compReal.component_family_type, 'ComponentRealFamily')

        
def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((ComponentTemplateTEST('test'),))
    suite.addTests((ComponentTEST('test'),))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
