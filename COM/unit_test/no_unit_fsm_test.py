'''
Created on Sep 28, 2019

@author: consultit
'''

from direct.fsm.FSM import FSM
from direct.showbase.ShowBase import ShowBase
  
# class MyFSM(FSM):
  
      
def __init__(self):
    super(MyFSM, self).__init__('FSM')
  
  
def AenterA(self):
    print('enterA')
  
      
def AexitA(self):
    print('exitA')
  
      
def AfilterA(self, request, args):
    if request == 'b':
        return 'B'
    return None
  
  
def BenterB(self):
    print('enterB')
  
      
def BexitB(self):
    print('exitB')
  
  
def CenterC(self):
    print('enterC')
  
      
def CexitC(self):
    print('exitC')
  
  
  
def DenterD(self):
    print('enterD')
  
      
def DexitD(self):
    print('exitD')
  
  
if __name__ == '__main__':
    app = ShowBase()
    # Create dynamically a Class
    MyFSM_Methods = {
        '__init__': __init__,
        'enterA':AenterA,
        'exitA':AexitA,
        'filterA':AfilterA,
        'enterB':BenterB,
        'exitB': BexitB,
        'enterC':CenterC,
        'exitC':CexitC
    }
      
    MyFSM = type('MyFSM', (FSM,), MyFSM_Methods)
    fsm = MyFSM()
    #
    fsm.request('A')
    fsm.request('b')
    fsm.request('A')
    fsm.request('B')
    fsm.request('b')
    fsm.request('C')
    # Dynamically add a Method to a Class
    setattr(MyFSM, 'enterD', DenterD)
    setattr(MyFSM, 'exitD', DexitD)
    fsm.request('D')
    fsm.request('A')
    # free functions
    CexitC('self')
    CenterC('self')
    print(dir(fsm))
    app.run()  
