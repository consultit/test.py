'''
Created on Aug 26, 2019

@author: consultit
'''

import unittest
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.componentTemplateManager import ComponentTemplateManager
from COM.unit_test.com_decls import CompATmpl, CompBTmpl, CompB1Tmpl, \
    CompA, CompB1, CompB
from COM.com.object import ObjectTemplate, ObjectId, \
    ComponentParameterTableMap
from COM.com.object import ObjectType
from COM.com.component import ComponentType, ComponentFamilyType
from COM.managers.gameManager import GameManager
from COM.tools.utilities import ParameterTable, StringSequence
from COM.managers.gameDataInfo import GameDataInfo


def setUpModule():
    global app
    app = GameManager()
    app.data_info[GameDataInfo.INITIALIZATIONS] = 'FakeInitLib'
    app.data_info[GameDataInfo.CALLBACKS] = 'FakeClbkLib'
    ComponentTemplateManager()
    compTmplMgr = ComponentTemplateManager.get_global_ptr()
    compTmplMgr.add_component_template(CompATmpl(app, app.window_framework,
                                                 app.data_info))
    compTmplMgr.add_component_template(CompBTmpl(app, app.window_framework,
                                                 app.data_info))
    compTmplMgr.add_component_template(CompB1Tmpl(app, app.window_framework,
                                                  app.data_info))
    ObjectTemplateManager()

    
def tearDownModule():
    global app
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    del objTmplMgr
    compTmplMgr = ComponentTemplateManager.get_global_ptr()
    compTmplMgr.remove_component_template(ComponentType(CompA.__name__))
    compTmplMgr.remove_component_template(ComponentType(CompB.__name__))
    compTmplMgr.remove_component_template(ComponentType(CompB1.__name__))
    del compTmplMgr
    del app


class COMTEST(unittest.TestCase):

    def setUp(self):
        objTmplMgr = ObjectTemplateManager.get_global_ptr()
        compTmplMgr = ComponentTemplateManager.get_global_ptr()
        compAType = ComponentType(CompA.__name__)
        compBType = ComponentType(CompB.__name__)
        compB1Type = ComponentType(CompB1.__name__)
        # Obj1Type objects will have one compAType and one compBType components
        obj1Tmpl = ObjectTemplate(ObjectType('Obj1Type'), objTmplMgr, app,
                                  app.window_framework, app.data_info)
        obj1Tmpl.add_component_template(
            compTmplMgr.get_component_template(compAType))
        obj1Tmpl.add_component_template(
            compTmplMgr.get_component_template(compBType))
        # Obj1Type objects will have one compAType and one compB1Type components
        obj2Tmpl = ObjectTemplate(ObjectType('Obj2Type'), objTmplMgr, app,
                                  app.window_framework, app.data_info)
        obj2Tmpl.add_component_template(
            compTmplMgr.get_component_template(compAType))
        obj2Tmpl.add_component_template(
            compTmplMgr.get_component_template(compB1Type))
        #
        objTmplMgr.add_object_template(obj2Tmpl)
        objTmplMgr.add_object_template(obj1Tmpl)
        
    def tearDown(self):
        objTmplMgr = ObjectTemplateManager.get_global_ptr()
        objTmplMgr.destroy_all_objects()
        obj1Type = ObjectType('Obj1Type')
        obj2Type = ObjectType('Obj2Type')
        obj1Tmpl = objTmplMgr.get_object_template(obj1Type)
        obj2Tmpl = objTmplMgr.get_object_template(obj2Type)
        objTmplMgr.remove_object_template(obj1Type)
        objTmplMgr.remove_object_template(obj2Type)
        obj1Tmpl.clear_component_templates()
        obj2Tmpl.clear_component_templates()
        
    def test1(self):
        # test manager
        objTmplMgr = ObjectTemplateManager.get_global_ptr()
        obj1Type = ObjectType('Obj1Type')
        obj2Type = ObjectType('Obj2Type')
        objTmplMgr.create_object(obj1Type, ObjectId('obj11'))
        objTmplMgr.create_object(obj1Type, ObjectId('obj11'))
        objTmplMgr.create_object(obj2Type)
        objTmplMgr.create_object(obj2Type)
        self.assertEqual(len(objTmplMgr.created_objects), 3)
        objTmplMgr.create_object(obj1Type, ObjectId('obj12'))
        self.assertEqual(len(objTmplMgr.created_objects), 4)
        self.assertEqual(objTmplMgr.get_created_object(
            ObjectId('obj11')).object_id, 'obj11')
        type1ObjList = [obj for obj in objTmplMgr.created_objects 
                        if obj.object_type == 'Obj1Type']
        self.assertEqual(len(type1ObjList), 2)
        for obj in type1ObjList:
            objTmplMgr.destroy_object(obj.object_id)
        self.assertEqual(len(objTmplMgr.created_objects), 2)
        self.assertEqual(len([obj for obj in objTmplMgr.created_objects 
                              if obj.object_type == 'Obj2Type']), 2)

    def test2(self):
        # test objects advanced
        compAType = ComponentType(CompA.__name__)
        compBType = ComponentType(CompB.__name__)
        objectParams = ParameterTable({'po1':StringSequence(['vo11', 'vo12']),
                                       'po2':StringSequence(['vo21', 'vo22'])})
        compParamTableMap = ComponentParameterTableMap({
            compAType:ParameterTable({'pA1':StringSequence(['vA1', 'vA2'])}),
            compBType:ParameterTable({'pB1':StringSequence(['vB1', 'vB2'])})})
        objTmplMgr = ObjectTemplateManager.get_global_ptr()
        obj1 = objTmplMgr.create_object(ObjectType('Obj1Type'),
                                        objId=ObjectId('obj1'),
                                        objectParams=objectParams,
                                        compParamTableMap=compParamTableMap,
                                        storeParams=True)
        self.assertEqual(obj1.num_components, 2)
        compA = obj1.get_component(compAType)
        compB = obj1.get_component(ComponentFamilyType('FamilyBType'))
        self.assertEqual(compA.component_family_type,
                         ComponentFamilyType('FamilyAType'))
        self.assertEqual(compB.component_type, compBType)
        # add component
        compB1Type = ComponentType(CompB1.__name__)
        objTmplMgr.add_component_to_object(obj1.object_id, compB1Type,
            componentParams=compParamTableMap[compBType])
        self.assertEqual(obj1.num_components, 2)
        compB1 = obj1.get_component(ComponentFamilyType('FamilyBType'))
        self.assertEqual(compB1.component_type, compB1Type)
        # owned objects
        obj2 = objTmplMgr.create_object(ObjectType('Obj2Type'), owner=obj1)
        objTmplMgr.destroy_all_objects(owned=False)
        self.assertEqual(len(objTmplMgr.created_objects), 1)
        objTmplMgr.destroy_all_objects()
        self.assertEqual(len(objTmplMgr.created_objects), 0)
        

def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((COMTEST('test1'),
                   (COMTEST('test2'))))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
