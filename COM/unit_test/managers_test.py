'''
Created on Aug 1, 2019

@author: consultit
'''

import unittest
from COM.tools.singleton import Singleton
from COM.com.componentTemplateManager import ComponentTemplateManager
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.unit_test.component_test import ComponentRealTemplate, \
    ComponentReal
from COM.com.component import ComponentType
from COM.com.object import ObjectTemplate, ObjectType, Object
from COM.managers.gameManager import GameManager
from COM.managers.gameDataInfo import GameDataInfo


def setUpModule():
    global app
    app = GameManager()
    app.data_info[GameDataInfo.INITIALIZATIONS] = 'FakeInitLib'
    app.data_info[GameDataInfo.CALLBACKS] = 'FakeClbkLib'

    
def tearDownModule():
    global app
    del app


class ComponentTemplateManagerTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test1(self):
        # test Singleton
        cmngr = ComponentTemplateManager()
        self.assertTrue(cmngr is cmngr.get_global_ptr())
        cmngr1 = ComponentTemplateManager()
        self.assertTrue(cmngr is cmngr1)
        self.assertTrue(cmngr is cmngr1.get_global_ptr())
        self.assertTrue(cmngr is ComponentTemplateManager.get_global_ptr())

        #
        class S(metaclass=Singleton):
            pass

        s = S()
        self.assertFalse(type(cmngr) is type(s))
        self.assertFalse(cmngr is s)

    def test2(self):
        global app
        # test methods
        cmngr = ComponentTemplateManager()
        compTmpl = ComponentRealTemplate(app, app.window_framework,
                                         app.data_info)
        compType = compTmpl.component_type
        ComponentTemplateManager.get_global_ptr().add_component_template(
            compTmpl)
        self.assertEqual(compTmpl, cmngr.get_component_template(compType))
        cmngr.remove_component_template(compType)
        self.assertEqual(len(cmngr._componentTemplates), 0)
        cmngr.add_component_template(compTmpl)
        # creation
        comp = cmngr._do_create_component(compType, True)
        self.assertTrue(isinstance(comp, ComponentReal))
        self.assertEqual(comp.component_type, compType)


class ObjectTemplateManagerTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test1(self):
        # test Singleton
        omngr = ObjectTemplateManager()
        self.assertTrue(omngr is ObjectTemplateManager.get_global_ptr())
        omngr1 = ObjectTemplateManager()
        self.assertTrue(omngr is omngr1)
        self.assertTrue(omngr is omngr1.get_global_ptr())
        self.assertTrue(omngr is ObjectTemplateManager.get_global_ptr())

        #
        class S(metaclass=Singleton):
            pass

        s = S()
        self.assertFalse(type(omngr) is type(s))
        self.assertFalse(omngr is s)

    def test2(self):
        global app
        omngr = ObjectTemplateManager()
        cmngr = ComponentTemplateManager()
        objType = ObjectType('ObjType')
        objTmpl = ObjectTemplate(objType, omngr, app, app.window_framework,
                                 app.data_info)
        compType = ComponentType('ComponentReal')
        objTmpl.add_component_template(cmngr.get_component_template(compType))
        omngr.add_object_template(objTmpl)
        #
        self.assertEqual(omngr.get_object_template(objType), objTmpl)
        obj = ObjectTemplateManager.get_global_ptr().create_object(objType)
        self.assertTrue(isinstance(obj, Object))
        self.assertEqual(omngr.get_object_template(objType), objTmpl)
        self.assertEqual(omngr.get_created_object(obj.object_id), obj)
        self.assertTrue(omngr.destroy_object(obj.object_id))
        self.assertFalse(omngr.created_objects)
        omngr.remove_object_template(ObjectType('ObjType'))
        self.assertEqual(len(omngr._objectTemplates), 0)

                
def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((ComponentTemplateManagerTEST('test1'),
                    ComponentTemplateManagerTEST('test2')))
    suite.addTests((ObjectTemplateManagerTEST('test1'),
                    ObjectTemplateManagerTEST('test2')))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
