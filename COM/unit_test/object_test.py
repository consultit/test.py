'''
Created on Aug  19, 2019

@author: consultit
'''

import unittest
from COM.com.objectTemplateManager import ObjectTemplateManager
from COM.com.object import ObjectTemplate, ObjectType, Object, ObjectId
from COM.com.component import ComponentType, ComponentFamilyType
from COM.tools.utilities import ParameterTable, StringSequence
from COM.unit_test.component_test import ComponentRealTemplate
from COM.managers.gameManager import GameManager


def setUpModule():
    global app
    ObjectTemplateManager()
    app = GameManager()


def tearDownModule():
    global app
    objTmplMgr = ObjectTemplateManager.get_global_ptr()
    del objTmplMgr
    del app


class ObjectTemplateTEST(unittest.TestCase):

    def setUp(self):
        self.paramTable = ParameterTable({
            'pA1': StringSequence(['vA11', 'vA12']),
            'is_steady': StringSequence(['true'])})

    def tearDown(self):
        del self.paramTable

    def test(self):
        objTmplMgr = ObjectTemplateManager.get_global_ptr()
        objTmpl = ObjectTemplate(ObjectType('Object'), objTmplMgr,
                                 app, app.win, 'initLibName')
        self.assertEqual(objTmpl.object_type, 'Object')
        self.assertEqual(len(objTmpl.component_templates), 0)
        compRealTmpl = ComponentRealTemplate(app, app.window_framework,
                                             'clbkLibName')
        objTmpl.add_component_template(compRealTmpl)
        self.assertEqual(len(objTmpl.component_templates), 1)
        compRealTmpl1 = ComponentRealTemplate(app, app.window_framework,
                                              'clbkLibName')
        objTmpl.add_component_template(compRealTmpl1)
        self.assertEqual(len(objTmpl.component_templates), 2)
        self.assertEqual(objTmpl.get_component_template(
            ComponentType('ComponentReal')), compRealTmpl)
        self.assertEqual(objTmpl.get_component_template(
            ComponentFamilyType('ComponentRealFamily')), compRealTmpl)
        objTmpl.clear_component_templates()
        self.assertEqual(len(objTmpl.component_templates), 0)
        #
        objTmpl.set_parameters(self.paramTable)
        self.paramTable.update(objTmpl.parameter_table)
        self.assertEqual(objTmpl.parameter_table, self.paramTable)
        self.assertTrue(objTmpl.parameter_table is not self.paramTable)
        self.assertEqual(objTmpl.parameter('paramName'), str())
        self.assertEqual(objTmpl.parameter_list('paramName'), list())
        self.assertEqual(objTmpl.parameter('is_steady'), 'true')
        self.assertEqual(objTmpl.parameter_list('pA1'),
                         StringSequence(['vA11', 'vA12']))
        self.assertEqual(objTmpl.parameter('rot'), '0.0,0.0,0.0')
        self.assertEqual(objTmpl.panda_framework, app)
        self.assertEqual(objTmpl.window_framework, app.win)
        #
        objTmpl.add_component_type_parameter('p1', 'v11:v12',
                                             ComponentType('ComponentReal'))
        objTmpl.add_component_type_parameter('p2', 'v21:v22',
                                             ComponentType('ComponentReal'))
        self.assertTrue(objTmpl.is_component_type_parameter_value(
            'p1', 'v12', ComponentType('ComponentReal')))
        self.assertTrue(objTmpl.is_component_type_parameter_value(
            'p2', 'v21', ComponentType('ComponentReal')))
        l1 = objTmpl.component_type_parameter_values(
            'p1', ComponentType('ComponentReal'))
        l2 = objTmpl.component_type_parameter_values(
            'p2', ComponentType('ComponentReal'))
        self.assertEqual(l1, StringSequence(['v11', 'v12']))
        self.assertEqual(l2, StringSequence(['v21', 'v22']))


class ObjectTEST(unittest.TestCase):

    def setUp(self):
        objTmplMgr = ObjectTemplateManager.get_global_ptr()
        self.objTmpl = ObjectTemplate(ObjectType('Object'), objTmplMgr,
                                      app, app.win, 'initLibName')
        self.objTmpl.add_component_template(
            ComponentRealTemplate(app, app.window_framework, 'clbkLibName'))

    def tearDown(self):
        del self.objTmpl

    def test(self):
        objTmpl = ObjectTemplate(ObjectType('ObjType'), ObjectTemplateManager(),
                                 app, app.win, 'initLibName')
        obj1 = Object(ObjectId('obj1'), objTmpl)
        self.assertEqual(obj1.get_component(ComponentType('CompType')), None)
        self.assertEqual(obj1.num_components, 0)
        self.assertEqual(obj1.object_id, 'obj1')
        self.assertEqual(obj1.object_type, 'ObjType')
        self.assertEqual(obj1.node_path.name, 'obj1')
        self.assertEqual(obj1.object_tmpl, objTmpl)
        self.assertFalse(obj1.is_steady)
        self.assertFalse(obj1.stored_obj_tmpl_params)
        self.assertFalse(obj1.stored_comp_tmpl_params)
        self.assertEqual(obj1.owner, None)


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTests((ObjectTemplateTEST('test'),))
    suite.addTests((ObjectTEST('test'),))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
