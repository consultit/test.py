'''
Created on Jun  2, 2019

@author: consultit
'''

import unittest
from COM.unit_test import suite
from COM.tools.utilities import BaseTable, BaseSequence, BaseType, \
    CheckArgType, ParameterTable, StringSequence, IdType, CheckTypeConversion, \
    Orderable, parse_compound_string, Relational
from collections.abc import Iterable
from heapq import heappush, heappop


def setUpModule():
    pass


def tearDownModule():
    pass


class RelationalTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):

        class Comparable(Relational):

            def __init__(self, v):
                self.v = v

            def __eq__(self, other):
                return self.v == other.v

            def __lt__(self, other):
                return self.v < other.v

        c1, c2 = Comparable(1), Comparable(2)
        self.assertFalse(c1 == c2)
        self.assertTrue(c1 != c2)
        self.assertTrue(c1 < c2)
        self.assertTrue(c1 <= c2)
        self.assertFalse(c1 > c2)
        self.assertFalse(c1 >= c2)


class BaseTypeTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def _suite1(self, Type):

        # test valid with ComponentType.valueType = str or numeric types
        class BadType:
            pass

        #
        badV = BadType()
        if not CheckTypeConversion(badV, Type):
            self.assertRaises(AssertionError, Type, badV)
        #
        _A, _B, _B2 = suite.RandInt32(), suite.RandInt32(), suite.RandInt32()
        if isinstance(Type.valueType()(), Iterable):
            A = Type.valueType()((_A,))
            B = Type.valueType()((_B,))
            B2 = Type.valueType()((_B2,))
        else:
            A = Type.valueType()(_A)
            B = Type.valueType()(_B)
            B2 = Type.valueType()(_B2)
        compTypeA = Type(A)
        compTypeA2 = Type(A)
        compTypeB = Type(B)
        self.assertEqual(compTypeA, compTypeA2, 'Equal A A2')
        self.assertNotEqual(compTypeA, compTypeB, 'NotEqual A B')
        self.assertEqual(compTypeA < compTypeB, A < B, 'Less A B')
        self.assertEqual(compTypeA > compTypeA2, A > A, 'LessEqual A A2')
        compTypeB2 = Type(B2)
        self.assertNotEqual(compTypeB, compTypeB2, 'NotEqual B B2')
        compTypeB2.assign(compTypeB)
        self.assertEqual(compTypeB, compTypeB2, 'Equal B B2')

    def _suite2(self, Type):
        # test hashable. Valid with Type.valueType = str or numeric types
        if isinstance(Type.valueType()(), Iterable):
            value1 = Type.valueType()((suite.RandInt32(),))
            value2 = Type.valueType()((suite.RandInt32(),))
        else:
            value1 = Type.valueType()(suite.RandInt32())
            value2 = Type.valueType()(suite.RandInt32())
        compType1 = Type(value1)
        compType2 = Type(value2)
        compType11 = Type(value1)
        self.assertNotEqual(compType1, compType2)
        self.assertEqual(compType1, compType11)
        table = {}
        table[compType1] = value1
        table.__setitem__(compType2, value2)
        self.assertNotEqual(table[compType1], table[compType2])
        self.assertEqual(table[compType1], table[compType11])
        self.assertNotEqual(id(compType1), id(compType2))
        # check emptyness
        self.assertFalse(Type())

    def testStr(self):

        class String(BaseType):

            @staticmethod
            def valueType():
                return str

        self._suite1(String)
        self._suite2(String)

    def testInt(self):

        class Integer(BaseType):

            @staticmethod
            def valueType():
                return int

        self._suite1(Integer)
        self._suite2(Integer)

    def testFloat(self):

        class Float(BaseType):

            @staticmethod
            def valueType():
                return float

        self._suite1(Float)
        self._suite2(Float)

    def testTuple(self):

        class Tuple(BaseType):

            @staticmethod
            def valueType():
                return tuple

        self._suite1(Tuple)
        self._suite2(Tuple)


class BaseTableTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def _setItem(self, t, k, v):
        t[k] = v

    def _suite(self, TableType, k, v, kBad, vBad, k1, v1):

        kvTable = TableType()
        # bad key
        item = {kBad: v}
        self.assertRaises(AssertionError, kvTable.update, item)
        self.assertRaises(AssertionError, kvTable.__setitem__, *(kBad, v))
        self.assertRaises(AssertionError, self._setItem, *(kvTable, kBad, v))
        # bad value
        item = {k: vBad}
        self.assertRaises(AssertionError, kvTable.update, item)
        self.assertRaises(AssertionError, kvTable.__setitem__, *(k, vBad))
        self.assertRaises(AssertionError, self._setItem, *(kvTable, k, vBad))
        #
        kvTable[k] = v
        self.assertEqual(kvTable[k], v)
        # test other methods
        self.assertEqual(kvTable.get(k), v)
        self.assertEqual(kvTable.get(k1, None), None)
        for kT, vT in kvTable.items():
            self.assertEqual((kT, vT), (k, v))
        for kT in kvTable.keys():
            self.assertEqual(kT, k)
        self.assertEqual(kvTable.pop(k), v)
        self.assertFalse(kvTable)
        self.assertEqual(len(kvTable), 0)
        self.assertFalse(kvTable)
        kvTable[k] = v
        self.assertEqual(kvTable.pop(k1, None), None)
        self.assertEqual(kvTable.popitem(), (k, v))
        self.assertEqual(len(kvTable), 0)
        self.assertFalse(kvTable)
        kvTable[k] = v
        self.assertEqual(kvTable.setdefault(k), v)
        self.assertEqual(kvTable.setdefault(k1, v1), v1)
        self.assertEqual(len(kvTable), 2)
        self.assertTrue(kvTable)
        vValues = kvTable.values()
        self.assertTrue((v in vValues) and (v1 in vValues))
        del kvTable[k1]
        self.assertTrue((v in vValues) and (v1 not in vValues))
        kvTable.clear()
        self.assertEqual(len(kvTable), 0)
        self.assertFalse(kvTable)
        # substitution
        kvTable[k] = v
        self.assertEqual(kvTable[k], v)
        kvTable[k] = v1
        self.assertEqual(kvTable[k], v1)
        kvTable.clear()
        # constructors
        d = {k: v, k1: v1}
        kvTable = TableType(d)
        self.assertEqual(len(kvTable), 2)
        self.assertTrue(kvTable)
        for item in kvTable.items():
            self.assertTrue(item[0] in d)
            self.assertTrue(item[1] in d.values())
        kvTable1 = TableType(kvTable)
        self.assertEqual(len(kvTable1), 2)
        self.assertTrue(kvTable)
        for item in kvTable1.items():
            self.assertTrue(item[0] in kvTable)
            self.assertTrue(item[1] in kvTable.values())
        self.assertTrue(kvTable is not kvTable1)
        self.assertRaises(AssertionError, TableType, {kBad: v, k1: v1})
        self.assertRaises(AssertionError, TableType, {k1: v1, k: vBad})
        # check emptyness
        self.assertFalse(TableType())
        self.assertFalse(TableType({}))

    def test1(self):
        '''
        Generic tests.
        '''

        class K:
            pass

        class V:
            pass

        class KVTable(BaseTable):
            '''
            test KVTable.
            '''

            def __init__(self, mapping=None):
                super().__init__(K, V, mapping)

        class BadK:
            pass

        class BadV:
            pass

        self._suite(KVTable, K(), V(), BadK(), BadV(), K(), V())
        # check Relational
        k = K()
        v = V()
        kvTable1 = KVTable({k: v})
        kvTable2 = KVTable({k: v})
        self.assertEqual(kvTable1, kvTable2)
        self.assertTrue(kvTable1 is not kvTable2)
        self.assertRaises(NotImplementedError, kvTable1.__lt__, kvTable2)
        self.assertRaises(NotImplementedError, kvTable2.__gt__, kvTable1)

    def test2(self):
        '''
        ParameterTable tests.
        '''

        k, v = 'X', StringSequence(('B', 'C'))
        kBad, vBad = b'X', ['B', 'C']
        k1, v1 = 'Y', StringSequence(('E', 'F'))
        self._suite(ParameterTable, k, v, kBad, vBad, k1, v1)


class BaseSequenceTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def _setItem(self, t, k, v):
        t[k] = v

    def test(self):

        class Value(object):
            pass

        class BadValue(object):
            pass

        class ValueSeq(BaseSequence):
            '''
            test of list of list.
            '''

            def __init__(self, sequence=None):
                super().__init__(Value, sequence)

        # test ParameterTable
        vSeq = ValueSeq()
        # bad value
        vBad = BadValue()
        self.assertRaises(AssertionError, vSeq.append, vBad)
        self.assertRaises(AssertionError, vSeq.__setitem__, *(0, vBad))
        self.assertRaises(AssertionError, vSeq.insert, *(0, vBad))
        #
        v = Value()
        vSeq.append(v)
        self.assertEqual(vSeq[0], v)
        # test other methods
        self.assertEqual(vSeq[0], v)
        self.assertEqual(vSeq[:][0], v)
        self.assertEqual(type(vSeq[:]), list)
        self.assertEqual(vSeq.count(v), 1)
        self.assertRaises(AssertionError, vSeq.extend, [vBad])
        vSeq1 = ValueSeq()
        v1 = Value()
        vSeq1.append(v1)
        vSeq.extend(vSeq1)
        self.assertEqual(len(vSeq), 2)
        self.assertTrue((v in vSeq) and (v1 in vSeq))
        self.assertEqual(vSeq.index(v), 0)
        self.assertEqual(vSeq.index(v1, 0, 2), 1)
        self.assertEqual(vSeq.pop(1), v1)
        self.assertEqual(len(vSeq), 1)
        vSeq.remove(v)
        self.assertEqual(len(vSeq), 0)
        vSeq.append(v)
        vSeq.append(v1)
        vSeq.reverse()
        self.assertTrue((vSeq[0] == v1) and (vSeq[1] == v))
        vSeq.clear()
        self.assertEqual(len(vSeq), 0)
        vSeq += [v]
        self.assertEqual(vSeq[0], v)
        vSeq += ValueSeq([v])
        self.assertTrue((vSeq[0] == v) and (vSeq[1] == v))
        # constructors
        vSeq = ValueSeq([v, v1])
        self.assertEqual(len(vSeq), 2)
        for vA, vB in zip(vSeq, [v, v1]):
            self.assertEqual(vA, vB)
        vSeqRev = [vA for vA in reversed(vSeq)]
        self.assertEqual(vSeqRev[0], v1)
        self.assertEqual(vSeqRev[1], v)
        vSeq1 = ValueSeq(vSeq)
        self.assertEqual(len(vSeq1), 2)
        for vA, vB in zip(vSeq, vSeq1):
            self.assertEqual(vA, vB)
        self.assertTrue(vSeq is not vSeq1)
        self.assertRaises(AssertionError, ValueSeq, [vBad])
        # check Relational
        vSeq1 = ValueSeq([v, v1])
        vSeq2 = ValueSeq([v, v1])
        self.assertEqual(vSeq1, vSeq2)
        self.assertTrue(vSeq1 is not vSeq2)
        vSeq3 = ValueSeq([v1, v])
        self.assertRaises(TypeError, vSeq1.__lt__, vSeq3)
        self.assertRaises(TypeError, vSeq1.__gt__, vSeq3)
        self.assertTrue(ValueSeq([v]))
        self.assertTrue(vSeq1)
        # check emptyness
        self.assertFalse(ValueSeq())
        self.assertFalse(ValueSeq([]))

        # #
        class IntSeq(BaseSequence):

            def __init__(self, sequence=None):
                super().__init__(int, sequence)

        iSeq3 = IntSeq([1, 2])
        iSeq4 = IntSeq([1, 3])
        self.assertTrue(iSeq3 < iSeq4)
        self.assertTrue(iSeq4 > iSeq3)


class IdTypeTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        idT = IdType()
        self.assertEqual(str(idT), '0')
        self.assertEqual(str(idT.incr()), '1')
        self.assertEqual(str(idT.incr()), '2')
        self.assertEqual(str(idT.decr()), '1')
        self.assertEqual(str(idT.decr()), '0')


class CheckArgTypeTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):

        class T1:
            pass

        class T2:
            pass

        t1 = T1()
        t2 = T2()
        res = CheckArgType((t1, T1), (t2, T2))
        self.assertTrue(res)
        self.assertTrue(res[t1] and res[t2])
        #
        res = CheckArgType((t1, T2), (t2, T1))
        self.assertFalse(res)
        self.assertFalse(res[t1] or res[t2])


class OrderableTEST(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):

        class Obj:

            def __init__(self, prio):
                self._prio = prio

            def __repr__(self):
                return str(self._prio)

        N = 500
        ordList = []
        storage = []
        for _ in range(N):
            prio = abs(suite.RandInt32(1000))
            ordList.append(prio)
            ordObj = Orderable(ptr=Obj(prio), prio=prio)
            heappush(storage, ordObj)
        ordList = sorted(ordList)
        # check Orderable
        for _ in range(N):
            self.assertEqual(ordList.pop(0), heappop(storage).prio)


class ParseTEST(unittest.TestCase):
    '''
    #include <vector>
    #include <string>
    #include <cstring>
    #include <iostream>

    std::string eraseCharacter(const std::string &source, int character)
    {
        int len = source.size() + 1;
        char *dest = new char[len];
        char *start = dest;
        strncpy(dest, source.c_str(), len);
        //erase
        char *pch;
        pch = strchr(dest, character);
        while (pch != NULL)
        {
            len -= pch - start;
            memmove(pch, pch + 1, len - 1);
            start = pch;
            //continue
            pch = strchr(pch, character);
        }
        std::string outStr(dest);
        delete[] dest;
        return outStr;
    }

    std::vector<std::string> parseCompoundString(
            const std::string &srcCompoundString, char separator)
    {
        //erase blanks
        std::string compoundString = srcCompoundString;
        compoundString = eraseCharacter(compoundString, ' ');
        compoundString = eraseCharacter(compoundString, '\t');
        compoundString = eraseCharacter(compoundString, '\n');
        //parse
        std::vector<std::string> substrings;
        int len = compoundString.size() + 1;
        char *dest = new char[len];
        strncpy(dest, compoundString.c_str(), len);
        //find
        char *pch;
        char *start = dest;
        bool stop = false;
        while (not stop)
        {
            std::string substring("");
            pch = strchr(start, separator);
            if (pch != NULL)
            {
                //insert the substring
                substring.append(start, pch - start);
                start = pch + 1;
                substrings.push_back(substring);
            }
            else
            {
                if (start < &dest[len - 1])
                {
                    //insert the last not empty substring
                    substring.append(start, &dest[len - 1] - start);
                    substrings.push_back(substring);
                }
                else if (start == &dest[len - 1])
                {
                    //insert the last empty substring
                    substrings.push_back(substring);
                }
                stop = true;
            }
        }
        delete[] dest;
        //
        return substrings;
    }

    int main(int argc, char **argv)
    {
        std::string str1 = "\t:  alfa: beta : gamma: : delta: \t  ";
        auto parsed = parseCompoundString(str1, ':');
        std::cout << (parsed.size() == 7) << std::endl;
        std::cout << (parsed[0] == std::string("")) << std::endl;
        std::cout << (parsed[1] == std::string("alfa")) << std::endl;
        std::cout << (parsed[2] == std::string("beta")) << std::endl;
        std::cout << (parsed[3] == std::string("gamma")) << std::endl;
        std::cout << (parsed[4] == std::string("")) << std::endl;
        std::cout << (parsed[5] == std::string("delta")) << std::endl;
        std::cout << (parsed[6] == std::string("")) << std::endl;
        return 0;
    }
    '''

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        str1 = '\t:  alfa: beta : gamma: : delta: \t  '
        parsed = parse_compound_string(str1, ':')
        self.assertEqual(len(parsed), 7)
        self.assertEqual(parsed[0], '')
        self.assertEqual(parsed[1], 'alfa')
        self.assertEqual(parsed[2], 'beta')
        self.assertEqual(parsed[3], 'gamma')
        self.assertEqual(parsed[4], '')
        self.assertEqual(parsed[5], 'delta')
        self.assertEqual(parsed[6], '')


def makeSuite():
    suite = unittest.TestSuite()
    suite.addTest(RelationalTEST('test'))
    suite.addTests((BaseTypeTEST('testStr'),
                    BaseTypeTEST('testInt'),
                    BaseTypeTEST('testFloat'),
                    BaseTypeTEST('testTuple')))
    suite.addTests((BaseTableTEST('test1'),
                    BaseTableTEST('test2')))
    suite.addTest(BaseSequenceTEST('test'))
    suite.addTest(IdTypeTEST('test'))
    suite.addTest(CheckArgTypeTEST('test'))
    suite.addTest(OrderableTEST('test'))
    suite.addTest(ParseTEST('test'))
    return suite


if __name__ == '__main__':
    unittest.TextTestRunner().run(makeSuite())  # option 1
#     unittest.main()# option 2
