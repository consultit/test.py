'''
Created on Aug 26, 2019

@author: consultit
'''

from COM.com.component import ComponentTemplate, ComponentId, \
    ComponentType, ComponentFamilyType, Component
from COM.tools.utilities import CheckArgType, StringSequence, \
    ParameterNameValue


# FamilyAType components
class CompATmpl(ComponentTemplate):

    def _make_component(self, compId):
        assert CheckArgType((compId, ComponentId))
        newComp = CompA(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(CompA.__name__)
 
    @property
    def component_family_type(self):
        return ComponentFamilyType('FamilyAType')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # several ways to set parameters and corresponding values
        self._parameterTable['pA1'] = StringSequence(['vA11'])
        self._parameterTable['pA2'] = StringSequence(['vA21', 'vA22'])
        self._parameterTable.insert(ParameterNameValue('pA3', 'vA31'))
        self._parameterTable.insert(ParameterNameValue('pA1', 'vA12'))


class CompA(Component):
    
    def _reset(self):
        print(self.__class__.__name__ + '._reset')
    
    def _initialize(self):
        print(self.__class__.__name__ + '._initialize')
        return True
    
    def _on_add_to_object_setup(self):
        print(self.__class__.__name__ + '._on_add_to_object_setup')

    def _on_remove_from_object_cleanup(self):
        print(self.__class__.__name__ + '._on_remove_from_object_cleanup')
    
    def _on_add_to_scene_setup(self):
        print(self.__class__.__name__ + '._on_add_to_scene_setup')
        return
    
    def _on_remove_from_scene_cleanup(self):
        print(self.__class__.__name__ + '._on_remove_from_scene_cleanup')
        return


# FamilyBType components
class CompBTmpl(ComponentTemplate):

    def _make_component(self, compId):
        assert CheckArgType((compId, ComponentId))
        newComp = CompB(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(CompB.__name__)
 
    @property
    def component_family_type(self):
        return ComponentFamilyType('FamilyBType')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # several ways to set parameters and corresponding values
        self._parameterTable['pB1'] = StringSequence(['vB11'])
        self._parameterTable['pB2'] = StringSequence(['vB21', 'vB22'])
        self._parameterTable.insert(ParameterNameValue('pB3', 'vB31'))
        self._parameterTable.insert(ParameterNameValue('pB1', 'vB12'))


class CompB(Component):
    
    def _reset(self):
        print(self.__class__.__name__ + '._reset')
    
    def _initialize(self):
        print(self.__class__.__name__ + '._initialize')
        return True
    
    def _on_add_to_object_setup(self):
        print(self.__class__.__name__ + '._on_add_to_object_setup')

    def _on_remove_from_object_cleanup(self):
        print(self.__class__.__name__ + '._on_remove_from_object_cleanup')
    
    def _on_add_to_scene_setup(self):
        print(self.__class__.__name__ + '._on_add_to_scene_setup')
        return
    
    def _on_remove_from_scene_cleanup(self):
        print(self.__class__.__name__ + '._on_remove_from_scene_cleanup')
        return


class CompB1Tmpl(ComponentTemplate):

    def _make_component(self, compId):
        assert CheckArgType((compId, ComponentId))
        newComp = CompB1(self)
        newComp._set_component_id(compId)
        if not newComp._initialize():
            return None
        return newComp

    @property
    def component_type(self):
        return ComponentType(CompB1.__name__)
 
    @property
    def component_family_type(self):
        return ComponentFamilyType('FamilyBType')

    def set_parameters_defaults(self):
        # self._parameterTable must be the first cleared
        self._parameterTable.clear()
        # several ways to set parameters and corresponding values
        self._parameterTable['pB1_1'] = StringSequence(['vB1_11'])
        self._parameterTable['pB1_2'] = StringSequence(['vB1_21', 'vB1_22'])
        self._parameterTable.insert(ParameterNameValue('pB1_3', 'vB1_31'))
        self._parameterTable.insert(ParameterNameValue('pB1_1', 'vB1_12'))


class CompB1(Component):
    
    def _reset(self):
        print(self.__class__.__name__ + '._reset')
    
    def _initialize(self):
        print(self.__class__.__name__ + '._initialize')
        return True
    
    def _on_add_to_object_setup(self):
        print(self.__class__.__name__ + '._on_add_to_object_setup')

    def _on_remove_from_object_cleanup(self):
        print(self.__class__.__name__ + '._on_remove_from_object_cleanup')
    
    def _on_add_to_scene_setup(self):
        print(self.__class__.__name__ + '._on_add_to_scene_setup')
        return
    
    def _on_remove_from_scene_cleanup(self):
        print(self.__class__.__name__ + '._on_remove_from_scene_cleanup')
        return

