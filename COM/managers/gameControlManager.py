'''
Created on Sep 4, 2019

@author: consultit
'''

from ..tools.singleton import Singleton
from ..tools.utilities import BaseSequence
from ..com.component import Component
from panda3d.core import ClockObject, GeomNode
try:
    from ely.control import GameControlManager as elyMgr
except ModuleNotFoundError:
    elyMgr = None


class _ControlComponentSequence(BaseSequence):
    '''
    Sequence of control components.
    '''

    def __init__(self, sequence=None):
        super().__init__(Component, sequence)


class GameControlManager(metaclass=Singleton):
    '''
    Singleton manager updating control components.
    '''

    def __init__(self, app, sort=0, priority=0, asyncTaskChain=None,
                 mask=GeomNode.get_default_collide_mask()):
        '''
        Constructor.

        Param app: The application framework.
        Param sort: The task sort.
        Param priority: The task priority.
        Param asyncTaskChain: The task chain.
        Param mask: The collision mask.
        '''
        self._taskMgr = app.taskMgr
        # List of control components to be updated.
        self._controlComponents = _ControlComponentSequence()
        # A task data for update.
        if asyncTaskChain:
            self._taskMgr.setupTaskChain(asyncTaskChain, numThreads=None,
                                         tickClock=None, threadPriority=None,
                                         frameBudget=None, frameSync=None,
                                         timeslicePriority=None)
        self._taskMgr.add(self.update, 'updateControl', sort=sort,
                          priority=priority, appendTask=True,
                          taskChain=asyncTaskChain)
        # create elyMgr
        if elyMgr:
            self._elyMgr = elyMgr(app.win, taskSort=sort, root=app.render,
                                  mask=mask)
            self._elyMgr.set_reference_node_path(app.render)

    def __del__(self):
        '''
        Destructor.
        '''
        self._taskMgr.remove('updateControl')
        self._controlComponents.clear()
        # destroy elyMgr
        if elyMgr:
            del self._elyMgr

    def add_to_control_update(self, controlComp):
        '''
        Adds (if not present) an control component to updating.

        Param controlComp: The control component.
        '''
        if controlComp not in self._controlComponents:
            self._controlComponents.append(controlComp)

    def remove_from_control_update(self, controlComp):
        '''
        Removes (if present) an control component from updating.

        Param controlComp: The control component.
        '''
        if controlComp in self._controlComponents:
            self._controlComponents.remove(controlComp)

    def update(self, task):
        '''
        Updates control components.

        Will be called automatically in a task.
        Param task: The task.
        Return: The 'done' status.
        '''
        dt = ClockObject.get_global_clock().get_dt()
        # call all control components update functions, passing delta time
        for controlComp in self._controlComponents:
            controlComp.update(dt)
        if self._elyMgr:
            self._elyMgr.update()
        return task.cont
