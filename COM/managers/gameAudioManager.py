'''
Created on Sep 4, 2019

@author: consultit
'''

from ..tools.singleton import Singleton
from ..tools.utilities import BaseSequence
from ..com.component import Component
from panda3d.core import ClockObject, GeomNode
try:
    from ely.audio import GameAudioManager as elyMgr
except ModuleNotFoundError:
    elyMgr = None


class _AudioComponentSequence(BaseSequence):
    '''
    Sequence of audio components.
    '''

    def __init__(self, sequence=None):
        super().__init__(Component, sequence)


class GameAudioManager(metaclass=Singleton):
    '''
    Singleton manager updating audio components.
    '''

    def __init__(self, app, sort=0, priority=0, asyncTaskChain=None,
                 mask=GeomNode.get_default_collide_mask()):
        '''
        Constructor.

        Param app: The application framework.
        Param sort: The task sort.
        Param priority: The task priority.
        Param asyncTaskChain: The task chain.
        Param mask: The collision mask.
        '''
        self._taskMgr = app.taskMgr
        # List of audio components to be updated.
        self._audioComponents = _AudioComponentSequence()
        # A task data for update.
        if asyncTaskChain:
            self._taskMgr.setupTaskChain(asyncTaskChain, numThreads=None, tickClock=None,
                                         threadPriority=None, frameBudget=None, frameSync=None, timeslicePriority=None)
        self._taskMgr.add(self.update, 'updateAudio', sort=sort,
                          priority=priority, appendTask=True, taskChain=asyncTaskChain)
        # create elyMgr
        if elyMgr:
            self._elyMgr = elyMgr(taskSort=sort, root=app.render, mask=mask)
            self._elyMgr.set_reference_node_path(app.render)

    def __del__(self):
        '''
        Destructor.
        '''
        self._taskMgr.remove('updateAudio')
        self._audioComponents.clear()
        # destroy elyMgr
        if elyMgr:
            del self._elyMgr

    def add_to_audio_update(self, audioComp):
        '''
        Adds (if not present) an audio component to updating.

        Param audioComp: The audio component.
        '''
        if audioComp not in self._audioComponents:
            self._audioComponents.append(audioComp)

    def remove_from_audio_update(self, audioComp):
        '''
        Removes (if present) an audio component from updating.

        Param audioComp: The audio component.
        '''
        if audioComp in self._audioComponents:
            self._audioComponents.remove(audioComp)

    def update(self, task):
        '''
        Updates audio components.

        Will be called automatically in a task.
        Param task: The task.
        Return: The 'done' status.
        '''
        dt = ClockObject.get_global_clock().get_dt()
        # call all audio components update functions, passing delta time
        for audioComp in self._audioComponents:
            audioComp.update(dt)
        if self._elyMgr:
            self._elyMgr.update()
        return task.cont
