'''
Created on Sep 27, 2019

@author: consultit
'''

from ..tools.singleton import Singleton
from ..tools.utilities import BaseSequence
from ..com.component import Component
from panda3d.core import ClockObject


class _BehaviorComponentSequence(BaseSequence):
    '''
    Sequence of behavior components.
    '''

    def __init__(self, sequence=None):
        super().__init__(Component, sequence)


class GameBehaviorManager(metaclass=Singleton):
    '''
    Singleton manager updating behavior components.
    '''

    def __init__(self, app, sort=0, priority=0, asyncTaskChain=None):
        '''
        Constructor.

        Param app: The application framework.
        Param sort: The task sort.
        Param priority: The task priority.
        Param asyncTaskChain: The task chain.
        '''
        self._taskMgr = app.taskMgr
        # List of behavior components to be updated.
        self._behaviorComponents = _BehaviorComponentSequence()
        # A task data for update.
        if asyncTaskChain:
            self._taskMgr.setupTaskChain(asyncTaskChain, numThreads=None,
                                         tickClock=None, threadPriority=None,
                                         frameBudget=None, frameSync=None,
                                         timeslicePriority=None)
        self._taskMgr.add(self.update, 'updateBehavior', sort=sort, priority=priority,
                          appendTask=True, taskChain=asyncTaskChain)

    def __del__(self):
        '''
        Destructor.
        '''
        self._taskMgr.remove('updateBehavior')
        self._behaviorComponents.clear()

    def add_to_behavior_update(self, behaviorComp):
        '''
        Adds (if not present) an behavior component to updating.

        Param behaviorComp: The behavior component.
        '''
        if behaviorComp not in self._behaviorComponents:
            self._behaviorComponents.append(behaviorComp)

    def remove_from_behavior_update(self, behaviorComp):
        '''
        Removes (if present) an behavior component from updating.

        Param behaviorComp: The behavior component.
        '''
        if behaviorComp in self._behaviorComponents:
            self._behaviorComponents.remove(behaviorComp)

    def update(self, task):
        '''
        Updates behavior components.

        Will be called automatically in a task.
        Param task: The task.
        Return: The 'done' status.
        '''
        dt = ClockObject.get_global_clock().get_dt()
        # call all behavior components update functions, passing delta time
        for behaviorComp in self._behaviorComponents:
            behaviorComp.update(dt)
        return task.cont
