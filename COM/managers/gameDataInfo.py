'''
Created on Sep 27, 2019

@author: consultit
'''

from enum import IntEnum


class GameDataInfo(IntEnum):
    DATA_DIRS = 0  # !< DATA paths
    CONFIG_FILE = 1  # !< CONFIG_FILE xml configuration file path
    CALLBACKS = 2  # !< CALLBACKS_LA library path
    TRANSITIONS = 3  # !< TRANSITIONS_LA library path
    INITIALIZATIONS = 4  # !< INITIALIZATIONS_LA library path
    INSTANCE_UPDATES = 5  # !< INSTANCE_UPDATES_LA library path
