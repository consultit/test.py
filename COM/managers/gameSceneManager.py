'''
Created on Sep 10, 2019

@author: consultit
'''

from ..tools.singleton import Singleton
from ..tools.utilities import BaseSequence
from ..com.component import Component
from panda3d.core import ClockObject


class _SceneComponentSequence(BaseSequence):
    '''
    Sequence of scene components.
    '''

    def __init__(self, sequence=None):
        super().__init__(Component, sequence)


class GameSceneManager(metaclass=Singleton):
    '''
    Singleton manager updating scene components.
    '''

    def __init__(self, app, sort=0, priority=0, asyncTaskChain=None):
        '''
        Constructor.

        Param app: The application framework.
        Param sort: The task sort.
        Param priority: The task priority.
        Param asyncTaskChain: The task chain.
        '''
        self._taskMgr = app.taskMgr
        # List of scene components to be updated.
        self._sceneComponents = _SceneComponentSequence()
        # A task data for update.
        if asyncTaskChain:
            self._taskMgr.setupTaskChain(asyncTaskChain, numThreads=None,
                                         tickClock=None, threadPriority=None,
                                         frameBudget=None, frameSync=None,
                                         timeslicePriority=None)
        self._taskMgr.add(self.update, 'updateScene', sort=sort, priority=priority,
                          appendTask=True, taskChain=asyncTaskChain)

    def __del__(self):
        '''
        Destructor.
        '''
        self._taskMgr.remove('updateScene')
        self._sceneComponents.clear()

    def add_to_scene_update(self, sceneComp):
        '''
        Adds (if not present) an scene component to updating.

        Param sceneComp: The scene component.
        '''
        if sceneComp not in self._sceneComponents:
            self._sceneComponents.append(sceneComp)

    def remove_from_scene_update(self, sceneComp):
        '''
        Removes (if present) an scene component from updating.

        Param sceneComp: The scene component.
        '''
        if sceneComp in self._sceneComponents:
            self._sceneComponents.remove(sceneComp)

    def update(self, task):
        '''
        Updates scene components.

        Will be called automatically in a task.
        Param task: The task.
        Return: The 'done' status.
        '''
        dt = ClockObject.get_global_clock().get_dt()
        # call all scene components update functions, passing delta time
        for sceneComp in self._sceneComponents:
            sceneComp.update(dt)
        return task.cont
