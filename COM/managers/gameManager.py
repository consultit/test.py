'''
Created on Jun 9, 2019

@author: consultit
'''

from ..tools.singleton import Singleton
from ..tools.utilities import BaseTable, GameException, Orderable, strtol, \
    ParameterTable, StringSequence, COMLogger
from ..com.objectTemplateManager import ObjectTemplateManager
from ..com.componentTemplateManager import ComponentTemplateManager
from ..com.object import ObjectTemplate, ComponentParameterTableMap, \
    ObjectType, ObjectId
from ..com.component import ComponentType
from .gameDataInfo import GameDataInfo
from direct.showbase.ShowBase import ShowBase
from panda3d.core import WindowProperties
from json.decoder import JSONDecoder
import xmltodict
from collections.abc import Sequence, Mapping
from heapq import heappush, heappop
# component templates
from ..components.ai.crowdAgent import CrowdAgentTemplate
from ..components.ai.navMesh import NavMeshTemplate
from ..components.ai.steerPlugIn import SteerPlugInTemplate
from ..components.ai.steerVehicle import SteerVehicleTemplate
from ..components.audio.listener import ListenerTemplate
from ..components.audio.sound3d import Sound3dTemplate
from ..components.behavior.activity import ActivityTemplate
from ..components.behavior.multiActivity import MultiActivityTemplate
from ..components.common.default import DefaultTemplate
from ..components.common.gameConfig import GameConfigTemplate
from ..components.control.driver import DriverTemplate
from ..components.control.chaser import ChaserTemplate
from ..components.physics.constraint import ConstraintTemplate
from ..components.physics.ghost import GhostTemplate
from ..components.physics.rigidBody import RigidBodyTemplate
from ..components.physics.softBody import SoftBodyTemplate
from ..components.physicsControl.characterController import \
    CharacterControllerTemplate
from ..components.physicsControl.vehicle import VehicleTemplate
from ..components.scene.actorModel import ActorModelTemplate
from ..components.scene.instanceOf import InstanceOfTemplate
from ..components.scene.model import ModelTemplate
from ..components.scene.nodePathWrapper import NodePathWrapperTemplate
from ..components.scene.terrain import TerrainTemplate


class GameDataInfoTable(BaseTable):
    '''
    Table for parameters management.

    dict{GameDataInfo:str,...}
    '''

    def __init__(self, mapping=None):
        super().__init__(GameDataInfo, str, mapping)


class GameManager(ShowBase, metaclass=Singleton):
    '''
    GameManager.

    It runs in the main thread.
    It manages Rocket initialization too.
    '''

    def __init__(self, title='Ely', debugMode=False, *args, **kwargs):
        '''
        Constructor. Initializes the environment.

        Param args: from main()
        Param kwargs: from main()
        '''
        # Open the framework
        super(GameManager, self).__init__(*args, **kwargs)
        # ShowBase.__init__(self)
        # Set a nice title
        props = WindowProperties()
        props.setTitle(title)
        self.win.requestProperties(props)
        # initialize members
        self._infoDB = GameDataInfoTable(
            {info: str() for info in GameDataInfo})
        self._debugMode = debugMode

    def __del__(self):
        '''
        Destructor. Clean the game up.
        '''
        self.game_cleanup()

    def game_play(self):
        '''
        Put here custom game object management code.
        '''
        pass

    def game_setup(self):
        '''
        Put here game setup code.
        '''
        # manageObjects component template manager
        self.setup_comp_tmpl_mgr()

        # create the game world (static definition)
        for configFile in self._infoDB[GameDataInfo.CONFIG_FILE].split(','):
            self.create_game_world(configFile)

        # play the game
        self.game_play()

    def game_cleanup(self):
        '''
        Put here game cleanup code.
        '''
        # destroy all created game Objects
        ObjectTemplateManager.get_global_ptr().destroy_all_objects()

    def setup_comp_tmpl_mgr(self):
        '''
        Set the Component template manager up.
        '''
        # Add all kind of component templates
        compTmplMgr = ComponentTemplateManager.get_global_ptr()

        # AI templates
        # CrowdAgent
        compTmplMgr.add_component_template(CrowdAgentTemplate(
            self, self.win, self._infoDB))
        # NavMesh
        compTmplMgr.add_component_template(NavMeshTemplate(
            self, self.win, self._infoDB))
        # SteerPlugIn
        compTmplMgr.add_component_template(SteerPlugInTemplate(
            self, self.win, self._infoDB))
        # SteerVehicle
        compTmplMgr.add_component_template(SteerVehicleTemplate(
            self, self.win, self._infoDB))

        # Audio templates
        # Listener
        compTmplMgr.add_component_template(ListenerTemplate(
            self, self.win, self._infoDB))
        # Sound3d
        compTmplMgr.add_component_template(Sound3dTemplate(
            self, self.win, self._infoDB))

        # Behavior templates
        # Activity
        compTmplMgr.add_component_template(ActivityTemplate(
            self, self.win, self._infoDB))
        # MultiActivity
        compTmplMgr.add_component_template(MultiActivityTemplate(
            self, self.win, self._infoDB))

        # Common templates
        # Default
        compTmplMgr.add_component_template(DefaultTemplate(self,
                                                           self.win, self._infoDB))
        # GameConfig
        compTmplMgr.add_component_template(GameConfigTemplate(
            self, self.win, self._infoDB))

        # Control templates
        # Chaser
        compTmplMgr.add_component_template(ChaserTemplate(
            self, self.win, self._infoDB))

        # Driver
        compTmplMgr.add_component_template(DriverTemplate(
            self, self.win, self._infoDB))

        # Physics templates
        # Constraint
        compTmplMgr.add_component_template(ConstraintTemplate(
            self, self.win, self._infoDB))
        # Ghost
        compTmplMgr.add_component_template(GhostTemplate(
            self, self.win, self._infoDB))
        # RigidBody
        compTmplMgr.add_component_template(RigidBodyTemplate(
            self, self.win, self._infoDB))
        # SoftBody
        compTmplMgr.add_component_template(SoftBodyTemplate(
            self, self.win, self._infoDB))

        # PhysicsControl templates
        # Character
        compTmplMgr.add_component_template(CharacterControllerTemplate(
            self, self.win, self._infoDB))
        # Vehicle
        compTmplMgr.add_component_template(VehicleTemplate(
            self, self.win, self._infoDB))

        # Scene templates
        # ActorModel
        compTmplMgr.add_component_template(ActorModelTemplate(
            self, self.win, self._infoDB))
        # InstanceOf
        compTmplMgr.add_component_template(InstanceOfTemplate(
            self, self.win, self._infoDB))
        # Model
        compTmplMgr.add_component_template(ModelTemplate(
            self, self.win, self._infoDB))
        # NodePathWrapper
        compTmplMgr.add_component_template(NodePathWrapperTemplate(
            self, self.win, self._infoDB))
        # Terrain
        compTmplMgr.add_component_template(TerrainTemplate(
            self, self.win, self._infoDB))

    @staticmethod
    def _read_configuration_file(confFile):
        msg = '\n'
        inputStr = open(confFile, 'r').read()
        isJSON = True
        reprDict = None
        # check json
        try:
            reprDict = JSONDecoder().decode(inputStr)
        except Exception:
            msg += 'No json file...\n'
            isJSON = False
        if not isJSON:
            # check xml
            try:
                reprDict = xmltodict.parse(inputStr)
            except Exception:
                msg += 'No xml file...\n'
                raise GameException(msg + 'Input file \'' + confFile +
                                    '\' must be in a correct xml or json format. Exiting...')
        return reprDict, isJSON

    def _makeSequence(self, element):
        if not element:
            return []
        if isinstance(element, Mapping):
            return [element]
        elif isinstance(element, Sequence):
            return element
        else:
            raise GameException('GameManager.create_game_world: ' + element +
                                ' != (Sequence|Mapping) ')

    def create_game_world(self, worldConfFile):
        '''
        Create a Game World loading description from a file (xml).

        Objects (inside object sets)  may have a creation priority and
        components (inside objects) may have a initialization priority:
        the higher is priority the earlier is creation/initialization
        (default priority = 0).
        Objects' initializations are performed 'after' the entire game
        world has been created and in particular hierarchies between
        all objects have been established.
        Param worldConfFile: The description file.
        '''
        # read and load the game configuration file
        COMLogger.debug('Loading ' + worldConfFile + '...')
        gameDoc, _ = GameManager._read_configuration_file(worldConfFile)

        # check <Game> tag
        COMLogger.debug('Checking <Game> tag ...')
        gameTAG = gameDoc.get('Game', None)
        if (not gameTAG) or (not isinstance(gameTAG, Mapping)):
            raise GameException(
                'GameManager.create_game_world: No unique <Game> in ' +
                worldConfFile)

        objTmplMgr = ObjectTemplateManager.get_global_ptr()
        compTmplMgr = ComponentTemplateManager.get_global_ptr()

        # # # # # # # # # # # # # # # # # # # # #
        # <!-- Object Templates Definition -.
        # Setup object template manager
        COMLogger.debug('Setting up Object Template Manager')
        # check <Game>--<ObjectTmplSet> tag
        COMLogger.debug('  Checking <ObjectTmplSet> tag ...')
        objectTmplSetTAG = gameTAG.get('ObjectTmplSet', None)
        if objectTmplSetTAG and (not isinstance(objectTmplSetTAG, Mapping)):
            raise GameException(
                'GameManager.create_game_world: No unique <ObjectTmplSet> in ' +
                worldConfFile)
        if objectTmplSetTAG:
            # cycle through the ObjectTmpl(s)' definitions and
            # add all kind of object templates
            objectTmplTAGSeq = self._makeSequence(
                objectTmplSetTAG.get('ObjectTmpl', None))
            for objectTmplTAG in objectTmplTAGSeq:
                objectTypeTAG = objectTmplTAG.get('@' + 'type', None)
                if not objectTypeTAG:
                    continue
                COMLogger.debug(
                    '  Adding Object Template for ' + objectTypeTAG + ' type')
                # create a new object template
                objTmplPtr = ObjectTemplate(ObjectType(objectTypeTAG),
                                            objTmplMgr,
                                            self, self.win,
                                            self._infoDB)

                # create a priority queue of component templates
                orderedComponentTmplsTAG = []
                componentTmplTAGSeq = self._makeSequence(
                    objectTmplTAG.get('ComponentTmpl', None))

                for componentTmplTAG in componentTmplTAGSeq:
                    ordCompTAG = Orderable()
                    ordCompTAG.ptr = componentTmplTAG
                    ordCompTAG.msg = str(
                        componentTmplTAG.get('@' + 'type', None))
                    priorityTAG = componentTmplTAG.get('@' + 'priority', None)
                    if priorityTAG:
                        # reversed priority
                        ordCompTAG.prio = -strtol(priorityTAG)
                    else:
                        ordCompTAG.prio = 0
                    heappush(orderedComponentTmplsTAG, ordCompTAG)
                # cycle through the ComponentTmpl(s)' definitions ...
                while orderedComponentTmplsTAG:
                    # access top object
                    componentTmplTAG = orderedComponentTmplsTAG[0].ptr
                    compFamilyTAG = componentTmplTAG.get('@' + 'family', None)
                    compTypeTAG = componentTmplTAG.get('@' + 'type', None)
                    if (not compFamilyTAG) or (not compTypeTAG):
                        continue
                    COMLogger.debug('    Component of family ' + compFamilyTAG +
                                    ' and type ' + compTypeTAG)

                    # cycle through the ComponentTmpl Param(s)' to be
                    # initialized
                    paramTAGSeq = self._makeSequence(
                        componentTmplTAG.get('Param', None))

                    for paramTAG in paramTAGSeq:
                        try:
                            attributeTAGName = next(iter(paramTAG))
                            if attributeTAGName[0] != '@':
                                raise StopIteration
                        except:
                            continue
                        attributeTAGValue = paramTAG.get(
                            attributeTAGName, None)
                        attributeTAGName = attributeTAGName[1:]
                        COMLogger.debug('      Param ' + attributeTAGName + ' = ' +
                                        attributeTAGValue)

                        # add attribute for this component type of this object.
                        objTmplPtr.add_component_type_parameter(attributeTAGName,
                                                                attributeTAGValue,
                                                                ComponentType(compTypeTAG))

                    # ... add all component templates
                    compTmpl = compTmplMgr.get_component_template(
                        ComponentType(compTypeTAG))

                    if not compTmpl:
                        heappop(orderedComponentTmplsTAG)
                        continue
                    objTmplPtr.add_component_template(compTmpl)
                    # remove top object from the priority queue
                    heappop(orderedComponentTmplsTAG)
                #  add 'type' object template to manager
                objTmplMgr.add_object_template(objTmplPtr)
        #
        # # # # # # # # # # # # # # # # # # # # #
        # <!-- Objects Creation -.
        # Create game objects
        COMLogger.debug('Creating Game Objects')
        # check <Game>--<ObjectSet> tag
        COMLogger.debug('  Checking <ObjectSet> tag ...')

        objectSetTAG = gameTAG.get('ObjectSet', None)
        if objectSetTAG and (not isinstance(objectSetTAG, Mapping)):
            raise GameException(
                'GameManager.create_game_world: No unique <ObjectSet> in ' +
                worldConfFile)
        if objectSetTAG:
            # reset all component templates parameters to their default values
            compTmplMgr.reset_component_templates_params()
            objectTAGSeq = self._makeSequence(objectSetTAG.get('Object', None))
            # store created objects in this queue
            createdObjectQueue = []
            # create a priority queue of objects
            orderedObjectsTAG = []
            for objectTAG in objectTAGSeq:
                ordObjTAG = Orderable()
                ordObjTAG.ptr = objectTAG
                ordObjTAG.msg = str(objectTAG.get('@' + 'id', None))
                priorityTAG = objectTAG.get('@' + 'priority', None)
                if priorityTAG:
                    ordObjTAG.prio = -strtol(priorityTAG)  # reversed priority
                else:
                    ordObjTAG.prio = 0
                heappush(orderedObjectsTAG, ordObjTAG)
            # cycle through the Object(s)' definitions in order of priority
            while orderedObjectsTAG:
                # access top object
                objectTAG = orderedObjectsTAG[0].ptr
                objTypeTAG = objectTAG.get('@' + 'type', None)
                #  get the related object template
                objectTmplPtr = objTmplMgr.get_object_template(
                    ObjectType(objTypeTAG))
                if (not objTypeTAG) or (not objectTmplPtr):
                    # no object without type allowed or object type doesn't
                    # exist
                    heappop(orderedObjectsTAG)
                    continue
                objIdTAG = objectTAG.get('@' + 'id', None)  # may be None
                COMLogger.debug('  Creating Object ' +
                                (objIdTAG if objIdTAG else 'UNNAMED') + '...')
                # set a ParameterTable for each component
                compParamTableMap = ComponentParameterTableMap()
                # cycle through the Object Component(s)' to be initialized in
                # order of priority
                componentTAGSeq = self._makeSequence(
                    objectTAG.get('Component', None))

                for componentTAG in componentTAGSeq:
                    compTypeTAG = componentTAG.get('@' + 'type', None)
                    if not compTypeTAG:
                        # no component without type allowed
                        # but not defined component types are allowed!
                        continue
                    COMLogger.debug(
                        '    Initializing Component ' + compTypeTAG)
                    # cycle through the Component Param(s)' to be initialized
                    paramTAGSeq = self._makeSequence(
                        componentTAG.get('Param', None))
                    for paramTAG in paramTAGSeq:
                        try:
                            attributeTAGName = next(iter(paramTAG))
                            if attributeTAGName[0] != '@':
                                raise StopIteration
                        except:
                            continue
                        attributeTAGValue = paramTAG.get(
                            attributeTAGName, None)
                        attributeTAGName = attributeTAGName[1:]
                        COMLogger.debug('      Param ' + attributeTAGName + ' = ' +
                                        attributeTAGValue)
                        compType = ComponentType(compTypeTAG)
                        paramTable = compParamTableMap.get(compType, None)
                        if not paramTable:
                            compParamTableMap[compType] = ParameterTable(
                                {attributeTAGName: StringSequence([attributeTAGValue])})

                        elif not attributeTAGName in paramTable:
                            paramTable[attributeTAGName] = StringSequence(
                                [attributeTAGValue])

                        else:
                            paramTable[attributeTAGName] += StringSequence(
                                [attributeTAGValue])
                #
                # # # # # # # # # # # # # # # # # # # # #
                # set parameters for the object
                COMLogger.debug('    Initializing object ' +
                                (objIdTAG if objIdTAG else 'UNNAMED') + ' itself')

                objParamTable = ParameterTable()
                # cycle through the Object Param(s)' to be initialized
                objParamTAGSeq = self._makeSequence(
                    objectTAG.get('Param', None))
                for objParamTAG in objParamTAGSeq:
                    try:
                        attributeTAGName = next(iter(objParamTAG))
                        if attributeTAGName[0] != '@':
                            raise StopIteration
                    except:
                        continue
                    attributeTAGValue = objParamTAG.get(attributeTAGName, None)
                    attributeTAGName = attributeTAGName[1:]
                    COMLogger.debug('      Param ' + attributeTAGName + ' = ' +
                                    attributeTAGValue)
                    if not attributeTAGName in objParamTable:
                        objParamTable[attributeTAGName] = StringSequence(
                            [attributeTAGValue])

                    else:
                        objParamTable[attributeTAGName] += StringSequence(
                            [attributeTAGValue])

                # # # # # # # # # # # # # # # # # # # # #
                # actually create the object
                if objIdTAG:
                    #  set id with the passed id
                    objectPtr = objTmplMgr.create_object(
                        ObjectType(objTypeTAG), ObjectId(objIdTAG),
                        objParamTable, compParamTableMap)

                else:
                    # set id with the internally generated id
                    objectPtr = objTmplMgr.createObject(
                        ObjectType(objTypeTAG), ObjectId(''),
                        objParamTable, compParamTableMap)

                if not objectPtr:
                    continue
                createdObjectQueue.append(objectPtr)
                COMLogger.debug('  ...Created Object ' +
                                str(objectPtr.object_id))
                # remove top object from the priority queue
                heappop(orderedObjectsTAG)
            # give a chance to objects to initialize themselves,
            # in order of creation, after the game world has been created.
            while createdObjectQueue:
                # get front element
                objectPtr = createdObjectQueue[0]
                # use front element
                objectPtr.world_setup()
                # remove front element
                createdObjectQueue.pop(0)

    @property
    def panda_framework(self):
        '''
        Gets/sets the PandaFramework.

        Return: A reference to the PandaFramework.
        '''
        return self

    @property
    def window_framework(self):
        '''
        Gets/sets the WindowFramework.

        Return: A reference to the WindowFramework.
        '''
        return self.win

    @property
    def data_info(self):
        '''
        Gets game data info.
        '''
        return self._infoDB
