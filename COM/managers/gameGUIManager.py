'''
Created on Aug 31, 2019

@author: consultit
'''

from ..tools.singleton import Singleton
try:
    from ely.gui import GameGUIManager as elyMgr
except ModuleNotFoundError:
    elyMgr = None


class GameGUIManager(metaclass=Singleton):
    '''
    Singleton manager updating gui components.
    '''

    def __init__(self, win, mw):
        '''
        Constructor.

        Param win: The application graphics window.
        Param mw: The application mouse watcher.
        '''
        # create elyMgr
        if elyMgr:
            self._elyMgr = elyMgr(win=win, mw=mw)

    def __del__(self):
        '''
        Destructor.
        '''
        # destroy elyMgr
        if elyMgr:
            del self._elyMgr
