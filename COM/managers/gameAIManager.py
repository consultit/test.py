'''
Created on Sep 4, 2019

@author: consultit
'''

from ..tools.singleton import Singleton
from ..tools.utilities import BaseSequence
from ..com.component import Component
from panda3d.core import ClockObject, GeomNode
try:
    from ely.ai import GameAIManager as elyMgr
except ModuleNotFoundError:
    elyMgr = None


class _AIComponentSequence(BaseSequence):
    '''
    Sequence of ai components.
    '''

    def __init__(self, sequence=None):
        super().__init__(Component, sequence)


class GameAIManager(metaclass=Singleton):
    '''
    Singleton manager updating ai components.
    '''

    def __init__(self, app, sort=0, priority=0, asyncTaskChain=None,
                 mask=GeomNode.get_default_collide_mask()):
        '''
        Constructor.

        Param app: The application framework.
        Param sort: The task sort.
        Param priority: The task priority.
        Param asyncTaskChain: The task chain.
        Param mask: The collision mask.
        '''
        self._taskMgr = app.taskMgr
        # List of ai components to be updated.
        self._aiComponents = _AIComponentSequence()
        # A task data for update.
        if asyncTaskChain:
            self._taskMgr.setupTaskChain(asyncTaskChain, numThreads=None,
                                         tickClock=None, threadPriority=None,
                                         frameBudget=None, frameSync=None,
                                         timeslicePriority=None)
        self._taskMgr.add(self.update, 'updateAI', sort=sort, priority=priority,
                          appendTask=True, taskChain=asyncTaskChain)
        # create elyMgr
        if elyMgr:
            self._elyMgr = elyMgr(taskSort=sort, root=app.render, mask=mask)
            self._elyMgr.set_reference_node_path(app.render)

    def __del__(self):
        '''
        Destructor.
        '''
        self._taskMgr.remove('updateAI')
        self._aiComponents.clear()
        # destroy elyMgr
        if elyMgr:
            del self._elyMgr

    def add_to_ai_update(self, aiComp):
        '''
        Adds (if not present) an ai component to updating.

        Param aiComp: The ai component.
        '''
        if aiComp not in self._aiComponents:
            self._aiComponents.append(aiComp)

    def remove_from_ai_update(self, aiComp):
        '''
        Removes (if present) an ai component from updating.

        Param aiComp: The ai component.
        '''
        if aiComp in self._aiComponents:
            self._aiComponents.remove(aiComp)

    def update(self, task):
        '''
        Updates ai components.

        Will be called automatically in a task.
        Param task: The task.
        Return: The 'done' status.
        '''
        dt = ClockObject.get_global_clock().get_dt()
        # call all ai components update functions, passing delta time
        for aiComp in self._aiComponents:
            aiComp.update(dt)
        if self._elyMgr:
            self._elyMgr.update()
        return task.cont
