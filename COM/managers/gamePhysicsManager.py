'''
Created on Sep 6, 2019

@author: consultit
'''

from ..tools.singleton import Singleton
from ..tools.utilities import BaseSequence
from ..com.component import Component
from panda3d.core import ClockObject, GeomNode
try:
    from ely.physics import GamePhysicsManager as elyMgr, BT3PhysicsInfo
except ModuleNotFoundError:
    elyMgr = None


class _PhysicsComponentSequence(BaseSequence):
    '''
    Sequence of physics components.
    '''

    def __init__(self, sequence=None):
        super().__init__(Component, sequence)


class GamePhysicsManager(metaclass=Singleton):
    '''
    Singleton manager updating physics components.
    '''

    def __init__(self, app, sort=0, priority=0, asyncTaskChain=None,
                 groupMask=GeomNode.get_default_collide_mask(),
                 collideMask=GeomNode.get_default_collide_mask()):
        '''
        Constructor.

        Param app: The application framework.
        Param sort: The task sort.
        Param priority: The task priority.
        Param asyncTaskChain: The task chain.
        Param groupMask: The collision group mask.
        Param collideMask: The collision mask.
        '''
        self._taskMgr = app.taskMgr
        # List of physics components to be updated.
        self._physicsComponents = _PhysicsComponentSequence()
        # A task data for update.
        if asyncTaskChain:
            self._taskMgr.setupTaskChain(asyncTaskChain, numThreads=None,
                                         tickClock=None, threadPriority=None,
                                         frameBudget=None, frameSync=None,
                                         timeslicePriority=None)
        self._taskMgr.add(self.update, 'updatePhysics', sort=sort,
                          priority=priority, appendTask=True,
                          taskChain=asyncTaskChain)
        # create elyMgr
        if elyMgr:
            self._elyMgr = elyMgr(taskSort=sort, root=app.render,
                                  groupMask=groupMask, collideMask=collideMask,
                                  info=BT3PhysicsInfo())
            self._elyMgr.set_reference_node_path(app.render)

    def __del__(self):
        '''
        Destructor.
        '''
        self._taskMgr.remove('updatePhysics')
        self._physicsComponents.clear()
        # destroy elyMgr
        if elyMgr:
            del self._elyMgr

    def add_to_physics_update(self, physicsComp):
        '''
        Adds (if not present) an physics component to updating.

        Param physicsComp: The physics component.
        '''
        if physicsComp not in self._physicsComponents:
            self._physicsComponents.append(physicsComp)

    def remove_from_physics_update(self, physicsComp):
        '''
        Removes (if present) an physics component from updating.

        Param physicsComp: The physics component.
        '''
        if physicsComp in self._physicsComponents:
            self._physicsComponents.remove(physicsComp)

    def update(self, task):
        '''
        Updates physics components.

        Will be called automatically in a task.
        Param task: The task.
        Return: The 'done' status.
        '''
        dt = ClockObject.get_global_clock().get_dt()
        # call all physics components update functions, passing delta time
        for physicsComp in self._physicsComponents:
            physicsComp.update(dt)
        if self._elyMgr:
            self._elyMgr.update()
        return task.cont
